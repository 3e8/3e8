const http = require("http");
const {log, logError, warn} = require("../my_modules/3e8-log");

function fetchData() {
  return new Promise((resolve, reject) => {
    let html = "";
    http.get("http://www.heavens-above.com/AllSats.aspx?lat=47.0559&lng=7.6272&loc=Burgdorf&alt=563&tz=CET", (r)=>{
      r.on("data", chunk => html += chunk);
      r.on("end", _=>resolve(html))
    });
  });

}

function getBrightest(html) {
  const rows = html.match(/<tr.*?>[\s\S]*?<\/tr>/gi);
  let cells = rows.map(r=>r.match(/(?:<td.*?>)[\s\S]*?(?:<\/td>)/gi))
  let sats = cells.filter(c=>c.length>9).map(c=>c.map(cell=>cell.match(/(?:<td.*?>)([\s\S]*?)(?:<\/td>)/)[1]));
  sats = sats.slice(1).map(([name, brightness, startTime, startHeight, startDir, maxTime, maxHeight, maxDir, endTime, endHeight, endDir])=>({name, brightness, startTime, startHeight, startDir, maxTime, maxHeight, maxDir, endTime, endHeight, endDir}))
  let brightest = sats.sort((a,b)=>a.brightness-b.brightness);
  let {name, brightness, startTime, startDir, endTime, endDir} = brightest[0];
  return brightness > 2 ? "" :`Hellster Satellit heute (Burgdorf): ${name} (${brightness}) ab ${startTime} von ${startDir} nach ${endDir}`;
}

let lastCall = 0;
let sat = "";

module.exports = async function fetchSats() {
  if(Date.now() - lastCall > 2*3600*1000) {
    log("fetchSatsData");
    let html = await fetchData().catch(logError);
    sat = getBrightest(html);
    lastCall = Date.now();
  }
  return sat;
};
