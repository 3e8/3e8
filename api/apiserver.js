process.chdir(__dirname);

//const path = require('path').posix;
//const {readDir, readFile, readStat, copyFile, rename, utimes} = require("prms-fs");
const http = require('http');
const fs = require('fs');
const {log, logError, warn} = require("../my_modules/3e8-log");

let apis = {
  sats: require("./fetchSats.js"),
  weather: require("./fetchWeather.js"),
  test: async _=>"ok"
};

let PORT = require("../entrance/serversettings.js").api.internalport;
let server = http.createServer(httpRequestHandler).listen(PORT);
log("API-Server is on port " + PORT);

async function httpRequestHandler(req, res) {
  if (req.method.toUpperCase() === "GET") {
    for(let api in apis) {
      if(req.url.includes("/"+api)) {
        const headers = {
          "Access-Control-Allow-Origin": "*"
        };
        res.writeHead(200, headers);
        res.end(await apis[api](req, res));
      }
    }
  }
}