const http = require("http");
const {log, logError, warn} = require("../my_modules/3e8-log");

function fetchData() {
  return new Promise((resolve, reject) => {
    let data = "";
    http.get("http://api.openweathermap.org/data/2.5/forecast?id=6458892&appid=56b7eaee8442e83b081af10c87f677cc&units=metric", (r)=>{
      r.on("data", chunk => data += chunk);
      r.on("end", _=>resolve(data))
    });
  });

}

function beautify(data) {
  let now = new Date(data.list[0].dt*1000);
  let T = data.list.map(rec => rec.main.temp);
  let p_sealevel = data.list.map(rec => rec.main.sea_level);
  let humidity = data.list.map(rec => rec.main.humidity);
  let clouds = data.list.map(rec => rec.clouds.all);
  let icons = data.list.map(rec => rec.weather[0].icon);
  let id = data.list.map(rec => rec.weather[0].id);
  let wordsshort = data.list.map(rec => rec.weather[0].main);
  let wordslong = data.list.map(rec => rec.weather[0].description);
  let wind = data.list.map(rec => rec.wind.speed);
  let winddeg = data.list.map(rec => rec.wind.deg);
  let rain = data.list.map(rec => rec.rain && rec.rain["3h"] || 0);
  let snow = data.list.map(rec => rec.snow && rec.snow["3h"] || 0);
  let answer = JSON.stringify({now, T, p_sealevel, humidity, clouds,
    icons, id, wordsshort, wordslong, wind, winddeg, rain, snow});
  return answer;
}

let lastCall = 0;
let w = "";

module.exports = async function fetchWeather() {
  if(Date.now() - lastCall > 0.75*3600*1000) {
    let json = await fetchData().catch(log);
    let data = JSON.parse(json);
    w = beautify(data);
    lastCall = Date.now();
  }
  return w;
};
