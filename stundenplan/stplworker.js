const {gzip} = require("zlib");
const {readFile, writeFile, copyFile, stat, readdir} = require("prms-fs");

const PATHTODATEN = "./DatenKB/gpp002.txt";
const PATHTOEXTRACTED = "./extractedVersions";
const {SJA, SJE, istFerien, getWeek, getWeekYear, weekArray, getJSW} = require('./Datehelpers');

async function importCurrentData() {
  let extracted = await readdir(PATHTOEXTRACTED);
  let oldVersions = extracted.filter(f=>f.startsWith('extracted_'));
  let oldStats = await Promise.all(oldVersions.map(f=>stat(PATHTOEXTRACTED + "/" + f)));
  let currentStat = await stat(PATHTODATEN);
  if(!oldStats.some(s=>s.size===currentStat.size && Math.abs(s.mtime.valueOf()-currentStat.mtime.valueOf()) < 10000)) {
    console.log("new File Found!");
    await copyFile(PATHTODATEN, PATHTOEXTRACTED + "/" + 'extracted_' + Date.now() + '.txt');
  }
}

async function main() {
  await importCurrentData();
  //await copyFile(PATHTODATEN, './extractedVersions/a.txt');
  let t = await readFile(PATHTODATEN, 'UTF-8');
  let lplist = JSON.parse(await readFile('lp.json', 'UTF-8'));
  let lessons = {};
  t.split("\r\n").filter(x => x!=="").forEach(x=>{
    let [lp,year,month,day,daylesson,subj,klassen,longroom,,,unr,starttime,duration] = x.split(",");  //aj,2015,2,23,2,sB,1c,E11,0,,249,915,45;
    let d = new Date(year, month-1, day, 12, 0, 0);
    let room = longroom.split("~")[0];
    let weekday = d.getDay();
    let week = getWeek(d);
    let weekyear = getWeekYear(d);
    let jsw = getJSW(week, weekyear);
    if(jsw === -1) console.warn(week, weekyear);
    let endtime = +starttime+(+duration);
    if (endtime%100 >= 60 || endtime%100 === 0) {endtime +=40;}
    let uniqueCode = [lp, unr, subj, weekday, daylesson, starttime, endtime, longroom, klassen.replace(/\s/g, '')].join('_');
    if(lessons[uniqueCode]) {
      //console.log(jsw, lessons[uniqueCode].occ);
      lessons[uniqueCode].occ = lessons[uniqueCode].occ.slice(0,jsw) + '1' + lessons[uniqueCode].occ.slice(jsw+1);
      //if(lessons[uniqueCode].occ.length >40) console.log(jsw);
    }
    else {
      let occ = weekArray.map((x, i)=>i===jsw?'1':'0').join('');
      lessons[uniqueCode] = {unr, lp, weekday, daylesson: +daylesson, subj, klassen, longroom, starttime: +starttime, endtime, occ}
    }
    //return {unr, lp, year: +year, month: +month, day: +day, week, weekyear, weekday, daylesson: +daylesson, subj, klassen, room, longroom, starttime: +starttime, endtime, duration:+duration}
  });
  console.log(Object.values(lessons).filter(l=>l.occ.split('').filter(c=>c=='1').length>25).length);
  await createGzip(JSON.stringify(Object.values(lessons).map(l=>Object.values(l))));

}

async function createGzip(obj) {
  let str = obj;
  let buf = Buffer.from(str);
  gzip(buf, async function (_, result) {
    await writeFile('./current.gz', result);
  });
}

main().then(console.log);
//
// async function start() {
//   let d = await fetch("import/gpp002.txt");
//   let t = await d.text();
//   let lpraw = await fetch("lp.json");
//   window.lplist = await lpraw.json();
//   a = t.split("\r\n").filter(x => x!=="");
//   a = a.map(x => {
//     let [lp,year,month,day,daylesson,subj,klassen,longroom,,,unr,starttime,duration] = x.split(",");  //aj,2015,2,23,2,sB,1c,E11,0,,249,915,45;
//     let d = new Date(year, month-1, day);
//     let room = longroom.split("~")[0];
//     let weekday = d.getDay();
//     let week = d.getWeek();
//     let weekyear = d.getWeekYear();
//     if(subj === "sAM^") subj = "sâM";
//     if(subj.length > 3 && parseInt(subj[3]) > 0) subj = subj.slice(0,3);
//
//     //[subj, klassen] = adaptSubjectsAndClasses(subj, klassen);
//     //if(ueberspringenImAktuellenSemester(subj, klassen, lp, weekday, daylesson, room)) return false;
//     //if(!isValidSubj(subj)) return false;
//     //[subj, klassen, lp, weekday, room] = ausnahmenAktuellesSemester(subj, klassen, lp, weekday, room, daylesson);
//     //subj = halbklassenNamen(subj, klassen); weiter unten
//     let endtime = +starttime+(+duration);
//     if (endtime%100 >= 60 || endtime%100 === 0) {endtime +=40;}
//     return {unr, lp, year: +year, month: +month, day: +day, week, weekyear, weekday, daylesson: +daylesson, subj, klassen, room, longroom, starttime: +starttime, endtime, duration:+duration}
//   }).filter(l=>l!==false);
//
//   a.forEach(l=>{
//     ["klassen", "room", "lp", "unr"].forEach(k=> {
//       if(!indexedBy[k][l[k]]) indexedBy[k][l[k]] = [];
//       indexedBy[k][l[k]].push(l);
//     });
//     let match = indexedBy.groupedunr[l.unr] && indexedBy.groupedunr[l.unr].find(gl=>["unr", "lp", "year", "month", "day", "daylesson", "subj", "room"].every(key=>gl[key] === l[key]));
//     if(match) {
//       match.klassen += " "+l.klassen;
//     }
//     else {
//       indexedBy.groupedunr[l.unr] = (indexedBy.groupedunr[l.unr] || []).concat([Object.assign({}, l)]);
//     }
//   });
//
//   for(let k in indexedBy.groupedunr) {
//     g = g.concat(indexedBy.groupedunr[k])
//   }
//
//   const mapLinks = (val) => `<a class="choicelink" href="#${val}" data-which="${val}">${val}</a>`;
//   let klassen = Array.from(new Set(a.map(l=>l.klassen))).sort((a,b)=>a>b?1:-1).filter(l=>l!=="");
//   $(".klassen").innerHTML = klassen.map(mapLinks).join("");
//   $(".klassen").addEventListener("change", e=>reset("klassen", e.target.value));
//
//   let lp = Array.from(new Set(a.map(l=>l.lp))).sort((a,b)=>a==="?"?1:b==="?"?-1:a>b?1:-1).filter(l=>l!=="");
//   $(".lp").innerHTML = lp.map(mapLinks).map(l=>l.replace(/>(..)<\/a>/, (str, lp)=>`title="${window.lplist.find(lpa=>lpa[0]===lp).slice(1,3).join(" ")}">${lp}</a>`)).join("");
//   $(".lp").addEventListener("change", e=>reset("lp", e.target.value));
//
//   let rooms = Array.from(new Set(a.map(l=>l.room))).sort((a,b)=>a>b?1:-1).filter(l=>l!=="").filter(l=>!l.includes("~"));
//   $(".room").innerHTML = rooms.map(mapLinks).join("");
//   $(".room").addEventListener("change", e=>reset("room", e.target.value));
//
//   let roomlengths = rooms.map(getTextWidth).map(dim=>dim.w);
//   let maxrl = Math.max(...roomlengths);
//   let maxroom = rooms.filter((r,i)=>roomlengths[i] === maxrl)
//
//   // let subjlengths = a.map(l=>l.subj).map(getTextWidth).map(dim=>dim.w);
//   // let maxsl = Math.max(...subjlengths);
//   // let maxsubj = a.map(l=>l.subj).filter((r,i)=>subjlengths[i] === maxsl)
//   // console.log(maxsubj);
//
//   $(".selector").addEventListener("click", e => {
//     let sel = $(".selector");
//     if(e.target.closest("button") && e.target.closest("button").classList.contains("viewselector")) {
//       if(sel.dataset.which===e.target.closest("button").dataset.which) {
//         sel.classList.toggle("open");
//       }
//       else {
//         sel.classList.add("open");
//       }
//       sel.dataset.which = e.target.closest("button").dataset.which;
//     }
//     else if(e.target.classList.contains("choicelink")) {
//       reset(sel.dataset.which, e.target.dataset.which);
//       sel.classList.remove("open");
//     }
//     else {
//       sel.classList.remove("open");
//     }
//   });
//
//   reset("klassen", klassen[0]);
// }
// start();
//
//
