let {SJA, SJE} = require('./config.js');

function istFerien(week) {
  const last = [2015, 2020, 2026, 2032, 2037, 2043].includes(SJA) ? 53 : 52;
  const ferienwochen = [1, 7, 15, 16, 28, 29, 30, 31, 32, 39, 40, 41, last];
  return ferienwochen.includes(week);
}

let runningDate = new Date(SJA, 8-1, 1, 12,0,0); //1. August vor Schuljahresbeginn
const weekArray = [];
while(runningDate < new Date(SJE, 8-1, 1, 12,0,0)) {
  if(!istFerien(getWeek(runningDate))) {
    weekArray.push({week: getWeek(runningDate), year: getWeekYear(runningDate)});
  }
  runningDate.setDate(runningDate.getDate() + 7);
}

/**
 *
 * @param {Date} date
 * @returns {number}
 */
function getWeek(date) {
  let target = new Date(date.valueOf());
  let dayNumber = (date.getDay() + 6) % 7;
  target.setDate(target.getDate() - dayNumber + 3);
  let firstThursday = target.valueOf();
  target.setMonth(0, 1);
  if (target.getDay() !== 4) {
    target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
  }
  return Math.ceil((firstThursday - target) /  604800000) + 1; //604800000 = 7 * 24 * 3600 * 1000
}

/**
 *
 * @param {Date} date
 * @returns {number}
 */
function getWeekYear(date) {
  let target = new Date(date.valueOf());
  target.setDate(target.getDate() - ((date.getDay() + 6) % 7) + 3); //Thursday of this week
  return target.getFullYear();
}

function getJSW(week, weekyear) {
  return weekArray.findIndex(w=>w.year === weekyear && w.week === week);
}

const Datehelpers = {SJA, SJE, istFerien, getWeek, getWeekYear, weekArray, getJSW};

// Support Node.js specific `module.exports` (which can be a function)
if (typeof module !== 'undefined' && module.exports) {
  exports = module.exports = Datehelpers;
}
// But always support CommonJS module 1.1.1 spec (`exports` cannot be a function)
exports.MyModule = Datehelpers;