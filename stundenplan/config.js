const CONFIG = {
  SJA: 2017,
  SJE: 2018,
};

// Support Node.js specific `module.exports` (which can be a function)
if (typeof module !== 'undefined' && module.exports) {
  exports = module.exports = CONFIG;
}
// But always support CommonJS module 1.1.1 spec (`exports` cannot be a function)
exports.MyModule = CONFIG;