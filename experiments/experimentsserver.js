const http = require('http'),
      url = require('url'),
      path = require('path'),
      fs = require('fs');
const {log, logError, warn} = require("../my_modules/3e8-log");

const express = require('express'),
      bodyParser = require('body-parser');

const PORT = require("../entrance/serversettings").experiments.internalport;

const app = express();

app.use(bodyParser.urlencoded({ extended: true, limit:1024*1024*5, parameterLimit: 1000000  })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json({limit:1024*1024*5, type:'application/json'})); // to support JSON-encoded bodies

const tasks = {
  inventory: require('../metamodules/inventory').inventory(),
  db: require('./db.js').dbHandler,
  //t: function (req, res) {res.send("ttt")},
};

for(let t in tasks) {
  app.use("(/*)?/"+t+"(/*)?", (req, res, next) => tasks[t](req, res, next));
}

app.use(express.static(__dirname));

app.listen(PORT, function () {
  log('Experiments Server on port ' + PORT);
});