//Globale Variabeln
var userOk = 0;
var password = "";
var lsgCode = window.lsg || false;
//var pathToUser from php;
var currentfile = pathToUser + "currentfile.html";
var code;
var parts = [];
var testmode = false;
var lastLoad = 0;
var autoupdate= false;
var activeEditor, alleditor, simeditor;

$(document).ready(function(){
  setupEditors();
  attachEditorEvents();
  loadFile(0);
}); // Ende document Ready

function setupEditors() {
  $('#all-editor').css('max-width', 460);
  $('#all-editor').css('height', innerHeight - 120);
  $('#all-editor').css('left', 20);
  $('#all-editor').css('top', 100);

  $('#sim-editor').css('max-width', 460);
  $('#sim-editor').css('height', innerHeight - 120);
  $('#sim-editor').css('left', 20);
  $('#sim-editor').css('top', 100);
  $('#sim-editor').hide();

  alleditor = ace.edit('all-editor');
  alleditor.resize();
  alleditor.setTheme("ace/theme/chrome");  // auch schön wäre Chrome oder cobalt (dunkel)
  alleditor.getSession().setMode("ace/mode/html");
  alleditor.getSession().setOptions({ tabSize: 2, useSoftTabs: true });
  alleditor.$blockScrolling = Infinity;

  simeditor = ace.edit('sim-editor');
  simeditor.resize();
  simeditor.setTheme("ace/theme/chrome");
  simeditor.getSession().setMode("ace/mode/javascript");
  simeditor.getSession().setOptions({ tabSize: 2, useSoftTabs: true });
  simeditor.renderer.setShowGutter(false);
  simeditor.$blockScrolling = Infinity;
  
  activeEditor = simeditor;
}

$('#output').css('width', innerWidth - 520 + 'px');
$(window).on('resize', function() {$('#output').css('width',innerWidth - 520 + 'px');});

function reloadiframe() {
  $('#output').attr('src', currentfile + "?cachevaoider=" + Math.random()).focus(); //.on("load.iframe", function(){
//    $('#output').attr('src', currentfile + "?cachevaoider=" + Math.random()).off("load.iframe");
//  });
}

function mergeEditorContent(fromWhich) {
  if (fromWhich === 'all') {
    code = alleditor.getSession().getValue();
    splitCode(code);
  }
  if (fromWhich === 'sim') {
    parts[1] = simeditor.getSession().getValue();
    code = parts.join("")
  }
  return saveCurrentFile();
}

function saveCurrentFile() {
  return $.post("writeContentsToFile.php", {"content": code, "filename": currentfile}).then(reloadiframe);
}

function attachEditorEvents() {
  $('#save').on('click', function() {saveFile();});
  $('#load').on('click', function() {loadFile(0);});
  $('#loadExercice').on('click', function() {loadFile(1);});
  $('#undo').on('click', function() {activeEditor.undo();});
  $('#redo').on('click', function() {activeEditor.redo();});
  
  alleditor.on('change', function(){editorUpdate('all');});
  simeditor.on('change', function(){editorUpdate('sim');});
  //$("input[name='whichEditor']:checked")
  
  $("#menuDiv").on("click", "#refresh.pressed", function() {
    $(this).removeClass("pressed").addClass("notpressed");
    autoupdate = false;
  });

  $("#menuDiv").on("dblclick", "#refresh.notpressed", function() {
    $(this).removeClass("notpressed").addClass("pressed");
    autoupdate = true;
  });

  $("#menuDiv").on("click", "#refresh.notpressed", function() {
    console.log(activeEditor == simeditor);
    mergeEditorContent(activeEditor == simeditor ? "sim" : "all");
  });
  
  $("#whichFile").on("change", function() {
    loadFile(0);
  });
  
  // Editor auswählen
  $('#editorSelector').on('change', function() {
    mergeEditorContent(activeEditor == simeditor ? "sim" : "all");
    activeEditor = ($("input[name='whichEditor']:checked").length == 0) ? alleditor : simeditor;
    if (activeEditor == alleditor) {
      $('#sim-editor').hide();
      $('#all-editor').show();
      alleditor.session.setValue(code);
    }
    else {
      $('#all-editor').hide();
      $('#sim-editor').show();
      simeditor.session.setValue(parts[1]);
    }
    $('#output').focus();
  });
}



function loadFile(serverInsteadOfUser) {
  try {
  $('#saveContents>button').each(function(index) {
    this.disabled = true;
    $(this).css('cursor', 'wait');
  });
  var whichFile = $("#whichFile").val();
  var url = (serverInsteadOfUser?("vorlagen/"+whichFile+'.php'):(pathToUser+whichFile+'.html')) + '?cacheavoid=' + Math.random();
  var req = $.post(url, {lsg: lsgCode /*auf ein Bier mit wem?*/ });
  req.success(function(data) {
    code = data;
    splitCode(data);
    alleditor.session.setValue(code);
    simeditor.session.setValue(parts[1]);
    lastLoad = Date.now();
    //Set Simeditor as Active
    $("input[name='whichEditor']").get(0).checked = true;
    $('#all-editor').hide();
    $('#sim-editor').show();
    $('#saveContents>button').each(function() {
        this.disabled = false;
        $(this).css('cursor', 'auto');
    });
    saveCurrentFile();
  });
  req.fail(function() {
    if (serverInsteadOfUser === 0) {
      loadFile(1);
    }
    else {
      console.log("File not found!")
      $('#saveContents>button').each(function() {
        this.disabled = false;
        $(this).css('cursor', 'auto');
      });
    }
  });
  }
  catch(error) {
    //console.log(error)
  }
}

function saveFile() {
  mergeEditorContent(activeEditor == simeditor ? "sim" : "all");
  var whichFile = $("#whichFile").val();
  var pathToFile = pathToUser + whichFile + ".html";
  //myLog(1, 'Save PathToFile: ' + PathToFile);
  console.log('Save PathToFile: ' + pathToFile)
  $.post("writeContentsToFile.php", {"content": parts.join(""), "filename": pathToFile }).done(function() {
    $('#userLog').html("erfolgreich gespeichert!");
    setTimeout(function() {$('#userLog').html("");}, 1000);
  });
}

function splitCode(code) {
  var limits = [code.indexOf("//SIM") + 6, code.indexOf('//ENDSIM')-1];
  // myLog(1, limits.join());
  parts[0] = code.slice(0, limits[0]);
  parts[1] = code.slice(limits[0], limits[1]);
  parts[2] = code.slice(limits[1], limits[2]);
}   
    
function editorUpdate(which) {
  if (autoupdate==true) {
    console.log("autoUpdate");
    var myTime = Date.now();
    if (myTime-lastLoad > 3000) {
      mergeEditorContent(which);
    }
  }
}

if(lsgCode) galleryOnly();
function galleryOnly() {
  $(".rightMenu").hide();
  $(".editors").hide();
  $("#editorSelector").hide();
  $("#editorHead").css({background: "none"});
  $("#codearea").css({width: "auto", position:"absolute", background: "none"});
  $("#output").css({width: "100%"});
}