function pyth(c1, c2) {
  return window.Math.sqrt(c1 * c1 + c2 * c2);
}
function dist(obj1, obj2) {
  return pyth(obj1.x - obj2.x, obj1.y - obj2.y);
}
function solveQuadratic(a, b, c) {
  var diskr = b*b - 4*a*c;
  if (diskr<0) {
    return [];
  }
  else if(diskr === 0) {
    return [-b/2/a];
  }
  else {
    return [(-b+window.Math.sqrt(diskr))/2/a, (-b-window.Math.sqrt(diskr))/2/a];
  }
}


var testmode = true;
var watches = [];

function myLog(a, msg) {
  console.log(msg);
}

log = console.log.bind(console);

// Return all pattern matches with captured groups //must use global/g
RegExp.prototype.execAll = function(string) {
    var match = null;
    var matches = new Array();
    while (match = this.exec(string)) {
        var matchArray = [];
        for (i in match) {
            if (parseInt(i) == i) {
                matchArray.push(match[i]);
            }
        }
        matches.push(matchArray);
    }
    return matches;
}

//myWatch------------------------------------------------------------------------------------------------

    $("<div id='myWatch' style='position:absolute; top:" + (innerHeight - 200) + "px; left:" + (innerWidth - 550) + "px; height:150px; width:180px; background-color:black;'></div>").appendTo('body')
    .css('overflow','auto').css('font-size', '0.7em');
    $("<table id='myWatchTable'></table>").appendTo('#myWatch').css('color', 'white');
    $("<button id='clearWatch' style='position:absolute; top:" + (innerHeight - 200) + "px; left:" + (innerWidth - 400) + "px; height:20px; width:50px; font-size:0.5em'>ClearWatch</button>").appendTo('body');
    $('#clearWatch').on('click', function() {$('#myWatch').html("-----");});
      
    $('#clearWatch').hide();
    $('#myWatch').hide();
    
    var myWatch = function myWatch(key) {
        if (testmode) {
        $("<tr><td>" + key + "</td><td id = 'watch" + watches.length + "'>" + eval(key) + "</td></tr>").appendTo('#myWatchTable');
        watches.push(key);
      }
    };
    var actualizeWatches = function() {
      for (i=0; i<watches.length; i++) {
      $("#watch" + i).html(Math.round(eval(watches[i])));
      }
    };


//END MY Watch---------------------------------------------------------------------------------