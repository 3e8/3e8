function World(params) {
  var world = this;
  params = params || {};
  world.wPx = params.wPx || maxWidth || window.innerWidth;
  world.hPx = params.hPx || maxHeight || window.innerHeight;
  world.bgColor = params.bgColor || 0x000000;
  world.fontColor = params.fontColor || "#ffffff";
  
  world.renderer = new PIXI.autoDetectRenderer(world.wPx, world.hPx);
  document.body.appendChild(world.renderer.view);  

  world.stage = new PIXI.Stage(world.bgColor);
  
  if(params.img) {
    var appendImage = function() {
      var bgTexture = new PIXI.Texture.fromImage(params.img)
      var background = new PIXI.Sprite(bgTexture);
      log(bgTexture, background);
      var bgScale = Math.min(world.wPx/bgTexture.width, world.hPx/bgTexture.height) 
      background.scale.x = bgScale;
      background.scale.y = bgScale;
      world.wPx = bgTexture.width*bgScale;
      world.hPx = bgTexture.height*bgScale;
      world.renderer.resize(world.wPx, world.hPx)
      world.stage.addChild(background);
      world.renderer.render(world.stage);
    }
    var imageLoader = new PIXI.ImageLoader(params.img);
    imageLoader.addEventListener("loaded", appendImage);
    imageLoader.load();
  }
  
    
  if(params.pxPerUnit) {
    world.pxPerUnit = params.pxPerUnit;
    world.wUnits = world.wPx / world.pxPerUnit;
    world.hUnits = world.hPx / world.pxPerUnit;
  }
  else if (params.wUnits) {
    world.wUnits = params.wUnits;
    world.pxPerUnit = world.wPx / world.wUnits;
    world.hUnits = world.hPx / world.pxPerUnit;
  }
  else if (params.hUnits) {
    world.hUnits = params.hUnits;
    world.pxPerUnit = world.hPx / world.hUnits;
    world.wUnits = world.wPx / world.pxPerUnit;
  }
  else {
    world.pxPerUnit = 1;
    world.wUnits = world.wPx / world.pxPerUnit;
    world.hUnits = world.hPx / world.pxPerUnit;
  }
  
  world.unit = params.unit || "m";
  world.minUnits = params.minUnits || {x: -world.wUnits/2, y: -world.hUnits/2};
  world.maxUnits = params.maxUnits || {x: world.minUnits.x + world.wUnits, y: world.minUnits.y + world.hUnits};
  
  if(params.coords) {
    world.koordinatenachse(params.coords);
  }
  
  world.actors = [];

  return world;
}

World.prototype.render = function() {this.renderer.render(this.stage);};
World.prototype.add = function(obj) {this.actors.push(obj); this.stage.addChild(obj.sprite);};
World.prototype.xToPx = function(xUnit) {return (xUnit - this.minUnits.x) * this.pxPerUnit;}
World.prototype.yToPx = function(yUnit) {return this.hPx - (yUnit - this.minUnits.y) * this.pxPerUnit;}
World.prototype.unitsToPx = function(units) {return {x: this.xToPx(units.x), y: this.yToPx(units.y)};}
World.prototype.xToUnit = function(xPx) {return xPx / this.pxPerUnit + this.minUnits.x;}
World.prototype.yToUnit = function(yPx) {return (this.hPx - yPx) / this.pxPerUnit + this.minUnits.y;}
World.prototype.pxToUnits = function(px) {return {x: this.xToUnit(px.x), y: this.yToUnit(px.y)};}

// Koordinatenachse
World.prototype.koordinatenachse = function(params) {
  var params = params || {};
  var step = params.step || 100;
  var world = this;
//  var koordinatenachse = new PIXI.Graphics();
//  koordinatenachse.lineStyle(4, 0xFFFFFF, 1);
//  koordinatenachse.moveTo(0, maxHeight);
//  koordinatenachse.lineTo(0, 0);
//  stage.addChild(koordinatenachse);
  var createLabel = function(val, axis) {
    var number = axis=="x" ? world.xToPx(val) : world.yToPx(val)
    var skala = new PIXI.Text(val + " " + world.unit, {font: "bold 12px Tahoma" , fill: world.fontColor});
    skala.position.x = axis=="x" ? number : offset.x;
    skala.position.y = axis=="y" ? number : world.hPx - offset.y;
    skala.anchor.x = axis=="x" ? 0.5 : 0;
    skala.anchor.y = axis=="y" ? 0.5 : 1;
    world.stage.addChild(skala);
  };
  
  var offset = {x: 5, y: 2} //px von Rand;
  
  if(!params.onlyX) {
    for(var i = step * Math.ceil((world.minUnits.y + 0.1*step)/step); i < this.maxUnits.y - 0.1 * step; i += step) {createLabel(i, "y");}
  }
  if(!params.onlyY) {
    for(var i = step * Math.ceil((world.minUnits.x - 0.1*step)/step); i < this.maxUnits.x - 0.1 * step; i += step) {createLabel(i, "x");}
  }
  this.render();
};

World.prototype.update = function() {
  for(var i = 0; i < this.actors.length; i++) {
    this.actors[i].draw();
  }
  this.render();
}




/** function drawArrow()
 * 
 * @param {object} params from, x1, y1, to, x2, y2, color, thickness, alpha, headsize, pointiness
 * @returns {undefined}
 */
function drawArrow(params) {
  var params = params || {};
  params.from = params.from || {};
  params.to = params.to || {};
  var x1 = params.x1 || params.from.x || 0;
  var y1 = params.y1 || params.from.y || 0;
  var x2 = params.x2 || params.to.x || 0;
  var y2 = params.y2 || params.to.y || 0;
  var length = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
  var angle = Math.atan2((y2-y1), (x2-x1));
  var color = params.color || "#900";
  var thickness = params.thickness || 3;
  var alpha = params.alpha || 1;
  var headsize = params.headsize || Math.min(10+10*thickness, length/3);
  var pointiness = params.pointiness || 0.2;

  var arrow = new PIXI.Graphics();
  
  arrow.lineStyle(thickness, color, alpha);
  arrow.moveTo(x1, y1);
  arrow.lineTo(x2 - 0.8*headsize*Math.cos(angle), y2 - 0.8*headsize*Math.sin(angle));
  arrow.lineStyle(color, 0, 0);
  arrow.beginFill(color, alpha);
  arrow.moveTo(x2, y2);
  arrow.lineTo(x2+headsize*Math.cos(angle+Math.PI+pointiness), y2+headsize*Math.sin(angle+Math.PI+pointiness));
  arrow.lineTo(x2+headsize*Math.cos(angle+Math.PI-pointiness), y2+headsize*Math.sin(angle+Math.PI-pointiness));
  arrow.lineTo(x2, y2);
  arrow.endFill();
  
  //stage.addChild(arrow);
  //renderer.render(stage);
  
  return arrow;
}

/** function drawLine()
 * 
 * @param {object} params from, x1, y1, to, x2, y2, color, thickness, alpha
 * @returns {undefined}
 */
function drawLine(params) {
  var params = params || {};
  params.from = params.from || {};
  params.to = params.to || {};
  var x1 = params.x1 || params.from.x || 0;
  var y1 = params.y1 || params.from.y || 0;
  var x2 = params.x2 || params.to.x || 0;
  var y2 = params.y2 || params.to.y || 0;
  var color = params.color || 0x000099;
  //console.log(color);
  var thickness = params.thickness || 3;
  var alpha = params.alpha || 1;

  var line = new PIXI.Graphics();
  
  line.lineStyle(thickness, color, alpha);
  line.moveTo(x1, y1);
  line.lineTo(x2, y2);
  
  //stage.addChild(line);
  //renderer.render(stage);
  
  return line;
}

function PassiveSprite(params) {
  var params = params || {};
  var passiveSprite = this;
  passiveSprite.world = params.world || world;
  
  var adjustSprite = function() {
    passiveSprite.x = params.x || 0;
    passiveSprite.y = params.y || 0;
    passiveSprite.sprite.position = passiveSprite.world.unitsToPx(passiveSprite);
    passiveSprite.sprite.rotation = params.rotation || 0;
    passiveSprite.sprite.scale = params.scale || {x:1, y:1};
    if(params.wUnits) {
      passiveSprite.sprite.scale.x = params.wUnits * passiveSprite.world.pxPerUnit / passiveSprite.texture.width;
      passiveSprite.sprite.scale.y = passiveSprite.sprite.scale.x;
    }
    if(params.hUnits) {
      passiveSprite.sprite.scale.y = params.hUnits * passiveSprite.world.pxPerUnit / passiveSprite.texture.height;
      if(!params.wUnits) passiveSprite.sprite.scale.x = passiveSprite.sprite.scale.y; //Verzerren möglich, falls erwünscht
    }
    passiveSprite.sprite.anchor = params.anchor || {x:0.5, y:0.5};
    passiveSprite.sprite.alpha = params.alpha || 1;
    passiveSprite.world.stage.addChildAt(passiveSprite.sprite, params.background ? 0 : passiveSprite.world.stage.sprites.length);
    passiveSprite.world.render();
    return passiveSprite;
  }
  
  
  if(params.img) {
    var appendImage = function() {
      passiveSprite.texture = params.texture || new PIXI.Texture.fromImage(params.img);
      passiveSprite.sprite = new PIXI.Sprite(passiveSprite.texture);
      adjustSprite();
    }
    var imageLoader = new PIXI.ImageLoader(params.img);
    imageLoader.addEventListener("loaded", appendImage);
    imageLoader.load();
  }
  else {
    // creat passiveSprite as shape!!
    adjustSprite();
  }
}

function Actor(params) {
  
  var actor = this;
  actor.params = params || {};
  actor.world = params.world || world;
  
  if(params.img) {
    actor.preloadImage();
    //will set the image and call create
  }
  else if(params.texture) {
    actor.setImage(); //will call create
  }
  else {
    //create Sprite from shape!!
    actor.create();
  }
  return actor;
}

Actor.prototype.preloadImage = function() {
  var imageLoader = new PIXI.ImageLoader(this.params.img);
  imageLoader.addEventListener("loaded", this.setImage.bind(this));
  imageLoader.load();
}

Actor.prototype.setImage = function() {
  var actor = this;
  actor.texture = actor.params.texture || new PIXI.Texture.fromImage(actor.params.img);
	actor.sprite = new PIXI.Sprite(actor.texture);
  actor.create();
}

Actor.prototype.create = function() {
  var actor = this;
  var params = this.params;
  actor.x = params.x || 0;
  actor.y = params.y || 0;
  actor.vx = params.vx || 0;
  actor.vy = params.vy || 0;
  actor.autorotate = params.autorotate; 
  
  actor.sprite.position = actor.world.unitsToPx(actor);
  actor.sprite.rotation = params.rotation || 0;
  actor.sprite.scale = params.scale || {x:1, y:1};
  if(params.wUnits) {
    actor.sprite.scale.x = params.wUnits * actor.world.pxPerUnit / actor.texture.width;
    actor.sprite.scale.y = actor.sprite.scale.x;
  }
  if(params.hUnits) {
    actor.sprite.scale.y = params.hUnits * actor.world.pxPerUnit / actor.texture.width;
    if(!params.wUnits) actor.sprite.scale.x = actor.sprite.scale.y; //Verzerren möglich, falls erwünscht
  }
  actor.sprite.anchor = params.anchor || {x:0.5, y:0.5};
  actor.sprite.alpha = params.alpha || 1;
  actor.world.add(actor);
  actor.world.render();
}

Actor.prototype.draw = function() {
  this.sprite.position = this.world.unitsToPx(this);
  if(this.autorotate) {this.sprite.rotation = Math.atan2(-this.vy, this.vx);}
}

Actor.prototype.destroy = function() {
  this.world.actors.splice(this.world.actors.indexOf(this), 1);
  this.world.stage.removeChild(this.sprite);
}

PIXI.DisplayObjectContainer.prototype.removeAll = function()
{
  while(this.children&&this.children.length>0)
  {
    this.removeChild(this.children[0]);
  }
};

