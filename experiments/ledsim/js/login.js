$(document).ready(function(){
    myLog(1, "documentready");
    $('#Name').on("input", checkPathToUser);
    //$('#LogInButton').on('click', function() {anmelden();});
    if (document.getElementById("Name").value!="") {checkPathToUser();}
}); // Ende document Ready

var userstatus = 0;

function checkPathToUser() {
    user = $('#Name').val();
    if(user.indexOf(".")>-1) {
      user = "16-" + user.replace(".", "-");
    }
    pathToUser = user + "/";
    myLog(1, "checkPathToUser: " + pathToUser);
    var req = $.post(
        "checkPathToUser.php",
        {pathToUser: pathToUser},
        function(userstatus) {
          window.userstatus = userstatus;
          myLog(userstatus, "userstatus = " + userstatus);
          if (userstatus == 0) {
            $('#Name').css("background-color","#EAA");
          }
          else {
            $('#Name').css("background-color","#AEA");
          }
        }
    );
}

$('#output').css('width',innerWidth - 520 + 'px');
$(window).on('resize', function() {$('#output').css('width',innerWidth - 520 + 'px');});
