import * as ace from 'brace' ;
import 'brace/mode/javascript';
import 'brace/theme/chrome';
import {getUserData, saveItem, loadItem, loadTemplate} from "modules/users";
const log = console.log.bind(console);
const $ = document.querySelector.bind(document);

getUserData().then(async u=>{
  $(".username").textContent = u.vorname || u.nickname || u.name.split(" ")[0];
  //await saveItem("test", {a:1, b:2})
  //await loadItem("test"));
});

var editor = ace.edit("editor");
editor.setTheme("ace/theme/chrome");
editor.session.setMode("ace/mode/javascript");
//editor.session.setUseWorker(false); //remove this if you want live error checking. Activated because of await error.
editor.resize();
editor.getSession().setOptions({ tabSize: 2, useSoftTabs: true });
editor.$blockScrolling = Infinity;

//console.log(editor.getSession().getTokens(1));

setTimeout(_=>{
  window.onbeforeunload = function(e) {
    e.returnValue = "wirklich verlassen?";
    return "wirklich verlassen?";
  };
}, 60000); //nicht sofort setzen, um Anmeldung zu erlauben;


class Menu {
  constructor(elem) {
    this._elem = elem;
    this.editor = editor;
    elem.addEventListener("click", this.onClick.bind(this));
  }
  onClick(event) {
    let action = event.target.closest("button") && event.target.closest("button").dataset.action;
    if (action && this[action]) {
      this[action]();
    }
  };
  getTaskname() {
    return this._elem.dataset.namespace + "-" + this._elem.querySelector(".taskselect").value;
  }
  async save() {
    await saveItem(this.getTaskname(), {code: editor.getSession().getValue()});
  }
  async load() {
    let response = await loadItem(this.getTaskname());
    if(response.error) {
      return console.log(response.error);
    }
    editor.getSession().setValue(response.code)
  }
  async loadTemplate() {
    editor.getSession().setValue((await loadTemplate(this.getTaskname())).code)
  }
  async undo() {
    this.editor.undo()
  }
  async redo() {
    this.editor.redo()
  }
  async run() {
    var errors = this.editor.getSession().getAnnotations().filter(a=>a.type==="error");
    return runCode(this.editor.getSession().getValue(), errors) //in other script tag for non-strict evaluation
  }
}

new Menu($(".menu"));