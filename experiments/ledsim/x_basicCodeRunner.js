//in separateFile for non-strict evaluation

var output = document.querySelector(".output");

function log(msg) {
  output.innerHTML += `<div class="log">${msg}</div>`
  console.log(msg);
}

function appendError({row, column = "unbekannt", text}) {
  output.innerHTML += `<div class="error">Fehler in Zeile ${row} (Spalte ${column}):<br>${text}</div>`
}

async function sleep(n) {
  return new Promise(resolve=>setTimeout(_=>resolve(true), n));
}

async function waitFor(predicate, interval=10) {
  return new Promise(resolve=>{
    let wait = _ => predicate() ? resolve(true) : setTimeout(wait, interval);
    wait();
  });
}

function runeveryFrame(errorCatcher) {
  if(window.loop && typeof window.loop === "function") window.loop();
  if(window.abort) {
    console.log("abort...");
    setTimeout(_=>window.abort = false, 1000);
  }
  else requestAnimationFrame(runeveryFrame)
}


async function runCode(code, errors) {
  if(window.running) {
    window.abort = true;
    await waitFor(_=>window.abort===false);
  }
  window.loop = null;
  window.running = true;
  try {
    requestAnimationFrame(runeveryFrame);
    output.innerHTML = "";
    errors.forEach(({row, column, text})=>appendError({row: row+1, column: column+1, text}));
    eval("(async _=> {"+code.replace(/sleep\(/g, 'await sleep(').replace(/while\(/g, 'while(!window.abort&&')+";if(typeof loop !== 'undefined')window.loop=loop;window.running=false;})();");
  }
  catch(err) {
    console.log(err);
    let [,row,column] = /(?:code|<anonymous>|eval):(\d):(\d)/ig.exec(""+(err.stack||err)) || [];
    if(row !== undefined || !errors.length) {
      appendError({row, column, text: err.message})
    }
    window.running = false;
  }
  //window.running = false;
}
