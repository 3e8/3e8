import highscores from "./highscores"
import {makeNode} from "modules/domhelpers";

var missions = ["parking", "asteroids", "circular", "orbit", "slingshot", "freereturn"];

var main = document.querySelector(".main");

document.querySelector(".entername").addEventListener("click", setname);

function setname() {
  let name = document.querySelector(".name").value;
  if(name) {
    localStorage.orbitname = name;
    //noinspection JSIgnoredPromiseFromCall
    start();
  }
}

if(localStorage.orbitname) {
  //noinspection JSIgnoredPromiseFromCall
  start();
}

async function start() {
  document.body.classList.remove("noname");
  let s = await highscores.getMyScores();
  missions.forEach(m=>{
    let mission = makeNode(`<div><a class="missionlink" href="${m}.html">${m}</a><span class="stars">${"&#x2605;".repeat(s.filter(score=>score.mission===m).map(highscores.mapToStars).sort().last())}</span></div>`);
    main.appendChild(mission);
  });
  window.highscores = await highscores.getHighScores();
}
