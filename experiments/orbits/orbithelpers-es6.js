var PIXI = require("pixi.js");
import {World, Actor, PassiveSprite, createCircleTexture} from "./pixi-helpers";

window.maxHeight = window.innerHeight;
window.maxWidth = window.innerWidth;

window.nextStep = (function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(t){window.setTimeout(t,1e3/60);};})();

const G = 6.67e-11;
window.G = G;

/**
 *
 * @param [el=document.body]
 * @returns {{trace: boolean, canvaselement, ctx: (CanvasRenderingContext2D|WebGLRenderingContext|*)}}
 */
function createTracer({el = document.body} = {}) {
  var canvaselement = document.createElement("canvas");
  canvaselement.width = maxWidth;
  canvaselement.height = maxHeight;
  canvaselement.style.cssText = "position:absolute; background-color:transparent; z-index:-1";
  var background=canvaselement.getContext("2d");
  background.strokeStyle="#ff8800";
  background.lineWidth=2;
  background.lineCap="round";
  el.appendChild(canvaselement);
  var tracer = {
    trace: true,
    canvaselement: canvaselement,
    ctx: background,
    toggle() {tracer.trace = !tracer.trace; background.clearRect(0,0,maxWidth, maxHeight);}
  };
  const toggletrace = function(e) {
    if(e.code==="KeyT" && !e.repeat) tracer.toggle();
  };
  window.tracer = tracer;
  window.addEventListener("keydown", toggletrace, false);
  return tracer;
}

/**
 *
 * @param params
 * @param [el=document.body]
 * @returns {{world, Stage, App}}
 */
function createWorld(params, el=document.body) {
  let world = new World(params);
  el.appendChild(world.view);
  let {stage, app} = world;
  Object.assign(window, {world, stage, app});
  return {world, stage, app};
}

/**
 *
 * @param unit
 * @param stage
 */
function createTime({unit = "days", world = window.world} = {}) {
  var kalender = new PIXI.Text(0 + " " + unit, {fontFamily: "Tahoma", fontSize: "20px", fontStyle: "bold", fill:"white"});
  kalender.anchor = {x: 0.5, y: 0};
  kalender.position.x = world.wPx*0.5;
  kalender.position.y = 0;
  kalender.update = function(val) {
    kalender.text = val + " " + unit;
  };
  world.stage.addChild(kalender);
  return kalender;
}

function addRocketControlHandlers() {
  
  window.rocketcontrol = {schub: false, leftarrow: false, rightarrow: false};
  
  var keydowncheck = function(e) {
    if(e.key==="Control" || e.code==="Space") {window.rocketcontrol.schub = true;}
    if(e.key==="ArrowLeft") {window.rocketcontrol.leftarrow = true;}
    if(e.key==="ArrowRight") {window.rocketcontrol.rightarrow = true;}
  };
  
  var keyupcheck = function(e) {
    if(e.key==="Control" || e.code==="Space") {window.rocketcontrol.schub = false;}
    if(e.key==="ArrowLeft") {window.rocketcontrol.leftarrow = false;}
    if(e.key==="ArrowRight") {window.rocketcontrol.rightarrow = false;}
  };
  
  window.addEventListener("keydown", keydowncheck, false);
  window.addEventListener("keyup", keyupcheck, false);
}

function addMass(obj, {m, neglect=false}) {
  return Object.assign(obj, {m, neglect});
}

function rotate(dt) {
  if (window.rocketcontrol.leftarrow)  {this.lage -= this.rotspeed * dt;}
  if (window.rocketcontrol.rightarrow) {this.lage += this.rotspeed * dt;}
  this.sprite.rotation = this.lage;
}

function addControls(obj, {ax=0, ay=0, a_max=0, rotspeed=0, lage=0}) {
  return Object.assign(obj, {ax, ay, a_max, rotspeed, lage, rotate: rotate});
}

// SPARKS
function addSparks(obj) {
  var sparks = window.sparks = [];
  var sparktexture = new PIXI.Texture.fromImage("../img/spark1.png");
  var laengs = 0.32*obj.sprite.width;
  var breit = 0.14*obj.sprite.width;
  function addSpark(offset, dt) {
    let [sinrak, cosrak] = [Math.sin(obj.lage), Math.cos(obj.lage)];
    var randomOffset = 0.15 * laengs / window.world.pxPerUnit / dt * (1 + Math.random()) * (1.1-Math.abs(offset.quer)/breit);
    let x = obj.x + (offset.quer*sinrak - offset.laengs*cosrak)/window.world.pxPerUnit;
    let y = obj.y + (offset.quer*cosrak + offset.laengs*sinrak)/window.world.pxPerUnit;
    let vx = obj.vx  - cosrak * randomOffset;
    let vy = obj.vy  + sinrak * randomOffset;
    let spark = new Actor({texture: sparktexture, scale: {x:0.75*breit/sparktexture.width, y: 0.75*breit/sparktexture.width}, x, y, vx, vy});
    spark.age=0;
    return spark;
  }
  obj.sparkle = function(dt) {
    var i = sparks.length-1;
    while(i>=0) {
      let s = sparks[i];
      s.age += 1;
      s.sprite.alpha -= 0.1;
      s.x += sparks[i].vx * dt;
      s.y += sparks[i].vy * dt;
      s.sprite.position = window.world.unitsToPx(sparks[i]);
      if (s.age >= 10) {
        s.destroy();
        sparks.splice(i,1);
      }
      i--;
    }
    if(window.rocketcontrol.schub) {
      for(let i=0; i<3; i++) {
        sparks.push(addSpark({laengs, quer: breit * (1-2*Math.random())}, dt));
      }
    }
  }
}

function checkCollisions(spaceObjects = window.spaceObjects || []) {
  return spaceObjects.find(so=> dist(this, so) < so.rCol + this.rCol);
}

function autorotate(obj) {
  obj.lage = -Math.atan2(obj.vy, obj.vx);
  obj.sprite.rotation = obj.lage;
}

/**
 *
 * @param params
 * @param {World} [params.world]
 * @param {Texture|boolean} [params.texture]
 * @param {Object|boolean} [params.useCircle]
 * @param {string|boolean} [params.img]
 * @param {number} [params.x=0]
 * @param {number} [params.y=0]
 * @param {number} [params.vx=0]
 * @param {number} [params.vy=0]
 * @param {number} [params.rotation=0]
 * @param {Object} [params.scale]
 * @param {number} [params.wUnits]
 * @param {number} [params.hUnits]
 * @param {Object} [params.anchor]
 * @param {number} [params.alpha=1]
 * @param {boolean} [params.autorotate=false]
 * @param {string} [params.name=""]
 * @param {number} [params.ax=0]
 * @param {number} [params.ay=0]
 * @param {number} [params.a_max=0]
 * @param {number} [params.rotspeed=0]
 * @param {number} [params.m=Math.epsilon]
 * @param {string} [params.trace="#5555aa"]
 */
function createSpaceObject(params) {
  let {
    name="",
    m=Math.epsilon,
    ax=0,
    ay=0,
    a_max=0,
    rotspeed=0,
    rCol=0,
    trace=false,
    useCircle,
    lage=0, //Ausrichtungswinkel
    texture=false,
    ...spriteparams
  } = params;
  if(useCircle) {
    texture = createCircleTexture(useCircle);
  }
  let spaceObject = new Actor({...spriteparams, rotation:lage, texture});
  addMass(spaceObject, {m, neglect: m<1e6});
  if(!window.spaceObjects) window.spaceObjects = [];
  window.spaceObjects.push(spaceObject);
  if(trace) spaceObject.trace = trace;
  Object.assign(spaceObject, {autorotate, checkCollisions, explode, rCol}, params);
  if(a_max) addControls(spaceObject, {ax, ay, a_max, rotspeed, lage});
  if(a_max) addSparks(spaceObject);
  return spaceObject;
}

const drawIfTrace = obj => obj.trace?drawSpaceObj(obj):0;
function moveSpaceObjects({t0=0, dt, dtStandard, iterations= 1, spaceObjects = window.spaceObjects || []}) {
  var t = t0;
  while(t < t0 + iterations * dtStandard ) {
    t += dt;
    dt = rungeKuttaMotion({spaceObjects, dt, dtStandard, TD: Infinity});
    spaceObjects.forEach(drawIfTrace);
  }
  return [t, dt];
}

function dist(obj1, obj2) {
  return Math.hypot(obj1.x-obj2.x, obj1.y-obj2.y)
}

function rungeKuttaMotion({spaceObjects = window.spaceObjects, dt, dtStandard, TD}) {
  let linkToValues = {planets: spaceObjects, dt, dtStandard, TD};
  let planets = spaceObjects;
  //Speichere aktuelle Werte
  planets.forEach(p => {
    let {x,y,vx,vy} = p;
    p.tempvars = {x,y,vx,vy};
  });

  //RK4
  planets.forEach(p => {p.k1 = k(p, linkToValues);});
  planets.forEach(p => {getTempvars(p, p.k1, 0.5);});
  planets.forEach(p => {p.k2 = k(p, linkToValues);});
  planets.forEach(p => {getTempvars(p, p.k2, 0.5);});
  planets.forEach(p => {p.k3 = k(p, linkToValues);});
  planets.forEach(p => {getTempvars(p, p.k3, 1);});
  planets.forEach(p => {p.k4 = k(p, linkToValues);});

  planets.forEach(p => {
    p.x  += ( p.k1.x  + 2 * p.k2.x  + 2 * p.k3.x  + p.k4.x ) / 6;
    p.y  += ( p.k1.y  + 2 * p.k2.y  + 2 * p.k3.y  + p.k4.y ) / 6;
    p.vx += ( p.k1.vx + 2 * p.k2.vx + 2 * p.k3.vx + p.k4.vx) / 6;
    p.vy += ( p.k1.vy + 2 * p.k2.vy + 2 * p.k3.vy + p.k4.vy) / 6;
  });
  
  //Schrittweitenanpassung:
  linkToValues.dt = Math.min(linkToValues.dtStandard, 0.03*Math.sqrt(linkToValues.TD));
  //if(linkToValues.dt<linkToValues.dtStandard) {console.log("Schrittweite reduziert: " + linkToValues.dt);}
  return linkToValues.dt
}
//RK-Helpers
const {getAcceleration, k, getTempvars} = {
  //berechne Zwischenmove
  getAcceleration: function (planet, linkToValues) {
    var me = planet.tempvars;
    me.ax = 0;
    me.ay = 0;
    
    linkToValues.planets.filter(p=>p!==planet && !p.neglect).forEach(p=>{
      let other =  p.tempvars;
      let r = Math.sqrt( (me.x-other.x)**2 + (me.y-other.y)**2 /*+ 1e17*/);
      me.ax += -(me.x-other.x)*G*p.m/(r*r*r);
      me.ay += -(me.y-other.y)*G*p.m/(r*r*r);
      //Schrittweitenanpassung:
      linkToValues.TD = Math.min(linkToValues.TD, 4*Math.PI**2*r*r*r/(G*p.m));
    });
    
    //Rakete zusätzlich beschleunigen
    if (planet.a_max && window.rocketcontrol.schub) {
      me.ax += planet.a_max * Math.cos(planet.lage);
      me.ay -= planet.a_max * Math.sin(planet.lage);
    }
        
    //if(planet === linkToValues.planets[2] && window.fg++ < 100) console.log(me.ax, me.ay);
  },
  //simuliere Zwischenmove
  k: function(planet, linkToValues) {
    getAcceleration(planet, linkToValues);
    return {
      x:  linkToValues.dt * planet.tempvars.vx,
      y:  linkToValues.dt * planet.tempvars.vy,
      vx: linkToValues.dt * planet.tempvars.ax,
      vy: linkToValues.dt * planet.tempvars.ay
    }
  },
  //mach Zwischenmove
  getTempvars: function(planet, kstep, factor) {
    planet.tempvars.x  = planet.x  + factor * kstep.x;
    planet.tempvars.y  = planet.y  + factor * kstep.y;
    planet.tempvars.vx = planet.vx + factor * kstep.vx;
    planet.tempvars.vy = planet.vy + factor * kstep.vy;
  },
}

/**
 *
 * @param obj
 * @param [tracer=window.tracer]
 * @param [world=window.world]
 */
function drawSpaceObj(obj, tracer = window.tracer || {}, world = window.world) {
  //console.log(tracer.trace, obj.trace);
  if(tracer.trace && obj.trace) {
    let background = tracer.ctx;
    //console.log(background.strokeStyle, world.unitsToPx(obj));
    background.strokeStyle = obj.trace;
    background.beginPath();
    background.moveTo(obj.sprite.position.x, obj.sprite.position.y);
    obj.sprite.position = world.unitsToPx(obj);
    background.lineTo(obj.sprite.position.x, obj.sprite.position.y);
    background.stroke();
  }
  else {
    obj.sprite.position = world.unitsToPx(obj);
  }
  if(obj.exploded) {
    obj.sprite.alpha *= 0.97;
    obj.trace = false;
  }
}

/**
 *
 * @param [cx]
 * @param [cy]
 * @param a
 * @param [b]
 * @param [f]
 * @param [rot]
 * @param difficulty
 * @param [world]
 */
function ellipseOrbit({cx=0, cy=0, a, b, f, rot=0, difficulty, world=window.world}) {
  if(b===undefined) b = Math.sqrt(a*a-f*f);
  if(f===undefined) f = Math.sqrt(a*a-b*b);
  let orbit = new PIXI.Container({transparent: true});
  orbit.position = {x: world.xToPx(cx), y: world.yToPx(cy)};
  orbit.pivot = {x: -world.pxPerUnit*f, y: 0};
  orbit.rotation = rot;
  orbit.cacheAsBitmap = true;
  world.stage.addChild(orbit);
  let orbitouter = new PIXI.Graphics();
  let orbitinner = new PIXI.Graphics();
  orbitouter.lineStyle(2, 0x33ddee, 0.5); //first pram was 2
  orbitinner.lineStyle(2, 0x33ddee, 0.5);
  orbitouter.beginFill(0x33ddee, 0.05);
  orbitinner.beginFill(0x000000, 1);
  //orbitmiddle.drawEllipse(0,0,world.pxPerUnit*a,world.pxPerUnit*b);
  let ppu = world.pxPerUnit;
  orbitouter.drawEllipse(-(1-1/difficulty)*ppu*f,0,ppu/difficulty*a,ppu/difficulty*b);
  orbitinner.drawEllipse(-(1-difficulty)*ppu*f,0,ppu*difficulty*a,ppu*difficulty*b);
  orbitouter.hitArea = new PIXI.Ellipse(-(1-1/difficulty)*ppu*f,0,ppu/difficulty*a,ppu/difficulty*b);
  orbitinner.hitArea = new PIXI.Ellipse(-(1-difficulty)*ppu*f,0,ppu*difficulty*a,ppu*difficulty*b);
  orbit.addChild(orbitouter);
  orbit.addChild(orbitinner);
  //orbit.interactive = true;
//    world.view.addEventListener("mousemove", e=>{
//      let {x,y} = orbit.worldTransform.applyInverse({x:e.clientX, y:e.clientY});
//      console.log(orbitinner.hitArea.contains(x,y));
//    });
  world.render();
  return {orbit, orbitinner, orbitouter};
}

const explosiontexture = PIXI.Texture.fromImage('../img/planets/bumm.png');
function explode() {
  if(!this.exploded) {
    this.originalsprite = {texture: this.sprite.texture, scale: {x: this.sprite.scale.x, y: this.sprite.scale.y}, alpha: this.sprite.alpha};
    let f = 2 * this.sprite.texture.width / explosiontexture.width;
    this.sprite.scale.x *= f;
    this.sprite.scale.y *= f;
    this.exploded = true;
    this.sprite.texture = explosiontexture;
  }
}

let orbithelpers = {dist};

export {
  createTracer,
  createWorld,
  createTime,
  addRocketControlHandlers,
  createSpaceObject,
  moveSpaceObjects,
  drawSpaceObj,
  ellipseOrbit,
  orbithelpers
};