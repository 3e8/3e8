import {preloadImages} from "./pixi-helpers";
import {createTracer, createWorld, createTime, addRocketControlHandlers, createSpaceObject, moveSpaceObjects, drawSpaceObj, autorotate} from "./orbithelpers-es6";

(async _=>{
  await preloadImages(["../img/planets/16/rakete.png"]);
  
  let tracer = createTracer();
  
  var {world, stage, app} = createWorld({wPx: maxWidth, hPx: maxHeight, hUnits: 1e12, unit: "m", transparent: true, coords: {label: "AE", multiplier: 1.5e11, labelstep: 1}});
  
  var kalender = createTime({unit: "Tage"});
  
  addRocketControlHandlers();
  
  let sonne = createSpaceObject({
    name: "Sun",
    m: 1.8e30, // in kg
    x: -100e9,
    y: 0,
    vx: 0,
    vy: -5000,
    //useCircle: {color: 0xffeedd, r:12},
    trace: "#aa9955", //Farbe 0xRRGGBB
    img: "../img/planets/32/sonne.png",
  });
  
  let neptun = createSpaceObject({
    m: 6e29,
    x: 300e9,
    y: 0,
    vx: 0,
    vy: 15000,
    //useCircle: {color: 0x88ccff, r:8},
    trace: "#4488bb",
    img: "../img/planets/16/neptun.png"
  });
  
  let rakete = createSpaceObject({
    name: "rakete",
    img: "../img/planets/16/rakete.png",
    x: 5e10,
    y: 0,
    vx: 0,
    vy: 15000,
    m: 1000,
    a_max: 0.005,
    rotspeed: 0.0000008,
    trace: "#990000",
    lage: 1.2, //Ausrichtungswinkel
  });
  
  var t = 0;
  var i = 0;
  var sum = 0;
  var dtStandard = 3600*2;
  var dt = dtStandard;
  var iterations= 8;// Anzahl Zeitschritte, die pro Bild gemacht werden
  
  function loop() {
    let t0=t;
    if(i++%100===0) {
      //console.log(sum);
      sum = 0;
    }
    let start = performance.now();
    [t, dt] = moveSpaceObjects({t0, dt, dtStandard, iterations});
    rakete.sparkle(t-t0);
    rakete.rotate(t-t0);
    window.spaceObjects.forEach(obj=>drawSpaceObj(obj));
    sum += performance.now() - start;
    kalender.text = (t/86400).toFixed(0) + " Tage";
    app.render();
    
    nextStep(loop);
  }
  
  loop();
})();
