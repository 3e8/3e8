import {preloadImages} from "./pixi-helpers";
import {createTracer, createWorld, createTime, addRocketControlHandlers, createSpaceObject, moveSpaceObjects, drawSpaceObj, autorotate} from "./orbithelpers-es6";
import highscores from "./highscores";

(async _=>{
  await preloadImages(["../img/planets/64/rakete.png", "../img/planets/station.png"]);
  
  var {world, stage, app} = createWorld({wPx: maxWidth, hPx: maxHeight, wUnits: 200, unit: "m", transparent: true, coords: {label: "m", labelstep: 50}});
  
  var kalender = createTime({unit: "Sekunden"});

  const speed = document.querySelector(".speed");
  
  addRocketControlHandlers();
  
  let station = createSpaceObject({
    name: "station",
    img: "../img/planets/station.png",
    x: +70,
    wUnits: 40,
    y: 0,
    anchor: {x: 0.5, y: 0.6},
    m: 1,
  });
  
  let rakete = createSpaceObject({
    name: "rakete",
    img: "../img/planets/64/rakete.png",
    x: -80,
    wUnits: 10,
    m: 1,
    a_max: 20,
    rotspeed: 2,
    trace: false,
  });

  
  var t = 0;
  var dtStandard = 0.016;
  var dt = dtStandard;
  var done = false;
  
  function loop() {
    let t0 = t;
    [t, dt] = moveSpaceObjects({t0, dt, dtStandard, iterations:1});
    rakete.sparkle(t-t0);
    rakete.rotate(t-t0);
    window.spaceObjects.forEach(obj=>drawSpaceObj(obj));
    
    if(Math.abs(rakete.x - station.x) < station.wUnits*0.5 && Math.abs(rakete.y - station.y) < station.wUnits*0.4 && Math.hypot(rakete.vx, rakete.vy) < 2 && !window.rocketcontrol.schub) {
      if(!done) {
        done=true;
        highscores.setHighscore("parking", +t.toFixed(2));
        highscores.success(`Good job! you parked in ${t.toFixed(2)} seconds.`, index => {
          if(index===0) location.href = "index.html";
          if(index===1) location.reload();
        }, false);
        //setTimeout(_=>location.reload(), 2000)
      }
    }
   
    kalender.text = (t).toFixed(2) + " s";
    speed.textContent = (Math.hypot(rakete.vx, rakete.vy)).toFixed(2);
  
    app.render();
    
    nextStep(loop);
  }
  
  loop();
})();

