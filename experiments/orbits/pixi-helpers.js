var PIXI = require("pixi.js");

//preload Images first!!
/**
 *
 * @param imgArray
 * @param callback
 */
async function preloadImages(imgArray, callback) {
  const loader = PIXI.loader;
  imgArray.forEach((img, nr) => loader.add(""+nr, img));
  return new Promise(resolve=>{loader.load((loader, resources)=>resolve(resources));});
}

/**
 *
 * @param {Object} params
 * @param {number} [params.wPx]
 * @param {number} [params.hPx]
 * @param {number} [params.pxPerUnit]
 * @param {number} [params.wUnits]
 * @param {number} [params.hUnits]
 * @param {string} [params.unit]
 * @param {number} [params.minUnits]
 * @param {number} [params.maxUnits]
 * @param {Object} [params.coords]
 * @param {boolean} [params.transparent]
 * @param {string} [params.bgColor]
 * @param {string} [params.fontColor]
 * @param {string} [params.img]
 * @returns {World}
 * @constructor
 */
function World(params = {}) {
  this.wPx = params.wPx || window.innerWidth;
  this.hPx = params.hPx || window.innerHeight;
  this.fontColor = params.fontColor || "#ffffff";
  this.unit = params.unit || "m";
  this.app = new PIXI.Application({width: this.wPx, height: this.hPx, transparent: params.transparent || false, backgroundColor: params.bgColor || 0x000000 });
  document.body.appendChild(this.app.view);
  
  this.stage = this.app.stage;
  this.view = this.app.view;
  
  if(params.img) {
    var bgTexture = new PIXI.Texture.fromImage(params.img);
    var background = new PIXI.Sprite(bgTexture);
    var bgScale = Math.min(this.wPx/bgTexture.width, this.hPx/bgTexture.height);
    background.scale.x = bgScale;
    background.scale.y = bgScale;
    this.wPx = bgTexture.width*bgScale;
    this.hPx = bgTexture.height*bgScale;
    this.app.resize (this.wPx, this.hPx);
    this.stage.addChild(background);
    this.app.render();
  }
  
  
  if(params.pxPerUnit) {
    this.pxPerUnit = params.pxPerUnit;
    this.wUnits = this.wPx / this.pxPerUnit;
    this.hUnits = this.hPx / this.pxPerUnit;
  }
  else if (params.wUnits) {
    this.wUnits = params.wUnits;
    this.pxPerUnit = this.wPx / this.wUnits;
    this.hUnits = this.hPx / this.pxPerUnit;
  }
  else if (params.hUnits) {
    this.hUnits = params.hUnits;
    this.pxPerUnit = this.hPx / this.hUnits;
    this.wUnits = this.wPx / this.pxPerUnit;
  }
  else {
    this.pxPerUnit = 1;
    this.wUnits = this.wPx / this.pxPerUnit;
    this.hUnits = this.hPx / this.pxPerUnit;
  }
  
  this.minUnits = params.minUnits || {x: -this.wUnits/2, y: -this.hUnits/2};
  this.maxUnits = params.maxUnits || {x: this.minUnits.x + this.wUnits, y: this.minUnits.y + this.hUnits};
  
  if(params.coords) {
    this.koordinatenachse(params.coords);
  }
  
  this.actors = [];
  return this;
}

World.prototype.render = function() {this.app.render();};
World.prototype.add = function(obj) {this.actors.push(obj); this.stage.addChild(obj.sprite);};
World.prototype.xToPx = function(xUnit) {return (xUnit - this.minUnits.x) * this.pxPerUnit;};
World.prototype.yToPx = function(yUnit) {return this.hPx - (yUnit - this.minUnits.y) * this.pxPerUnit;};
World.prototype.unitsToPx = function(units) {return {x: this.xToPx(units.x), y: this.yToPx(units.y)};};
World.prototype.xToUnit = function(xPx) {return xPx / this.pxPerUnit + this.minUnits.x;};
World.prototype.yToUnit = function(yPx) {return (this.hPx - yPx) / this.pxPerUnit + this.minUnits.y;};
World.prototype.pxToUnits = function(px) {return {x: this.xToUnit(px.x), y: this.yToUnit(px.y)};};

// Koordinatenachse
/**
 *
 * @param {Object} params {step, onlyX, onlyY}
 * @param {number} [params.step]
 * @param {string} [params.label]
 * @param {number} [params.labelstep]
 * @param {number} [params.multiplier]
 * @param {number} [params.onlyX=false]
 * @param {number} [params.onlyY=false]
 */
World.prototype.koordinatenachse = function({label = this.unit, step = this.wUnits/4, labelstep = false, multiplier= 1, onlyX, onlyY} = {}) {
  var world = this;
  if(labelstep) step = labelstep * multiplier;
//  var koordinatenachse = new PIXI.Graphics();
//  koordinatenachse.lineStyle(4, 0xFFFFFF, 1);
//  koordinatenachse.moveTo(0, maxHeight);
//  koordinatenachse.lineTo(0, 0);
//  stage.addChild(koordinatenachse);
  var createLabel = function(val, axis) {
    var number = axis==="x" ? world.xToPx(val) : world.yToPx(val);
    var skala = new PIXI.Text(val/multiplier + " " + label, {fontSize: "12px", fontFamily:"Tahoma", fontWeight: "bold", fill: world.fontColor});
    skala.position.x = axis==="x" ? number : offset.x;
    skala.position.y = axis==="y" ? number : world.hPx - offset.y;
    skala.anchor.x = axis==="x" ? 0.5 : 0;
    skala.anchor.y = axis==="y" ? 0.5 : 1;
    world.stage.addChild(skala);
  };
  
  var offset = {x: 5, y: 2}; //px von Rand;
  
  
  if(!onlyX) {
    for(let i = step * Math.ceil((world.minUnits.y + 0.1*step)/step); i < this.maxUnits.y - 0.1 * step; i += step) {createLabel(i, "y");}
  }
  if(!onlyY) {
    for(let i = step * Math.ceil((world.minUnits.x - 0.1*step)/step); i < this.maxUnits.x - 0.1 * step; i += step) {createLabel(i, "x");}
  }
  this.render();
};

World.prototype.update = function() {
  for(var i = 0; i < this.actors.length; i++) {
    this.actors[i].draw();
  }
  this.render();
};

/** function drawArrow()
 *
 * @param {Object} [params={}]
 * @param {Object} params.from
 * @param {Object} params.to
 * @param {int} params.x1
 * @param {int} params.x2
 * @param {int} params.y1
 * @param {int} params.y2
 * @param {string} params.color
 * @param {int} params.thickness
 * @param {number} params.alpha
 * @param {number} params.headsize
 * @param {number} params.pointiness
 * @returns {Object}
 */
function drawArrow(params = {}) {
  params.from = params.from || {};
  params.to = params.to || {};
  var x1 = params.x1 || params.from.x || 0;
  var y1 = params.y1 || params.from.y || 0;
  var x2 = params.x2 || params.to.x || 0;
  var y2 = params.y2 || params.to.y || 0;
  var length = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
  var angle = Math.atan2((y2-y1), (x2-x1));
  var color = params.color || "#900";
  var thickness = params.thickness || 3;
  var alpha = params.alpha || 1;
  var headsize = params.headsize || Math.min(10+10*thickness, length/3);
  var pointiness = params.pointiness || 0.2;
  
  var arrow = new PIXI.Graphics();
  
  arrow.lineStyle(thickness, color, alpha);
  arrow.moveTo(x1, y1);
  arrow.lineTo(x2 - 0.8*headsize*Math.cos(angle), y2 - 0.8*headsize*Math.sin(angle));
  arrow.lineStyle(color, 0, 0);
  arrow.beginFill(color, alpha);
  arrow.moveTo(x2, y2);
  arrow.lineTo(x2+headsize*Math.cos(angle+Math.PI+pointiness), y2+headsize*Math.sin(angle+Math.PI+pointiness));
  arrow.lineTo(x2+headsize*Math.cos(angle+Math.PI-pointiness), y2+headsize*Math.sin(angle+Math.PI-pointiness));
  arrow.lineTo(x2, y2);
  arrow.endFill();
  
  //stage.addChild(arrow);
  //app.render();
  
  return arrow;
}

/** function drawLine()
 *
 * @param {object} params
 * @param {object} params.from
 * @param {object} params.to
 * @param {int} params.x1
 * @param {int} params.x2
 * @param {int} params.y1
 * @param {int} params.y2
 * @param {string} params.color
 * @param {number} params.thickness
 * @param {number} params.alpha
 * @returns {undefined}
 */
function drawLine(params = {}) {
  params.from = params.from || {};
  params.to = params.to || {};
  var x1 = params.x1 || params.from.x || 0;
  var y1 = params.y1 || params.from.y || 0;
  var x2 = params.x2 || params.to.x || 0;
  var y2 = params.y2 || params.to.y || 0;
  var color = params.color || 0x000099;
  //console.log(color);
  var thickness = params.thickness || 3;
  var alpha = params.alpha || 1;
  
  var line = new PIXI.Graphics();
  
  line.lineStyle(thickness, color, alpha);
  line.moveTo(x1, y1);
  line.lineTo(x2, y2);
  
  //stage.addChild(line);
  //app.render();
  
  return line;
}

/**
 *
 * @param params
 * @returns {PassiveSprite}
 * @constructor
 */
function PassiveSprite(params = {}) {
  this.world = params.world || world;
  var texture = params.texture || new PIXI.Texture.fromImage(params.img);
  this.sprite = new PIXI.Sprite(texture);
  this.x = params.x || 0;
  this.y = params.y || 0;
  this.sprite.position = this.world.unitsToPx(this);
  this.sprite.rotation = params.rotation || 0;
  this.sprite.scale = params.scale || {x:1, y:1};
  if(params.wUnits) {
    this.sprite.scale.x = params.wUnits * this.world.pxPerUnit / texture.width;
    //noinspection JSSuspiciousNameCombination
    this.sprite.scale.y = this.sprite.scale.x;
  }
  if(params.hUnits) {
    this.sprite.scale.y = params.hUnits * this.world.pxPerUnit / texture.height;
    if(!params.wUnits) { //noinspection JSSuspiciousNameCombination
      this.sprite.scale.x = this.sprite.scale.y;
    } //Verzerren möglich, falls erwünscht
  }
  this.sprite.anchor = params.anchor || {x:0.5, y:0.5};
  this.sprite.alpha = params.alpha || 1;
  this.world.stage.addChildAt(this.sprite, params.background ? 0 : this.world.stage.children.length);
  this.world.render();
  return this;
}

/**
 *
 * @param {Object} params
 * @param {World} [params.world]
 * @param {Texture|boolean} [params.texture]
 * @param {string|boolean} [params.img]
 * @param {number} [params.x=0]
 * @param {number} [params.y=0]
 * @param {number} [params.vx=0]
 * @param {number} [params.vy=0]
 * @param {number} [params.rotation=0]
 * @param {Object} [params.scale]
 * @param {number} [params.wUnits]
 * @param {number} [params.hUnits]
 * @param {Object} [params.anchor]
 * @param {number} [params.alpha=1]
 * @param {boolean} [params.autorotate=false]
 * @returns {Actor}
 * @constructor
 */
function Actor({world=window.world, texture=false, img=false, x=0, y=0, vx=0, vy=0,
                 autorotate=false, rotation=0, scale={x:1, y:1}, wUnits, hUnits, anchor={x:0.5, y:0.5}, alpha=1} = {}) {
  texture = texture || new PIXI.Texture.fromImage(img);
  this.sprite = new PIXI.Sprite(texture);
  Object.assign(this, {x, y, vx, vy, autorotate, world});
  Object.assign(this.sprite, {position: this.world.unitsToPx(this), rotation, scale, anchor, alpha});
  if(wUnits) {
    scale = wUnits * this.world.pxPerUnit / texture.width;
    this.sprite.scale = {x: scale, y: scale};
  }
  if(hUnits) {
    scale = hUnits * this.world.pxPerUnit / texture.height;
    this.sprite.scale = {x: scale, y: scale};
    if(wUnits) {
      this.sprite.scale.x = wUnits * this.world.pxPerUnit / texture.width; //Verzerren möglich, falls erwünscht
    }
  }
  world.add(this);
  world.render();
  return this;
}

Actor.prototype.draw = function() {
  this.sprite.position = this.world.unitsToPx(this);
  this.sprite.rotation = this.rotation!==undefined ? -this.rotation : this.sprite.rotation;
  if(this.autorotate) {this.sprite.rotation = Math.atan2(-this.vy, this.vx);}
};

Actor.prototype.destroy = function() {
  this.world.actors.splice(this.world.actors.indexOf(this), 1);
  this.world.stage.removeChild(this.sprite);
};

//PIXI.Container.prototype.removeAll = function()
//{
//  while(this.children&&this.children.length>0)
//  {
//    this.removeChild(this.children[0]);
//  }
//};
//use PIXI.Container.removeChildren();

function createCircleTexture({r, color = 0x333399, renderer = window.world.app.renderer}) {
  const p = new PIXI.Graphics();
  p.beginFill(color); //
  p.lineStyle(0);
  p.drawCircle(r, r, r);
  p.endFill();
  let t = PIXI.RenderTexture.create(p.width, p.height);
  renderer.render(p, t);
  return t;
}


export {PIXI, World, Actor, PassiveSprite, preloadImages, drawArrow, drawLine, createCircleTexture}