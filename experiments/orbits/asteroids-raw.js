import {preloadImages} from "./pixi-helpers";
import {createTracer, createWorld, createTime, addRocketControlHandlers, createSpaceObject, moveSpaceObjects, drawSpaceObj, orbithelpers} from "./orbithelpers-es6";
import highscores from "./highscores";

(async _=>{
  const useAsteroidImages = 16; //max. 64
  
  await preloadImages(["../img/planets/64/rakete.png", ...new Array(useAsteroidImages).fill(0).map((el, i)=>`../img/planets/asteroids/asteroid_${i}.png`)]);
  
  var {world, stage, app} = createWorld({wPx: maxWidth, hPx: maxHeight, xmin: -20, hUnits: 200, unit: "m", transparent: true});
  
  var kalender = createTime({unit: "Sekunden"});

  addRocketControlHandlers();
  
  //var raw = new Array(100).fill(0).map((el, i)=>({x: 10*i+10*Math.random(), y:90-180*Math.random(), r: 15+Math.random()*15}));
  var raw = [{x:0,y:-84,r:17},{x:24,y:-74,r:26},{x:36,y:62,r:28},{x:57,y:-22,r:18},{x:60,y:84,r:22},{x:75,y:-48,r:29},{x:81,y:-78,r:16},{x:90,y:45,r:29},{x:109,y:87,r:21},{x:113,y:-71,r:24},{x:129,y:-11,r:21},{x:147,y:6,r:17},{x:159,y:-79,r:26},{x:165,y:-32,r:17},{x:170,y:-9,r:15},{x:182,y:-55,r:29},{x:191,y:-85,r:20},{x:208,y:80,r:21},{x:229,y:53,r:26},{x:232,y:-71,r:18},{x:248,y:-35,r:26},{x:254,y:-79,r:17},{x:273,y:49,r:16},{x:293,y:-55,r:28},{x:317,y:59,r:26},{x:325,y:28,r:26},{x:336,y:1,r:24},{x:344,y:-61,r:21},{x:358,y:70,r:15},{x:361,y:34,r:27},{x:375,y:-37,r:21},{x:394,y:-30,r:16},{x:413,y:27,r:27},{x:424,y:-65,r:25},{x:445,y:38,r:17},{x:455,y:-48,r:20},{x:480,y:-42,r:23},{x:500,y:37,r:23},{x:523,y:-79,r:28},{x:530,y:10,r:20},{x:548,y:43,r:26},{x:551,y:-87,r:16},{x:565,y:86,r:15},{x:579,y:43,r:27},{x:581,y:48,r:24},{x:591,y:76,r:25},{x:601,y:29,r:19},{x:625,y:-63,r:28},{x:639,y:87,r:23},{x:649,y:-46,r:17},{x:653,y:67,r:26},{x:661,y:-86,r:24},{x:673,y:-57,r:16},{x:686,y:44,r:17},{x:699,y:-11,r:23},{x:718,y:45,r:21},{x:736,y:-16,r:22},{x:745,y:-58,r:25},{x:769,y:-38,r:17},{x:773,y:-36,r:24},{x:785,y:75,r:20},{x:802,y:22,r:18},{x:822,y:-5,r:22},{x:835,y:50,r:29},{x:841,y:78,r:23},{x:857,y:25,r:24},{x:871,y:-15,r:25},{x:885,y:41,r:18},{x:891,y:88,r:19},{x:913,y:20,r:25},{x:922,y:-33,r:20},{x:937,y:-71,r:27},{x:956,y:49,r:29},{x:966,y:-52,r:28},{x:973,y:31,r:18},{x:981,y:-76,r:24},{x:992,y:11,r:18}];
  var ast = raw.map((r,i)=>{
    return createSpaceObject({
      img:    `../img/planets/asteroids/asteroid_${i%useAsteroidImages}.png`,
      x0:      r.x,
      x:      r.x,
      y:      r.y,
      wUnits:  r.r,
      m:       1,
      rCol:    r.r/2+3, //3 von der Rakete
      neglect: true
    });
  });
//  window.addEventListener("click", e=>{
//    let clc = world.pxToUnits({x: e.clientX, y: e.clientY});
//    clc.x = clc.x + rakete.x;
//    let akill = ast.find(a=>orbithelpers.dist(a, clc)<a.wUnits/2);
//    console.log(akill);
//    if(akill) {
//      ast.splice(ast.indexOf(akill), 1);
//      akill.destroy();
//      console.log(ast);
//    }
//  });
  
  let rakete = createSpaceObject({
    name: "rakete",
    img: "../img/planets/64/rakete.png",
    x: 0,
    y: 0,
    wUnits: 10,
    m: 1,
    a_max: 40,
    rotspeed: 4,
    trace: false,
    neglect: true
  });

  
  var t = 0;
  var dtStandard = 0.016;
  var dt = dtStandard;
  var done = false;
  
  function loop() {
    let t0 = t;
    [t, dt] = moveSpaceObjects({t0, dt, dtStandard, iterations:1});
    rakete.sparkle(t-t0);
    rakete.rotate(t-t0);
    
    window.spaceObjects.forEach(obj=>drawSpaceObj(obj));
    window.spaceObjects.forEach(a=>a.sprite.position.x = world.xToPx(a.x - rakete.x));
    window.sparks.forEach(a=>a.sprite.position.x = world.xToPx(a.x - rakete.x));
    if(!rakete.exploded && rakete.checkCollisions()) {
      rakete.explode();
      setTimeout(_=>location.reload(), 2000);
    }
    if(rakete.y > 104 || rakete.y < -104) location.reload();
    
    kalender.text = (t).toFixed(3) + " s";
    app.render();
    
    if(rakete.x > 1020 && !rakete.exploded) {
      if(!done) {
        done=true;
        highscores.setHighscore("asteroids", +t.toFixed(2));
        highscores.success(`Good job! you parked in ${t.toFixed(2)} seconds.`, index => {
          if(index===0) location.href = "index.html";
          if(index===1) location.reload();
        }, false);
      }
    }
    else {
      nextStep(loop);
    }
    

  }
  
  loop();
})();

