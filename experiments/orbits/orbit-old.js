import {PIXI, World, Actor, preloadImages} from "./pixi-helpers";
import {createTracer, createWorld, createTime, addRocketControlHandlers, createSpaceObject, moveSpaceObjects, drawSpaceObj, ellipseOrbit} from "./orbithelpers-es6";

(async _ => {

  let resources = await preloadImages(["../img/planets/erde.png", "../img/planets/16/rakete.png"]);
  
  let tracer = createTracer();
  var {world, stage, app} = createWorld({wPx: maxWidth, hPx: maxHeight, hUnits: 124000000, unit: "m", transparent: true});
  
  var kalender = createTime({unit: "Minuten"});
  addRocketControlHandlers();
  
  let a = 15e6 + Math.random()*25e6;
  let f = (a - 8000000) * Math.random();
  let b = Math.sqrt(a*a-f*f);
  let rot = 2*Math.PI*Math.random();
  let difficulty = 0.8;
  let {orbit, orbitinner, orbitouter} = ellipseOrbit({a, f, rot, difficulty});
  
  let mErde = 5.97e24;
  
  let helpers = [];
  for(let i=0; i<4; i++) {
    for(let c = difficulty + 0.05; c < 1 / difficulty; c += 0.05) {
      let [ac, bc, fc] = [a*c,b*c,Math.sqrt((a*c)**2-(b*c)**2)];
      let v_med = Math.sqrt(G * mErde / (ac));
      let v_aphel = v_med * bc / (ac + fc);
      let v_perihel = v_med * bc / (ac - fc);
      let cosr = Math.cos(rot), sinr = Math.sin(rot);
      helpers.push = createSpaceObject({
        useCircle: {r: 2, color: 0x3377ff},
        //wUnits: 2000000,
        x:         [cosr * (ac + fc), cosr * fc - sinr * bc, -cosr * (ac - fc), cosr * fc + sinr * bc][i],
        y:         [-sinr * (ac + fc), -sinr * fc - cosr * bc, sinr * (ac - fc), -sinr * fc + cosr * bc][i],
        name:      "helper" + i + c,
        vx:        [sinr * v_aphel, cosr * v_med, -sinr * v_perihel, -cosr * v_med][i],
        vy:        [cosr * v_aphel, -sinr * v_med, -cosr * v_perihel, sinr * v_med][i],
        m:         1,
      });
    }
  }
  world.render();
  
  let erde = createSpaceObject({
    name: "erde",
    img: "../img/planets/erde.png",
    hUnits: 2*6371000,
    m: mErde
  });
  
  moveSpaceObjects({t0: 0, dt: 80, dtStandard: 80, iterations: 5000});
  
  let rakete = createSpaceObject({
    name: "rakete",
    img: "../img/planets/16/rakete.png",
    wUnits: 2000000,
    x: 6371000+10000000,
    y:0,
    vx:       0.1,
    vy:       0.1,
    m:        1e3,
    a_max:    10, //variable
    rotspeed: 0.002,
    trace:    "#990000",
    lage:     1.2, //Ausrichtungswinkel
  });

  var t=0; // in Sekunden
  var dtStandard = 20; //Zeitschritt der Simulation
  var dt = dtStandard;
  var iterations = 1; // Anzahl Zeitschritte, die pro Bild gemacht werden
  
  //var planets = [erde, rakete, ...helpers];

  let i=0, sum=0;
  var success = {anglesum: 0, oldangle: false};
  function loop() {
    let t0=t;
    if(i++%100===0) {
      //console.log(sum);
      sum = 0;
    }
    let {x,y} = orbit.worldTransform.applyInverse(rakete.sprite.position);

    if(success.oldangle !== false) {
      let a = Math.atan2(rakete.y, rakete.x)*180/Math.PI;
      success.anglesum += (a - success.oldangle + 540) % 360 - 180;
      success.oldangle = a;
      if(Math.abs(success.anglesum) > 360 && !success.done) {console.log("Yay!!"); success.done=true}
    }
    if(orbitouter.hitArea.contains(x,y) && !orbitinner.hitArea.contains(x,y) && !tracer.trace && !window.rocketcontrol.schub) {
      tracer.toggle();
      success.anglesum = 0;
      success.oldangle = Math.atan2(rakete.y, rakete.x)*180/Math.PI;
    }
    if((!orbitouter.hitArea.contains(x,y) || orbitinner.hitArea.contains(x,y) || window.rocketcontrol.schub) && tracer.trace) {
      tracer.toggle();
      success.oldangle = false;
    }
    let start = performance.now();
    rakete.a_max = 3 + 1.2 * G * erde.m / (rakete.x**2 + rakete.y**2);
    [t, dt] = moveSpaceObjects({t0, dt, dtStandard, iterations});
    rakete.sparkle(t-t0);
    rakete.rotate(t-t0);
    window.spaceObjects.forEach(obj=>drawSpaceObj(obj));
    sum += performance.now() - start;
    kalender.text = (t/60).toFixed(0) + " Minuten";
    app.render();
    
    nextStep(loop);
  }
  
  loop();
  
})();
