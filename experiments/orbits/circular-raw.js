import {PIXI, World, Actor, preloadImages} from "./pixi-helpers";
import {createTracer, createWorld, createTime, addRocketControlHandlers, createSpaceObject, moveSpaceObjects, drawSpaceObj, ellipseOrbit} from "./orbithelpers-es6";
import highscores from "./highscores";

(async _ => {
  
  let resources = await preloadImages(["../img/planets/earthpolar.png", "../img/planets/16/rakete.png"]);
  
  let tracer = createTracer();
  var {world, stage, app} = createWorld({wPx: maxWidth, hPx: maxHeight, hUnits: 124000000, unit: "m", transparent: true});
  
  var kalender = createTime({unit: "Minuten"});
  addRocketControlHandlers();
  
  var [degree, free, speed] = [".degree", ".free", ".speed"].map(c=>document.querySelector(c));
  
  let a = 15e6 + Math.random()*25e6;
  let f = 0;
  let b = Math.sqrt(a*a-f*f);
  let rot = 2*Math.PI*Math.random();
  let difficulty = 0.7;
  let {orbit, orbitinner, orbitouter} = ellipseOrbit({a, f, rot, difficulty});
  
  let mErde = 5.97e24;
  
  let helpers = [];
  for(let i=0; i<4; i++) {
    for(let c = difficulty + 0.05; c < 1 / difficulty; c += 0.1) {
      let [ac, bc, fc] = [a*c,b*c,Math.sqrt((a*c)**2-(b*c)**2)];
      let v_med = Math.sqrt(G * mErde / (ac));
      let v_aphel = v_med * bc / (ac + fc);
      let v_perihel = v_med * bc / (ac - fc);
      let cosr = Math.cos(rot), sinr = Math.sin(rot);
      helpers.push = createSpaceObject({
        useCircle: {r: 2, color: 0x3377ff},
        //wUnits: 2000000,
        x:         [cosr * (ac + fc), cosr * fc - sinr * bc, -cosr * (ac - fc), cosr * fc + sinr * bc][i],
        y:         [-sinr * (ac + fc), -sinr * fc - cosr * bc, sinr * (ac - fc), -sinr * fc + cosr * bc][i],
        name:      "helper" + i + c,
        vx:        [sinr * v_aphel, cosr * v_med, -sinr * v_perihel, -cosr * v_med][i],
        vy:        [cosr * v_aphel, -sinr * v_med, -cosr * v_perihel, sinr * v_med][i],
        m:         1,
      });
    }
  }
  world.render();
  
  let erde = createSpaceObject({
    name: "erde",
    img: "../img/planets/earthpolar.png",
    hUnits: 2*6371000,
    rCol: 6e6,
    m: mErde
  });
  
  moveSpaceObjects({t0: 0, dt: 80, dtStandard: 80, iterations: 5000});
  
  let rakete = createSpaceObject({
    name: "rakete",
    img: "../img/planets/16/rakete.png",
    wUnits: 2000000,
    x: 6371000+10000000,
    y:0,
    vx:       0.1,
    vy:       0.1,
    m:        1e3,
    a_max:    10, //variable
    rotspeed: 0.002,
    trace:    "#990000",
  });
  
  var t=0; // in Sekunden
  var dtStandard = 25; //Zeitschritt der Simulation
  var dt = dtStandard;
  var iterations = 1; // Anzahl Zeitschritte, die pro Bild gemacht werden
  
  //var planets = [erde, rakete, ...helpers];
  
  let i=0, sum=0;
  var success = {anglesum: 0, freesum: 0, oldangle: false};
  var done = false;
  function loop() {
    let t0=t;
    if(i++%100===0) {
      //console.log(sum);
      sum = 0;
    }
    let {x,y} = orbit.worldTransform.applyInverse(rakete.sprite.position);
    
    if(success.oldangle !== false) {
      let a = Math.atan2(rakete.y, rakete.x)*180/Math.PI;
      success.anglesum += (a - success.oldangle + 540) % 360 - 180;
      success.freesum += (a - success.oldangle + 540) % 360 - 180;
      if(window.rocketcontrol.schub) {
        success.freesum = 0;
        if(tracer.trace) tracer.toggle();
      }
      success.oldangle = a;
    }
    if(orbitouter.hitArea.contains(x,y) && !orbitinner.hitArea.contains(x,y) && success.oldangle === false) {
      success.anglesum = 0;
      success.freesum = 0;
      success.oldangle = Math.atan2(rakete.y, rakete.x)*180/Math.PI;
    }
    if((!orbitouter.hitArea.contains(x,y) || orbitinner.hitArea.contains(x,y))) {
      if(tracer.trace) tracer.toggle();
      success.oldangle = false;
    }
    let start = performance.now();
    rakete.a_max = 3 + 1.3 * G * erde.m / (rakete.x**2 + rakete.y**2);
    [t, dt] = moveSpaceObjects({t0, dt, dtStandard, iterations});
    erde.sprite.rotation -= (t-t0) * 2 * Math.PI / 86400;
    if(!rakete.started) {
      rakete.x = Math.cos(erde.sprite.rotation) * 7e6;
      rakete.y = -Math.sin(erde.sprite.rotation) * 7e6;
      rakete.vx = 0;
      rakete.vy = 0;
      rakete.lage = erde.sprite.rotation;
    }
    else {
      rakete.sparkle(t-t0);
      if(!rakete.exploded && rakete.checkCollisions()) {
        rakete.explode();
        setTimeout(_=>location.reload(), 2000);
      }
    }
    rakete.rotate(t-t0);
    if(window.rocketcontrol.schub) rakete.started = true;
    window.spaceObjects.forEach(obj=>drawSpaceObj(obj));
    sum += performance.now() - start;
    kalender.text = (t/60).toFixed(0) + " Minuten";
  
    //degree.textContent = success.anglesum.toFixed(0);
    free.textContent = (success.freesum-20).toFixed(0);
    //speed.textContent = Math.hypot(rakete.vx, rakete.vy).toFixed(0);
    if(Math.abs(success.freesum) > 20 && !tracer.trace) tracer.toggle();
    if(Math.abs(success.freesum) > 390 && !done) {
      done=true;
      highscores.setHighscore("circular", +(t/60).toFixed(0));
      highscores.success(`Good job! You reached your orbit in ${(t/60).toFixed(0)} minutes.`, index => {
        if(index===0) location.href = "index.html";
        if(index===1) location.reload();
      }, true);
    }
    
    app.render();
    
    nextStep(loop);
  }
  
  loop();
  
})();