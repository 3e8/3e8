import {Store, Item} from "modules/itemstore";
import {choosebox} from "modules/messages";

var store = new Store("orbitscores", {
  dbcentral: "mydb/db", // "/db" wird entfernt, Rest sollte der Pfad zum Ordner sein
  dbname: "main",
  leavesonly: true,
  longpolling: false,
  keysByType: {}
});

function setHighscore(mission, score, code) {
  store.dDb.addItem("score", {name: localStorage.orbitname, mission, score}).then(r=>console.log(r));
}

function success(msg, callback, canContinue = false) {
  choosebox(`<div>Success!</div><div>${msg}</div>`, ["Back to Mission control", "Try again"].concat(canContinue?["Continue"]:[]), callback)
}

async function getMyScores() {
  await store.waitUntilReady();
  let name = localStorage.orbitname;
  return store.getLeavesAsArray().filter(s=>s.name === name);
}

async function getHighScores() {
  await store.waitUntilReady();
  console.log(1);
  return store.getLeavesAsArray();
}

function mapToStars({mission, score}) {
  if(mission==="parking") return +score < 12 ? 5 : score < 18 ? 4 : 3;
  if(mission==="asteroids") return +score < 25 ? 5 : score < 40 ? 4 : 3;
  if(mission==="circular") return +score < 1600 ? 5 : score < 3000 ? 4 : 3;
  if(mission==="orbit") return +score < 1600 ? 5 : score < 3000 ? 4 : 3;
  if(mission==="slingshot") return +score > 50 ? 5 : score > 45 ? 4 : 3;
  if(mission==="freereturn") return 5;
}

export default {setHighscore, success, getMyScores, mapToStars, getHighScores}