import {preloadImages} from "./pixi-helpers";
import {createTracer, createWorld, createTime, addRocketControlHandlers, createSpaceObject, moveSpaceObjects, drawSpaceObj, orbithelpers} from "./orbithelpers-es6";
import highscores from "./highscores";

(async _=>{
  await preloadImages(["../img/planets/64/rakete.png", "../img/planets/erde.png", "../img/planets/mond.png"]);
  
  let tracer = createTracer();
  
  var {world, stage, app} = createWorld({wPx: maxWidth, hPx: maxHeight, hUnits: 9e8, unit: "m", transparent: true, coords: {label: "Ls", multiplier: 3e8, labelstep: 1}});
  
  var kalender = createTime({unit: "Tage"});

  //addRocketControlHandlers();
  
  
  function addHandlers() {
    window.params = {};
    document.querySelectorAll("[data-key]").forEach(el=>{
      let setValue = _ => {
        if(+el.value>el.getAttribute("max")) {
          el.value = el.getAttribute("max");
        }
        el.value = ("00"+el.value).slice(-2);
        let key = el.dataset.key;
        window.params[key] = +el.value;
      };
      setValue();
      el.addEventListener("change", _=>setValue(el));
    });
    document.querySelector(".run").addEventListener("click", e=>{
      let r = running;
      reset();
      if(!r) {
        running=true;
        loop();
      }
    })
  }
  addHandlers();
  
  let erde = createSpaceObject({
    name: "Erde",
    img: "../img/planets/erde.png",
    m: 6e24, // in kg
    x: 0,
    y: 0,
    vx: +50,
    wUnits: 6371000*5,
  });
  
  let mond = createSpaceObject({
    img: "../img/planets/mond.png",
    m: 3e23,
    y: 384000000,
    vx: -1000,
    rCol: 1500000*3,
    wUnits: 1500000*10,
  });
  
  let tstart = -600*Math.floor(6*12*Math.random());

  
  let rakete = createSpaceObject({
    name: "rakete",
    img: "../img/planets/64/rakete.png",
    wUnits: 2000000*10,
    m: 1000,
    autorotate: true
  });
  
  function reset() {
    running = false;
    tracer.ctx.clearRect(0,0,maxWidth, maxHeight);
    t = tstart;
    if(rakete.exploded) {
      rakete.exploded = false;
      Object.assign(rakete.sprite, rakete.originalsprite);
    }
    Object.assign(erde, {x:0, y:0, vx: 100, vy: 0});
    Object.assign(mond, {x:0, y:384e6, vx: -1000, vy: 0});
    Object.assign(rakete, {
      y: 0.5e8,
      x: 0,
      vx: -Math.sqrt(G*erde.m/0.5e8)+erde.vx,
      vy: 0,
      trace: false,
      started: false
    });
    window.spaceObjects.forEach(obj=>drawSpaceObj(obj));
    app.render();
  }
  
  reset();
  window.spaceObjects.forEach(obj=>drawSpaceObj(obj));
  app.render();
  
  var running = false;
  var t = tstart;
  var dtStandard = 600;
  var dt = dtStandard;
  var done = false;

  function checkTime(t) {
    if(!rakete.started) {
//      let a = Math.atan2(erde.vy, erde.vx);
//      rakete.x = erde.x + Math.sin(a) * 7e8;
//      rakete.y = erde.y - Math.cos(a) * 7e8;
//      rakete.sprite.rotation = -a+Math.PI/2;
//      rakete.vx = rakete.vy = 0;
      if(t >= 86400*window.params.date + 3600*window.params.hours + 60*window.params.minutes) {
        rakete.vx *= 1.4;
        rakete.vy *= 1.4;
        rakete.trace="#770000";
        rakete.started = true;
        app.render();
      }
    }
  }

  const speed = document.querySelector(".speed");
  
  function loop() {
    let t0 = t;
    for(let i=0; i<2; i++) {
      checkTime(t);
      [t, dt] = moveSpaceObjects({t0: t, dt, dtStandard, iterations:1});
      if(!rakete.exploded && rakete.checkCollisions()) {
        rakete.explode();
        setTimeout(_=>reset(), 1000);
      }
      
    }
    if(t >= 86400*window.params.date + 3600*window.params.hours + 60*window.params.minutes + 15*86400) reset();
    if(Math.abs(rakete.x) > world.maxUnits.x || Math.abs(rakete.y) > world.maxUnits.y) reset();
    
    if(t >= 86400*window.params.date + 3600*window.params.hours + 60*window.params.minutes + 3*86400 &&
      Math.hypot(rakete.x-erde.x, rakete.y-erde.y) < erde.wUnits * 2) {
      if(!done) {
        done=true;
        highscores.setHighscore("freereturn", 1);
        highscores.success(`Your crew returns to earth!`, index => {
          running= false;
          if(index===0) location.href = "index.html";
          if(index===1) location.reload();
          if(index===2) reset();
        }, true);
      }
    }
    
    window.spaceObjects.forEach(obj=>drawSpaceObj(obj));
  
//    rakete.a_max = 0.1 + 1.2 * G * erde.m / (rakete.x**2 + rakete.y**2);
//    rakete.sparkle(t-t0);
//    rakete.rotate(t-t0);
    
    kalender.text = Math.abs(t/86400).toFixed(0) + " days";
    app.render();
    
    //speed.textContent = (Math.hypot(rakete.vx, rakete.vy) / 1000).toFixed(2);
    
    if(running) nextStep(loop);
  }
})();
