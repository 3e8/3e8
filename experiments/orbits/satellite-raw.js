import {PIXI, World, Actor, preloadImages} from "./pixi-helpers";

(async _ => {
  
  
  var nextStep = (function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(t){window.setTimeout(t,1e3/60);};})();
  
  
  let resources = await preloadImages(["../img/planets/erde.png", "../img/planets/16/rakete.png"]);
  console.log(resources);
  
  let world = new World({bgColor: 0x000000, hUnits: 84000000});
  
  var G = 6.67e-11; // in kgm^2kg^-2
  
  let a = 15e6 + Math.random()*25e6;
  let f = (a - 8000000) * Math.random();
  let difficultyscale = 0.8;
  let b = Math.sqrt(a*a-f*f);
  let rot = 2*Math.PI*Math.random();
  let orbit = new PIXI.Container();
  orbit.position = {x: world.xToPx(0), y: world.yToPx(0)};
  orbit.pivot = {x: -world.pxPerUnit*f, y: 0};
  orbit.rotation = rot;
  orbit.cacheAsBitmap = true;
  world.stage.addChild(orbit);
  var orbitouter = new PIXI.Graphics();
  var orbitinner = new PIXI.Graphics();
  orbitouter.lineStyle(2, 0x66ff44, 1);
  orbitinner.lineStyle(2, 0x66ff44, 1);
  orbitouter.beginFill(0x66ff44, 0.25);
  orbitinner.beginFill(0x000000, 1);
  //orbitmiddle.drawEllipse(0,0,world.pxPerUnit*a,world.pxPerUnit*b);
  orbitouter.drawEllipse(-(1-1/difficultyscale)*world.pxPerUnit*f,0,world.pxPerUnit/difficultyscale*a,world.pxPerUnit/difficultyscale*b);
  orbitinner.drawEllipse(-(1-difficultyscale)*world.pxPerUnit*f,0,world.pxPerUnit*difficultyscale*a,world.pxPerUnit*difficultyscale*b);
  orbitouter.hitArea = new PIXI.Ellipse(-(1-1/difficultyscale)*world.pxPerUnit*f,0,world.pxPerUnit/difficultyscale*a,world.pxPerUnit/difficultyscale*b);
  orbitinner.hitArea = new PIXI.Ellipse(-(1-difficultyscale)*world.pxPerUnit*f,0,world.pxPerUnit*difficultyscale*a,world.pxPerUnit*difficultyscale*b);
  orbit.addChild(orbitouter);
  orbit.addChild(orbitinner);
  
  world.render();
  
//  orbit.interactive = true;
//  orbit.on("mousemove", e=>{
//    let {x,y} = orbit.worldTransform.applyInverse(e.data.global);
//  });
  
  function toScreen(pos) {
    return world.unitsToPx(pos)
  }
  
  function fromScreen(pos) {
    return world.pxToUnits(pos)
  }
  
  //move To World
  var kalender = new PIXI.Text(0 + " Tage", {fontFamily: "Tahoma", fontSize: "20px", fontStyle: "bold", fill:"white"});
  kalender.position.x = world.wPx*0.3;
  kalender.position.y = 20;
  world.stage.addChild(kalender);
  
  const sqrt = Math.sqrt;
  
  var keydowncheck = function(e) {
    if(e.key==="Control") {schub = true;}
    if(e.key==="ArrowLeft") {leftarrow = true;}
    if(e.key==="ArrowRight") {rightarrow = true;}
    if(e.code==="KeyT") {trace = !trace; background.clearRect(0,0,maxWidth, maxHeight);}
  };
  
  var keyupcheck = function(e) {
    if(e.key==="Control") {schub = false;}
    if(e.key==="ArrowLeft") {leftarrow = false;}
    if(e.key==="ArrowRight") {rightarrow = false;}
  }
  
  window.addEventListener("keydown", keydowncheck, false);
  window.addEventListener("keyup", keyupcheck, false);
  
  let erde = new Actor({img: "../img/planets/erde.png", world, hUnits: 2*6371000});
  Object.assign(erde, {
    name: "erde",
    m: 5.97e24,
    vx:       0,
    vy:       0,
    ax:       0,
    ay:       0,
  });
  
  let rakete = new Actor({
    img: "../img/planets/16/rakete.png",
    world,
    wUnits: 2000000,
    x: 6371000+10000000,
    y:0
  });
  world.render();
  Object.assign(rakete, {
    name:     "rakete",
    vx:       0.1,
    vy:       0.1,
    ax:       0,
    ay:       0,
    m:        1,
    a_max:    10,
    rotspeed: 0.002,
    trace:    "#990000",
    lage:     1.2, //Ausrichtungswinkel
    rotate:   function(dt) {
      if(leftarrow) {this.lage -= this.rotspeed * dt;}
      if(rightarrow) {this.lage += this.rotspeed * dt;}
      this.sprite.rotation = this.lage;
    }
  });
  
  
  let v_med = Math.sqrt(G*erde.m/a);
  let v_aphel = v_med * b/(a+f);
  let v_perihel = v_med * b/(a-f);
  let cosr = Math.cos(rot), sinr = Math.sin(rot)
  let helpers = [];
  for(let i=0; i<4; i++) {
    helpers[i] = new Actor({
      img: "../img/planets/16/rakete.png",
      world,
      wUnits: 2000000,
      x: [ cosr * (a+f), cosr*f - sinr*b, -cosr * (a-f), cosr*f + sinr*b][i],
      y: [-sinr * (a+f), -sinr*f - cosr*b, sinr * (a-f), -sinr*f + cosr*b][i]
    });
    world.render();
    Object.assign(helpers[i], {
      name:     "helper",
      vx: [sinr * v_aphel, cosr * v_med, -sinr * v_perihel, -cosr*v_med][i],
      vy: [cosr * v_aphel, -sinr *v_med, -cosr * v_perihel, sinr*v_med][i],
      ax:       0,
      ay:       0,
      m:        1,
    });
  }

  
  
  // SPARKS
  var sparks = [];
  var sparkimage = new PIXI.Texture.fromImage("../img/spark1.png");
  function Spark(offset) {
    this.sprite = new PIXI.Sprite(sparkimage);
    this.sprite.anchor = {x: 0.5, y: 0.5};
    var sparkleSpeed = rakete.a_max * dt * 12;
    var randomOffset = sparkleSpeed + Math.random() * sparkleSpeed + (4 - Math.abs(offset.quer)) * 4 * sparkleSpeed;
    this.x = rakete.x + (offset.quer*Math.sin(rakete.lage) - offset.laengs*Math.cos(rakete.lage))/massstab;
    this.y = rakete.y + (offset.quer*Math.cos(rakete.lage) + offset.laengs*Math.sin(rakete.lage))/massstab;
    this.vx = rakete.vx  - Math.cos(rakete.lage) * randomOffset;
    this.vy = rakete.vy  + Math.sin(rakete.lage) * randomOffset;
    this.age = 0;
    this.sprite.position = toScreen(this);
    world.stage.addChild(this.sprite);
  }
  function sparkle(dt) {
    var i = 0;
    while(i<sparks.length) {
      sparks[i].age += 1;
      sparks[i].sprite.alpha -= 0.1;
      sparks[i].x += sparks[i].vx * dt;
      sparks[i].y += sparks[i].vy * dt;
      sparks[i].sprite.position = toScreen(sparks[i]);
      if (sparks[i].age >= 10) {
        world.stage.removeChild(sparks[i].sprite);
        sparks.splice(i,1);
      }
      else i++;
    }
    if(schub) {
      for(i=0; i<4; i++) {
        sparks.push(new Spark({laengs: 7, quer: 3-6*Math.random()}));
      }
    }
  }
  
  
  function zeichne(obj) {
    if(trace) {
      background.strokeStyle = obj.trace;
      background.beginPath();
      background.moveTo(obj.sprite.position.x, obj.sprite.position.y);
      obj.sprite.position = toScreen(obj);
      background.lineTo(obj.sprite.position.x, obj.sprite.position.y);
      background.stroke();
    }
    else {
      obj.sprite.position = toScreen(obj);
    }
  }
 
  
  function getAcceleration(planet) {
    var me = planet.tempvars;
    me.ax = 0;
    me.ay = 0;
    for(let j in planets) {
      if(planet !== planets[j] && planets[j].m > 1) {
        let other =  planets[j].tempvars;
        let r = sqrt( (me.x-other.x)**2 + (me.y-other.y)**2 /*+ 1e17*/);
        me.ax += -(me.x-other.x)*G*planets[j].m/(r*r*r);
        me.ay += -(me.y-other.y)*G*planets[j].m/(r*r*r);
        //Schrittweitenanpassung:
        TD = Math.min(TD, 4*Math.PI**2*r*r*r/(G*planets[j].m));
      }
    }
    
    //Rakete zusätzlich beschleunigen
    if (schub && planet === rakete) {
      me.ax += rakete.a_max * Math.cos(rakete.lage);
      me.ay -= rakete.a_max * Math.sin(rakete.lage);
    }
  }
  
  function k(planet) {
    getAcceleration(planet);
    return {
      x:  dt * planet.tempvars.vx,
      y:  dt * planet.tempvars.vy,
      vx: dt * planet.tempvars.ax,
      vy: dt * planet.tempvars.ay
    }
  }
  
  function getTempvars(planet, kstep, factor) {
    planet.tempvars.x  = planet.x  + factor * kstep.x;
    planet.tempvars.y  = planet.y  + factor * kstep.y;
    planet.tempvars.vx = planet.vx + factor * kstep.vx;
    planet.tempvars.vy = planet.vy + factor * kstep.vy;
  }
  
  function rungeKuttaMotion() {
    
    for(let i in planets) {
      planets[i].tempvars = {
        x: planets[i].x,
        y: planets[i].y,
        vx: planets[i].vx,
        vy: planets[i].vy
      }
    }
    
    for(let i in planets) {  planets[i].k1 = k(planets[i]); }
    for(let i in planets) {  getTempvars(planets[i], planets[i].k1, 0.5); }
    for(let i in planets) {  planets[i].k2 = k(planets[i]); }
    for(let i in planets) {  getTempvars(planets[i], planets[i].k2, 0.5); }
    for(let i in planets) {  planets[i].k3 = k(planets[i]); }
    for(let i in planets) {  getTempvars(planets[i], planets[i].k3, 1); }
    for(let i in planets) {  planets[i].k4 = k(planets[i]); }
    
    for(let i in planets) {
      let p = planets[i];
      p.x  += ( p.k1.x  + 2 * p.k2.x  + 2 * p.k3.x  + p.k4.x ) / 6;
      p.y  += ( p.k1.y  + 2 * p.k2.y  + 2 * p.k3.y  + p.k4.y ) / 6;
      p.vx += ( p.k1.vx + 2 * p.k2.vx + 2 * p.k3.vx + p.k4.vx) / 6;
      p.vy += ( p.k1.vy + 2 * p.k2.vy + 2 * p.k3.vy + p.k4.vy) / 6;
      
      zeichne(planets[i]);
    }
    
    //Schrittweitenanpassung:
    dt = Math.min(dtStandard, 0.03*sqrt(TD));
    TD = Infinity;
    //if(dt<dtStandard) {console.log("Schrittweite reduziert: " + dt);}
  }
  
  var addPlanets = false;
  var trace = false;
  var schub = false;
  var leftarrow = false;
  var rightarrow = false;
  var TD = Infinity;

//SIM
  

  var t=0; // in Sekunden
  var dtStandard = 4; //Zeitschritt der Simulation
  var dt = dtStandard;
  var iterations = 8; // Anzahl Zeitschritte, die pro Bild gemacht werden
  var massstab = world.pxPerUnit;  // in px pro Meter
  
  var planets = [erde, rakete, ...helpers];
  
  function loop() {
    
    var t0 = t;
    
    //@todo check adjust rocket acceleration
    rakete.a_max = 3 + 1.2 * G * erde.m / (rakete.x**2 + rakete.y**2);
    
    while(t < t0 + iterations * dtStandard ) {
      t += dt;
      rungeKuttaMotion();
    }
    
    kalender.text = (t/60).toFixed(0) + " Minuten";
    sparkle(t-t0);
    rakete.rotate(t-t0);
    world.render();
    nextStep(loop);
  }
  
  loop();
  
})();
