import {preloadImages} from "./pixi-helpers";
import {createTracer, createWorld, createTime, addRocketControlHandlers, createSpaceObject, moveSpaceObjects, drawSpaceObj, orbithelpers} from "./orbithelpers-es6";
import highscores from "./highscores";

(async _=>{
  await preloadImages(["../img/planets/16/rakete.png"]);
  
  let tracer = createTracer();
  
  var {world, stage, app} = createWorld({wPx: maxWidth, hPx: maxHeight, wUnits: 1e11, unit: "m", transparent: true, coords: {label: "AE", multiplier: 1.5e11, labelstep: 0.1}});
  
  var kalender = createTime({unit: "Tage"});

//addRocketControlHandlers();
  
  const yHide = 2e11;
  
  function addHandlers() {
    window.params = {};
    document.querySelectorAll("[data-key]").forEach(el=>{
      let setValue = _ => {
        let key = el.dataset.key;
        //document.querySelectorAll(`[data-key="${key}"]`).forEach(n=>n.value = +el.value);
        window.params[key] = el.value;
      };
      setValue();
      el.addEventListener("change", _=>setValue(el));
      //el.addEventListener("input", _=>setValue(el));
    });
//    window.params.date = document.querySelector("[data-key='date']").value*86400;
//    document.querySelector("[data-key='date']").addEventListener("input", e=>{
//      window.params.date = +e.target.value*86400
//    });
//    window.params.hours = document.querySelector(".time").value*86400;
//    document.querySelector(".time").addEventListener("input", e=>{
//      window.params.time = +e.target.value.split(":")[0]*3600 + e.target.value.split(":")[1]*60
//    });
    document.querySelector(".run").addEventListener("click", e=>{
      reset();
      if(!running) {
        running=true;
        loop();
      }
    })
  }
  addHandlers();
  
  const ySun = -1.5e11;
  let sonne = createSpaceObject({
    name: "Sun",
    m: 1.99e30, // in kg
    x: 0,
    y: ySun,
    wUnits: 1e9,
    useCircle: {color: 0xffeedd, r:12},
  });
  
  const v = r => Math.sqrt(G*sonne.m/r);
  
  let erde = createSpaceObject({
    m: 1e26,
    y: yHide,
    wUnits: 9e8,
    useCircle: {r: 16, color: 0x8888ff}
  });
  
  let mars = createSpaceObject({
    name: "mars",
    m: 2e27,
    y: yHide,
    rCol: 3e8,
    wUnits: 6e8,
    useCircle: {r: 16, color: 0xff8833}
  });
  
  let rakete = createSpaceObject({
    name: "rakete",
    img: "../img/planets/16/rakete.png",
    y: yHide,
    wUnits: 1e9,
    m: 1000,
    trace: false,
    lage: -Math.PI/2,
    autorotate: true
  });
  
  let tstart = -3600*Math.floor(Math.random()*24*3);
  
  function reset() {
    t = tstart;
    Object.assign(sonne, {
      x: 0,
      y: ySun,
      vx: 0,
      vy: 0,
    });
    let r1 = 1.4e11;
    let phi0 = 25 / 180 * Math.PI;
    let [cosp, sinp] = [Math.cos(phi0), Math.sin(phi0)];
    Object.assign(erde, {
      x: sinp * r1,
      y: ySun + cosp * r1,
      vx: -v(r1) * cosp,
      vy: v(r1) * sinp,
    });
    r1 = 1.6e11;
    phi0 = 21 / 180 * Math.PI;
    [cosp, sinp] = [Math.cos(phi0), Math.sin(phi0)];
    Object.assign(mars, {
      x: sinp * r1,
      y: ySun + cosp * r1,
      vx: -v(r1) * cosp,
      vy: v(r1) * sinp,
    });
    Object.assign(rakete, {
      trace: false,
      started: false,
      vx: 0,
      vy: 0,
    });
    if(rakete.exploded) {
      rakete.exploded = false;
      Object.assign(rakete.sprite, rakete.originalsprite);
    }
  }
  
  var running = false;
  var t = tstart;
  var dtStandard = 60;
  var dt = dtStandard;
  var vmax = 0;
  var done = false;

  function checkTime(t) {
    if(!rakete.started) {
      let a = Math.atan2(erde.vy, erde.vx);
      rakete.x = erde.x + Math.sin(a) * 7e8;
      rakete.y = erde.y - Math.cos(a) * 7e8;
      rakete.sprite.rotation = -a+Math.PI/2;
      rakete.vx = rakete.vy = 0;
      if(t >= 86400*window.params.date + 3600*window.params.hours + 60*window.params.minutes) {
        rakete.vx = erde.vx + 0.4*erde.vy;
        rakete.vy = erde.vy - 0.4*erde.vx;
        rakete.x += rakete.vx * dtStandard;
        rakete.y += rakete.vy * dtStandard;
        rakete.sprite.position = world.unitsToPx(rakete);
        rakete.trace="#770000";
        rakete.started = true;
        app.render();
      }
    }
  }
  
  const speed = document.querySelector(".speed");
  
  function loop() {
    for(let i=0; i<60; i++) {
      checkTime(t);
      [t, dt] = moveSpaceObjects({t0: t, dt, dtStandard, iterations:1});
      if(!rakete.exploded && rakete.checkCollisions()) {
        rakete.explode();
        running=false;
      }
    }
    window.spaceObjects.forEach(obj=>drawSpaceObj(obj));
   
    kalender.text = (t/86400+1).toFixed(0) + " days";
    app.render();
    
    let vnow = Math.hypot(rakete.vx, rakete.vy) / 1000;
    speed.textContent = (vnow).toFixed(1);
    
    if(vnow > vmax) vmax = vnow;
  
    if(rakete.x < world.minUnits.x && !rakete.exploded && vmax >= 40) {
      if(!done) {
        done=true;
        highscores.setHighscore("slingshot", +vmax.toFixed(1));
        highscores.success(`Your slingshot maneuvre increased the spacecraft's energy!`, index => {
          if(index===0) location.href = "index.html";
          //if(index===1) location.reload();
          done=false;
          running=false;
          reset();
        }, false);
      }
    }
    
    if(running) nextStep(loop);
  }
})();
