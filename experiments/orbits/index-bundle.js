/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 395);
/******/ })
/************************************************************************/
/******/ ({

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return on; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return makeNode; });
/* unused harmony export makeNodes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return empty; });
function on(elSelector, eventName, selector, fn) {
  let elements = document.querySelectorAll(elSelector);
  
  for (let i = 0; i < elements.length; ++i) {
    let element = elements[i];
    element.addEventListener(eventName, function(event) {
      let possibleTargets = element.querySelectorAll(selector);
      let target = event.target;
    
      for(let j = 0, l = possibleTargets.length; j < l; j++) {
        let el = target;
        let p = possibleTargets[j];
        while(el && el !== element) {
          if(el === p) {
            return fn.call(p, event);
          }
          el = el.parentNode;
        }
      }
    });
  }
}

function getPosition(el) {
  let xPos = 0;
  let yPos = 0;
  
  while (el) {
    if (el.tagName === "BODY") {
      // deal with browser quirks with body/window/document and page scroll
      var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
      var yScroll = el.scrollTop || document.documentElement.scrollTop;
      
      xPos += (el.offsetLeft - xScroll + el.clientLeft);
      yPos += (el.offsetTop - yScroll + el.clientTop);
    } else {
      // for all other non-BODY elements
      xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
      yPos += (el.offsetTop - el.scrollTop + el.clientTop);
    }
    
    el = el.offsetParent;
  }
  return {
    x: xPos,
    y: yPos
  };
}

/**
 * @param {String} html representing a single element
 * @return {Node}
 */
function makeNode(html) {
  var template = document.createElement('template');
  template.innerHTML = html;
  return template.content.firstChild;
}

/**
 * @param {String} html representing any number of sibling elements
 * @return {NodeList}
 */
function makeNodes(html) {
  var template = document.createElement('template');
  template.innerHTML = html;
  return template.content.childNodes;
}

/**
 *
 * @param {Element} node
 * @returns {Element}
 */
function empty(node) {
  node.innerHTML = "";
  return node;
}




/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_modules_itemstore__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_modules_messages__ = __webpack_require__(42);



var store = new __WEBPACK_IMPORTED_MODULE_0_modules_itemstore__["a" /* Store */]("orbitscores", {
  dbcentral: "mydb/db", // "/db" wird entfernt, Rest sollte der Pfad zum Ordner sein
  dbname: "main",
  leavesonly: true,
  longpolling: false,
  keysByType: {}
});

function setHighscore(mission, score, code) {
  store.dDb.addItem("score", {name: localStorage.orbitname, mission, score}).then(r=>console.log(r));
}

function success(msg, callback, canContinue = false) {
  __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_modules_messages__["b" /* choosebox */])(`<div>Success!</div><div>${msg}</div>`, ["Back to Mission control", "Try again"].concat(canContinue?["Continue"]:[]), callback)
}

async function getMyScores() {
  await store.waitUntilReady();
  let name = localStorage.orbitname;
  return store.getLeavesAsArray().filter(s=>s.name === name);
}

async function getHighScores() {
  await store.waitUntilReady();
  console.log(1);
  return store.getLeavesAsArray();
}

function mapToStars({mission, score}) {
  if(mission==="parking") return +score < 12 ? 5 : score < 18 ? 4 : 3;
  if(mission==="asteroids") return +score < 25 ? 5 : score < 40 ? 4 : 3;
  if(mission==="circular") return +score < 1600 ? 5 : score < 3000 ? 4 : 3;
  if(mission==="orbit") return +score < 1600 ? 5 : score < 3000 ? 4 : 3;
  if(mission==="slingshot") return +score > 50 ? 5 : score > 45 ? 4 : 3;
  if(mission==="freereturn") return 5;
}

/* harmony default export */ __webpack_exports__["a"] = ({setHighscore, success, getMyScores, mapToStars, getHighScores});

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__highscores__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_modules_domhelpers__ = __webpack_require__(101);



var missions = ["parking", "asteroids", "circular", "orbit", "slingshot", "freereturn"];

var main = document.querySelector(".main");

document.querySelector(".entername").addEventListener("click", setname);

function setname() {
  let name = document.querySelector(".name").value;
  if(name) {
    localStorage.orbitname = name;
    //noinspection JSIgnoredPromiseFromCall
    start();
  }
}

if(localStorage.orbitname) {
  //noinspection JSIgnoredPromiseFromCall
  start();
}

async function start() {
  document.body.classList.remove("noname");
  let s = await __WEBPACK_IMPORTED_MODULE_0__highscores__["a" /* default */].getMyScores();
  missions.forEach(m=>{
    let mission = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_modules_domhelpers__["c" /* makeNode */])(`<div><a class="missionlink" href="${m}.html">${m}</a><span class="stars">${"&#x2605;".repeat(s.filter(score=>score.mission===m).map(__WEBPACK_IMPORTED_MODULE_0__highscores__["a" /* default */].mapToStars).sort().last())}</span></div>`);
    main.appendChild(mission);
  });
  window.highscores = await __WEBPACK_IMPORTED_MODULE_0__highscores__["a" /* default */].getHighScores();
}


/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return msg; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return choosebox; });
const msg = (html) => {
  let div = document.createElement("div");
  let overlay = document.createElement("div");
  overlay.classList.add("overlay");
  div.classList.add("message");
  div.innerHTML = html;
  document.body.appendChild(overlay);
  document.body.appendChild(div);
  let removeMe = e=>{
    document.body.removeChild(div);
    document.body.removeChild(overlay);
  };
  overlay.addEventListener("click", removeMe);
  div.addEventListener("click", removeMe);
};

const choosebox = (html, options, callback) => {
  var div = document.createElement("div");
  var overlay = document.createElement("div");
  var btn = document.createElement("button");
  overlay.classList.add("overlay");
  div.classList.add("message");
  div.classList.add("choosebox");
  let chooseMe = (i) => {
    document.body.removeChild(div);
    document.body.removeChild(overlay);
    callback(i);
  };
  div.innerHTML = html;
  options.forEach((o,i)=>{
    let btn = document.createElement("button");
    btn.classList.add("choosebox__option");
    btn.addEventListener("click", e=>chooseMe(i));
    btn.innerHTML = o;
    div.appendChild(btn);
  });
  document.body.appendChild(overlay);
  document.body.appendChild(div);
};

var css = `
.overlay {
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0%;
  left: 0%;
  background-color: #888;
  opacity: 0.2;
}

.message {
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 70vw;
  padding: 2em;
  background-color: white;
  opacity: 1;
}

.choosebox__option {
  display: block;
  width: 100%;
  background-color: #ddd;
  border: none;
  padding: 0;
  margin: 0.3em 0;
  text-align: left;
  cursor: pointer;
}

.choosebox__option:hover {
  color: #003366;
  background-color: #ccc;
}
`;
var style = document.createElement('style');
style.type = 'text/css';
if (style.styleSheet){
  style.styleSheet.cssText = css;
} else {
  style.appendChild(document.createTextNode(css));
}

document.head.appendChild(style);




/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return post; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return postCors; });
/**
 *
 * @param url
 * @param data
 * @returns {Promise}
 */
function post(url, data) {
  return window.fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  .then(checkStatus)
  .then(extractContent)
}

/**
 *
 * @param url
 * @param data
 * @returns {Promise}
 */
function postCors(url, data) {
  return window.fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, text/json, */*',
      'Content-Type': 'application/x-www-form-urlencoded' // instead of application/json because of cors
    },
    body: JSON.stringify(data)
  })
  .then(checkStatus)
  .then(extractContent)
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response);
  } else {
    console.log(response);
    return Promise.reject(new Error("invalid status text: " + response.statusText))
  }
}

function extractContent(r) {
  var ct = r.headers.get("content-type");
  if(~ct.indexOf("json") || ~ct.indexOf("x-www-form-urlencoded")) return r.json();
  if(~ct.indexOf("text")) return r.text();
  return r;
}




/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Store; });
/* unused harmony export Item */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_modules_messages__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_modules_helpers__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_modules_helpers___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_modules_extendNatives__ = __webpack_require__(99);
//let Immutable = require("Immutable");
//let store = Immutable.fromJS({a:1, b: [1,2,3]});







let defaultSettings = {
  dbcentral: "mydb/db", //can also be "MEMORY"
  dbname: "main",
  leavesonly: false,
  longpolling: true,
  keysByType: {},
  logActions: true
};

let defaultGlobal = {
  globalHistory: {show: false, undoneIdvs: [], redoneIdvs: [], maxItems: 10}
};

class Store {
  constructor(name, settings = {}) {
    this.name = name;

    this.settings = Object.assign({}, defaultSettings, settings);
    this.namedStates = {};
    this.namedStates.global = Object.assign({}, defaultGlobal, JSON.parse(localStorage[this.name + "_state_global"] || "{}")); //loadStates;
    this.dbStates = {};

    this.createDispatchers();
    this.ready = false;
    this.fromUndo = false;
    this.channels = {}; //ready, stateChange, dbChange, instantchange, ...
    this.start();
  }
  get undoneIdvs() {return this.namedStates.global.globalHistory.undoneIdvs}
  get redoneIdvs() {return this.namedStates.global.globalHistory.redoneIdvs}
  subscribe(channel, target, subchannel = "", methodname) {
    if(!this.channels[channel]) this.channels[channel] = [];
    let mn = methodname || ("on"+channel.slice(0,1).toUpperCase()+channel.slice(1));
    let listener = {target, subchannel, methodname: mn};
    this.channels[channel].push(listener);
    return listener;
  }
  publish(channel, msg, subchannel = "") {
    if(!this.channels[channel]) return;
    this.channels[channel].forEach(listener => {
      if(listener.target && listener.subchannel === subchannel) {
        typeof listener.target === "function" ? listener.target(msg) : listener.target[listener.methodname](msg);
      }
    });
  }
  unsubscribe(targetOrListener) {
    for(let i in this.channels) {
      this.channels[i] = this.channels[i].filter(l => l.target !== targetOrListener && l !== targetOrListener);
    }
  }
  start() {
    return this.loadItemsFromScratch()
    .then(o=>{
      this.ready = true;
      this.publish("ready");
      document.body.addEventListener("keydown", e => {
        if(e.which === 90 && e.shiftKey && e.ctrlKey) {
          this.dNs.toggle("global", "globalHistory.show");
        }
      });
    })
    .then(()=>this.settings.longpolling ? this.poll() : false)
    .catch(e=>__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["warn"])(e));
  }
  loadItemsFromScratch() {
    if(this.settings.dbcentral === "MEMORY") {
      this.handleItems.init();
      return Promise.resolve("initialized from Memory");
    }
    return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__["a" /* post */])(this.settings.dbcentral, {dbname: this.settings.dbname, task: this.settings.leavesonly ? "getLeaves" : "getAll"})
    .then(o=> {
      this.handleItems.init();
      this.handleItems.addItems(Object.keys(o).map(idv => (new Item(idv, JSON.parse(o[idv])) )));
      //use dispatcher?
      this.settings.keysByType = Object.assign(this.settings.keysByType, this.getKeysByType(this.getLeavesAsArray()));
    }).catch(e=>__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["warn"])(e));
  }
  poll() {
    if(this.settings.dbcentral === "MEMORY") return;
    if(document.hidden) {
      setTimeout(()=>this.poll(), 1000);
    }
    else {
      __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])("polling...");
      __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__["a" /* post */])(this.settings.dbcentral, {dbname: this.settings.dbname, task: "pollForChanges", since: this.dbStates.latest}).then(r=>{
        if(!this.willunmount) {
          if(r === "nochanges") {
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])("nochanges");
          }
          else {
            let external = Object.keys(r).filter(newv => !this.dbStates.versions.some(v=>v.idv === newv));
            let newVersions = external.map(idv => (new Item(idv, JSON.parse(r[idv])) ));
            if(newVersions.length) {
              this.dDb.gotRemoteItems(newVersions);
            }
          }
          setTimeout(()=>this.poll(), 1000);
        }
      }).catch(e=>{__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["warn"])(e); setTimeout(()=>this.poll(), 1000);});
    }
  }
  waitUntilReady() {
    return new Promise((resolve, reject)=>{
      if(this.ready) resolve(this);
      else this.subscribe("ready", _ => resolve(this) );
    });
  }
  moveOldToArchive() {
    if(this.settings.dbcentral === "MEMORY") {
      return this.loadItemsFromScratch();
    }
    let p = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__["a" /* post */])(this.settings.dbcentral, {dbname: this.settings.dbname, task: "moveOldToArchive"}).then(r=>__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(r));
    return p.then(_=>this.loadItemsFromScratch());
  }
  createDispatchers() {
    this.dispatchSettings = {
      //this.eventEmitter.dispatchEvent(new CustomEvent("ready", {detail: "ready"}));
    };
    this.dispatchNamed = {
      mountShelf: (shelfname, values) => {
        //loadStates
        let shelfstate = Object.assign({}, values, JSON.parse(localStorage[this.name+"_state_"+shelfname] || "{}")); //loadStates
        this.namedStates[shelfname] = shelfstate;
      },
      changeShelfStates: (shelfname, newStates) => {
        this.namedStates[shelfname] = Object.assign({},  this.namedStates[shelfname], newStates);
      },
      setShelfStateOfIdn: (shelfname, itemstate, idn, value) => {
        let oldItemstate = this.namedStates[shelfname][itemstate];
        let newItemstate = value === true ? oldItemstate.addUnique(idn) : oldItemstate.deleteWhere(i=>i === idn);
        this.namedStates[shelfname] = Object.assign({},  this.namedStates[shelfname], {[itemstate]: newItemstate});
      },
      toggleShelfStateOfIdn: (shelfname, itemstate, idn) => {
        this.dispatchNamed.setShelfStateOfIdn(shelfname, itemstate, idn, !this.namedStates[shelfname][itemstate].includes(idn));
      },
      toggle: (shelfname, pathArrayOrString) => {
        this.dispatchNamed.setValue(shelfname, pathArrayOrString, null, true);
      },
      setValue: (shelfname, pathArrayOrString, val, toggle=false) => {
        let old = this.namedStates[shelfname];
        this.namedStates[shelfname] = Object.assign({}, this.namedStates[shelfname]);
        let pathToVariable = this.namedStates[shelfname];
        let patharray = pathArrayOrString instanceof Array ? pathArrayOrString : pathArrayOrString.split(".");
        patharray.slice(0,-1).forEach(p => {
          pathToVariable = Object.assign({}, pathToVariable);
          pathToVariable = pathToVariable[p];
        });
        let nameOfVariable =  patharray.slice(-1)[0];
        pathToVariable[nameOfVariable] = toggle ? !pathToVariable[nameOfVariable] : val;
      }
    };
    this.dispatchDb = {
      addItem: (itemtype, newDoc) => {
        let idn = itemtype + "-" + this.getTimestamp() + "-" + this.getUsr();
        let idv = idn + "-" + this.lastTimestamp + "-" + this.getUsr() + ".json";
        return this.dispatchDb.addVersions([idv], [newDoc], [""], [""]).then(_=>idn);
      },
      debouncedUpdate: (idn, obj) => {
        if(Object.keys(obj).length !== 1) throw new Error("Debounced update must only change one key!");
        let key = Object.keys(obj)[0];
        let val = obj[key];
        let item = this.dbStates.leaves[idn];
        let getDebounceHandler = () => {
          let r = null;
          let t = null;
          let p = new Promise(resolve => {
            r = resolve;
            t = setTimeout(()=>resolve(this.dispatchDb.dispatchDebouncedUpdate(idn)), 2000);
          });
          return {promise: p, resolver: r, timeout: t};
        };
        if(item.debounced) {
          if(item.debounced.key === key) {
            clearTimeout(item.debounced.handler.timeout);
            this.handleItems.updateDebouncedIdn(idn, {val, handler: getDebounceHandler()});
          }
          else {
            this.dispatchDebouncedUpdate(idn).then(
              _ => item.dispatchDb.debouncedUpdate(idn, {[key]: val})
            );
          }
        }
        else {
          this.handleItems.updateDebouncedIdn(idn, {key, val, originalval: item[key], handler: getDebounceHandler()});
        }
      },
      dispatchDebouncedUpdate: (idn) => {
        let item = this.dbStates.leaves[idn];
        clearTimeout(item.debounced.handler.timeout);
        let {key, val, originalval, handler} = item.debounced;
        this.handleItems.updateDebouncedIdn(idn, "delete");
        let p;
        if(JSON.stringify(originalval) === JSON.stringify(val)) {
          __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])("changed back to original value, no update");
          p = Promise.resolve("changed back to original value, no update");
        }
        else {
          p = this.dispatchDb.updateItem(idn, {[key]: val});
        }
        handler.resolver(p);
        return p;
      },
      //Single item
      deleteItem:   (idn) => this.dispatchDb.deleteItems([idn]),
      undeleteItem: (idn) => this.dispatchDb.undeleteItems([idn]),
      updateItem: (idn, obj) => this.dispatchDb.updateItems([idn], [obj]),
      replaceItem: (idn, newDoc) => this.dispatchDb.replaceItems([idn], [newDoc]),
      //Array of items
      deleteItems:   (idns) => this.dispatchDb.replaceItems(idns, false, "delete"),
      undeleteItems: (idns) => this.dispatchDb.replaceItems(idns, false, "undelete"),
      updateItems: (idns, objs) => this.dispatchDb.replaceItems(idns, objs, "updateKeys"),
      replaceItems: (idns, newDocsOrObjs, task = "update") => {
        let prevItems = idns.map(idn => this.dbStates.leaves[idn]);
        if(prevItems.some(i=>i.debounced)) {
          return Promise.all(
            prevItems.filter(i=>i.debounced).map(i=>this.dispatchDb.dispatchDebouncedUpdate(i.idn))
          ).then(_=>this.dispatchDb.replaceItems(idns, newDocsOrObjs, task));
        }
        let timestamp = this.getTimestamp();
        let prevIdvs = prevItems.map(item => item.idv);  // ?? let versionsToChange = action.previousIdvs.map(idv=>n.versions.pick({idv}));
        let prevDocs = prevItems.map(item => item.doc);  // ?? let prevDocs = versionsToChange.map(v=>v.doc);
        let newDocs = newDocsOrObjs;
        if(!newDocs) newDocs = prevDocs;
        if(task === "updateKeys") newDocs = newDocsOrObjs.map((obj, index) => Object.assign({}, this.dbStates.leaves[idns[index]].doc, obj));
        if(task!=="undelete" && task!=="delete") {
          if(newDocs.every((doc, i)=>JSON.stringify(doc)===JSON.stringify(prevDocs[i]))) {
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])("no items changed");
            return "no items changed"
          }
        }
        let getNewFlags = (olditem, task) => {
          let newFlags = olditem.flags;
          if(task==="delete") newFlags = ~newFlags.indexOf("x") ? newFlags : "x"+newFlags;
          if(task==="undelete") newFlags = newFlags.replace(/x/ig, "");
         return newFlags ? "-"+newFlags : "";
        };
        let idvs = idns.map( (idn, index) => idn+ "-" + timestamp + "-" + this.getUsr() + getNewFlags(prevItems[index], task) + ".json");
        return this.dispatchDb.addVersions(idvs, newDocs, prevIdvs, prevDocs);
      },
      addVersions: (idvs, newDocs, prevIdvs, prevDocs, fromLocal=true) => {
        if(!fromLocal) {alert("Noch nicht implementiert: MultiUser")} //@TODO
        idvs.forEach((idv, index) => {
          let item = new Item(idv, newDocs[index]);
          let idn = item.idn;
          this.handleItems.addItems([item]);
          //this.handleItems.addValueToArray(idn, "dirtyIdns");
          this.handleItems.addValueToArray(idv, "pendingIdvs");
        });
        //UndoRedo //!differs from react version in dev folder!!
        this.dNs.setValue("global", "globalHistory", Object.assign({}, this.namedStates.global.globalHistory, {
          undoneIdvs: this.fromUndo ? this.undoneIdvs.concat(idvs) : this.undoneIdvs,
          redoneIdvs: this.fromRedo ? this.redoneIdvs.concat(idvs) : this.redoneIdvs,
        }));
        return this.dispatchDb.sendVersionsToDb(idvs, newDocs, prevIdvs, prevDocs);
      },
      sendVersionsToDb: (newIdvs, newDocs, prevIdvs, prevDocs) => {
        //at the moment, this returns a resolved promise with a nested dbprom (resolve after finished request)
        if(this.settings.dbcentral === "MEMORY") {
          return Promise.resolve({dbprom: Promise.resolve(this.dDb.dbResponse("updates ok!", newIdvs, newDocs, prevIdvs, prevDocs)), newIdvs});
        }
        let dbprom = new Promise(resolve=>{
          this.dbStates.updateQueue = this.dbStates.updateQueue.then(r=>{
            let p = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__["a" /* post */])(this.settings.dbcentral,
                {dbname: this.settings.dbname, task: "addUpdatedVersions", idvs: newIdvs, previousIdvs: prevIdvs, data: newDocs.map(newDoc=>JSON.stringify(newDoc))}
            ).then(r=>this.dDb.dbResponse(r, newIdvs, newDocs, prevIdvs, prevDocs));
            resolve(p);
          });
        });
        return Promise.resolve({dbprom, newIdvs});
      },
      dbResponse: (r, newIdvs, newDocs, prevIdvs, prevDocs) => {
        if(r === "updates ok!") {
          newIdvs.forEach(idv => {this.handleItems.removeValueFromArray(idv, "pendingIdvs")} );
          return newIdvs;
        }
        else if(r.indexOf("Konflikte mit Eintrag von anderswo:") === 0) { //@TODO
          console.groupCollapsed("Konflikte mit Eintrag von anderswo:");
          let conflictingWrap = JSON.parse(r.slice("Konflikte mit Eintrag von anderswo:".length));
          let newDocsNextTry = conflictingWrap.map((c, index) => {
            this.handleItems.removeItems([this.dbStates.versions.find(i=>i.idv==newIdvs[index])]); //delete also the pending even if no error
            if(!c) {
              return newDocs[index];
            }
            let conflicting = Object.keys(c)[0];
            let confl = JSON.parse(c[conflicting]);
            let conflictingItem = new Item(conflicting, confl);
            this.handleItems.addItems([conflictingItem]);
            let prev = prevDocs[index].clone();
            let self = newDocs[index].clone();
            let merged = prev.clone();
            let errors = [];
            for(let key in confl) {
              if(self[key]===undefined && prev[key]===undefined)      {merged[key] = confl[key]; __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Conflicting added new key ${key}`);}
              if(self[key] == prev[key] && prev[key] !== confl[key])  {merged[key] = confl[key]; __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Conflicting changed key ${key}, self didn't.`);}
              if(self[key] !== prev[key] && self[key] === confl[key]) {merged[key] = confl[key]; __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Both changed key ${key} to same value.`);}
              if(self[key] == prev[key] && prev[key] === confl[key])  {merged[key] = confl[key]; __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Both did not change key ${key}.`);}
            }
            for(let key in self) {
              if(confl[key]===undefined && prev[key]===undefined)     {merged[key] = self[key];  __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Self added new key ${key}`);}
              if(confl[key] == prev[key] && prev[key] !== self[key])  {merged[key] = self[key];  __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Self changed key ${key}, conflicting didn't.`);}
              if(confl[key] !== prev[key] && prev[key] !== self[key] && confl[key] !== self[key]) {merged = false; errors.push({key, self: self[key], confl: confl[key]}); __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Both changes key ${key} to different value.`); break;}
            }
            if(merged!==false) {
              __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])("item was merged with conflicting item:", merged);
              console.groupEnd();
              return merged;
            }
            else {
              console.groupEnd();
              __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_modules_messages__["a" /* msg */])("<h3>Aktion konnte nicht abgeschlossen werden!</h3><div>Eine andere Quelle hat den Eintrag ebenfalls geändert.</div>"
                  + errors.map(e=>`<div>Ihr Vorschlag für ${e.key}:</div><div>${e.self}</div><div>Externer Vorschlag:</div><div>${e.confl}</div>`)
              + "<div>Bitte beheben Sie den Konflikt.</div>");
              return "error";
            }
          });
          if(!newDocsNextTry.some(e=>e=="error")) {
            return this.dispatchDb.replaceItems(newIdvs.map(Item.getIdnFromIdv), newDocsNextTry);
          }
          else return false;
        }
      },
      gotRemoteItems(newVersions) {
        return newVersions.map(item => {
          if(this.dbStates.leaves[item.idn].debounced) __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["warn"])("check what happens if remote updates debounced");
          this.handleItems.addItems([item]);
          return item;
        });
      }
    };
    this.dDb = {};
    for(let key in this.dispatchDb) {
      this.dDb[key] = (...args) => {
        if(this.settings.logActions) {
          console.groupCollapsed(key);
          __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(args);
        }
        this.fromUndo = args.includes("undo"); //UndoRedo, differs from version in dev folder (react)
        this.fromRedo = args.includes("redo");
        setTimeout(() => {
          this.publish("dbChange", {action: key, args});
          this.settings.logActions && console.groupEnd();
        }, 0);
        return this.dispatchDb[key].apply(this, args);
      }
    }
    this.dNs = {};
    for(let key in this.dispatchNamed) {
      this.dNs[key] = (...args) => {
        if(this.settings.logActions) {
          console.groupCollapsed(key);
          __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(args);
        }
        let shelfname = args[0];
        let old = this.namedStates[shelfname] || {};
        setTimeout(()=>{
          //save states
          localStorage[this.name+"_state_"+shelfname] = JSON.stringify(this.namedStates[shelfname] || {});
          this.publish("stateChange", {action: key, old, current: this.namedStates[shelfname]}, shelfname); //subchannel shelfname
          this.settings.logActions && console.groupEnd();
        }, 0);
        return this.dispatchNamed[key].apply(this, args);
      }
    }
    // Helpers to make dbStates Immutable
    this.handleItems = {
      init: () => {
        this.dbStates = {
          versions: [],
          versiongroups: {},
          leaves: {},
          latest: "0",
          pendingIdvs: [],
          updateQueue: __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["newP"])()
        }
      },
      addItems: (items)=> {
        let a = this.dbStates;
        let dbs = this.dbStates = Object.assign({}, this.dbStates);
        items.forEach(i=> {
          let pos = dbs.versions.findIndex(v=>v.modified <= i.modified);
          pos = pos == -1 ? dbs.versions.length : pos;
          dbs.versions = [...dbs.versions.slice(0, pos), i, ...dbs.versions.slice(pos)];
          dbs.versiongroups = Object.assign({}, dbs.versiongroups);
          dbs.versiongroups[i.idn] = [i.idv].concat(dbs.versiongroups[i.idn]||[]);
          dbs.versiongroups[i.idn].sort((a,b)=>a>b?-1:a<b?+1:0);
          dbs.leaves = Object.assign({}, dbs.leaves, {[i.idn]: dbs.versions.find(v=>v.idn == i.idn)});
          dbs.latest = dbs.versions.length ? dbs.versions[0].modified : "0";
        });
      },
      removeItems: (items)=> {
        let dbs = this.dbStates = Object.assign({}, this.dbStates);
        items.forEach(i=> {
          dbs.versions = dbs.versions.filter(v=>v.idv !== i.idv);
          dbs.versiongroups = Object.assign({}, dbs.versiongroups);
          dbs.leaves = Object.assign({}, dbs.leaves);
          if(dbs.versiongroups[i.idn].length ==1) {
            delete dbs.versiongroups[i.idn];
            delete dbs.leaves[i.idn];
          }
          else {
            dbs.versiongroups[i.idn] = dbs.versiongroups[i.idn].filter(v=>v!==i.idv);
            dbs.leaves[i.idn] = dbs.versions.find(v=>v.idn == i.idn);
          }
          if(dbs.pendingIdvs.includes(i.idv)) {dbs.pendingIdvs = dbs.pendingIdvs.filter(idv=>idv!==i.idv);}
        });
      },
      updateDebouncedIdn: (idn, obj) =>{
        let item = this.dbStates.leaves[idn];
        let clonedItem = Object.assign(new Item(item.idv, item.doc), item);
        if(obj=="delete") {
          delete clonedItem.debounced;
        }
        else {
          clonedItem.debounced = Object.assign({}, clonedItem.debounced, obj);
        }
        this.handleItems.removeItems([item]);
        this.handleItems.addItems([clonedItem]);
        if(obj!=="delete") {
          this.publish("instantchange", {old: item, current: clonedItem}, clonedItem.idn);
        }
      },
      addValueToArray: (val, array) => {
        this.dbStates = Object.assign({}, this.dbStates, {[array]: this.dbStates[array].concat([val]).unique()});
      },
      removeValueFromArray: (val, array) => {
        this.dbStates = Object.assign({}, this.dbStates, {[array]: this.dbStates[array].filter(v=>v!==val)});
      }
    };
    this.isLeaf = v => this.dbStates.versiongroups[v.idn][0] == v.idv;
    this.getLeavesAsArray = () => this.dbStates.versions.filter(v=>!v.deleted && this.isLeaf(v));
    this.getTimestamp = (lastTimestamp) => {
      let time = new Date().getTime()+31536000000;
      let timestamp = time.toString(36);
      while(timestamp === this.lastTimestamp || timestamp <= this.dbStates.latest) {
        timestamp = (time+1).toString(36)
      }
      this.dbStates.latest = timestamp;
      this.lastTimestamp = timestamp;
      return timestamp;
    };
    this.getUsr = () => "l"; //@TODO
    this.getItem = (idn) => this.dbStates.leaves[idn];
    this.timestampToDate = (t) => new Date(parseInt(t, 36) - 31536000000);
  }
  getKeysByType(versions) {
    let keysByTypeFomProps = this.settings.keysByType || {};
    let keysByType = {};
    let types = versions.map(v=>v.type).unique();
    types.forEach(type=>{
      keysByTypeFomProps[type] = keysByTypeFomProps[type] || {};
      keysByType[type] = {};
      let itemsOfType = versions.filter(v=>v.type == type);
      let keys = Object.keys(keysByTypeFomProps[type]).concat(itemsOfType.map(v=>Object.keys(v.doc).join(",")).join(",").split(",")).unique(); //muss so kompliziert sein wegen Reihenfolge
      keys.forEach(key => {
        keysByTypeFomProps[type][key] = keysByTypeFomProps[type][key] || {};
        keysByType[type][key] = {
          key: key,
          isPublic: !~key.indexOf("_"),
          isNumeric: itemsOfType.map(v=>v.doc[key]).filter(x=>!!x).every(x=>parseFloat(x)==x)
        };
        keysByType[type][key] = Object.assign(keysByType[type][key], keysByTypeFomProps[type][key]);
      });
    });
    return keysByType;
  }
  
  //Undo, Redo Methods
  undo() {
    let v = this.getVersionToUndoOrRedo("undo");
    if(!v) return console.warn("no undo possible");
    this.revertVersion(v, "undo");
  }
  redo() {
    let v = this.getVersionToUndoOrRedo("redo");
    if(!v) return console.warn("no redo possible");
    this.revertVersion(v, "redo");
  };
  getVersionToUndoOrRedo(undoOrRedo = "undo") {
    let vs = this.dbStates.versions.slice(0).sort((u,v)=> u.modified > v.modified ? -1 : 1);
    let index = 0;
    let skippingstoDo = 0;
    let coll = undoOrRedo === "undo" ? this.undoneIdvs : this.redoneIdvs;
    while(index < vs.length && (coll.includes(vs[index].idv) || skippingstoDo > 0)) {
      skippingstoDo += (coll.includes(vs[index].idv) ? +2 : +0) - 1;
      index++;
    }
    let candidate = vs[index];
    return candidate && (undoOrRedo === "undo" || this.undoneIdvs.includes(candidate.idv)) ? candidate:  false;
  }
  revertVersion(v, undoOrRedo="undo") {
    let history = this.dbStates.versiongroups[v.idn];
    let prevIdv = history[history.indexOf(v.idv) + 1] || false;
    if(!prevIdv || this.dbStates.versions.find(i=>i.idv===prevIdv).deleted) {
      this.dDb.deleteItem(v.idn, undoOrRedo)
    }
    else if(v.deleted) {
      this.dDb.undeleteItem(v.idn, undoOrRedo)
    }
    else {
      this.dDb.updateItem(v.idn, this.dbStates.versions.find(i=>i.idv===prevIdv).doc, undoOrRedo)
    }
  }
  

  


  //Tree methods
  getPathOf(item) {
    return item.tree_parent ? this.getPathOf(this.dbStates.leaves[item.tree_parent]).concat([item.idn]) : [item.idn];
  }
  sortByTreePos(a,b) { //MUSS WOHL IN SHELF!!!
    let p1 = this.getPathOf(a).map(idn=>this.dbStates.leaves[idn].tree_pos);
    let p2 = this.getPathOf(b).map(idn=>this.dbStates.leaves[idn].tree_pos);
    let i = 0;
    while(p1[i] === p2[i]) i++;
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(a.nr, b.nr, p1, p2, (+p1[i]||0) > (+p2[i]||0) ? 1 : -1);
    return (+p1[i]||0) > (+p2[i]||0) ? 1 : -1;
  };
  findAll(relation, idnOrItem) {
    let item = typeof idnOrItem == "string" ? this.dbStates.leaves[idnOrItem] : idnOrItem;
    let candidates = this.getLeavesAsArray().filter(i=>i.type==item.type);
    let filterfunction;
    if(relation == "siblings")    filterfunction = i => i.tree_parent===item.tree_parent;
    if(relation == "kids")        filterfunction = i => i.tree_parent===item.idn;
    if(relation == "ancestors")   filterfunction = i => ~this.getPathOf(item).slice(0,-1).indexOf(i.idn);
    if(relation == "descendants") filterfunction = i => ~this.getPathOf(i).slice(0,-1).indexOf(item.idn);
    return candidates.filter(filterfunction).filter(i=>!i.deleted).sort((a,b)=>a.tree_pos - b.tree_pos).map(i=>i.idn);
  }
  isThatMy(relation, selfIdnOrItem, otherIdnOrItem) {
    return ~this.findAll(relation + "s", selfIdnOrItem, false).indexOf(otherIdnOrItem.idn || otherIdnOrItem);
  }
}

//ychoufe-12345678-l-98765432-l.json
//<type>-<created>-<modified>.<extension>
//<----------idv----------------------->
//<-----version------------->
//<----idn-------> (identifier)

class Item {
  constructor(idv, doc) {
    this.idv = idv;
    this.version   = idv.split(".")[0];
    this.extension = idv.split(".")[1];
    this.type     = this.version.split("-")[0];
    this.created  = this.version.split("-")[1];
    this.creator  = this.version.split("-")[2];
    this.modified = this.version.split("-")[3];
    this.modifier = this.version.split("-")[4];
    this.flags    = this.version.split("-")[5] || "";
    this.deleted  = this.flags.indexOf("x") > -1;
    this.idn      = this.type+"-"+this.created+"-"+this.creator;
    this.doc = doc;
    this.createGetSet();
  };
  createGetSet() {
    for(let prop in this.doc) {
      if(prop in Item.prototype) continue;
      Object.defineProperty(Item.prototype, prop, {
        get: function getProp() {return (this.debounced && this.debounced.key == prop) ? this.debounced.val : this.doc[prop];},
        set: function setProp(val) {this.doc[prop] = val;}
      });
    }
  };
  static getIdnFromIdv(idv) {return idv.split("-").slice(0,3).join("-");}
  get(prop) {
    let p = this[prop] !== undefined ? this[prop] : (this.doc[prop] !== undefined ? this.doc[prop] : undefined);
    if(this.debounced && this.debounced.key == prop) p = this.debounced.val;
    return (typeof p == "function") ? p.bind(this)() : p;
  };
  getPublicKeys() {
    return Object.keys(this.doc).filter(k=>!~k.indexOf("_"));
  }
  hasType(typegroup) {
    if(typegroup === "all") return true;
    if(typegroup === "deleted") return this.deleted === true;
    return this.type === typegroup;
  };
  //Tree Methods
  isTreeItem() {return this.tree_pos !== undefined;}
  getParent() {
    return this.tree_parent;
  }

}



/***/ }),

/***/ 98:
/***/ (function(module, exports) {

const _ = {};

/**
 *
 * @param array
 * @returns {*}
 */
_.last = function(array) {return array[array.length-1];};

_.log = console.log.bind(console);
_.warn = console.warn.bind(console);
_.newP = _=>Promise.resolve();

if(window) Object.assign(window, _);

module.exports = _;

//(function () {
//  var noop = function () { };
//  var methods = [
//    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
//    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
//    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
//    'timeStamp', 'trace', 'warn'
//  ];
//  var console = (window.console = window.console || {});
//  for(var i = methods.length - 1; i>=0; i--) {
//    console[methods[i]] = console[methods[i]] || noop; // Only stub undefined methods.
//  }
//
//  if (Function.prototype.bind && 0) {
//    window.log = Function.prototype.bind.call(console.log, console);
//  }
//  else {
//    window.log = function() {
//      if(!$(".log").length) $("body").append("<div class='log'></div>");
//      $(".log").empty().append("<p>"+arguments[0]+"</p>");
//      Function.prototype.apply.call(console.log, console, arguments);
//    };
//  }
//})();

/**

var self = function(s) {log(s); return s;}

function replaceUml(a) {
  if(typeof a !== "string") return a;
  a = a.toLowerCase();
  a = a.replace("ä", "a");
  a = a.replace("ö", "o");
  a = a.replace("ü", "u");
  a = a.replace("à ", "a");
  a = a.replace("é", "e");
  a = a.replace("è", "e");
  a = a.replace("ë", "e");
  a = a.replace("ï", "i");
  a = a.replace("ñ", "n");
  return a;
}

function htmlEscape(str) {
  return String(str)
  .replace(/&/g, '&amp;')
  .replace(/"/g, '&quot;')
  .replace(/'/g, '&#39;')
  .replace(/</g, '&lt;')
  .replace(/>/g, '&gt;');
}

function htmlUnescape(value){
  return String(value)
  .replace(/&quot;/g, '"')
  .replace(/&#39;/g, "'")
  .replace(/&lt;/g, '<')
  .replace(/&gt;/g, '>')
  .replace(/&amp;/g, '&');
}

function areShallowEqual(a, b) {
  return !Object.keys(a).concat(Object.keys(b)).some(k=> !(k in a) || !(k in b) || a[k] !== b[k]);
}

/*Lodash
array
    .diff
    .chunk
    .fill(value, [start=0], [end=array.length])
    .last
    .first
    .flatten([isDeep])
    .union
    .unique
    .groupBy([4.2, 6.1, 6.4], function(n) {return Math.floor(n);}); // → { '4': [4.2], '6': [6.1, 6.4] }
    .pluck(path)
    .sample([n=1]) // n elemente per Zufall auswählen
    .shuffle()
    .sortByAll
    .sortByOrder
    .curry
    .
    

// aliasing important functions into global namespace
var sin     = Math.sin;
var cos     = Math.cos;
var tan     = Math.tan;
var arctan  = Math.atan;
var atan2   = Math.atan2;
var arcsin  = Math.asin;
var arccos  = Math.acos;

var exp     = Math.exp;
var ln      = Math.log;
var log10   = Math.log10 || function(x) {return Math.log(x) / Math.LN10;}
var pow     = Math.pow;
var sqrt    = Math.sqrt;
var cbrt    = Math.cbrt;

var abs     = Math.abs;
var round   = Math.round;
var ceil    = Math.ceil;
var floor   = Math.floor;
var max     = Math.max;
var min     = Math.min;

var sign    = Math.sign || function(x) {if(typeof x !== "number") {return Math.NaN;} return x>0?1:x<0?-1:0;};

var rand    = Math.random;

var hypot   = Math.hypot || function(a,b) {return sqrt(a*a+b*b);};
var cath    = function(c,b) {return sqrt(c*c-b*b);};

var dist    = function(p1, p2) {hypot(p2.x-p1.x, p2.y-p1.y);};

var constrain = function(value, min, max) {
  if(value < Math.min(min, max)) return Math.min(min, max);
  if(value > Math.max(min, max)) return Math.max(min, max);
  return value;
};

//CONSTANT NUMBERS
var PI      = Math.PI;
var E       = Math.E;
var SQRT1_2 = Math.SQRT1_2;
var SQRT2   = Math.SQRT2;
var LN2     = Math.LN2;

var cblog = function(msg) {return function(data) {log(msg, data);};};

var g = 9.81;
var g_round = 10;
var euler = Math.E;
var pi = Math.PI;
var q_e = 1.6e-19;
var G = 6.67e-11;
var c = 299792458;
var c_round = 3e8;

var globalConstants = {
  g : {val:  9.81, unit: "m/s^2"},
  g_round : {val:  10, unit: "m/s^2"},
  euler : {val:  2.71, unit: false},
  pi : {val:  3.1415926535, unit: false},
  q_e : {val:  1.6e-19, unit: "C"},
  G : {val:  6.67e-11, unit: "Nm^2/kg^2"},
  c : {val:  299792458, unit: "m/s"},
  c_round : {val:  3e8, unit: "m/s"},
};

var newP = ()=>Promise.resolve(1); //better implement it with a real Promise Request instead of jQuery.post

var pointerX = e => e.pageX || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).pageX;
var pointerY = e => e.pageY || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).pageY;

var pointerClientX = e => e.clientX || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).clientX;
var pointerClientY = e => e.clientY || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).clientY;

window.onload=function() {
  if(location.host.indexOf("localhost") !== 0) return;
  var scripts = document.getElementsByTagName("script");
  var scriptlengths = new Map();
  var ct = 'application/json';
  var h = {"Accept": ct, "Content-Type": ct};
  for (let i=0;i<scripts.length;i++) {
    if (scripts[i].src) {
      window.fetch("dev/pfdc111", {method: 'POST', headers: h, body: JSON.stringify({file: scripts[i].src})})
      .then(r=>r.text()).then(r=>{scriptlengths.set(scripts[i].src, r)});
      setInterval(function() {
        if(document.hidden) return;
        window.fetch("dev/pfdc111", {method: 'POST', headers: h, body: JSON.stringify({file: scripts[i].src})})
        .then(r=>r.text()).then(r=>{if(scriptlengths.get(scripts[i].src) !== r) location.reload();});
      }, 2000);
    }
  }
};*/

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//ARRAY
Object.defineProperty(Array.prototype, "searchAll", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    var filtered = this.slice(0); //faster than (deep) clone
    for(var prop in compareObj) {
      filtered = filtered.filter(obj => obj[prop] === compareObj[prop]);
    }
    return filtered;
  }
});

Object.defineProperty(Array.prototype, "has", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    return this.searchAll(compareObj).length>0;
  }
});

Object.defineProperty(Array.prototype, "pick", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    var m = this.searchAll(compareObj);
    if(m.length > 1) {warn("Pick Element: More than 1 match, first one was picked", compareObj, m); console.trace(); return m[0];}
    if(m.length == 0) {warn("Pick Element: not found", compareObj); console.trace(); return false;}
    return m[0];
  }
});

//Object.defineProperty(Array.prototype, "searchItems", {
//  enumerable: false,
//  writable: true,
//  value: function(compareObj) {
//    var filtered = this.slice(0); //faster than (deep) clone
//    for(var prop in compareObj) {
//      filtered = filtered.filter(obj => obj.get(prop) === compareObj[prop]);
//    }
//    return filtered;
//  }
//});

Object.defineProperty(Array.prototype, "unique", {
  enumerable: false,
  writable: true,
  value: function() {
    let unicates = this.filter(function(el, i, ar) {
      return typeof el === "object" ? !ar.slice(0,i).has(el) : ar.indexOf(el) === i;
    });
    return unicates;
  }
});

//Object.defineProperty(Array.prototype, "pushUnique", {
//  enumerable: false,
//  writable: true,
//  value: function(el, byValue = true) {
//    let exists = el === "object" && byValue ? this.has(el) : this.indexOf(el) > -1;
//    if(!exists) this.push(el);
//    return this;
//  }
//});

Object.defineProperty(Array.prototype, "addUnique", {
  enumerable: false,
  writable: true,
  value: function(el, byValue = true) {
    var arr = [...this];
    let exists = (el === "object" && byValue) ? this.has(el) : this.indexOf(el) > -1;
    if(!exists) arr = [...arr, el];
    return arr;
  }
});

Object.defineProperty(Array.prototype, "deleteWhere", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    var arr = [...this];
    let matches = typeof compareObj == "function" ? this.filter(compareObj) : this.searchAll(compareObj);
    matches.forEach(m => arr = [...arr.slice(0,this.indexOf(m)), ...arr.slice(this.indexOf(m) + 1)]);
    return arr;
  }
});

Object.defineProperty(Array.prototype, "replaceAt", {
  enumerable: false,
  writable: true,
  value: function(index, newItem) {
    return [...this.slice(0, index), newItem, ...this.slice(index+1)];
  }
});

Object.defineProperty(Array.prototype, "last", {
  enumerable: false,
  writable: true,
  value: function() {return this[this.length-1];}
});

Object.defineProperty(Array.prototype, "sum", {
  enumerable: false,
  writable: true,
  value: function() {return this.reduce((o,n)=>o+n, 0);}
});

Object.defineProperty(Array.prototype, "avg", {
  enumerable: false,
  writable: true,
  value: function() {if(this.length) return this.sum()/this.length;}
});

/**
 * Works for numbers and strings!
 * Pass in Array ["key", "desc"] for descending sort
 */
Object.defineProperty(Array.prototype, "sortByMultipleKeys", {
  enumerable: false,
  writable: true,
  value: function(...keys) {
    this.sort((a,b) => {
      for (let prop of keys) {
        let [p, ...options] = prop instanceof Array ? prop : [prop];
        let desc = options.indexOf("desc") > -1;
        let propsequence = p.split(".");
        var o1 = a, o2 = b;
        for(var j in propsequence) {o1 = o1[propsequence[j]]; o2 = o2[propsequence[j]];}
        if(replaceUml(o1) > replaceUml(o2)) return desc?-1:1;
        if(replaceUml(o1) < replaceUml(o2)) return desc?1:-1;
      }
    });
    return this;
  }
});



//Object

Object.defineProperty(Object.prototype, "extractKeys", {
  enumerable: false,
  writable: true,
  value: function(...keys) {
    let extracted = {};
    keys.forEach(prop => extracted[prop] = this[prop]);
    return extracted;
  }
});


Object.defineProperty(Object.prototype, "values", {
  enumerable: false,
  writable: true,
  value: function() {
    return Object.keys(this).map(key => this[key]);
  }
});

Object.defineProperty(Object.prototype, "clone", {
  enumerable: false,
  writable: true,
  value: function() {
    return JSON.parse( JSON.stringify( this ) );
  }
});




/***/ })

/******/ });
//# sourceMappingURL=index-bundle.js.map