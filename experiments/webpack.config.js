// in the console: webpack --colors --progress --watch

let webpack = require('webpack');
let path = require('path');
let fs = require("fs");

//let AddHtmlPlugin = require('./AddHtmlPlugin');
let entryObj = {};
let subfolders = fs.readdirSync("./").filter(n=>!~n.indexOf("."));
subfolders.forEach(sf=>{
  let files = fs.readdirSync("./"+sf);
  files.forEach(f=>{
    let found = f.match(/(.*)(raw.jsx?)/);
    if(found) {
      entryObj["./"+sf+"/"+found[1]] = "./"+sf+"/"+found[1]+found[2];
    }
  });
//  if(files.includes("raw.js")) {
//    entryObj["./"+sf] = "./"+sf + "/raw.js";
//  }
//  if(files.includes("raw.jsx")) {
//    entryObj["./"+sf] = "./"+sf + "/raw.jsx";
//  }
});
console.log(entryObj);


// let BUILD_DIR = path.resolve(__dirname, 'build');
// let APP_DIR = path.resolve(__dirname, 'src');

let config = {
  devtool: "source-map",
  entry: entryObj,
  output: {
    filename: '[name]bundle.js'
  },
  resolveLoader: {
    modules: ['node_modules', path.join(process.cwd(), "")]
  },
  resolve: {
    modules: [path.resolve(__dirname), "node_modules"]
  },
  module: {
    rules: [
      {test: /es6\.js/,   use: [{loader: "babel-loader"}] },
      {test: /.jsx/,   use: [{loader: "babel-loader"}] },
      //{test: /\.jsx?$/, use: [{loader: 'replace-loader'}]},
     ],
  },
  plugins: [
    //new AddHtmlPlugin({title: "test", outpath: BUILD_DIR}),
  ],
};

module.exports = config;