/**
 * Add Html for Bundled file
 */

const fs = require("fs");

function AddHtmlPlugin(options) {
  this.outpath = options.outpath;
  this.title = options.title;
  console.log(this.outpath);
}

AddHtmlPlugin.prototype.apply = function(compiler) {
  // Setup callback for accessing a compilation:
  compiler.plugin("compilation", compilation => {
    console.log("AddHtmlPlugin");
    let content = fs.readFileSync("./template.html", "utf-8");
    content = content.replace("${TITLE}", this.title);
    fs.writeFileSync(this.outpath + "/index.html", content)
  });
};

module.exports = AddHtmlPlugin;