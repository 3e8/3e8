import threading, sys, time
import serial
import picamera, picamera.array
from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
sleep = time.sleep

# Open a serial connection to Roomba
ser = serial.Serial(port='/dev/ttyAMA0', baudrate=19200, timeout=0.5)

clients = []

class Camserver(WebSocket):

    def handleMessage(self):
        ser.write(self.data)
        #ser.write('\x84')
        #ser.write('\x8c\x00\x02\x24\x10\x28\x10')
        #ser.write('\x8d\x00')
        print(self.data[0])


    def handleConnected(self):
        clients.append(self)
        print(self.address, "connected")

    def handleClose(self):
        clients.remove(self)
        print(self.adress, "disconnected")

    def sendData(self, data):
        try:
            self.sendMessage(data)
        except:
            print sys.exc_info()[0]
        

print("server starting on port 5001")

server = SimpleWebSocketServer('', 5001, Camserver)
ws=threading.Thread(target=server.serveforever)
ws.setDaemon(True)
ws.start()

print("ok")


W = 128*2
H = 96*2
A = W*H
offsetV = A*5/4
Wh = W/2
Wd = W*2
sys.setrecursionlimit(W*H*3)



camera = picamera.PiCamera(resolution = (W, H), framerate = 8, sensor_mode = 0)
camera.brightness = 80
start = time.time()
print("start")

def getImage():
    global camera
    #rawCapture.truncate(0)
    rawCapture =  bytearray(A*3/2)
    output = bytearray(A*3/4)
    camera.capture(rawCapture, format="yuv", use_video_port=True)
    for c in clients:
        c.sendData(rawCapture)


while(True):
    sleep(0.001)
    try: 
        getImage()
    except:
        print sys.exc_info()[0]

print(time.time()-start)
