import serial
import time

# Open a serial connection to Roomba
ser = serial.Serial(port='/dev/ttyAMA0', baudrate=19200, timeout=0.5)

time.sleep(0.1)

print(ser.isOpen())


# Start SCI
ser.write('\x80')
# Enable full mode
ser.write('\x84')
# Spot clean
#ser.write('\x85')

time.sleep(1)

ser.write('\x8c\x00\x02\x24\x10\x28\x10')
ser.write('\x8d\x00')

time.sleep(5)

#ser.write('\x8e\x18')

#ser.write('\x91\xff\xee\xff\xee')
#time.sleep(3)
#ser.write('\x91\xff\xff\xff\xff')
#time.sleep(10)
#ser.write('\x91\xff\xee\xff\xee')


#sensor
ser.write('\x8e\x18')
#ser.write(chr(2))
#ser.write(chr(7))
#ser.write(chr(13))

while True:
    size = ser.inWaiting()
    if size:
        print(size)
        data = ser.read(size)
        print(str(data))
    else:
        print('no data')
    time.sleep(1)

time.sleep(20)

# Leave the Roomba in passive mode; this allows it to keep
#  running Roomba behaviors while we wait for more commands.
#ser.write('\x80')

# Close the serial port; we're done for now.
ser.close()
