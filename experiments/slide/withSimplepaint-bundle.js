/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 407);
/******/ })
/************************************************************************/
/******/ ({

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return on; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return makeNode; });
/* unused harmony export makeNodes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return empty; });
function on(elSelector, eventName, selector, fn) {
  let elements = document.querySelectorAll(elSelector);
  
  for (let i = 0; i < elements.length; ++i) {
    let element = elements[i];
    element.addEventListener(eventName, function(event) {
      let possibleTargets = element.querySelectorAll(selector);
      let target = event.target;
    
      for(let j = 0, l = possibleTargets.length; j < l; j++) {
        let el = target;
        let p = possibleTargets[j];
        while(el && el !== element) {
          if(el === p) {
            return fn.call(p, event);
          }
          el = el.parentNode;
        }
      }
    });
  }
}

function getPosition(el) {
  let xPos = 0;
  let yPos = 0;
  
  while (el) {
    if (el.tagName === "BODY") {
      // deal with browser quirks with body/window/document and page scroll
      var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
      var yScroll = el.scrollTop || document.documentElement.scrollTop;
      
      xPos += (el.offsetLeft - xScroll + el.clientLeft);
      yPos += (el.offsetTop - yScroll + el.clientTop);
    } else {
      // for all other non-BODY elements
      xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
      yPos += (el.offsetTop - el.scrollTop + el.clientTop);
    }
    
    el = el.offsetParent;
  }
  return {
    x: xPos,
    y: yPos
  };
}

/**
 * @param {String} html representing a single element
 * @return {Node}
 */
function makeNode(html) {
  var template = document.createElement('template');
  template.innerHTML = html;
  return template.content.firstChild;
}

/**
 * @param {String} html representing any number of sibling elements
 * @return {NodeList}
 */
function makeNodes(html) {
  var template = document.createElement('template');
  template.innerHTML = html;
  return template.content.childNodes;
}

/**
 *
 * @param {Element} node
 * @returns {Element}
 */
function empty(node) {
  node.innerHTML = "";
  return node;
}




/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Simplepainter; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__ = __webpack_require__(101);
// TODO
/*
-history
-group
-pousi
*/

const base62 = __webpack_require__(214);


const PRESSUREBASE = 0.18;
const PRESSUREINCREASE = 0.82; //should add up to one
const MAXTEMPPATH = 50;

class Simplepainter {
  constructor({selector = ".simplepainter", menu="auto", itemstore = false, onlypen = true}={}) {
    this.el = document.querySelector(selector);
    this.el.classList.add("simplepainter");
    this.el.style.touchAction = "none"; //native touch-actions not possible, otherwise pointermove blocked
    this.pousi = document.createElement("canvas");
    this.pousi.classList.add("simplepainter__canvas", "pousi");
    this.el.appendChild(this.pousi);
    this.svgcontainer = document.createElementNS('http://www.w3.org/2000/svg', "svg");
    this.svgcontainer.setAttribute('class', "simplepainter__svgcontainer");
    this.el.appendChild(this.svgcontainer);
    this.svgcontainer.setAttribute("width", window.getComputedStyle(this.el).width);
    this.svgcontainer.setAttribute("height", window.getComputedStyle(this.el).height);
    if(menu === "auto") {
      this.menu = document.createElement("div");
      this.menu.classList.add("toggleContent", "on", "simplepainter__menu");
      let colors = ["ffffff", "999999", "444444", "000000", "992222", "222266", "ffaa22"].map(c=>{
        return `<span class="simplepainter__menu--color" style="background-color:#${c}" data-color="${c}">&nbsp;</span>`
      }).join("");
      let thickness = [0.5, 1, 1.5, 2, 3, 4, 6, 8, 10, 12].map(t=>{
        return `<div class="simplepainter__menu--thickness" data-thickness="${t}"><div style="height: ${2+t}px; width: ${2+t}px; border-radius: ${1+t*0.5}px; background-color:#000; margin: 0px ${(16-t)/2}px; vertical-align:middle;">&nbsp;</div></div>`
      }).join("");
      this.menu.innerHTML = `
        <div class="toggler ifOn" style="color: grey;">...</div>
        <div class="toggler ifOff">...</div>
        <div class="ifOn"><input type="color" class="opt_color" value="#222266"></div>
        <div class="ifOn">${colors}</div>
        <div class="ifOn" style="verical-align: middle">${thickness}</div>
        <div class="ifOn"><input class="opt_thickness" value="3"></div>
        <div class="ifOn simplepainter__history toggleContent off">
          <div class="toggler ifOn" style="color: grey;">...</div>
          <div class="toggler ifOff">Tree</div>
          <div class="historytree ifOn"></div>
        </div>
        <div class="ifOn simplepainter__undo">Undo</div>
        <div class="ifOn simplepainter__redo">Redo</div>
      `;
      this.el.appendChild(this.menu);
    }
    this.itemstore = itemstore;
    if(this.itemstore) {
      this.itemstore.dNs.mountShelf("simplepainter", this.options);
      this.historyEl = this.el.querySelector(".historytree");
      let showHistory = ({action, args}={}) => {
        let l = this.itemstore.getLeavesAsArray();
        let sketchlist = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__["c" /* makeNode */])("<div class='sketchlist'></div>");
        let rootsOfThisPainter = l.filter(i=>i.type === "sketch"&&i.group&&(i.painter===this.options.painter||(i.painter===null&&this.options.painter===null)));
        rootsOfThisPainter.forEach(r=>{
          let rPreview = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__["c" /* makeNode */])("<svg class='sketchgroup' style='width:100px' viewBox='0 0 1200 800'></svg>");
          l.filter(s=>s.tree_parent===r.idn).forEach(s=>{
            rPreview.appendChild(this.getSvgFromStroke(s.idn));
          });
          let rEl = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__["c" /* makeNode */])("<div class='root'></div>");
          rEl.appendChild(rPreview);
          rEl.appendChild(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__["c" /* makeNode */])(`<span>${r.idn}</span>`))
          sketchlist.appendChild(rEl);
        });
        let groups = rootsOfThisPainter.map(r=>r.idn + ":<br>" + l.filter(s=>s.tree_parent===r.idn).map(s=>s.idn).join("<br>"));
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__["d" /* empty */])(this.historyEl).appendChild(sketchlist);
      };
      this.itemstore.subscribe("dbChange", showHistory);
      this.itemstore.subscribe("ready", showHistory);
      this.itemstore.subscribe("stateChange", e=>console.log("22statechange", e), "simplepainter");
    }
    
    
    
    this.capturing = false;	// tracks in/out of canvas context
    this.onlyPen = onlypen;
    this.setOptions(true);
    this.lastOptions = this.options.clone();
    this.lastGroup = null;
    this.p = null;
    this.path = "";
    this.x1 = 0;
    this.y1 = 0;
    this.p1 = 0;
    this.x2 = 0;
    this.y2 = 0;
    this.p2 = 0;
    this.n = 0;
//     this.lastP = 0.5;
    this.drawn = 0;
    
    //this.def = 0;
    this.zoomlevel = 1;
    this.zoomlevelcontrol = false;
  
    //@temppaths
    this.temppaths = [];
    this.tempdone = 0;
    for(let i = 0; i < MAXTEMPPATH; i++) {
      let ptemp = document.createElementNS('http://www.w3.org/2000/svg', "path");
      ptemp.setAttribute("class", "temppath");
      ptemp.setAttribute("style", `stroke:none; fill: none;`);
      ptemp.setAttribute("d", "M0,0");
      this.svgcontainer.appendChild(ptemp);
      this.temppaths.push(ptemp);
    }
    
    this.addEventListeners();
    this.draw();
    
    //log(this.arraysToPressureContour([0,2,4,6,8,10,12,14,16], [16,8,0,8,16,8,0,8,16], [1,1,1,1,1,1,1,1,1], 1).replace(/[MC]/g, "").replace(/\s/g, "\n").replace(/,/g, "\t"))
    //log(this.arraysToPressureContour([0,2,4,6,8,10,12,14,16], [0,2,4,6,8,10,12,14,16], [1,1,1,1,1,1,1,1,1], 3).replace(/[MC]/g, "").replace(/\s/g, "\n").replace(/,/g, "\t"))
  
  }
  
  addEventListeners() {
    this.svgcontainer.addEventListener("pointerdown", (e)=>this.pointerdown(e));
    this.svgcontainer.addEventListener("pointerup", (e)=>this.pointerup(e));
    this.svgcontainer.addEventListener("pointermove", (e)=>this.pointermove(e));
    this.svgcontainer.addEventListener("pointerleave", (e)=>this.pointerup(e));
    this.menu.addEventListener("pointerdown", (e)=>e.stopPropagation());
    let togglers = document.querySelectorAll(".toggler");
    for(let i = 0; i < togglers.length; i++)
      togglers[i].addEventListener("click", e=>{
      e.target.closest(".toggleContent").classList.toggle("on");
      e.target.closest(".toggleContent").classList.toggle("off");
    });
    let colorbuttons = document.querySelectorAll(".simplepainter__menu--color");
    for(let i = 0; i < colorbuttons.length; i++) {
      colorbuttons[i].addEventListener("pointerdown", e=>{
        Array.from(colorbuttons).forEach(c=>c.classList.remove("active"));
        e.target.classList.add("active");
        document.querySelector(".opt_color").value = "#"+e.target.dataset.color;
      });
    }
    let thicknessbuttons = document.querySelectorAll(".simplepainter__menu--thickness");
    for(let i = 0; i < thicknessbuttons.length; i++) {
      thicknessbuttons[i].addEventListener("pointerdown", e=>{
        Array.from(thicknessbuttons).forEach(c=>c.classList.remove("active"));
        let button = e.target.closest(".simplepainter__menu--thickness");
        button.classList.add("active");
        document.querySelector(".opt_thickness").value = button.dataset.thickness;
      });
    }
    document.querySelector(".simplepainter__undo").addEventListener("click", _=>{
      //@TODO: check if undo in current slide?? console.log(this.itemstore.getVersionToUndoOrRedo("undo"));
      this.itemstore && this.itemstore.undo();
    });
    document.querySelector(".simplepainter__redo").addEventListener("click", _=>this.itemstore && this.itemstore.redo());
  }
  
  setOptions(nullify) {
    this.options = {};
    this.options.thickness = nullify ? null : +document.querySelector(".opt_thickness").value;
    this.options.color = nullify ? null : "#" + document.querySelector(".opt_color").value.slice(1);
    this.options.painter = nullify ? null : this.el.dataset.painter || null;
    if(this.itemstore) {
      this.itemstore.dNs.changeShelfStates(this.options);
    }
    //localStorage.simplepainter__options = JSON.stringify(this.options);
    //localStorage.simplepainter__currentPathIdn = itemstore.dbStates.latest;
  }

  pointerdown(ev) {
    this.zoomlevelcontrol = {screenX: ev.screenX, clientX: ev.clientX};
    if(ev.pointerType === "pen" || !this.onlyPen) {
      this.setOptions();
      this.c = {
        stroke: this.options.color,
        fill:   false,
        xs:     [getX(ev)],
        ys:     [getY(ev)],
        ps:     [ev.pressure||0],
        //Simplified Points
        sxs:     [getX(ev)],
        sys:     [getY(ev)],
        sps:     [ev.pressure||0],
        //Temppoints
        tempx:   [],
        tempy:   [],
        tempp:   [],
        //other stuff
        lastX:  getX(ev),
        lastY:  getY(ev),
        vx: 0,
        vy: 0,
        ax: 0,
        ay: 0,
        lpx: getX(ev),
        lpy: getY(ev),
      };
      this.p = document.createElementNS('http://www.w3.org/2000/svg', "path");
      this.prof = document.createElementNS('http://www.w3.org/2000/svg', "path");
      //this.path = "M" +getX(ev) + "," + getY(ev) + " L" + getX(ev)+ "," + getY(ev) + " ";
      //this.path = "M" + getX(ev) + "," + getY(ev) + " L";
      //this.p.setAttribute("style", "stroke:#660000; stroke-width:"+this.options.thickness+" ; fill:none;");
      this.prof.setAttribute("style", "stroke:rgba(100,0,0,0.5); stroke-width:"+(this.options.thickness*0.2)+" ; fill:none;");
      this.p.setAttribute("style", `stroke:none; stroke-width: 0; fill: ${this.options.color};`);
      this.p.setAttribute("d", this.path);
      this.svgcontainer.insertBefore(this.p, this.temppaths[0]);
      
      //this.svgcontainer.appendChild(this.prof);
      this.capturing = true;
      this.drawn = 0;
      this.thickenStart = false;
      this.lastPointWasCorner = false;
      this.n = 0;
    }
  }

  pointerup(ev) {
    if((ev.pointerType === "pen" || !this.onlyPen) && this.capturing) {
      
      let c = this.c;
      c.xs.push(getX(ev));
      c.ys.push(getY(ev));
      c.ps.push(c.ps.last() * 0.95);
      //this.path += c.xs.last() + "," + c.ys.last() + " ";
      
      if(c.tempx.length && !(c.tempx.last() === c.sxs.last() && c.tempy.last() === c.sys.last() )) {
        c.sxs.push(c.tempx.last());
        c.sys.push(c.tempy.last());
        c.sps.push(c.tempp.last());
      }
      
      if(c.xs.last() !== c.sxs.last() || c.ys.last() !== c.sys.last()) {
        c.sxs.push(c.xs.last());
        c.sys.push(c.ys.last());
        c.sps.push(c.sps.last()*0.94); //special case because pressure should not decrease linearly over several pixels
      }
  
      //smoothen Pressure (to avoid strange rendering)
      var coeff = this.options.thickness;
      let getThickness = p => coeff*(PRESSUREBASE + PRESSUREINCREASE*p);
      let smoothenPressure = n => {
        let didSomething = false;
        for(let i = 1; i < c.sxs.length; i++) {
          let d = Math.hypot(c.sxs[i] - c.sxs[i-1], c.sys[i] - c.sys[i-1]);
          let dp = c.sps[i] - c.sps[i-1];
          let dt = getThickness(c.sps[i]) - getThickness(c.sps[i-1]);
          const TOLERANCE = 0.05;
          
          if(Math.abs(dt) > TOLERANCE * d) {
            let equalizeP = 0;
            const MaxFactor = 0.25;
            while(equalizeP < MaxFactor && Math.abs(getThickness(c.sps[i] - dp*equalizeP) - getThickness(c.sps[i-1] + dp*equalizeP)) > TOLERANCE * d) {
              equalizeP += 0.05;
            }
            c.sps[i] -= dp*equalizeP;
            c.sps[i-1] += dp*equalizeP;
            didSomething = true;
          }
        }
        if(didSomething && n<100) smoothenPressure(n+1);
      };
      smoothenPressure(0);
      
      this.p.setAttribute("d", this.arraysToPressureContour(this.c.sxs, this.c.sys, this.c.sps));
      //this.svgcontainer.removeChild(this.prof);
      this.capturing = false;
      
      //@Temppath reset
      if(this.tempdone > 0) {
        for(let i = 0; i < Math.min(MAXTEMPPATH, this.tempdone/10); i++) {
          this.temppaths[i].setAttribute("d", "M0,0");
          this.temppaths[i].setAttribute("style", `stroke:none; fill: none;`);
        }
        this.tempdone = 0;
      }

      if(this.itemstore) {
        setTimeout(_=>this.saveItem(), 0);
      }
    }
  }

  pointermove(ev) {
    if(this.capturing && (ev.pointerType === "pen" || !this.onlyPen)) {
      let p = ev.pressure===undefined ? 0.5 : (ev.pressure === 0 ? this.c.ps.last() * 0.9 : ev.pressure);
      this.addPoint(getX(ev), getY(ev), p);
    }
//     if(this.capturing &&  ev.pointerType === "pen" && ev.pressure === 0) {                                                                                                                                                                                             qqqqqqqq11) {
//       log("emergency up");
//       this.pointerup(ev);
//     }
    if(Math.abs(ev.clientX - this.zoomlevelcontrol.clientX) > 10) {
      this.zoomlevel = (ev.screenX - this.zoomlevelcontrol.screenX) / (ev.clientX - this.zoomlevelcontrol.clientX);
    }
  }

  addPoint(x,y,pressure) {
    if(x===this.c.xs.last() && y===this.c.ys.last()) {
      this.c.ps[this.c.ps.length - 1] = 0.5 * (this.c.ps.last() + pressure);
      return;
    }
    this.c.xs.push(x);
    this.c.ys.push(y);
    this.c.ps.push(pressure);
    let {vx, vy, ax, ay, lpx, lpy, xs, ys, ps} = this.c;
    let L = xs.length;
    var px = (17*xs[L-1] + 12*xs[L-2] - (3*xs[L-3]||3*xs[L-2]) ) / 26;
    var py = (17*ys[L-1] + 12*ys[L-2] - (3*ys[L-3]||3*ys[L-2]) ) / 26;
	  var nvx = L===3 ? (xs[L-1] - xs[L-2]) : 0.8 * vx + 0.2 * (px - lpx);
	  var nvy = L===3 ? (ys[L-1] - ys[L-2]) : 0.8 * vy + 0.2 * (py - lpy);
	  ax = 0.8 * ax + 0.2 * ( (xs[L-1] - xs[L-2]) - vx);
	  ay = 0.8 * ay + 0.2 * ( (ys[L-1] - ys[L-2]) - vy);
    Object.assign(this.c, {vx: nvx, vy: nvy, ax, ay, lpx: px, lpy: py});
    //this.path += px + "," + py + " ";
    
    this.c.tempx.push(this.aggregate(this.c.xs, L));
    this.c.tempy.push(this.aggregate(this.c.ys, L));
    this.c.tempp.push(this.aggregate(this.c.ps, L));
    
    //corner
    if(L>=8) {
      let x1stBefore = this.get1stDerivative(this.c.xs, L-6);
      let y1stBefore = this.get1stDerivative(this.c.ys, L-6);
      let x1stAfter = this.get1stDerivative(this.c.xs, L-3);
      let y1stAfter = this.get1stDerivative(this.c.ys, L-3);
      let anglebefore = (Math.atan2(-y1stBefore, x1stBefore)*180/Math.PI + 360)%360;
      let angleafter = (Math.atan2(-y1stAfter, x1stAfter)*180/Math.PI + 360)%360;
      let da = ((anglebefore - angleafter) + 720 + 180)%360 - 180;
      if(Math.abs(da)>45 && !this.lastPointWasCorner) {
        if(this.c.tempx.length>=2 || 0) {
          this.c.sxs = this.c.sxs.concat(this.c.tempx.slice(-2));
          this.c.sys = this.c.sys.concat(this.c.tempy.slice(-2));
          this.c.sps = this.c.sps.concat(this.c.tempp.slice(-2));
        }
        else {
          this.c.sxs.push(this.c.tempx[0]);
          this.c.sys.push(this.c.tempy[0]);
          this.c.sps.push(this.c.tempp[0]);
        }
        this.lastPointWasCorner = true;
        this.c.tempx = [];
        this.c.tempy = [];
        this.c.tempp = [];
      }
    }
    
    //Test
    // var point = document.createElementNS('http://www.w3.org/2000/svg',"circle");
        // point.setAttribute("style", "stroke:none; fill:"+this.options.color+";");
        // point.setAttribute("cx", 5*x);
        // point.setAttribute("cy", 5*y);
        // point.setAttribute("r", 2);
        // this.svgcontainer.appendChild(point);
  }
  
  aggregate(a, L) {
    if(L>9)  return (-11*a[L-10] + /*0*a[L-9] +*/ 9*a[L-8] + 16*a[L-7] + 21*a[L-6] + 24*a[L-5] + 12.5*a[L-4])/143 + (3.5*a[L-4] + 6*a[L-3] + 3*a[L-2] - 2*a[L-1])/21; //assymmetrische Savitzky Golay
    if(L===1) return a[0];
    if(L===2) return 0.25*a[0] + 0.75*a[1];
    if(L===3) return 0.1*a[0] + 0.4*a[1] + 0.5*a[2];
    if(L===4) return 0.08*a[0] + 0.22*a[1] + 0.40*a[2] + 0.30*a[3];
    if(L===5) return 0.05*a[0] + 0.15*a[1] + 0.30*a[2] + 0.30*a[3] + 0.20*a[4];
    if(L===6) return (-2*a[0] + 3*a[1] + 6*a[2] + 3.5*a[3])/21 + (8.5*a[3] + 12*a[4] - 3*a[5])/35;
    if(L===7) return (-21*a[0] + 14*a[1] + 39*a[2] + 54*a[3] + 29.5*a[4])/231 + (8.5*a[4] + 12*a[5] - 3*a[6])/35;
    if(L===8) return (-36*a[0] + 9*a[1] + 44*a[2] + 69*a[3] + 84*a[4] + 44.5*a[5])/429 + (8.5*a[5] + 12*a[6] - 3*a[7])/35;
    if(L===9) return (-36*a[0] + 9*a[1] + 44*a[2] + 69*a[3] + 84*a[4] + 44.5*a[5])/429 + (3.5*a[5] + 6*a[6] + 3*a[7] - 2*a[8])/21;
  }
  
  get1stDerivative(arr, center) {
    return -0.2*arr[center-2] - 0.1*arr[center-1] + 0.1*arr[center+1] + 0.2*arr[center+2];
  }

  arraysToPressureContour(xs, ys, ps, thickness) {
    var coeff = thickness === undefined ? this.options.thickness : thickness;
    var L = xs.length;
    var points = xs.map(function(e, i) {return {x: xs[i], y: ys[i], t: coeff*(PRESSUREBASE + PRESSUREINCREASE*ps[i])};});
    let rs = {left: [], right: []};
    //cap inverted edges
    if(L>=3) {
      if((xs[L-2]-xs[L-1])*(xs[L-3]-xs[L-2]) + (ys[L-2]-ys[L-1])*(ys[L-3]-ys[L-2]) < 0) {
        points = points.slice(0, -1);
      }
      if(L >= 4 && (xs[1]-xs[0])*(xs[2]-xs[1]) + (ys[1]-ys[0])*(ys[2]-ys[1]) < 0) {
        points = points.slice(1);
      }
    }
    let getTurningEdge = function(role = "start||turn", edge, mid, inner) {
      if(!(edge&&mid&&inner)) log(edge, mid, inner)
      let a = getControlPoints(edge.x, edge.y, mid.x, mid.y, inner.x, inner.y, 0.4);
      let dx = edge.x - mid.x;
      let dy = edge.y - mid.y;
      let dsq = dx*dx + dy*dy;
      let tm = {x: a[0]-mid.x, y: a[1]-mid.y}; //Tangente an Mid
      let mt = {x: tm.x - 2*(tm.x*dx+tm.y*dy)/dsq * dx, y: tm.y - 2*(tm.x*dx+tm.y*dy)/dsq * dy}; //gespiegelte Tangente an Kurvensegment
      let mtl = Math.hypot(mt.x, mt.y);
      let pressthick = edge.t;
      let [corrx, corry] = [-pressthick * mt.x/mtl, -pressthick * mt.y/mtl];
      if(dsq==0) {warn(dsq, " GLEICH NULL!?? (Schaue in alter Version)");}
      let helperpoints = [];
      for(let step = 0; step < 9; step+=2) {
        let da = - 4/9*Math.PI + step/9*Math.PI; // + Math.atan2(mt.y, mt.x);
        helperpoints.push({x: edge.x + Math.cos(da) * corrx - Math.sin(da) * corry, y: edge.y + Math.cos(da) * corry + Math.sin(da) * corrx})
      }
      return {
        results: helperpoints
      }
    };
    let getTangents = function(p, i, arr) {
      if(i === arr.length-1) return; //last point
      let next = arr[i+1];
      let dx = next.x - p.x;
      let dy = next.y - p.y;
      let d = hypot(dx, dy);
      //log(dx, dy, d);
      if(i===0) {
        rs.left.push({x: p.x + dy / d * p.t, y: p.y - dx / d * p.t});
        rs.right.push({x: p.x - dy / d * p.t, y: p.y + dx / d * p.t});
        rs.left.push({x: next.x + dy / d * next.t, y: next.y - dx / d * next.t});
        rs.right.push({x: next.x - dy / d * next.t, y: next.y + dx / d * next.t});
        //log(rs);
        return;
      }
      let getpoint = function(side) {
        let sign = side === "left" ? +1 : -1;
        let start = {x: p.x + sign * dy / d * p.t, y: p.y - sign * dx / d * p.t};
        let end = {x: next.x + sign * dy / d * next.t, y: next.y - sign * dx / d * next.t};
        let rx = end.x - start.x, ry = end.y - start.y;
        let [prevstart, prevend] = rs[side].slice(-2);
        let ox = prevend.x - prevstart.x, oy = prevend.y - prevstart.y;
        
        if(ox*ry - oy*rx === 0) {
          //if(ox*rx+oy*ry < 0) warn("antiparallel");
          rs[side][rs[side].length - 1] = {x: 0.5 * (start.x + prevend.x), y: 0.5 * (start.y + prevend.y)};
        }
        else {
          let anglebefore = (Math.atan2(oy, ox)*180/Math.PI + 360)%360;
          let angleafter = (Math.atan2(ry, rx)*180/Math.PI + 360)%360;
          let da = ((anglebefore - angleafter) + 720 + 180)%360 - 180;
          if(Math.abs(da) > 60) {
            let loopover = da*sign > 0;
            let turnangle = loopover ? 360-Math.abs(da) : Math.abs(da);
            let numsteps = Math.floor(turnangle/30);
            let helperangle = turnangle / (numsteps + 1);
            let helperpoints = [];
            //log(side, da, loopover, turnangle, numsteps, helperangle);
            let rotx = start.x - p.x, roty = start.y - p.y;
            for(let step = 0; step<=numsteps; step++) {
              let a = - sign * helperangle*step/180*Math.PI;
              helperpoints.unshift({x: p.x + rotx * Math.cos(a) - roty * Math.sin(a), y: p.y + roty * Math.cos(a) + rotx * Math.sin(a)})
            }
            rs[side] = rs[side].concat(helperpoints)
          }
          else {
            let t = (start.x*ry - start.y*rx - prevstart.x * ry + prevstart.y * rx) / (ox*ry - oy*rx);
            if(t > 1) {
              rs[side][rs[side].length - 1] = {x: 0.5 * (start.x + prevend.x), y: 0.5 * (start.y + prevend.y)};
            }
            else if(t > 0) {
              rs[side][rs[side].length - 1] = {x: prevstart.x + t * ox, y: prevstart.y + t * oy};
            }
            else if(t < 0) {
              rs[side].pop();
            }
          }
        }
        rs[side].push(end);
      };
      getpoint("left");
      getpoint("right");
    };
    let results = [];
    if(points.length === 1) {
      for(let step = 0; step <= 11; step++) {
        let a = step * 30 / 180 * Math.PI;
        let p = points[0];
        results.push({x: p.x + p.t * Math.cos(a), y: p.y + p.t * Math.sin(a)})
      }
    }
    else {
      if(points.length === 2) {
        points = [points[0], {x: 0.5*(points[0].x+points[1].x), y: 0.5*(points[0].y+points[1].y), t: 0.5*(points[0].t+points[1].t)}, points[1]];
      }
      points.forEach(getTangents);
      let s = getTurningEdge("start", ...points.slice(0, 3));
      let t = getTurningEdge("turn", ...points.slice(-3).reverse());
      results = [...s.results, ...rs.left, ...t.results, ...rs.right.reverse()];
    }
    let path = results.length ? arraysToBezier(results.map(p=>p.x).concat([results[0].x]), results.map(p=>p.y).concat([results[0].y])) : "";
    return path;
  }
  
  checkForCorner() {
    let L = this.c.xs.length;
    if(L<8) return false;
    let x1stBefore = this.get1stDerivative(this.c.xs, L-6);
    let y1stBefore = this.get1stDerivative(this.c.ys, L-6);
    let x1stAfter = this.get1stDerivative(this.c.xs, L-3);
    let y1stAfter = this.get1stDerivative(this.c.ys, L-3);
    let anglebefore = (Math.atan2(-y1stBefore, x1stBefore)*180/Math.PI + 360)%360;
    let angleafter = (Math.atan2(-y1stAfter, x1stAfter)*180/Math.PI + 360)%360;
    let da = ((anglebefore - angleafter) + 720 + 180)%360 - 180;
    return Math.abs(da)>45;
  }

  draw() {
    requestAnimationFrame(_=>this.draw());
    if(!this.capturing) return;
    let {vx, vy, ax, ay, lpx, lpy, xs, ys} = this.c;
    let L = xs.length;
    if(L === this.drawn) return;
    this.drawn = L;
    let lag = 0.4;
    let factora = 0.2;
    var predictorsX = [1,2,3].map(t=>lpx + lag*vx*t + factora*t*t*ax);
    var predictorsY = [1,2,3].map(t=>lpy + lag*vy*t + factora*t*t*ay);
    //var qp = "M"+lpx+","+lpy+" L" +  // + " " + c.xs[c.xs.length-1]+","+ c.ys[c.ys.length-1];
    var dprof = ""; //this.c.xs[this.c.xs.length-1]+","+ this.c.ys[this.c.ys.length-1] + " " + this.c.xs[this.c.xs.length-1]+","+ this.c.ys[this.c.ys.length-1];
    //this.p.setAttribute("d", this.path);
    //this.prof.setAttribute("d", qp);
    if(this.c.tempx.length) {
      var d = this.distanceFromLastPoint();
      
      if(d>2*Math.sqrt(this.options.thickness)/this.zoomlevel) {
        if(d>4*this.c.tempx.length) {
          this.c.sxs = this.c.sxs.concat(this.c.tempx);
          this.c.sys = this.c.sys.concat(this.c.tempy);
          this.c.sps = this.c.sps.concat(this.c.tempp);
        }
        else {
          this.c.sxs.push(this.c.tempx.last());
          this.c.sys.push(this.c.tempy.last());
          this.c.sps.push(this.c.tempp.last());
        }
        this.c.tempx = [];
        this.c.tempy = [];
        this.c.tempp = [];
        this.lastPointWasCorner = false;
      }
    }
    if(!this.thickenStart && this.c.sps.length>=2) {
      this.thickenStart = "done";
      this.c.sps[0] = 0.9 * this.c.sps[1];
    }
    
    //@Temppath
    if(this.c.sxs.length - this.tempdone >= 15 && this.tempdone < 10*MAXTEMPPATH) {
      let ptemp = this.temppaths[Math.floor(this.tempdone/10)];
      ptemp.setAttribute("style", `stroke:none; stroke-width: 0; fill: ${this.options.color};`);
      let txs = this.tempdone === 0 ? this.c.sxs.slice(0,15) : this.c.sxs.slice(this.tempdone - 5, this.tempdone + 15);
      let tys = this.tempdone === 0 ? this.c.sys.slice(0,15) : this.c.sys.slice(this.tempdone - 5, this.tempdone + 15);
      let tps = this.tempdone === 0 ? this.c.sps.slice(0,15) : this.c.sps.slice(this.tempdone - 5, this.tempdone + 15);
      tps = this.tempdone === 0 ? tps : tps.map((p,i)=> i<5 ? p*(1-0.2*(5-i)) : p);
      tps = tps.map((p,i)=> i>tps.length-5 ? p*(1-0.2*(i-(tps.length-5))) : p);
      let ptemppath = this.arraysToPressureContour(txs, tys, tps);
      ptemp.setAttribute("d", ptemppath);
      this.tempdone = this.c.sxs.length-5;
    }
    let sxs = this.c.sxs.slice(Math.max(0, (this.tempdone||0)-5));
    let sys = this.c.sys.slice(Math.max(0, (this.tempdone||0)-5));
    let sps = this.c.sps.slice(Math.max(0, (this.tempdone||0)-5));
    sps = this.tempdone === 0 ? sps : sps.map((p,i)=> i<5 ? p*(1-0.2*(5-i)) : p);
    let path;
    if(!vx && !vy && !ax && !ay) {
      path = this.arraysToPressureContour(sxs, sys, sps);
    }
    else {
      path = this.arraysToPressureContour(sxs.concat([lpx, ...predictorsX]), sys.concat([lpy, ...predictorsY]), sps.concat([0,1,2,3].map(_=>this.c.ps.last())))
    }
    this.p.setAttribute("d", path);
    //localStorage.simplepainter__currentPath = path;
  }
  
  distanceFromLastPoint() {
    return Math.hypot(this.c.tempx.last()-this.c.sxs.last(), this.c.tempy.last()-this.c.sys.last())
  }

  saveItem() {
    if(Object.keys(this.options).filter(k=>this.lastOptions[k] !== this.options[k]).length) {
      let leaves = this.itemstore.getLeavesAsArray();
      var tree_parent = this.lastOptions.painter === null ? "" : (leaves.find(i=>i.painter === this.options.painter) || {}).tree_parent;
      var tree_pos = leaves.filter(i=>i.painter === this.options.painter && i.tree_parent === tree_parent).map(i=>+i.tree_pos).sort((u,v)=>v-u)[0] + 8 || 8;
      var obj = Object.assign({}, this.options, {group: true, tree_parent, tree_pos});
      this.itemstore.dDb.addItem("sketch", obj).then(idn=>this.lastGroup = idn).then(_=>this.saveStroke());
      this.lastOptions = this.options.clone();
    }
    else {this.saveStroke();}
  }

  saveStroke() {
    var tree_pos = this.itemstore.getLeavesAsArray().filter(i=>i.tree_parent === this.lastGroup).map(i=>+i.tree_pos).sort()[0] + 8 || 8;
    var obj = {c: this.deflate(this.c), tree_parent: this.lastGroup, tree_pos};
    this.itemstore.dDb.addItem("sketch", obj);
  }
  
  deflate(c) {
    const coorddeflator = x => base62.intToB62(x*64, 3);
    const pressuredeflator = x => base62.intToB62(x*60*64, 2);
    let {sxs, sys, sps} = c;
    let [xsd, ysd, psd] = [sxs.map(coorddeflator).join(""), sys.map(coorddeflator).join(""), sps.map(pressuredeflator).join("")];
    return {xsd, ysd, psd}  //xsdeflated etc
  }
  
  inflate(c) {
    const coordinflator = x => +base62.b62ToInt(x)/64;
    const pressureinflator = x => +base62.b62ToInt(x)/64/60;
    let {xsd, ysd, psd} = c;
    let [xsa, ysa, psa] = [xsd.match(/.{3}/g), ysd.match(/.{3}/g), psd.match(/.{2}/g)];
    let [sxs, sys, sps] = [xsa.map(coordinflator), ysa.map(coordinflator), psa.map(pressureinflator)];
    return {sxs, sys, sps};
  }
  
  loadStrokes() {
    var leaves = this.itemstore.getLeavesAsArray();
    leaves.filter(l=>l.tree_parent === "").sort((u,v)=>u.tree_pos>v.tree_pos?1:-1).forEach(l=>this.renderStrokeOrGroup(l.idn, leaves));
  }
  
  renderStrokeOrGroup(idn, leaves = this.itemstore.getLeavesAsArray() ) {
    leaves.filter(k => k.tree_parent === idn).sort((u,v)=>u.tree_pos>v.tree_pos?1:-1).forEach(k=>{
      if(k.group) {
        this.renderStrokeOrGroup(k.idn, leaves)
      }
      else {
        this.loadStroke(k.idn);
      }
    });
  }

  getSvgFromStroke(idn) {
    var item = this.itemstore.getItem(idn);
    if(item.group) return;
    var parent = this.itemstore.getItem(item.tree_parent);
    var p = document.createElementNS('http://www.w3.org/2000/svg', "path");
    if(!item.c.sxs) {
      Object.assign(item.c, this.inflate(item.c));
    }
    p.setAttribute("style", "stroke:none; stroke-width:0 ; fill:" + parent.color + ";");
    p.setAttribute("d", this.arraysToPressureContour(item.c.sxs, item.c.sys, item.c.sps, parent.thickness));
    return p;
  }
  
  loadStroke(idn) {
    let p = this.getSvgFromStroke(idn)
    p.setAttribute("data-idn", idn);
    this.svgcontainer.insertBefore(p, this.temppaths[0]);
//    for(let i=0; i<item.c.sxs.length; i++) {
//      let c = document.createElementNS('http://www.w3.org/2000/svg', "circle");
//      c.setAttribute("style", "stroke:none; stroke-width:0 ; fill:white;");
//      c.setAttribute("cx", item.c.sxs[i]);
//      c.setAttribute("cy", item.c.sys[i]);
//      c.setAttribute("r", "1");
//      this.svgcontainer.appendChild(c);
//    }
  }

  clear() {
    while (this.svgcontainer.firstChild && !this.svgcontainer.firstChild.classList.contains("temppath")) this.svgcontainer.removeChild(this.svgcontainer.firstChild);
  }
}

function getX(ev) {
  return ev.offsetX !== undefined ? ev.offsetX : warn("offsetX needs polyfill");
}

function getY(ev) {
  return ev.offsetY !== undefined ? ev.offsetY : warn("offsetY needs polyfill");
}

function getNeighbours(arr, i, len) {
  var L = len || arr.length;
  return {prev: arr[(L + i - 1) % L], next: arr[(i + 1) % L]};
}

function getControlPoints(x0, y0, x1, y1, x2, y2, t) {
  var d01 = Math.hypot(x1-x0, y1-y0);
  var d12 = Math.hypot(x2-x1, y2-y1);
  if(d01 + d12 === 0) {return [x1, y1, x1, y1]}
  var fa = t * d01 / (d01 + d12);   // scaling factor for triangle Ta
  var fb = t * d12 / (d01 + d12);   // ditto for Tb, simplifies to fb=t-fa
  var p1x = x1 - fa * (x2 - x0);    // x2-x0 is the width of triangle T
  var p1y = y1 - fa * (y2 - y0);    // y2-y0 is the height of T
  var p2x = x1 + fb * (x2 - x0);
  var p2y = y1 + fb * (y2 - y0);
  return [p1x.toFixed(2), p1y.toFixed(2), p2x.toFixed(2), p2y.toFixed(2)];
}

function arraysToBezier(xs, ys) {
  const rnd = x=>x.toPrecision(3);
  var p = "M" + xs[0] + "," + ys[0] + " ";
  p += " C" + (xs[0] + (xs[1] - xs[0]) / 3) + "," + (ys[0] + (ys[1] - ys[0]) / 3) + " ";
  for(var i = 1; i < xs.length - 1; i++) {
    var a = getControlPoints(xs[i - 1], ys[i - 1], xs[i], ys[i], xs[i + 1], ys[i + 1], 0.4);
    p += a[0] + "," + a[1] + " " + xs[i].toFixed(2) + "," + ys[i].toFixed(2) + " " + a[2] + "," + a[3] + " ";
  }
  //log(xs.last(), xs[xs.length - 2],  xs.last() + (xs[xs.length - 2] - xs.last()) / 3);
  p += (xs.last() + (xs[xs.length - 2] - xs.last()) / 3) + "," + (ys.last() + (ys[ys.length - 2] - ys.last()) / 3) + " ";
  p += xs.last() + "," + ys.last();
  return p;
}

//TODO
// //pousi
// function render(src){
// var image = new Image();
// image.onload = function(){
// var canvas = document.getElementsByClassName("pousi")[0];
// canvas.width = window.innerWidth;
// canvas.height = window.innerHeight;
// var ctx = canvas.getContext("2d");
// ctx.clearRect(0, 0, canvas.width, canvas.height);
// var scalefactor = Math.max(image.width/canvas.width, image.height/canvas.height);
// //      canvas.width = image.width;
// //      canvas.height = image.height;
// ctx.drawImage(image, 0, 0, image.width/scalefactor, image.height/scalefactor);
// };
// image.src = src;
// }
//
// function loadImage(src){
// //	Prevent any non-image file type from being read.
// log(src);
// if(!src || !src.type.match(/image.*/)){
//  console.log("The dropped file is not an image: ", src ? src.type : "no src");
//  return;
//}
//
////	Create our FileReader and run the results through the render function.
//var reader = new FileReader();
//reader.onload = function(e){
//  render(e.target.result);
//};
//reader.readAsDataURL(src);
//}
//
//var target = document.getElementById("mysvg");
//target.addEventListener("dragover", function(e){e.preventDefault(); e.dataTransfer.dropEffect = 'copy';}, false);
//target.addEventListener("drop", function(e){
//  e.preventDefault();
//  log(e);
//  log(e.dataTransfer.files[0]);
//  loadImage(e.dataTransfer.files[0]);
//}, true);
//
//document.onpaste = function (event) {
//      // use event.originalEvent.clipboard for newer chrome versions
//      var items = (event.clipboardData  || event.originalEvent.clipboardData).items;
//      console.log((event.clipboardData  || event.originalEvent.clipboardData).getData("text/plain"), items); // will give you the mime types
//      // find pasted image among pasted items
//      var blob = null;
//      for (var i = 0; i < items.length; i++) {
//        if (items[i].type.indexOf("image") === 0) {
//          blob = items[i].getAsFile();
//        }
//      }
////    // load image if there is a pasted image
////    if (blob !== null) {
////      var reader = new FileReader();
////      reader.onload = function(event) {
////        console.log(event.target.result); // data url!
////        document.getElementById("pastedImage").src = event.target.result;
////      };
////      reader.readAsDataURL(blob);
////    }
//    }




/***/ }),

/***/ 214:
/***/ (function(module, exports) {

var CHARACTER_SET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

/**
 *
 * @param {Number} int
 * @param {Number||Boolean} zeropad - Defaults to false
 * @returns {string}
 */
function intToB62(int, zeropad=false){
  var integer = Math.round(int);
  var s = integer===0 ? '0' : '';
  while (integer > 0) {
    s = CHARACTER_SET[integer % 62] + s;
    integer = Math.floor(integer/62);
  }
  return zeropad ? ("0000000000000000000000000000000"+s).slice(-zeropad) : s;
}

/**
 *
 * @param {string} base62String
 * @returns {Integer}
 */
function b62ToInt(base62String) {
  var val = 0;
  var base62Chars = base62String.split("").reverse();
  base62Chars.forEach(function(character, index){
    val += CHARACTER_SET.indexOf(character) * Math.pow(62, index);
  });
  return val;
}

/**
 *
 * @param {Date} date
 * @returns {String}
 */
function dateToB62(date = new Date()) {
  return intToB62(date.getTime(), 7);
}

/**
 *
 * @param {string} base62String
 * @returns {Date}
 */
function b62ToDate(base62String) {
  return new Date(b62ToInt(base62String));
}


module.exports = {intToB62, b62ToInt, dateToB62, b62ToDate};

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_modules_itemstore__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_modules_simplepainter__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_modules_domhelpers__ = __webpack_require__(101);






//require("../js/libs/pdf.js"); //apparently must be loaded from script tag

const fileSelector = document.querySelector(".fileSelector");
const pdfPreview = document.querySelector(".pdf--preview");

let store = new __WEBPACK_IMPORTED_MODULE_1_modules_itemstore__["a" /* Store */]("slideshow", {
  dbcentral: "mydb/db",
  dbname: "slides",
  leavesonly: false,
  longpolling: false,
  keysByType: {}
});

store.dNs.mountShelf("slideshowstates", {activeSlide: null});

let simplepainter = new __WEBPACK_IMPORTED_MODULE_2_modules_simplepainter__["a" /* Simplepainter */]({itemstore: store, onlypen: false});

store.subscribe("ready", {onReady: _=>pickSlidesOfThisfile()});
store.subscribe("dbChange", {onDbChange({action, args}) {
  if(action === "deleteItem" && args[0].startsWith("pdfSlide")) {
    deleteSlide(args[0]);
  }
}});

store.subscribe("stateChange", ({action, current, old}) => {
  if(action==="changeShelfStates" && current.activeSlide !== old.activeSlide) {
    showSlide(slides.pick({idn: current.activeSlide}));
    simplepainter.setOptions();
  }
}, "slideshowstates");

let slides = [];
//itemworld.$container.on("loaded", pickSlidesOfThisfile);

function pickSlidesOfThisfile() {
  log("loaded");
  slides = store.getLeavesAsArray().filter(i=>i.type==="pdfSlide" && i.file===mypdf.file);
}

// let win = window.open("showSlides.html", "_blank", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=1280, height=800, left=800');
// win.moveTo(400,0);
// $(window).on("beforeunload", function() {win.close();});

let mypdf = {
  file: null,
  path: 'pdf/',
  url: null,
  data: null,
  numPages: 0,
  defaultWidth: 595,
  activeSlide: null
};

let temp = {};

let settings = {
  previewWidth: 200, //auch in css ändern
  ratio: 16/10,
  w_max: 640,
};

window.onload = load;

function getOffsetFromPage(event, el) {
  let {x, y} = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3_modules_domhelpers__["a" /* getPosition */])(el);
  return {x: event.pageX - x, y: event.pageY - y + pdfPreview.scrollTop};
}

__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3_modules_domhelpers__["b" /* on */])(".pdf--preview", "pointerdown", ".pdfPage", function(e) {
  let {x, y} = getOffsetFromPage(e, this);
  temp.startx = x;
  temp.starty = y;
  temp.page = +this.dataset.page;
  if(temp.rect) temp.rect.remove();
  temp.rect = $("<div class='rect temprect hidden'></div>").appendTo(this);
});

__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3_modules_domhelpers__["b" /* on */])(".pdf--preview", "pointermove", ".pdfPage", function(e) {
  if(!temp.rect) return;
  let {x, y} = getOffsetFromPage(e, this);
  let rect = {
    x: Math.min(temp.startx, x),
    y: Math.min(temp.starty, y),
    w: Math.abs(temp.startx - x),
    h: Math.abs(temp.starty - y)
  };
  if(rect.w > 5 && rect.h > 5) temp.rect.removeClass("hidden");
  temp.rect.css({left: rect.x, top: rect.y, width: rect.w, height: rect.h})
});

__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3_modules_domhelpers__["b" /* on */])(".pdf--preview", "pointerup", ".pdfPage", function(e) {
  let {x, y} = getOffsetFromPage(e, this);
  let {width} = this.getBoundingClientRect();
  let newSlide = {
    file: mypdf.file,
    x: 100 * Math.min(temp.startx, x)  / width,
    y: 100 * Math.min(temp.starty, y)  / width,
    w: 100 * Math.abs(temp.startx - x) / width,
    h: 100 * Math.abs(temp.starty - y) / width,
    page: temp.page
  };
  if(temp.rect) temp.rect.remove();
  if(newSlide.w > 5 && newSlide.h > 5 && +newSlide.page === +this.dataset.page) {
    store.dDb.addItem("pdfSlide", newSlide).then(newSlideIdn =>{
      let newSlideItem = store.getItem(newSlideIdn);
      slides.push(newSlideItem);
      drawRect(newSlideItem);
      showSlide(newSlideItem);
    });
  }
});

__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3_modules_domhelpers__["b" /* on */])(".pdf--preview", "click", ".slideRect", function(e) {
  e.preventDefault();
  store.dNs.changeShelfStates("slideshowstates", {activeSlide: $(this).data("slideidn")});
});

//$(".pdf--slide").on("pointermove", function(e) {
//  localStorage.setItem("pdfSlideMouse", "["+ e.offsetX+","+e.offsetY+"]");
//});
//
//$(".pdf--slide").on("pointerleave", function(e) {
//  localStorage.setItem("pdfSlideMouse", "[-100,-100]");
//});

function load() {
  __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__["a" /* post */])("/inventory/slide/pdf").then(l=>{
    let options = l.map((f,i)=>`<option ${i===0?"selected":""} value="${f}">${f}</option>`);
    fileSelector.innerHTML = options.join("");
    fileSelector.addEventListener("change", setup);
    setup();
  });
}

function setup() {
  log("setup");
  mypdf.file = fileSelector.value;
  mypdf.url = mypdf.path + mypdf.file;
  localStorage.pdfviewer__file = mypdf.url;
  log($('.pdf--preview').length);
  $('.pdf--preview').empty();
  pickSlidesOfThisfile();
  PDFJS.getDocument(mypdf.url).then(function(pdf) {
    mypdf.data = pdf;
    mypdf.numPages = pdf.numPages;
    mypdf.data.getPage(1).then(function(p) {
      mypdf.defaultWidth = p.getViewport(1.0).width;
      renderPdfs();
    });
  });
}

function renderPdfs() {
  renderPreview().then(function() {
    slides.forEach(drawRect);
    if(slides.length) showSlide( slides.pick({idn: store.namedStates.slideshowstates.activeSlide}) || slides[0] );
  });
}

function renderPreview() {
  let p = Promise.resolve();
  for(let i = 1; i <= mypdf.numPages; i++) {
    p = p.then( _ => mypdf.data.getPage(i) ).then( page => addPreviewPage(i, page) );
  }
  return p;
}

function addPreviewPage(num, page) {
  let $nextPage = $(`<div class='pdfPage page--${num}' data-page="${num}"></div>`).appendTo('.pdf--preview');
  return renderPageInDiv(page, $nextPage).promise;
}

function renderPageInDiv( page, $div ) {
  let canvas = $("<canvas class='pdfCanvas'></canvas>").appendTo($div).get(0);
  let context = canvas.getContext('2d');
  let outputScale = getOutputScale(context);
  canvas.width = Math.floor($(canvas).width() * outputScale) | 0;
  let viewport = page.getViewport( $(canvas).width() / mypdf.defaultWidth, 0 );
  canvas.height = viewport.height/viewport.width * canvas.width;
  context._scaleX = outputScale;  context._scaleY = outputScale;
  if (outputScale!==1) {
    context.scale(outputScale, outputScale);
  }
  return {promise: page.render({canvasContext: context, viewport: viewport}), canvas, context, outputScale, viewport};
}

function showSlide(slide) {
  localStorage.setItem("pdfActiveSlide", slide.idn);
  $(".slideRect").removeClass("activeSlide").filter(function(){return $(this).data("slideidn") == slide.idn}).addClass("activeSlide");
  mypdf.activeSlide = slide.idn;
  mypdf.data.getPage(slide.page).then(renderSlide.bind(null, slide));
}

function renderSlide(slide, page) {
  $(".hiddenSlide").remove();
  document.querySelector(".pdf--slide.simplepainter").dataset.painter = slide.idn;
  let $hiddenSlide = $(`<div class='hiddenSlide'></div>`).appendTo('.pdf--slide');
  let maxWidth = Math.min(settings.w_max, slide.w/slide.h*settings.w_max/settings.ratio);  //w_max = 640 und h_max = 640/ratio
  let $hiddenSlideInner  = $(`<div class='hiddenSlideInner' style="width: ${maxWidth * 100 / slide.w}px"></div>`).appendTo($hiddenSlide)
  let {promise, context} = renderPageInDiv(page, $hiddenSlideInner);
  let slidecanvas = $(".slide--canvas").get(0);
  let slidecontext = slidecanvas.getContext('2d');
  let outputScale = getOutputScale(slidecontext);
  slidecanvas.width = Math.floor($(slidecanvas).width() * outputScale) | 0;
  //$(slidecanvas).height(Math.min(640/settings.ratio, $(slidecanvas).width() *  slide.h/slide.w );
  slidecanvas.height = Math.floor($(slidecanvas).height() * outputScale) | 0;  //slide.h/slide.w * slidecanvas.width;
  slidecontext._scaleX = outputScale;  slidecontext._scaleY = outputScale;
  if (outputScale!==1) {
    slidecontext.scale(outputScale, outputScale);
  }
  promise.then(function() {
    let imgData = context.getImageData(maxWidth*outputScale*slide.x/slide.w, maxWidth*outputScale*slide.y/slide.w, maxWidth*outputScale, maxWidth*outputScale*slide.h/slide.w);
    slidecontext.putImageData(imgData, outputScale*(settings.w_max-maxWidth)*0.5, 0);
  });
  

  //TODO besser machen, damit es auch bei Update funktioniert. Immer neue Tree?
  let roots = store.getLeavesAsArray().filter(i => i.type==="sketch" && i.tree_parent === "" && i.painter===slide.idn);
  let strokes = store.getLeavesAsArray().filter(i => roots.some(r=>store.isThatMy("ancestor", i.idn, r.idn)));
  //let tree = $(".pdf--sketchlist").empty()[0].tree;
  simplepainter.clear();
  //roots.forEach(s=>tree.renderTreeitem($(".pdf--sketchlist").append("<div></div>"), s));
  //log("try load stroke", strokes.length, store.getLeavesAsArray());
  strokes.forEach(s=>simplepainter.loadStroke(s.idn));
}

function drawRect(slide) {
  let rect = $(`<div class='rect slideRect' data-slideidn="${slide.idn}"></div>`).appendTo(`.pdfPage.page--${slide.page}`);
  let deleteButton = $(`<div class="deleteSlide" data-slideidn="${slide.idn}">X</div>`).appendTo(rect);
  deleteButton.on("click", function(e) {
    store.dDb.deleteItem(this.dataset.slideidn);
  });
  let f = $(`.pdfPage.page--${slide.page}`).width()/100;
  rect.css({left: f*slide.x, top: f*slide.y, width: f*slide.w, height: f*slide.h})
}

function deleteSlide(idn) {

}

function getOutputScale(ctx) {
  let devicePixelRatio = window.devicePixelRatio || 1;
  let backingStoreRatio = ctx.webkitBackingStorePixelRatio || ctx.mozBackingStorePixelRatio ||
      ctx.msBackingStorePixelRatio || ctx.oBackingStorePixelRatio || ctx.backingStorePixelRatio || 1;
  return devicePixelRatio / backingStoreRatio;
}

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return msg; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return choosebox; });
const msg = (html) => {
  let div = document.createElement("div");
  let overlay = document.createElement("div");
  overlay.classList.add("overlay");
  div.classList.add("message");
  div.innerHTML = html;
  document.body.appendChild(overlay);
  document.body.appendChild(div);
  let removeMe = e=>{
    document.body.removeChild(div);
    document.body.removeChild(overlay);
  };
  overlay.addEventListener("click", removeMe);
  div.addEventListener("click", removeMe);
};

const choosebox = (html, options, callback) => {
  var div = document.createElement("div");
  var overlay = document.createElement("div");
  var btn = document.createElement("button");
  overlay.classList.add("overlay");
  div.classList.add("message");
  div.classList.add("choosebox");
  let chooseMe = (i) => {
    document.body.removeChild(div);
    document.body.removeChild(overlay);
    callback(i);
  };
  div.innerHTML = html;
  options.forEach((o,i)=>{
    let btn = document.createElement("button");
    btn.classList.add("choosebox__option");
    btn.addEventListener("click", e=>chooseMe(i));
    btn.innerHTML = o;
    div.appendChild(btn);
  });
  document.body.appendChild(overlay);
  document.body.appendChild(div);
};

var css = `
.overlay {
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0%;
  left: 0%;
  background-color: #888;
  opacity: 0.2;
}

.message {
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 70vw;
  padding: 2em;
  background-color: white;
  opacity: 1;
}

.choosebox__option {
  display: block;
  width: 100%;
  background-color: #ddd;
  border: none;
  padding: 0;
  margin: 0.3em 0;
  text-align: left;
  cursor: pointer;
}

.choosebox__option:hover {
  color: #003366;
  background-color: #ccc;
}
`;
var style = document.createElement('style');
style.type = 'text/css';
if (style.styleSheet){
  style.styleSheet.cssText = css;
} else {
  style.appendChild(document.createTextNode(css));
}

document.head.appendChild(style);




/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return post; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return postCors; });
/**
 *
 * @param url
 * @param data
 * @returns {Promise}
 */
function post(url, data) {
  return window.fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  .then(checkStatus)
  .then(extractContent)
}

/**
 *
 * @param url
 * @param data
 * @returns {Promise}
 */
function postCors(url, data) {
  return window.fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, text/json, */*',
      'Content-Type': 'application/x-www-form-urlencoded' // instead of application/json because of cors
    },
    body: JSON.stringify(data)
  })
  .then(checkStatus)
  .then(extractContent)
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response);
  } else {
    console.log(response);
    return Promise.reject(new Error("invalid status text: " + response.statusText))
  }
}

function extractContent(r) {
  var ct = r.headers.get("content-type");
  if(~ct.indexOf("json") || ~ct.indexOf("x-www-form-urlencoded")) return r.json();
  if(~ct.indexOf("text")) return r.text();
  return r;
}




/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Store; });
/* unused harmony export Item */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_modules_messages__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_modules_helpers__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_modules_helpers___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_modules_extendNatives__ = __webpack_require__(99);
//let Immutable = require("Immutable");
//let store = Immutable.fromJS({a:1, b: [1,2,3]});







let defaultSettings = {
  dbcentral: "mydb/db", //can also be "MEMORY"
  dbname: "main",
  leavesonly: false,
  longpolling: true,
  keysByType: {},
  logActions: true
};

let defaultGlobal = {
  globalHistory: {show: false, undoneIdvs: [], redoneIdvs: [], maxItems: 10}
};

class Store {
  constructor(name, settings = {}) {
    this.name = name;

    this.settings = Object.assign({}, defaultSettings, settings);
    this.namedStates = {};
    this.namedStates.global = Object.assign({}, defaultGlobal, JSON.parse(localStorage[this.name + "_state_global"] || "{}")); //loadStates;
    this.dbStates = {};

    this.createDispatchers();
    this.ready = false;
    this.fromUndo = false;
    this.channels = {}; //ready, stateChange, dbChange, instantchange, ...
    this.start();
  }
  get undoneIdvs() {return this.namedStates.global.globalHistory.undoneIdvs}
  get redoneIdvs() {return this.namedStates.global.globalHistory.redoneIdvs}
  subscribe(channel, target, subchannel = "", methodname) {
    if(!this.channels[channel]) this.channels[channel] = [];
    let mn = methodname || ("on"+channel.slice(0,1).toUpperCase()+channel.slice(1));
    let listener = {target, subchannel, methodname: mn};
    this.channels[channel].push(listener);
    return listener;
  }
  publish(channel, msg, subchannel = "") {
    if(!this.channels[channel]) return;
    this.channels[channel].forEach(listener => {
      if(listener.target && listener.subchannel === subchannel) {
        typeof listener.target === "function" ? listener.target(msg) : listener.target[listener.methodname](msg);
      }
    });
  }
  unsubscribe(targetOrListener) {
    for(let i in this.channels) {
      this.channels[i] = this.channels[i].filter(l => l.target !== targetOrListener && l !== targetOrListener);
    }
  }
  start() {
    return this.loadItemsFromScratch()
    .then(o=>{
      this.ready = true;
      this.publish("ready");
      document.body.addEventListener("keydown", e => {
        if(e.which === 90 && e.shiftKey && e.ctrlKey) {
          this.dNs.toggle("global", "globalHistory.show");
        }
      });
    })
    .then(()=>this.settings.longpolling ? this.poll() : false)
    .catch(e=>__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["warn"])(e));
  }
  loadItemsFromScratch() {
    if(this.settings.dbcentral === "MEMORY") {
      this.handleItems.init();
      return Promise.resolve("initialized from Memory");
    }
    return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__["a" /* post */])(this.settings.dbcentral, {dbname: this.settings.dbname, task: this.settings.leavesonly ? "getLeaves" : "getAll"})
    .then(o=> {
      this.handleItems.init();
      this.handleItems.addItems(Object.keys(o).map(idv => (new Item(idv, JSON.parse(o[idv])) )));
      //use dispatcher?
      this.settings.keysByType = Object.assign(this.settings.keysByType, this.getKeysByType(this.getLeavesAsArray()));
    }).catch(e=>__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["warn"])(e));
  }
  poll() {
    if(this.settings.dbcentral === "MEMORY") return;
    if(document.hidden) {
      setTimeout(()=>this.poll(), 1000);
    }
    else {
      __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])("polling...");
      __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__["a" /* post */])(this.settings.dbcentral, {dbname: this.settings.dbname, task: "pollForChanges", since: this.dbStates.latest}).then(r=>{
        if(!this.willunmount) {
          if(r === "nochanges") {
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])("nochanges");
          }
          else {
            let external = Object.keys(r).filter(newv => !this.dbStates.versions.some(v=>v.idv === newv));
            let newVersions = external.map(idv => (new Item(idv, JSON.parse(r[idv])) ));
            if(newVersions.length) {
              this.dDb.gotRemoteItems(newVersions);
            }
          }
          setTimeout(()=>this.poll(), 1000);
        }
      }).catch(e=>{__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["warn"])(e); setTimeout(()=>this.poll(), 1000);});
    }
  }
  waitUntilReady() {
    return new Promise((resolve, reject)=>{
      if(this.ready) resolve(this);
      else this.subscribe("ready", _ => resolve(this) );
    });
  }
  moveOldToArchive() {
    if(this.settings.dbcentral === "MEMORY") {
      return this.loadItemsFromScratch();
    }
    let p = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__["a" /* post */])(this.settings.dbcentral, {dbname: this.settings.dbname, task: "moveOldToArchive"}).then(r=>__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(r));
    return p.then(_=>this.loadItemsFromScratch());
  }
  createDispatchers() {
    this.dispatchSettings = {
      //this.eventEmitter.dispatchEvent(new CustomEvent("ready", {detail: "ready"}));
    };
    this.dispatchNamed = {
      mountShelf: (shelfname, values) => {
        //loadStates
        let shelfstate = Object.assign({}, values, JSON.parse(localStorage[this.name+"_state_"+shelfname] || "{}")); //loadStates
        this.namedStates[shelfname] = shelfstate;
      },
      changeShelfStates: (shelfname, newStates) => {
        this.namedStates[shelfname] = Object.assign({},  this.namedStates[shelfname], newStates);
      },
      setShelfStateOfIdn: (shelfname, itemstate, idn, value) => {
        let oldItemstate = this.namedStates[shelfname][itemstate];
        let newItemstate = value === true ? oldItemstate.addUnique(idn) : oldItemstate.deleteWhere(i=>i === idn);
        this.namedStates[shelfname] = Object.assign({},  this.namedStates[shelfname], {[itemstate]: newItemstate});
      },
      toggleShelfStateOfIdn: (shelfname, itemstate, idn) => {
        this.dispatchNamed.setShelfStateOfIdn(shelfname, itemstate, idn, !this.namedStates[shelfname][itemstate].includes(idn));
      },
      toggle: (shelfname, pathArrayOrString) => {
        this.dispatchNamed.setValue(shelfname, pathArrayOrString, null, true);
      },
      setValue: (shelfname, pathArrayOrString, val, toggle=false) => {
        let old = this.namedStates[shelfname];
        this.namedStates[shelfname] = Object.assign({}, this.namedStates[shelfname]);
        let pathToVariable = this.namedStates[shelfname];
        let patharray = pathArrayOrString instanceof Array ? pathArrayOrString : pathArrayOrString.split(".");
        patharray.slice(0,-1).forEach(p => {
          pathToVariable = Object.assign({}, pathToVariable);
          pathToVariable = pathToVariable[p];
        });
        let nameOfVariable =  patharray.slice(-1)[0];
        pathToVariable[nameOfVariable] = toggle ? !pathToVariable[nameOfVariable] : val;
      }
    };
    this.dispatchDb = {
      addItem: (itemtype, newDoc) => {
        let idn = itemtype + "-" + this.getTimestamp() + "-" + this.getUsr();
        let idv = idn + "-" + this.lastTimestamp + "-" + this.getUsr() + ".json";
        return this.dispatchDb.addVersions([idv], [newDoc], [""], [""]).then(_=>idn);
      },
      debouncedUpdate: (idn, obj) => {
        if(Object.keys(obj).length !== 1) throw new Error("Debounced update must only change one key!");
        let key = Object.keys(obj)[0];
        let val = obj[key];
        let item = this.dbStates.leaves[idn];
        let getDebounceHandler = () => {
          let r = null;
          let t = null;
          let p = new Promise(resolve => {
            r = resolve;
            t = setTimeout(()=>resolve(this.dispatchDb.dispatchDebouncedUpdate(idn)), 2000);
          });
          return {promise: p, resolver: r, timeout: t};
        };
        if(item.debounced) {
          if(item.debounced.key === key) {
            clearTimeout(item.debounced.handler.timeout);
            this.handleItems.updateDebouncedIdn(idn, {val, handler: getDebounceHandler()});
          }
          else {
            this.dispatchDebouncedUpdate(idn).then(
              _ => item.dispatchDb.debouncedUpdate(idn, {[key]: val})
            );
          }
        }
        else {
          this.handleItems.updateDebouncedIdn(idn, {key, val, originalval: item[key], handler: getDebounceHandler()});
        }
      },
      dispatchDebouncedUpdate: (idn) => {
        let item = this.dbStates.leaves[idn];
        clearTimeout(item.debounced.handler.timeout);
        let {key, val, originalval, handler} = item.debounced;
        this.handleItems.updateDebouncedIdn(idn, "delete");
        let p;
        if(JSON.stringify(originalval) === JSON.stringify(val)) {
          __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])("changed back to original value, no update");
          p = Promise.resolve("changed back to original value, no update");
        }
        else {
          p = this.dispatchDb.updateItem(idn, {[key]: val});
        }
        handler.resolver(p);
        return p;
      },
      //Single item
      deleteItem:   (idn) => this.dispatchDb.deleteItems([idn]),
      undeleteItem: (idn) => this.dispatchDb.undeleteItems([idn]),
      updateItem: (idn, obj) => this.dispatchDb.updateItems([idn], [obj]),
      replaceItem: (idn, newDoc) => this.dispatchDb.replaceItems([idn], [newDoc]),
      //Array of items
      deleteItems:   (idns) => this.dispatchDb.replaceItems(idns, false, "delete"),
      undeleteItems: (idns) => this.dispatchDb.replaceItems(idns, false, "undelete"),
      updateItems: (idns, objs) => this.dispatchDb.replaceItems(idns, objs, "updateKeys"),
      replaceItems: (idns, newDocsOrObjs, task = "update") => {
        let prevItems = idns.map(idn => this.dbStates.leaves[idn]);
        if(prevItems.some(i=>i.debounced)) {
          return Promise.all(
            prevItems.filter(i=>i.debounced).map(i=>this.dispatchDb.dispatchDebouncedUpdate(i.idn))
          ).then(_=>this.dispatchDb.replaceItems(idns, newDocsOrObjs, task));
        }
        let timestamp = this.getTimestamp();
        let prevIdvs = prevItems.map(item => item.idv);  // ?? let versionsToChange = action.previousIdvs.map(idv=>n.versions.pick({idv}));
        let prevDocs = prevItems.map(item => item.doc);  // ?? let prevDocs = versionsToChange.map(v=>v.doc);
        let newDocs = newDocsOrObjs;
        if(!newDocs) newDocs = prevDocs;
        if(task === "updateKeys") newDocs = newDocsOrObjs.map((obj, index) => Object.assign({}, this.dbStates.leaves[idns[index]].doc, obj));
        if(task!=="undelete" && task!=="delete") {
          if(newDocs.every((doc, i)=>JSON.stringify(doc)===JSON.stringify(prevDocs[i]))) {
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])("no items changed");
            return "no items changed"
          }
        }
        let getNewFlags = (olditem, task) => {
          let newFlags = olditem.flags;
          if(task==="delete") newFlags = ~newFlags.indexOf("x") ? newFlags : "x"+newFlags;
          if(task==="undelete") newFlags = newFlags.replace(/x/ig, "");
         return newFlags ? "-"+newFlags : "";
        };
        let idvs = idns.map( (idn, index) => idn+ "-" + timestamp + "-" + this.getUsr() + getNewFlags(prevItems[index], task) + ".json");
        return this.dispatchDb.addVersions(idvs, newDocs, prevIdvs, prevDocs);
      },
      addVersions: (idvs, newDocs, prevIdvs, prevDocs, fromLocal=true) => {
        if(!fromLocal) {alert("Noch nicht implementiert: MultiUser")} //@TODO
        idvs.forEach((idv, index) => {
          let item = new Item(idv, newDocs[index]);
          let idn = item.idn;
          this.handleItems.addItems([item]);
          //this.handleItems.addValueToArray(idn, "dirtyIdns");
          this.handleItems.addValueToArray(idv, "pendingIdvs");
        });
        //UndoRedo //!differs from react version in dev folder!!
        this.dNs.setValue("global", "globalHistory", Object.assign({}, this.namedStates.global.globalHistory, {
          undoneIdvs: this.fromUndo ? this.undoneIdvs.concat(idvs) : this.undoneIdvs,
          redoneIdvs: this.fromRedo ? this.redoneIdvs.concat(idvs) : this.redoneIdvs,
        }));
        return this.dispatchDb.sendVersionsToDb(idvs, newDocs, prevIdvs, prevDocs);
      },
      sendVersionsToDb: (newIdvs, newDocs, prevIdvs, prevDocs) => {
        //at the moment, this returns a resolved promise with a nested dbprom (resolve after finished request)
        if(this.settings.dbcentral === "MEMORY") {
          return Promise.resolve({dbprom: Promise.resolve(this.dDb.dbResponse("updates ok!", newIdvs, newDocs, prevIdvs, prevDocs)), newIdvs});
        }
        let dbprom = new Promise(resolve=>{
          this.dbStates.updateQueue = this.dbStates.updateQueue.then(r=>{
            let p = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_modules_ajaxhelpers__["a" /* post */])(this.settings.dbcentral,
                {dbname: this.settings.dbname, task: "addUpdatedVersions", idvs: newIdvs, previousIdvs: prevIdvs, data: newDocs.map(newDoc=>JSON.stringify(newDoc))}
            ).then(r=>this.dDb.dbResponse(r, newIdvs, newDocs, prevIdvs, prevDocs));
            resolve(p);
          });
        });
        return Promise.resolve({dbprom, newIdvs});
      },
      dbResponse: (r, newIdvs, newDocs, prevIdvs, prevDocs) => {
        if(r === "updates ok!") {
          newIdvs.forEach(idv => {this.handleItems.removeValueFromArray(idv, "pendingIdvs")} );
          return newIdvs;
        }
        else if(r.indexOf("Konflikte mit Eintrag von anderswo:") === 0) { //@TODO
          console.groupCollapsed("Konflikte mit Eintrag von anderswo:");
          let conflictingWrap = JSON.parse(r.slice("Konflikte mit Eintrag von anderswo:".length));
          let newDocsNextTry = conflictingWrap.map((c, index) => {
            this.handleItems.removeItems([this.dbStates.versions.find(i=>i.idv==newIdvs[index])]); //delete also the pending even if no error
            if(!c) {
              return newDocs[index];
            }
            let conflicting = Object.keys(c)[0];
            let confl = JSON.parse(c[conflicting]);
            let conflictingItem = new Item(conflicting, confl);
            this.handleItems.addItems([conflictingItem]);
            let prev = prevDocs[index].clone();
            let self = newDocs[index].clone();
            let merged = prev.clone();
            let errors = [];
            for(let key in confl) {
              if(self[key]===undefined && prev[key]===undefined)      {merged[key] = confl[key]; __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Conflicting added new key ${key}`);}
              if(self[key] == prev[key] && prev[key] !== confl[key])  {merged[key] = confl[key]; __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Conflicting changed key ${key}, self didn't.`);}
              if(self[key] !== prev[key] && self[key] === confl[key]) {merged[key] = confl[key]; __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Both changed key ${key} to same value.`);}
              if(self[key] == prev[key] && prev[key] === confl[key])  {merged[key] = confl[key]; __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Both did not change key ${key}.`);}
            }
            for(let key in self) {
              if(confl[key]===undefined && prev[key]===undefined)     {merged[key] = self[key];  __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Self added new key ${key}`);}
              if(confl[key] == prev[key] && prev[key] !== self[key])  {merged[key] = self[key];  __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Self changed key ${key}, conflicting didn't.`);}
              if(confl[key] !== prev[key] && prev[key] !== self[key] && confl[key] !== self[key]) {merged = false; errors.push({key, self: self[key], confl: confl[key]}); __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(`Both changes key ${key} to different value.`); break;}
            }
            if(merged!==false) {
              __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])("item was merged with conflicting item:", merged);
              console.groupEnd();
              return merged;
            }
            else {
              console.groupEnd();
              __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_modules_messages__["a" /* msg */])("<h3>Aktion konnte nicht abgeschlossen werden!</h3><div>Eine andere Quelle hat den Eintrag ebenfalls geändert.</div>"
                  + errors.map(e=>`<div>Ihr Vorschlag für ${e.key}:</div><div>${e.self}</div><div>Externer Vorschlag:</div><div>${e.confl}</div>`)
              + "<div>Bitte beheben Sie den Konflikt.</div>");
              return "error";
            }
          });
          if(!newDocsNextTry.some(e=>e=="error")) {
            return this.dispatchDb.replaceItems(newIdvs.map(Item.getIdnFromIdv), newDocsNextTry);
          }
          else return false;
        }
      },
      gotRemoteItems(newVersions) {
        return newVersions.map(item => {
          if(this.dbStates.leaves[item.idn].debounced) __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["warn"])("check what happens if remote updates debounced");
          this.handleItems.addItems([item]);
          return item;
        });
      }
    };
    this.dDb = {};
    for(let key in this.dispatchDb) {
      this.dDb[key] = (...args) => {
        if(this.settings.logActions) {
          console.groupCollapsed(key);
          __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(args);
        }
        this.fromUndo = args.includes("undo"); //UndoRedo, differs from version in dev folder (react)
        this.fromRedo = args.includes("redo");
        setTimeout(() => {
          this.publish("dbChange", {action: key, args});
          this.settings.logActions && console.groupEnd();
        }, 0);
        return this.dispatchDb[key].apply(this, args);
      }
    }
    this.dNs = {};
    for(let key in this.dispatchNamed) {
      this.dNs[key] = (...args) => {
        if(this.settings.logActions) {
          console.groupCollapsed(key);
          __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(args);
        }
        let shelfname = args[0];
        let old = this.namedStates[shelfname] || {};
        setTimeout(()=>{
          //save states
          localStorage[this.name+"_state_"+shelfname] = JSON.stringify(this.namedStates[shelfname] || {});
          this.publish("stateChange", {action: key, old, current: this.namedStates[shelfname]}, shelfname); //subchannel shelfname
          this.settings.logActions && console.groupEnd();
        }, 0);
        return this.dispatchNamed[key].apply(this, args);
      }
    }
    // Helpers to make dbStates Immutable
    this.handleItems = {
      init: () => {
        this.dbStates = {
          versions: [],
          versiongroups: {},
          leaves: {},
          latest: "0",
          pendingIdvs: [],
          updateQueue: __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["newP"])()
        }
      },
      addItems: (items)=> {
        let a = this.dbStates;
        let dbs = this.dbStates = Object.assign({}, this.dbStates);
        items.forEach(i=> {
          let pos = dbs.versions.findIndex(v=>v.modified <= i.modified);
          pos = pos == -1 ? dbs.versions.length : pos;
          dbs.versions = [...dbs.versions.slice(0, pos), i, ...dbs.versions.slice(pos)];
          dbs.versiongroups = Object.assign({}, dbs.versiongroups);
          dbs.versiongroups[i.idn] = [i.idv].concat(dbs.versiongroups[i.idn]||[]);
          dbs.versiongroups[i.idn].sort((a,b)=>a>b?-1:a<b?+1:0);
          dbs.leaves = Object.assign({}, dbs.leaves, {[i.idn]: dbs.versions.find(v=>v.idn == i.idn)});
          dbs.latest = dbs.versions.length ? dbs.versions[0].modified : "0";
        });
      },
      removeItems: (items)=> {
        let dbs = this.dbStates = Object.assign({}, this.dbStates);
        items.forEach(i=> {
          dbs.versions = dbs.versions.filter(v=>v.idv !== i.idv);
          dbs.versiongroups = Object.assign({}, dbs.versiongroups);
          dbs.leaves = Object.assign({}, dbs.leaves);
          if(dbs.versiongroups[i.idn].length ==1) {
            delete dbs.versiongroups[i.idn];
            delete dbs.leaves[i.idn];
          }
          else {
            dbs.versiongroups[i.idn] = dbs.versiongroups[i.idn].filter(v=>v!==i.idv);
            dbs.leaves[i.idn] = dbs.versions.find(v=>v.idn == i.idn);
          }
          if(dbs.pendingIdvs.includes(i.idv)) {dbs.pendingIdvs = dbs.pendingIdvs.filter(idv=>idv!==i.idv);}
        });
      },
      updateDebouncedIdn: (idn, obj) =>{
        let item = this.dbStates.leaves[idn];
        let clonedItem = Object.assign(new Item(item.idv, item.doc), item);
        if(obj=="delete") {
          delete clonedItem.debounced;
        }
        else {
          clonedItem.debounced = Object.assign({}, clonedItem.debounced, obj);
        }
        this.handleItems.removeItems([item]);
        this.handleItems.addItems([clonedItem]);
        if(obj!=="delete") {
          this.publish("instantchange", {old: item, current: clonedItem}, clonedItem.idn);
        }
      },
      addValueToArray: (val, array) => {
        this.dbStates = Object.assign({}, this.dbStates, {[array]: this.dbStates[array].concat([val]).unique()});
      },
      removeValueFromArray: (val, array) => {
        this.dbStates = Object.assign({}, this.dbStates, {[array]: this.dbStates[array].filter(v=>v!==val)});
      }
    };
    this.isLeaf = v => this.dbStates.versiongroups[v.idn][0] == v.idv;
    this.getLeavesAsArray = () => this.dbStates.versions.filter(v=>!v.deleted && this.isLeaf(v));
    this.getTimestamp = (lastTimestamp) => {
      let time = new Date().getTime()+31536000000;
      let timestamp = time.toString(36);
      while(timestamp === this.lastTimestamp || timestamp <= this.dbStates.latest) {
        timestamp = (time+1).toString(36)
      }
      this.dbStates.latest = timestamp;
      this.lastTimestamp = timestamp;
      return timestamp;
    };
    this.getUsr = () => "l"; //@TODO
    this.getItem = (idn) => this.dbStates.leaves[idn];
    this.timestampToDate = (t) => new Date(parseInt(t, 36) - 31536000000);
  }
  getKeysByType(versions) {
    let keysByTypeFomProps = this.settings.keysByType || {};
    let keysByType = {};
    let types = versions.map(v=>v.type).unique();
    types.forEach(type=>{
      keysByTypeFomProps[type] = keysByTypeFomProps[type] || {};
      keysByType[type] = {};
      let itemsOfType = versions.filter(v=>v.type == type);
      let keys = Object.keys(keysByTypeFomProps[type]).concat(itemsOfType.map(v=>Object.keys(v.doc).join(",")).join(",").split(",")).unique(); //muss so kompliziert sein wegen Reihenfolge
      keys.forEach(key => {
        keysByTypeFomProps[type][key] = keysByTypeFomProps[type][key] || {};
        keysByType[type][key] = {
          key: key,
          isPublic: !~key.indexOf("_"),
          isNumeric: itemsOfType.map(v=>v.doc[key]).filter(x=>!!x).every(x=>parseFloat(x)==x)
        };
        keysByType[type][key] = Object.assign(keysByType[type][key], keysByTypeFomProps[type][key]);
      });
    });
    return keysByType;
  }
  
  //Undo, Redo Methods
  undo() {
    let v = this.getVersionToUndoOrRedo("undo");
    if(!v) return console.warn("no undo possible");
    this.revertVersion(v, "undo");
  }
  redo() {
    let v = this.getVersionToUndoOrRedo("redo");
    if(!v) return console.warn("no redo possible");
    this.revertVersion(v, "redo");
  };
  getVersionToUndoOrRedo(undoOrRedo = "undo") {
    let vs = this.dbStates.versions.slice(0).sort((u,v)=> u.modified > v.modified ? -1 : 1);
    let index = 0;
    let skippingstoDo = 0;
    let coll = undoOrRedo === "undo" ? this.undoneIdvs : this.redoneIdvs;
    while(index < vs.length && (coll.includes(vs[index].idv) || skippingstoDo > 0)) {
      skippingstoDo += (coll.includes(vs[index].idv) ? +2 : +0) - 1;
      index++;
    }
    let candidate = vs[index];
    return candidate && (undoOrRedo === "undo" || this.undoneIdvs.includes(candidate.idv)) ? candidate:  false;
  }
  revertVersion(v, undoOrRedo="undo") {
    let history = this.dbStates.versiongroups[v.idn];
    let prevIdv = history[history.indexOf(v.idv) + 1] || false;
    if(!prevIdv || this.dbStates.versions.find(i=>i.idv===prevIdv).deleted) {
      this.dDb.deleteItem(v.idn, undoOrRedo)
    }
    else if(v.deleted) {
      this.dDb.undeleteItem(v.idn, undoOrRedo)
    }
    else {
      this.dDb.updateItem(v.idn, this.dbStates.versions.find(i=>i.idv===prevIdv).doc, undoOrRedo)
    }
  }
  

  


  //Tree methods
  getPathOf(item) {
    return item.tree_parent ? this.getPathOf(this.dbStates.leaves[item.tree_parent]).concat([item.idn]) : [item.idn];
  }
  sortByTreePos(a,b) { //MUSS WOHL IN SHELF!!!
    let p1 = this.getPathOf(a).map(idn=>this.dbStates.leaves[idn].tree_pos);
    let p2 = this.getPathOf(b).map(idn=>this.dbStates.leaves[idn].tree_pos);
    let i = 0;
    while(p1[i] === p2[i]) i++;
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_modules_helpers__["log"])(a.nr, b.nr, p1, p2, (+p1[i]||0) > (+p2[i]||0) ? 1 : -1);
    return (+p1[i]||0) > (+p2[i]||0) ? 1 : -1;
  };
  findAll(relation, idnOrItem) {
    let item = typeof idnOrItem == "string" ? this.dbStates.leaves[idnOrItem] : idnOrItem;
    let candidates = this.getLeavesAsArray().filter(i=>i.type==item.type);
    let filterfunction;
    if(relation == "siblings")    filterfunction = i => i.tree_parent===item.tree_parent;
    if(relation == "kids")        filterfunction = i => i.tree_parent===item.idn;
    if(relation == "ancestors")   filterfunction = i => ~this.getPathOf(item).slice(0,-1).indexOf(i.idn);
    if(relation == "descendants") filterfunction = i => ~this.getPathOf(i).slice(0,-1).indexOf(item.idn);
    return candidates.filter(filterfunction).filter(i=>!i.deleted).sort((a,b)=>a.tree_pos - b.tree_pos).map(i=>i.idn);
  }
  isThatMy(relation, selfIdnOrItem, otherIdnOrItem) {
    return ~this.findAll(relation + "s", selfIdnOrItem, false).indexOf(otherIdnOrItem.idn || otherIdnOrItem);
  }
}

//ychoufe-12345678-l-98765432-l.json
//<type>-<created>-<modified>.<extension>
//<----------idv----------------------->
//<-----version------------->
//<----idn-------> (identifier)

class Item {
  constructor(idv, doc) {
    this.idv = idv;
    this.version   = idv.split(".")[0];
    this.extension = idv.split(".")[1];
    this.type     = this.version.split("-")[0];
    this.created  = this.version.split("-")[1];
    this.creator  = this.version.split("-")[2];
    this.modified = this.version.split("-")[3];
    this.modifier = this.version.split("-")[4];
    this.flags    = this.version.split("-")[5] || "";
    this.deleted  = this.flags.indexOf("x") > -1;
    this.idn      = this.type+"-"+this.created+"-"+this.creator;
    this.doc = doc;
    this.createGetSet();
  };
  createGetSet() {
    for(let prop in this.doc) {
      if(prop in Item.prototype) continue;
      Object.defineProperty(Item.prototype, prop, {
        get: function getProp() {return (this.debounced && this.debounced.key == prop) ? this.debounced.val : this.doc[prop];},
        set: function setProp(val) {this.doc[prop] = val;}
      });
    }
  };
  static getIdnFromIdv(idv) {return idv.split("-").slice(0,3).join("-");}
  get(prop) {
    let p = this[prop] !== undefined ? this[prop] : (this.doc[prop] !== undefined ? this.doc[prop] : undefined);
    if(this.debounced && this.debounced.key == prop) p = this.debounced.val;
    return (typeof p == "function") ? p.bind(this)() : p;
  };
  getPublicKeys() {
    return Object.keys(this.doc).filter(k=>!~k.indexOf("_"));
  }
  hasType(typegroup) {
    if(typegroup === "all") return true;
    if(typegroup === "deleted") return this.deleted === true;
    return this.type === typegroup;
  };
  //Tree Methods
  isTreeItem() {return this.tree_pos !== undefined;}
  getParent() {
    return this.tree_parent;
  }

}



/***/ }),

/***/ 98:
/***/ (function(module, exports) {

const _ = {};

/**
 *
 * @param array
 * @returns {*}
 */
_.last = function(array) {return array[array.length-1];};

_.log = console.log.bind(console);
_.warn = console.warn.bind(console);
_.newP = _=>Promise.resolve();

if(window) Object.assign(window, _);

module.exports = _;

//(function () {
//  var noop = function () { };
//  var methods = [
//    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
//    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
//    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
//    'timeStamp', 'trace', 'warn'
//  ];
//  var console = (window.console = window.console || {});
//  for(var i = methods.length - 1; i>=0; i--) {
//    console[methods[i]] = console[methods[i]] || noop; // Only stub undefined methods.
//  }
//
//  if (Function.prototype.bind && 0) {
//    window.log = Function.prototype.bind.call(console.log, console);
//  }
//  else {
//    window.log = function() {
//      if(!$(".log").length) $("body").append("<div class='log'></div>");
//      $(".log").empty().append("<p>"+arguments[0]+"</p>");
//      Function.prototype.apply.call(console.log, console, arguments);
//    };
//  }
//})();

/**

var self = function(s) {log(s); return s;}

function replaceUml(a) {
  if(typeof a !== "string") return a;
  a = a.toLowerCase();
  a = a.replace("ä", "a");
  a = a.replace("ö", "o");
  a = a.replace("ü", "u");
  a = a.replace("à ", "a");
  a = a.replace("é", "e");
  a = a.replace("è", "e");
  a = a.replace("ë", "e");
  a = a.replace("ï", "i");
  a = a.replace("ñ", "n");
  return a;
}

function htmlEscape(str) {
  return String(str)
  .replace(/&/g, '&amp;')
  .replace(/"/g, '&quot;')
  .replace(/'/g, '&#39;')
  .replace(/</g, '&lt;')
  .replace(/>/g, '&gt;');
}

function htmlUnescape(value){
  return String(value)
  .replace(/&quot;/g, '"')
  .replace(/&#39;/g, "'")
  .replace(/&lt;/g, '<')
  .replace(/&gt;/g, '>')
  .replace(/&amp;/g, '&');
}

function areShallowEqual(a, b) {
  return !Object.keys(a).concat(Object.keys(b)).some(k=> !(k in a) || !(k in b) || a[k] !== b[k]);
}

/*Lodash
array
    .diff
    .chunk
    .fill(value, [start=0], [end=array.length])
    .last
    .first
    .flatten([isDeep])
    .union
    .unique
    .groupBy([4.2, 6.1, 6.4], function(n) {return Math.floor(n);}); // → { '4': [4.2], '6': [6.1, 6.4] }
    .pluck(path)
    .sample([n=1]) // n elemente per Zufall auswählen
    .shuffle()
    .sortByAll
    .sortByOrder
    .curry
    .
    

// aliasing important functions into global namespace
var sin     = Math.sin;
var cos     = Math.cos;
var tan     = Math.tan;
var arctan  = Math.atan;
var atan2   = Math.atan2;
var arcsin  = Math.asin;
var arccos  = Math.acos;

var exp     = Math.exp;
var ln      = Math.log;
var log10   = Math.log10 || function(x) {return Math.log(x) / Math.LN10;}
var pow     = Math.pow;
var sqrt    = Math.sqrt;
var cbrt    = Math.cbrt;

var abs     = Math.abs;
var round   = Math.round;
var ceil    = Math.ceil;
var floor   = Math.floor;
var max     = Math.max;
var min     = Math.min;

var sign    = Math.sign || function(x) {if(typeof x !== "number") {return Math.NaN;} return x>0?1:x<0?-1:0;};

var rand    = Math.random;

var hypot   = Math.hypot || function(a,b) {return sqrt(a*a+b*b);};
var cath    = function(c,b) {return sqrt(c*c-b*b);};

var dist    = function(p1, p2) {hypot(p2.x-p1.x, p2.y-p1.y);};

var constrain = function(value, min, max) {
  if(value < Math.min(min, max)) return Math.min(min, max);
  if(value > Math.max(min, max)) return Math.max(min, max);
  return value;
};

//CONSTANT NUMBERS
var PI      = Math.PI;
var E       = Math.E;
var SQRT1_2 = Math.SQRT1_2;
var SQRT2   = Math.SQRT2;
var LN2     = Math.LN2;

var cblog = function(msg) {return function(data) {log(msg, data);};};

var g = 9.81;
var g_round = 10;
var euler = Math.E;
var pi = Math.PI;
var q_e = 1.6e-19;
var G = 6.67e-11;
var c = 299792458;
var c_round = 3e8;

var globalConstants = {
  g : {val:  9.81, unit: "m/s^2"},
  g_round : {val:  10, unit: "m/s^2"},
  euler : {val:  2.71, unit: false},
  pi : {val:  3.1415926535, unit: false},
  q_e : {val:  1.6e-19, unit: "C"},
  G : {val:  6.67e-11, unit: "Nm^2/kg^2"},
  c : {val:  299792458, unit: "m/s"},
  c_round : {val:  3e8, unit: "m/s"},
};

var newP = ()=>Promise.resolve(1); //better implement it with a real Promise Request instead of jQuery.post

var pointerX = e => e.pageX || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).pageX;
var pointerY = e => e.pageY || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).pageY;

var pointerClientX = e => e.clientX || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).clientX;
var pointerClientY = e => e.clientY || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).clientY;

window.onload=function() {
  if(location.host.indexOf("localhost") !== 0) return;
  var scripts = document.getElementsByTagName("script");
  var scriptlengths = new Map();
  var ct = 'application/json';
  var h = {"Accept": ct, "Content-Type": ct};
  for (let i=0;i<scripts.length;i++) {
    if (scripts[i].src) {
      window.fetch("dev/pfdc111", {method: 'POST', headers: h, body: JSON.stringify({file: scripts[i].src})})
      .then(r=>r.text()).then(r=>{scriptlengths.set(scripts[i].src, r)});
      setInterval(function() {
        if(document.hidden) return;
        window.fetch("dev/pfdc111", {method: 'POST', headers: h, body: JSON.stringify({file: scripts[i].src})})
        .then(r=>r.text()).then(r=>{if(scriptlengths.get(scripts[i].src) !== r) location.reload();});
      }, 2000);
    }
  }
};*/

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//ARRAY
Object.defineProperty(Array.prototype, "searchAll", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    var filtered = this.slice(0); //faster than (deep) clone
    for(var prop in compareObj) {
      filtered = filtered.filter(obj => obj[prop] === compareObj[prop]);
    }
    return filtered;
  }
});

Object.defineProperty(Array.prototype, "has", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    return this.searchAll(compareObj).length>0;
  }
});

Object.defineProperty(Array.prototype, "pick", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    var m = this.searchAll(compareObj);
    if(m.length > 1) {warn("Pick Element: More than 1 match, first one was picked", compareObj, m); console.trace(); return m[0];}
    if(m.length == 0) {warn("Pick Element: not found", compareObj); console.trace(); return false;}
    return m[0];
  }
});

//Object.defineProperty(Array.prototype, "searchItems", {
//  enumerable: false,
//  writable: true,
//  value: function(compareObj) {
//    var filtered = this.slice(0); //faster than (deep) clone
//    for(var prop in compareObj) {
//      filtered = filtered.filter(obj => obj.get(prop) === compareObj[prop]);
//    }
//    return filtered;
//  }
//});

Object.defineProperty(Array.prototype, "unique", {
  enumerable: false,
  writable: true,
  value: function() {
    let unicates = this.filter(function(el, i, ar) {
      return typeof el === "object" ? !ar.slice(0,i).has(el) : ar.indexOf(el) === i;
    });
    return unicates;
  }
});

//Object.defineProperty(Array.prototype, "pushUnique", {
//  enumerable: false,
//  writable: true,
//  value: function(el, byValue = true) {
//    let exists = el === "object" && byValue ? this.has(el) : this.indexOf(el) > -1;
//    if(!exists) this.push(el);
//    return this;
//  }
//});

Object.defineProperty(Array.prototype, "addUnique", {
  enumerable: false,
  writable: true,
  value: function(el, byValue = true) {
    var arr = [...this];
    let exists = (el === "object" && byValue) ? this.has(el) : this.indexOf(el) > -1;
    if(!exists) arr = [...arr, el];
    return arr;
  }
});

Object.defineProperty(Array.prototype, "deleteWhere", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    var arr = [...this];
    let matches = typeof compareObj == "function" ? this.filter(compareObj) : this.searchAll(compareObj);
    matches.forEach(m => arr = [...arr.slice(0,this.indexOf(m)), ...arr.slice(this.indexOf(m) + 1)]);
    return arr;
  }
});

Object.defineProperty(Array.prototype, "replaceAt", {
  enumerable: false,
  writable: true,
  value: function(index, newItem) {
    return [...this.slice(0, index), newItem, ...this.slice(index+1)];
  }
});

Object.defineProperty(Array.prototype, "last", {
  enumerable: false,
  writable: true,
  value: function() {return this[this.length-1];}
});

Object.defineProperty(Array.prototype, "sum", {
  enumerable: false,
  writable: true,
  value: function() {return this.reduce((o,n)=>o+n, 0);}
});

Object.defineProperty(Array.prototype, "avg", {
  enumerable: false,
  writable: true,
  value: function() {if(this.length) return this.sum()/this.length;}
});

/**
 * Works for numbers and strings!
 * Pass in Array ["key", "desc"] for descending sort
 */
Object.defineProperty(Array.prototype, "sortByMultipleKeys", {
  enumerable: false,
  writable: true,
  value: function(...keys) {
    this.sort((a,b) => {
      for (let prop of keys) {
        let [p, ...options] = prop instanceof Array ? prop : [prop];
        let desc = options.indexOf("desc") > -1;
        let propsequence = p.split(".");
        var o1 = a, o2 = b;
        for(var j in propsequence) {o1 = o1[propsequence[j]]; o2 = o2[propsequence[j]];}
        if(replaceUml(o1) > replaceUml(o2)) return desc?-1:1;
        if(replaceUml(o1) < replaceUml(o2)) return desc?1:-1;
      }
    });
    return this;
  }
});



//Object

Object.defineProperty(Object.prototype, "extractKeys", {
  enumerable: false,
  writable: true,
  value: function(...keys) {
    let extracted = {};
    keys.forEach(prop => extracted[prop] = this[prop]);
    return extracted;
  }
});


Object.defineProperty(Object.prototype, "values", {
  enumerable: false,
  writable: true,
  value: function() {
    return Object.keys(this).map(key => this[key]);
  }
});

Object.defineProperty(Object.prototype, "clone", {
  enumerable: false,
  writable: true,
  value: function() {
    return JSON.parse( JSON.stringify( this ) );
  }
});




/***/ })

/******/ });
//# sourceMappingURL=withSimplepaint-bundle.js.map