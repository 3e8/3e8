import {post} from "modules/ajaxhelpers";
import {Store} from "modules/itemstore";
import {Simplepainter} from "modules/simplepainter";

import {on, getPosition} from "modules/domhelpers";

//require("../js/libs/pdf.js"); //apparently must be loaded from script tag

const fileSelector = document.querySelector(".fileSelector");
const pdfPreview = document.querySelector(".pdf--preview");

let store = new Store("slideshow", {
  dbcentral: "mydb/db",
  dbname: "slides",
  leavesonly: false,
  longpolling: false,
  keysByType: {}
});

store.dNs.mountShelf("slideshowstates", {activeSlide: null});

let simplepainter = new Simplepainter({itemstore: store, onlypen: false});

store.subscribe("ready", {onReady: _=>pickSlidesOfThisfile()});
store.subscribe("dbChange", {onDbChange({action, args}) {
  if(action === "deleteItem" && args[0].startsWith("pdfSlide")) {
    deleteSlide(args[0]);
  }
}});

store.subscribe("stateChange", ({action, current, old}) => {
  if(action==="changeShelfStates" && current.activeSlide !== old.activeSlide) {
    showSlide(slides.pick({idn: current.activeSlide}));
    simplepainter.setOptions();
  }
}, "slideshowstates");

let slides = [];
//itemworld.$container.on("loaded", pickSlidesOfThisfile);

function pickSlidesOfThisfile() {
  log("loaded");
  slides = store.getLeavesAsArray().filter(i=>i.type==="pdfSlide" && i.file===mypdf.file);
}

// let win = window.open("showSlides.html", "_blank", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=1280, height=800, left=800');
// win.moveTo(400,0);
// $(window).on("beforeunload", function() {win.close();});

let mypdf = {
  file: null,
  path: 'pdf/',
  url: null,
  data: null,
  numPages: 0,
  defaultWidth: 595,
  activeSlide: null
};

let temp = {};

let settings = {
  previewWidth: 200, //auch in css ändern
  ratio: 16/10,
  w_max: 640,
};

window.onload = load;

function getOffsetFromPage(event, el) {
  let {x, y} = getPosition(el);
  return {x: event.pageX - x, y: event.pageY - y + pdfPreview.scrollTop};
}

on(".pdf--preview", "pointerdown", ".pdfPage", function(e) {
  let {x, y} = getOffsetFromPage(e, this);
  temp.startx = x;
  temp.starty = y;
  temp.page = +this.dataset.page;
  if(temp.rect) temp.rect.remove();
  temp.rect = $("<div class='rect temprect hidden'></div>").appendTo(this);
});

on(".pdf--preview", "pointermove", ".pdfPage", function(e) {
  if(!temp.rect) return;
  let {x, y} = getOffsetFromPage(e, this);
  let rect = {
    x: Math.min(temp.startx, x),
    y: Math.min(temp.starty, y),
    w: Math.abs(temp.startx - x),
    h: Math.abs(temp.starty - y)
  };
  if(rect.w > 5 && rect.h > 5) temp.rect.removeClass("hidden");
  temp.rect.css({left: rect.x, top: rect.y, width: rect.w, height: rect.h})
});

on(".pdf--preview", "pointerup", ".pdfPage", function(e) {
  let {x, y} = getOffsetFromPage(e, this);
  let {width} = this.getBoundingClientRect();
  let newSlide = {
    file: mypdf.file,
    x: 100 * Math.min(temp.startx, x)  / width,
    y: 100 * Math.min(temp.starty, y)  / width,
    w: 100 * Math.abs(temp.startx - x) / width,
    h: 100 * Math.abs(temp.starty - y) / width,
    page: temp.page
  };
  if(temp.rect) temp.rect.remove();
  if(newSlide.w > 5 && newSlide.h > 5 && +newSlide.page === +this.dataset.page) {
    store.dDb.addItem("pdfSlide", newSlide).then(newSlideIdn =>{
      let newSlideItem = store.getItem(newSlideIdn);
      slides.push(newSlideItem);
      drawRect(newSlideItem);
      showSlide(newSlideItem);
    });
  }
});

on(".pdf--preview", "click", ".slideRect", function(e) {
  e.preventDefault();
  store.dNs.changeShelfStates("slideshowstates", {activeSlide: $(this).data("slideidn")});
});

//$(".pdf--slide").on("pointermove", function(e) {
//  localStorage.setItem("pdfSlideMouse", "["+ e.offsetX+","+e.offsetY+"]");
//});
//
//$(".pdf--slide").on("pointerleave", function(e) {
//  localStorage.setItem("pdfSlideMouse", "[-100,-100]");
//});

function load() {
  post("/inventory/slide/pdf").then(l=>{
    let options = l.map((f,i)=>`<option ${i===0?"selected":""} value="${f}">${f}</option>`);
    fileSelector.innerHTML = options.join("");
    fileSelector.addEventListener("change", setup);
    setup();
  });
}

function setup() {
  log("setup");
  mypdf.file = fileSelector.value;
  mypdf.url = mypdf.path + mypdf.file;
  localStorage.pdfviewer__file = mypdf.url;
  log($('.pdf--preview').length);
  $('.pdf--preview').empty();
  pickSlidesOfThisfile();
  PDFJS.getDocument(mypdf.url).then(function(pdf) {
    mypdf.data = pdf;
    mypdf.numPages = pdf.numPages;
    mypdf.data.getPage(1).then(function(p) {
      mypdf.defaultWidth = p.getViewport(1.0).width;
      renderPdfs();
    });
  });
}

function renderPdfs() {
  renderPreview().then(function() {
    slides.forEach(drawRect);
    if(slides.length) showSlide( slides.pick({idn: store.namedStates.slideshowstates.activeSlide}) || slides[0] );
  });
}

function renderPreview() {
  let p = Promise.resolve();
  for(let i = 1; i <= mypdf.numPages; i++) {
    p = p.then( _ => mypdf.data.getPage(i) ).then( page => addPreviewPage(i, page) );
  }
  return p;
}

function addPreviewPage(num, page) {
  let $nextPage = $(`<div class='pdfPage page--${num}' data-page="${num}"></div>`).appendTo('.pdf--preview');
  return renderPageInDiv(page, $nextPage).promise;
}

function renderPageInDiv( page, $div ) {
  let canvas = $("<canvas class='pdfCanvas'></canvas>").appendTo($div).get(0);
  let context = canvas.getContext('2d');
  let outputScale = getOutputScale(context);
  canvas.width = Math.floor($(canvas).width() * outputScale) | 0;
  let viewport = page.getViewport( $(canvas).width() / mypdf.defaultWidth, 0 );
  canvas.height = viewport.height/viewport.width * canvas.width;
  context._scaleX = outputScale;  context._scaleY = outputScale;
  if (outputScale!==1) {
    context.scale(outputScale, outputScale);
  }
  return {promise: page.render({canvasContext: context, viewport: viewport}), canvas, context, outputScale, viewport};
}

function showSlide(slide) {
  localStorage.setItem("pdfActiveSlide", slide.idn);
  $(".slideRect").removeClass("activeSlide").filter(function(){return $(this).data("slideidn") == slide.idn}).addClass("activeSlide");
  mypdf.activeSlide = slide.idn;
  mypdf.data.getPage(slide.page).then(renderSlide.bind(null, slide));
}

function renderSlide(slide, page) {
  $(".hiddenSlide").remove();
  document.querySelector(".pdf--slide.simplepainter").dataset.painter = slide.idn;
  let $hiddenSlide = $(`<div class='hiddenSlide'></div>`).appendTo('.pdf--slide');
  let maxWidth = Math.min(settings.w_max, slide.w/slide.h*settings.w_max/settings.ratio);  //w_max = 640 und h_max = 640/ratio
  let $hiddenSlideInner  = $(`<div class='hiddenSlideInner' style="width: ${maxWidth * 100 / slide.w}px"></div>`).appendTo($hiddenSlide)
  let {promise, context} = renderPageInDiv(page, $hiddenSlideInner);
  let slidecanvas = $(".slide--canvas").get(0);
  let slidecontext = slidecanvas.getContext('2d');
  let outputScale = getOutputScale(slidecontext);
  slidecanvas.width = Math.floor($(slidecanvas).width() * outputScale) | 0;
  //$(slidecanvas).height(Math.min(640/settings.ratio, $(slidecanvas).width() *  slide.h/slide.w );
  slidecanvas.height = Math.floor($(slidecanvas).height() * outputScale) | 0;  //slide.h/slide.w * slidecanvas.width;
  slidecontext._scaleX = outputScale;  slidecontext._scaleY = outputScale;
  if (outputScale!==1) {
    slidecontext.scale(outputScale, outputScale);
  }
  promise.then(function() {
    let imgData = context.getImageData(maxWidth*outputScale*slide.x/slide.w, maxWidth*outputScale*slide.y/slide.w, maxWidth*outputScale, maxWidth*outputScale*slide.h/slide.w);
    slidecontext.putImageData(imgData, outputScale*(settings.w_max-maxWidth)*0.5, 0);
  });
  

  //TODO besser machen, damit es auch bei Update funktioniert. Immer neue Tree?
  let roots = store.getLeavesAsArray().filter(i => i.type==="sketch" && i.tree_parent === "" && i.painter===slide.idn);
  let strokes = store.getLeavesAsArray().filter(i => roots.some(r=>store.isThatMy("ancestor", i.idn, r.idn)));
  //let tree = $(".pdf--sketchlist").empty()[0].tree;
  simplepainter.clear();
  //roots.forEach(s=>tree.renderTreeitem($(".pdf--sketchlist").append("<div></div>"), s));
  //log("try load stroke", strokes.length, store.getLeavesAsArray());
  strokes.forEach(s=>simplepainter.loadStroke(s.idn));
}

function drawRect(slide) {
  let rect = $(`<div class='rect slideRect' data-slideidn="${slide.idn}"></div>`).appendTo(`.pdfPage.page--${slide.page}`);
  let deleteButton = $(`<div class="deleteSlide" data-slideidn="${slide.idn}">X</div>`).appendTo(rect);
  deleteButton.on("click", function(e) {
    store.dDb.deleteItem(this.dataset.slideidn);
  });
  let f = $(`.pdfPage.page--${slide.page}`).width()/100;
  rect.css({left: f*slide.x, top: f*slide.y, width: f*slide.w, height: f*slide.h})
}

function deleteSlide(idn) {

}

function getOutputScale(ctx) {
  let devicePixelRatio = window.devicePixelRatio || 1;
  let backingStoreRatio = ctx.webkitBackingStorePixelRatio || ctx.mozBackingStorePixelRatio ||
      ctx.msBackingStorePixelRatio || ctx.oBackingStorePixelRatio || ctx.backingStorePixelRatio || 1;
  return devicePixelRatio / backingStoreRatio;
}