// from Events to slightly smoothed point arrays [xs], [ys], [ps]

let settings = {
  THICKNESS: 3,
  ZOOMFACTOR: 1
};

self.addEventListener('message', function({data}) {
  if(data.newsettings) {
    Object.assign(settings, data.newsettings);
  }
  else {
    pointerActions[data.type](data.coords);
  }
}, false);

let capturing = false;
let c;

const pointerActions = {
  down: initCurve,
  enter: initCurve,
  move: progressCurve,
  up: finishCurve,
  leave: finishCurve,
  cancel: finishCurve
};

function initCurve({x, y, p=0}) {
  if(p===0) return;
  capturing = true;
  c = {
    xs: [x],
    ys: [y],
    ps: [p],
    //Simplified Points
    sxs: [x],
    sys: [y],
    sps: [p],
    //Temppoints
    tempx:   [],
    tempy:   [],
    tempp:   [],
    //other stuff
    lastX:  x,
    lastY:  y,
    vx: 0,
    vy: 0,
    ax: 0,
    ay: 0,
    lpx: x,
    lpy: y,
    drawn: 0,
    thickenStart: false,
    sdone: 0,
    lastPointWasCorner: false,
  }
}

function finishCurve({x, y, p=0}) {
  if(!capturing) return;

  c.xs.push(x);
  c.ys.push(y);
  c.ps.push(last(c.ps) * 0.95);

  if(c.tempx.length && !(last(c.tempx) === last(c.sxs) && last(c.tempy) === last(c.sys))) {
    c.sxs.push(last(c.tempx));
    c.sys.push(last(c.tempy));
    c.sps.push(last(c.tempp));
  }
  if(last(c.xs) !== last(c.sxs) || last(c.ys) !== last(c.sys)) {
    c.sxs.push(last(c.xs));
    c.sys.push(last(c.ys));
    c.sps.push(last(c.sps)*0.94); //special case because pressure should not decrease linearly over several pixels
  }
  
  //smoothen Pressure (to avoid strange rendering)
  const coeff = settings.THICKNESS;
  let getThickness = p => coeff*(settings.PRESSUREBASE + (1-settings.PRESSUREBASE)*p);
  let smoothenPressure = n => {
    let didSomething = false;
    for(let i = 1; i < c.sxs.length; i++) {
      let d = Math.hypot(c.sxs[i] - c.sxs[i-1], c.sys[i] - c.sys[i-1]);
      let dp = c.sps[i] - c.sps[i-1];
      let dt = getThickness(c.sps[i]) - getThickness(c.sps[i-1]);
      const TOLERANCE = 0.05;
      
      if(Math.abs(dt) > TOLERANCE * d) {
        let equalizeP = 0;
        const MaxFactor = 0.25;
        while(equalizeP < MaxFactor && Math.abs(getThickness(c.sps[i] - dp*equalizeP) - getThickness(c.sps[i-1] + dp*equalizeP)) > TOLERANCE * d) {
          equalizeP += 0.05;
        }
        c.sps[i] -= dp*equalizeP;
        c.sps[i-1] += dp*equalizeP;
        didSomething = true;
      }
    }
    if(didSomething && n<100) smoothenPressure(n+1);
  };
  smoothenPressure(0);
  
  postMessage({wholePath: true, xs: c.sxs, ys: c.sys, ps: c.sps})
  capturing = false;
}

function progressCurve({x, y, p=0.5}) {
  if(!capturing) return;
  if(p===0) return finishCurve({x,y,p});
  addPoint(x,y,p);
  draw();
}

function addPoint(x,y,p) {
  if(x===last(c.xs) && y===last(c.ys)) {
    c.ps[c.ps.length - 1] = 0.5 * (c.ps[c.ps.length-1] + p);
    return;
  }
  c.xs.push(x);
  c.ys.push(y);
  c.ps.push(p);
  let {vx, vy, ax, ay, lpx, lpy, xs, ys, ps} = c;
  let L = xs.length;
  var px = (17*xs[L-1] + 12*xs[L-2] - (3*xs[L-3]||3*xs[L-2]) ) / 26;
  var py = (17*ys[L-1] + 12*ys[L-2] - (3*ys[L-3]||3*ys[L-2]) ) / 26;
  var nvx = L===3 ? (xs[L-1] - xs[L-2]) : 0.8 * vx + 0.2 * (px - lpx);
  var nvy = L===3 ? (ys[L-1] - ys[L-2]) : 0.8 * vy + 0.2 * (py - lpy);
  ax = 0.8 * ax + 0.2 * ( (xs[L-1] - xs[L-2]) - vx);
  ay = 0.8 * ay + 0.2 * ( (ys[L-1] - ys[L-2]) - vy);
  Object.assign(c, {vx: nvx, vy: nvy, ax, ay, lpx: px, lpy: py});

  c.tempx.push(aggregate(c.xs, L));
  c.tempy.push(aggregate(c.ys, L));
  c.tempp.push(aggregate(c.ps, L));

  //corner
  let isCorner = checkForCorner();
  if(isCorner && !c.lastPointWasCorner) {
    if(c.tempx.length>=2 || 0) {
      c.sxs = c.sxs.concat(c.tempx.slice(-2));
      c.sys = c.sys.concat(c.tempy.slice(-2));
      c.sps = c.sps.concat(c.tempp.slice(-2));
    }
    else {
      c.sxs.push(c.tempx[0]);
      c.sys.push(c.tempy[0]);
      c.sps.push(c.tempp[0]);
    }
    c.lastPointWasCorner = true;
    c.tempx = [];
    c.tempy = [];
    c.tempp = [];
  }
}

function draw() {
  let {vx, vy, ax, ay, lpx, lpy, xs, ys} = c;
  let L = xs.length;
  if(L === c.drawn) return;
  c.drawn = L;
  let lag = 0.4;
  let factora = 0.2;
  var predictorsX = (!vx && !vy && !ax && !ay) ? [] : [lpx, ...[1,2,3].map(t=>lpx + lag*vx*t + factora*t*t*ax)];
  var predictorsY = (!vx && !vy && !ax && !ay) ? [] : [lpy, ...[1,2,3].map(t=>lpy + lag*vy*t + factora*t*t*ay)];

  if(c.tempx.length) {
    var d = distanceFromLastPoint();

    if(d>2*Math.sqrt(settings.THICKNESS)/settings.ZOOMFACTOR) {
      if(d>4*c.tempx.length) {
        c.sxs = c.sxs.concat(c.tempx);
        c.sys = c.sys.concat(c.tempy);
        c.sps = c.sps.concat(c.tempp);
      }
      else {
        c.sxs.push(c.tempx[c.tempx.length-1]);
        c.sys.push(c.tempy[c.tempy.length-1]);
        c.sps.push(c.tempp[c.tempp.length-1]);
      }
      c.tempx = [];
      c.tempy = [];
      c.tempp = [];
      c.lastPointWasCorner = false;
    }
  }
  if(!c.thickenStart && c.sps.length>=2) {
    c.thickenStart = "done";
    c.sps[0] = 0.9 * c.sps[1];
  }
  
  postMessage({newsxs: c.sxs.slice(c.sdone), newsys: c.sys.slice(c.sdone), newsps: c.sps.slice(c.sdone),
    //oxs: c.sxs.slice(start, end), oys: c.sys.slice(start, end), ops: c.sps.slice(start, end).map((p,i)=>i===0?0.3*p:i===1?0.4*p:i===2?0.9*p:i===3?0.99*p:p),
    txs: predictorsX, tys: predictorsY, tps: predictorsX.map(_=>c.ps[c.ps.length-1])
  });
  c.sdone = c.sxs.length;
}

function checkForCorner() {
  let L = c.xs.length;
  if(L<8) return false;
  let x1stBefore = this.get1stDerivative(c.xs, L-6);
  let y1stBefore = this.get1stDerivative(c.ys, L-6);
  let x1stAfter = this.get1stDerivative(c.xs, L-3);
  let y1stAfter = this.get1stDerivative(c.ys, L-3);
  let anglebefore = (Math.atan2(-y1stBefore, x1stBefore)*180/Math.PI + 360)%360;
  let angleafter = (Math.atan2(-y1stAfter, x1stAfter)*180/Math.PI + 360)%360;
  let da = ((anglebefore - angleafter) + 720 + 180)%360 - 180;
  return Math.abs(da)>45;
}

function aggregate(a, L) {
  if(L>9)  return (-11*a[L-10] + /*0*a[L-9] +*/ 9*a[L-8] + 16*a[L-7] + 21*a[L-6] + 24*a[L-5] + 12.5*a[L-4])/143 + (3.5*a[L-4] + 6*a[L-3] + 3*a[L-2] - 2*a[L-1])/21; //assymmetrische Savitzky Golay
  if(L===1) return a[0];
  if(L===2) return 0.25*a[0] + 0.75*a[1];
  if(L===3) return 0.1*a[0] + 0.4*a[1] + 0.5*a[2];
  if(L===4) return 0.08*a[0] + 0.22*a[1] + 0.40*a[2] + 0.30*a[3];
  if(L===5) return 0.05*a[0] + 0.15*a[1] + 0.30*a[2] + 0.30*a[3] + 0.20*a[4];
  if(L===6) return (-2*a[0] + 3*a[1] + 6*a[2] + 3.5*a[3])/21 + (8.5*a[3] + 12*a[4] - 3*a[5])/35;
  if(L===7) return (-21*a[0] + 14*a[1] + 39*a[2] + 54*a[3] + 29.5*a[4])/231 + (8.5*a[4] + 12*a[5] - 3*a[6])/35;
  if(L===8) return (-36*a[0] + 9*a[1] + 44*a[2] + 69*a[3] + 84*a[4] + 44.5*a[5])/429 + (8.5*a[5] + 12*a[6] - 3*a[7])/35;
  if(L===9) return (-36*a[0] + 9*a[1] + 44*a[2] + 69*a[3] + 84*a[4] + 44.5*a[5])/429 + (3.5*a[5] + 6*a[6] + 3*a[7] - 2*a[8])/21;
}

function get1stDerivative(arr, center) {
  return -0.2*arr[center-2] - 0.1*arr[center-1] + 0.1*arr[center+1] + 0.2*arr[center+2];
}

function distanceFromLastPoint() {
  return Math.hypot(c.tempx[c.tempx.length-1]-c.sxs[c.sxs.length-1], c.tempy[c.tempy.length-1]-c.sys[c.sys.length-1])
}

function last(arr) {
  return arr[arr.length-1];
}