// from slightly smoothed point arrays [xs], [ys], [ps] to contour points for SVG or canvas path

let settings = {
  THICKNESS: 3,
  PRESSUREBASE: 0.18
};

self.addEventListener('message', function({data}) {
  if(data.newsettings) {
    Object.assign(settings, data.newsettings);
  }
  else if(data.wholePath || 1) {
    postMessage({pathdata: renderPath(data), task: data.task});
  }
}, false);

function renderPath({xs, ys, ps, thickness=settings.THICKNESS}) {
  const coeff = thickness;
  const L = xs.length;
  let points = xs.map(function(e, i) {return {x: xs[i], y: ys[i], t: coeff*(settings.PRESSUREBASE + (1-settings.PRESSUREBASE)*ps[i])};});
  let rs = {left: [], right: []};
  //cap inverted edges
  if(L>=3) {
    if((xs[L-2]-xs[L-1])*(xs[L-3]-xs[L-2]) + (ys[L-2]-ys[L-1])*(ys[L-3]-ys[L-2]) < 0) {
      points = points.slice(0, -1);
    }
    if(L >= 4 && (xs[1]-xs[0])*(xs[2]-xs[1]) + (ys[1]-ys[0])*(ys[2]-ys[1]) < 0) {
      points = points.slice(1);
    }
  }
  let getTurningEdge = function(role = "start||turn", edge, mid, inner) {
    if(!(edge&&mid&&inner)) console.log(edge, mid, inner);
    let a = getControlPoints(edge.x, edge.y, mid.x, mid.y, inner.x, inner.y, 0.4);
    let dx = edge.x - mid.x;
    let dy = edge.y - mid.y;
    let dsq = dx*dx + dy*dy;
    let tm = {x: a[0]-mid.x, y: a[1]-mid.y}; //Tangente an Mid
    let mt = {x: tm.x - 2*(tm.x*dx+tm.y*dy)/dsq * dx, y: tm.y - 2*(tm.x*dx+tm.y*dy)/dsq * dy}; //gespiegelte Tangente an Kurvensegment
    let mtl = Math.hypot(mt.x, mt.y);
    let pressthick = edge.t;
    let [corrx, corry] = [-pressthick * mt.x/mtl, -pressthick * mt.y/mtl];
    if(dsq===0) {console.warn(dsq, " GLEICH NULL!?? (Schaue in alter Version)", xs, ys, ps);}
    let helperpoints = [];
    for(let step = 0; step < 9; step+=2) {
      let da = - 4/9*Math.PI + step/9*Math.PI; // + Math.atan2(mt.y, mt.x);
      helperpoints.push({x: edge.x + Math.cos(da) * corrx - Math.sin(da) * corry, y: edge.y + Math.cos(da) * corry + Math.sin(da) * corrx})
    }
    return {
      results: helperpoints
    }
  };
  let getTangents = function(p, i, arr) {
    if(i === arr.length-1) return; //last point
    let next = arr[i+1];
    let dx = next.x - p.x;
    let dy = next.y - p.y;
    let d = Math.hypot(dx, dy);
    //log(dx, dy, d);
    if(i===0) {
      rs.left.push({x: p.x + dy / d * p.t, y: p.y - dx / d * p.t});
      rs.right.push({x: p.x - dy / d * p.t, y: p.y + dx / d * p.t});
      rs.left.push({x: next.x + dy / d * next.t, y: next.y - dx / d * next.t});
      rs.right.push({x: next.x - dy / d * next.t, y: next.y + dx / d * next.t});
      //log(rs);
      return;
    }
    let getpoint = function(side) {
      let sign = side === "left" ? +1 : -1;
      let start = {x: p.x + sign * dy / d * p.t, y: p.y - sign * dx / d * p.t};
      let end = {x: next.x + sign * dy / d * next.t, y: next.y - sign * dx / d * next.t};
      let rx = end.x - start.x, ry = end.y - start.y;
      let [prevstart, prevend] = rs[side].slice(-2);
      let ox = prevend.x - prevstart.x, oy = prevend.y - prevstart.y;
      
      if(ox*ry - oy*rx === 0) {
        //if(ox*rx+oy*ry < 0) warn("antiparallel");
        rs[side][rs[side].length - 1] = {x: 0.5 * (start.x + prevend.x), y: 0.5 * (start.y + prevend.y)};
      }
      else {
        let anglebefore = (Math.atan2(oy, ox)*180/Math.PI + 360)%360;
        let angleafter = (Math.atan2(ry, rx)*180/Math.PI + 360)%360;
        let da = ((anglebefore - angleafter) + 720 + 180)%360 - 180;
        if(Math.abs(da) > 60) {
          let loopover = da*sign > 0;
          let turnangle = loopover ? 360-Math.abs(da) : Math.abs(da);
          let numsteps = Math.floor(turnangle/30);
          let helperangle = turnangle / (numsteps + 1);
          let helperpoints = [];
          //log(side, da, loopover, turnangle, numsteps, helperangle);
          let rotx = start.x - p.x, roty = start.y - p.y;
          for(let step = 0; step<=numsteps; step++) {
            let a = - sign * helperangle*step/180*Math.PI;
            helperpoints.unshift({x: p.x + rotx * Math.cos(a) - roty * Math.sin(a), y: p.y + roty * Math.cos(a) + rotx * Math.sin(a)})
          }
          rs[side] = rs[side].concat(helperpoints)
        }
        else {
          let t = (start.x*ry - start.y*rx - prevstart.x * ry + prevstart.y * rx) / (ox*ry - oy*rx);
          if(t > 1) {
            rs[side][rs[side].length - 1] = {x: 0.5 * (start.x + prevend.x), y: 0.5 * (start.y + prevend.y)};
          }
          else if(t > 0) {
            rs[side][rs[side].length - 1] = {x: prevstart.x + t * ox, y: prevstart.y + t * oy};
          }
          else if(t < 0) {
            rs[side].pop();
          }
        }
      }
      rs[side].push(end);
    };
    getpoint("left");
    getpoint("right");
  };
  let results = [];
  if(points.length === 1) {
    for(let step = 0; step <= 11; step++) {
      let a = step * 30 / 180 * Math.PI;
      let p = points[0];
      results.push({x: p.x + p.t * Math.cos(a), y: p.y + p.t * Math.sin(a)})
    }
  }
  else {
    if(points.length === 2) {
      points = [points[0], {x: 0.5*(points[0].x+points[1].x), y: 0.5*(points[0].y+points[1].y), t: 0.5*(points[0].t+points[1].t)}, points[1]];
    }
    points.forEach(getTangents);
    let s = getTurningEdge("start", ...points.slice(0, 3));
    let t = getTurningEdge("turn", ...points.slice(-3).reverse());
    results = [...s.results, ...rs.left, ...t.results, ...rs.right.reverse()];
  }
  let path = results.length ? arraysToBezier(results.map(p=>p.x).concat([results[0].x]), results.map(p=>p.y).concat([results[0].y])) : "";
  return path;
}

function arraysToBezier(xs, ys) {
  var p = "M" + xs[0] + "," + ys[0] + " ";
  p += " C" + (xs[0] + (xs[1] - xs[0]) / 3) + "," + (ys[0] + (ys[1] - ys[0]) / 3) + " ";
  for(var i = 1; i < xs.length - 1; i++) {
    var a = getControlPoints(xs[i - 1], ys[i - 1], xs[i], ys[i], xs[i + 1], ys[i + 1], 0.4);
    p += a[0] + "," + a[1] + " " + xs[i].toFixed(2) + "," + ys[i].toFixed(2) + " " + a[2] + "," + a[3] + " ";
  }
  p += (last(xs) + (xs[xs.length - 2] - last(xs)) / 3) + "," + (last(ys) + (ys[ys.length - 2] - last(ys)) / 3) + " ";
  p += last(xs) + "," + last(ys);
  return p;
}

function getControlPoints(x0, y0, x1, y1, x2, y2, t) {
  var d01 = Math.hypot(x1-x0, y1-y0);
  var d12 = Math.hypot(x2-x1, y2-y1);
  if(d01 + d12 === 0) {return [x1, y1, x1, y1]}
  var fa = t * d01 / (d01 + d12);   // scaling factor for triangle Ta
  var fb = t * d12 / (d01 + d12);   // ditto for Tb, simplifies to fb=t-fa
  var p1x = x1 - fa * (x2 - x0);    // x2-x0 is the width of triangle T
  var p1y = y1 - fa * (y2 - y0);    // y2-y0 is the height of T
  var p2x = x1 + fb * (x2 - x0);
  var p2y = y1 + fb * (y2 - y0);
  return [p1x.toFixed(2), p1y.toFixed(2), p2x.toFixed(2), p2y.toFixed(2)];
}

function last(arr) {
  return arr[arr.length-1];
}