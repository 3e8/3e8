/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 404);
/******/ })
/************************************************************************/
/******/ ({

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return on; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return makeNode; });
/* unused harmony export makeNodes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return empty; });
function on(elSelector, eventName, selector, fn) {
  let elements = document.querySelectorAll(elSelector);
  
  for (let i = 0; i < elements.length; ++i) {
    let element = elements[i];
    element.addEventListener(eventName, function(event) {
      let possibleTargets = element.querySelectorAll(selector);
      let target = event.target;
    
      for(let j = 0, l = possibleTargets.length; j < l; j++) {
        let el = target;
        let p = possibleTargets[j];
        while(el && el !== element) {
          if(el === p) {
            return fn.call(p, event);
          }
          el = el.parentNode;
        }
      }
    });
  }
}

function getPosition(el) {
  let xPos = 0;
  let yPos = 0;
  
  while (el) {
    if (el.tagName === "BODY") {
      // deal with browser quirks with body/window/document and page scroll
      var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
      var yScroll = el.scrollTop || document.documentElement.scrollTop;
      
      xPos += (el.offsetLeft - xScroll + el.clientLeft);
      yPos += (el.offsetTop - yScroll + el.clientTop);
    } else {
      // for all other non-BODY elements
      xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
      yPos += (el.offsetTop - el.scrollTop + el.clientTop);
    }
    
    el = el.offsetParent;
  }
  return {
    x: xPos,
    y: yPos
  };
}

/**
 * @param {String} html representing a single element
 * @return {Node}
 */
function makeNode(html) {
  var template = document.createElement('template');
  template.innerHTML = html;
  return template.content.firstChild;
}

/**
 * @param {String} html representing any number of sibling elements
 * @return {NodeList}
 */
function makeNodes(html) {
  var template = document.createElement('template');
  template.innerHTML = html;
  return template.content.childNodes;
}

/**
 *
 * @param {Element} node
 * @returns {Element}
 */
function empty(node) {
  node.innerHTML = "";
  return node;
}




/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Simplepainter; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__ = __webpack_require__(101);
// TODO
/*
-history
-group
-pousi
*/

const base62 = __webpack_require__(214);


const PRESSUREBASE = 0.18;
const PRESSUREINCREASE = 0.82; //should add up to one
const MAXTEMPPATH = 50;

class Simplepainter {
  constructor({selector = ".simplepainter", menu="auto", itemstore = false, onlypen = true}={}) {
    this.el = document.querySelector(selector);
    this.el.classList.add("simplepainter");
    this.el.style.touchAction = "none"; //native touch-actions not possible, otherwise pointermove blocked
    this.pousi = document.createElement("canvas");
    this.pousi.classList.add("simplepainter__canvas", "pousi");
    this.el.appendChild(this.pousi);
    this.svgcontainer = document.createElementNS('http://www.w3.org/2000/svg', "svg");
    this.svgcontainer.setAttribute('class', "simplepainter__svgcontainer");
    this.el.appendChild(this.svgcontainer);
    this.svgcontainer.setAttribute("width", window.getComputedStyle(this.el).width);
    this.svgcontainer.setAttribute("height", window.getComputedStyle(this.el).height);
    if(menu === "auto") {
      this.menu = document.createElement("div");
      this.menu.classList.add("toggleContent", "on", "simplepainter__menu");
      let colors = ["ffffff", "999999", "444444", "000000", "992222", "222266", "ffaa22"].map(c=>{
        return `<span class="simplepainter__menu--color" style="background-color:#${c}" data-color="${c}">&nbsp;</span>`
      }).join("");
      let thickness = [0.5, 1, 1.5, 2, 3, 4, 6, 8, 10, 12].map(t=>{
        return `<div class="simplepainter__menu--thickness" data-thickness="${t}"><div style="height: ${2+t}px; width: ${2+t}px; border-radius: ${1+t*0.5}px; background-color:#000; margin: 0px ${(16-t)/2}px; vertical-align:middle;">&nbsp;</div></div>`
      }).join("");
      this.menu.innerHTML = `
        <div class="toggler ifOn" style="color: grey;">...</div>
        <div class="toggler ifOff">...</div>
        <div class="ifOn"><input type="color" class="opt_color" value="#222266"></div>
        <div class="ifOn">${colors}</div>
        <div class="ifOn" style="verical-align: middle">${thickness}</div>
        <div class="ifOn"><input class="opt_thickness" value="3"></div>
        <div class="ifOn simplepainter__history toggleContent off">
          <div class="toggler ifOn" style="color: grey;">...</div>
          <div class="toggler ifOff">Tree</div>
          <div class="historytree ifOn"></div>
        </div>
        <div class="ifOn simplepainter__undo">Undo</div>
        <div class="ifOn simplepainter__redo">Redo</div>
      `;
      this.el.appendChild(this.menu);
    }
    this.itemstore = itemstore;
    if(this.itemstore) {
      this.itemstore.dNs.mountShelf("simplepainter", this.options);
      this.historyEl = this.el.querySelector(".historytree");
      let showHistory = ({action, args}={}) => {
        let l = this.itemstore.getLeavesAsArray();
        let sketchlist = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__["c" /* makeNode */])("<div class='sketchlist'></div>");
        let rootsOfThisPainter = l.filter(i=>i.type === "sketch"&&i.group&&(i.painter===this.options.painter||(i.painter===null&&this.options.painter===null)));
        rootsOfThisPainter.forEach(r=>{
          let rPreview = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__["c" /* makeNode */])("<svg class='sketchgroup' style='width:100px' viewBox='0 0 1200 800'></svg>");
          l.filter(s=>s.tree_parent===r.idn).forEach(s=>{
            rPreview.appendChild(this.getSvgFromStroke(s.idn));
          });
          let rEl = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__["c" /* makeNode */])("<div class='root'></div>");
          rEl.appendChild(rPreview);
          rEl.appendChild(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__["c" /* makeNode */])(`<span>${r.idn}</span>`))
          sketchlist.appendChild(rEl);
        });
        let groups = rootsOfThisPainter.map(r=>r.idn + ":<br>" + l.filter(s=>s.tree_parent===r.idn).map(s=>s.idn).join("<br>"));
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__modules_domhelpers__["d" /* empty */])(this.historyEl).appendChild(sketchlist);
      };
      this.itemstore.subscribe("dbChange", showHistory);
      this.itemstore.subscribe("ready", showHistory);
      this.itemstore.subscribe("stateChange", e=>console.log("22statechange", e), "simplepainter");
    }
    
    
    
    this.capturing = false;	// tracks in/out of canvas context
    this.onlyPen = onlypen;
    this.setOptions(true);
    this.lastOptions = this.options.clone();
    this.lastGroup = null;
    this.p = null;
    this.path = "";
    this.x1 = 0;
    this.y1 = 0;
    this.p1 = 0;
    this.x2 = 0;
    this.y2 = 0;
    this.p2 = 0;
    this.n = 0;
//     this.lastP = 0.5;
    this.drawn = 0;
    
    //this.def = 0;
    this.zoomlevel = 1;
    this.zoomlevelcontrol = false;
  
    //@temppaths
    this.temppaths = [];
    this.tempdone = 0;
    for(let i = 0; i < MAXTEMPPATH; i++) {
      let ptemp = document.createElementNS('http://www.w3.org/2000/svg', "path");
      ptemp.setAttribute("class", "temppath");
      ptemp.setAttribute("style", `stroke:none; fill: none;`);
      ptemp.setAttribute("d", "M0,0");
      this.svgcontainer.appendChild(ptemp);
      this.temppaths.push(ptemp);
    }
    
    this.addEventListeners();
    this.draw();
    
    //log(this.arraysToPressureContour([0,2,4,6,8,10,12,14,16], [16,8,0,8,16,8,0,8,16], [1,1,1,1,1,1,1,1,1], 1).replace(/[MC]/g, "").replace(/\s/g, "\n").replace(/,/g, "\t"))
    //log(this.arraysToPressureContour([0,2,4,6,8,10,12,14,16], [0,2,4,6,8,10,12,14,16], [1,1,1,1,1,1,1,1,1], 3).replace(/[MC]/g, "").replace(/\s/g, "\n").replace(/,/g, "\t"))
  
  }
  
  addEventListeners() {
    this.svgcontainer.addEventListener("pointerdown", (e)=>this.pointerdown(e));
    this.svgcontainer.addEventListener("pointerup", (e)=>this.pointerup(e));
    this.svgcontainer.addEventListener("pointermove", (e)=>this.pointermove(e));
    this.svgcontainer.addEventListener("pointerleave", (e)=>this.pointerup(e));
    this.menu.addEventListener("pointerdown", (e)=>e.stopPropagation());
    let togglers = document.querySelectorAll(".toggler");
    for(let i = 0; i < togglers.length; i++)
      togglers[i].addEventListener("click", e=>{
      e.target.closest(".toggleContent").classList.toggle("on");
      e.target.closest(".toggleContent").classList.toggle("off");
    });
    let colorbuttons = document.querySelectorAll(".simplepainter__menu--color");
    for(let i = 0; i < colorbuttons.length; i++) {
      colorbuttons[i].addEventListener("pointerdown", e=>{
        Array.from(colorbuttons).forEach(c=>c.classList.remove("active"));
        e.target.classList.add("active");
        document.querySelector(".opt_color").value = "#"+e.target.dataset.color;
      });
    }
    let thicknessbuttons = document.querySelectorAll(".simplepainter__menu--thickness");
    for(let i = 0; i < thicknessbuttons.length; i++) {
      thicknessbuttons[i].addEventListener("pointerdown", e=>{
        Array.from(thicknessbuttons).forEach(c=>c.classList.remove("active"));
        let button = e.target.closest(".simplepainter__menu--thickness");
        button.classList.add("active");
        document.querySelector(".opt_thickness").value = button.dataset.thickness;
      });
    }
    document.querySelector(".simplepainter__undo").addEventListener("click", _=>{
      //@TODO: check if undo in current slide?? console.log(this.itemstore.getVersionToUndoOrRedo("undo"));
      this.itemstore && this.itemstore.undo();
    });
    document.querySelector(".simplepainter__redo").addEventListener("click", _=>this.itemstore && this.itemstore.redo());
  }
  
  setOptions(nullify) {
    this.options = {};
    this.options.thickness = nullify ? null : +document.querySelector(".opt_thickness").value;
    this.options.color = nullify ? null : "#" + document.querySelector(".opt_color").value.slice(1);
    this.options.painter = nullify ? null : this.el.dataset.painter || null;
    if(this.itemstore) {
      this.itemstore.dNs.changeShelfStates(this.options);
    }
    //localStorage.simplepainter__options = JSON.stringify(this.options);
    //localStorage.simplepainter__currentPathIdn = itemstore.dbStates.latest;
  }

  pointerdown(ev) {
    this.zoomlevelcontrol = {screenX: ev.screenX, clientX: ev.clientX};
    if(ev.pointerType === "pen" || !this.onlyPen) {
      this.setOptions();
      this.c = {
        stroke: this.options.color,
        fill:   false,
        xs:     [getX(ev)],
        ys:     [getY(ev)],
        ps:     [ev.pressure||0],
        //Simplified Points
        sxs:     [getX(ev)],
        sys:     [getY(ev)],
        sps:     [ev.pressure||0],
        //Temppoints
        tempx:   [],
        tempy:   [],
        tempp:   [],
        //other stuff
        lastX:  getX(ev),
        lastY:  getY(ev),
        vx: 0,
        vy: 0,
        ax: 0,
        ay: 0,
        lpx: getX(ev),
        lpy: getY(ev),
      };
      this.p = document.createElementNS('http://www.w3.org/2000/svg', "path");
      this.prof = document.createElementNS('http://www.w3.org/2000/svg', "path");
      //this.path = "M" +getX(ev) + "," + getY(ev) + " L" + getX(ev)+ "," + getY(ev) + " ";
      //this.path = "M" + getX(ev) + "," + getY(ev) + " L";
      //this.p.setAttribute("style", "stroke:#660000; stroke-width:"+this.options.thickness+" ; fill:none;");
      this.prof.setAttribute("style", "stroke:rgba(100,0,0,0.5); stroke-width:"+(this.options.thickness*0.2)+" ; fill:none;");
      this.p.setAttribute("style", `stroke:none; stroke-width: 0; fill: ${this.options.color};`);
      this.p.setAttribute("d", this.path);
      this.svgcontainer.insertBefore(this.p, this.temppaths[0]);
      
      //this.svgcontainer.appendChild(this.prof);
      this.capturing = true;
      this.drawn = 0;
      this.thickenStart = false;
      this.lastPointWasCorner = false;
      this.n = 0;
    }
  }

  pointerup(ev) {
    if((ev.pointerType === "pen" || !this.onlyPen) && this.capturing) {
      
      let c = this.c;
      c.xs.push(getX(ev));
      c.ys.push(getY(ev));
      c.ps.push(c.ps.last() * 0.95);
      //this.path += c.xs.last() + "," + c.ys.last() + " ";
      
      if(c.tempx.length && !(c.tempx.last() === c.sxs.last() && c.tempy.last() === c.sys.last() )) {
        c.sxs.push(c.tempx.last());
        c.sys.push(c.tempy.last());
        c.sps.push(c.tempp.last());
      }
      
      if(c.xs.last() !== c.sxs.last() || c.ys.last() !== c.sys.last()) {
        c.sxs.push(c.xs.last());
        c.sys.push(c.ys.last());
        c.sps.push(c.sps.last()*0.94); //special case because pressure should not decrease linearly over several pixels
      }
  
      //smoothen Pressure (to avoid strange rendering)
      var coeff = this.options.thickness;
      let getThickness = p => coeff*(PRESSUREBASE + PRESSUREINCREASE*p);
      let smoothenPressure = n => {
        let didSomething = false;
        for(let i = 1; i < c.sxs.length; i++) {
          let d = Math.hypot(c.sxs[i] - c.sxs[i-1], c.sys[i] - c.sys[i-1]);
          let dp = c.sps[i] - c.sps[i-1];
          let dt = getThickness(c.sps[i]) - getThickness(c.sps[i-1]);
          const TOLERANCE = 0.05;
          
          if(Math.abs(dt) > TOLERANCE * d) {
            let equalizeP = 0;
            const MaxFactor = 0.25;
            while(equalizeP < MaxFactor && Math.abs(getThickness(c.sps[i] - dp*equalizeP) - getThickness(c.sps[i-1] + dp*equalizeP)) > TOLERANCE * d) {
              equalizeP += 0.05;
            }
            c.sps[i] -= dp*equalizeP;
            c.sps[i-1] += dp*equalizeP;
            didSomething = true;
          }
        }
        if(didSomething && n<100) smoothenPressure(n+1);
      };
      smoothenPressure(0);
      
      this.p.setAttribute("d", this.arraysToPressureContour(this.c.sxs, this.c.sys, this.c.sps));
      //this.svgcontainer.removeChild(this.prof);
      this.capturing = false;
      
      //@Temppath reset
      if(this.tempdone > 0) {
        for(let i = 0; i < Math.min(MAXTEMPPATH, this.tempdone/10); i++) {
          this.temppaths[i].setAttribute("d", "M0,0");
          this.temppaths[i].setAttribute("style", `stroke:none; fill: none;`);
        }
        this.tempdone = 0;
      }

      if(this.itemstore) {
        setTimeout(_=>this.saveItem(), 0);
      }
    }
  }

  pointermove(ev) {
    if(this.capturing && (ev.pointerType === "pen" || !this.onlyPen)) {
      let p = ev.pressure===undefined ? 0.5 : (ev.pressure === 0 ? this.c.ps.last() * 0.9 : ev.pressure);
      this.addPoint(getX(ev), getY(ev), p);
    }
//     if(this.capturing &&  ev.pointerType === "pen" && ev.pressure === 0) {                                                                                                                                                                                             qqqqqqqq11) {
//       log("emergency up");
//       this.pointerup(ev);
//     }
    if(Math.abs(ev.clientX - this.zoomlevelcontrol.clientX) > 10) {
      this.zoomlevel = (ev.screenX - this.zoomlevelcontrol.screenX) / (ev.clientX - this.zoomlevelcontrol.clientX);
    }
  }

  addPoint(x,y,pressure) {
    if(x===this.c.xs.last() && y===this.c.ys.last()) {
      this.c.ps[this.c.ps.length - 1] = 0.5 * (this.c.ps.last() + pressure);
      return;
    }
    this.c.xs.push(x);
    this.c.ys.push(y);
    this.c.ps.push(pressure);
    let {vx, vy, ax, ay, lpx, lpy, xs, ys, ps} = this.c;
    let L = xs.length;
    var px = (17*xs[L-1] + 12*xs[L-2] - (3*xs[L-3]||3*xs[L-2]) ) / 26;
    var py = (17*ys[L-1] + 12*ys[L-2] - (3*ys[L-3]||3*ys[L-2]) ) / 26;
	  var nvx = L===3 ? (xs[L-1] - xs[L-2]) : 0.8 * vx + 0.2 * (px - lpx);
	  var nvy = L===3 ? (ys[L-1] - ys[L-2]) : 0.8 * vy + 0.2 * (py - lpy);
	  ax = 0.8 * ax + 0.2 * ( (xs[L-1] - xs[L-2]) - vx);
	  ay = 0.8 * ay + 0.2 * ( (ys[L-1] - ys[L-2]) - vy);
    Object.assign(this.c, {vx: nvx, vy: nvy, ax, ay, lpx: px, lpy: py});
    //this.path += px + "," + py + " ";
    
    this.c.tempx.push(this.aggregate(this.c.xs, L));
    this.c.tempy.push(this.aggregate(this.c.ys, L));
    this.c.tempp.push(this.aggregate(this.c.ps, L));
    
    //corner
    if(L>=8) {
      let x1stBefore = this.get1stDerivative(this.c.xs, L-6);
      let y1stBefore = this.get1stDerivative(this.c.ys, L-6);
      let x1stAfter = this.get1stDerivative(this.c.xs, L-3);
      let y1stAfter = this.get1stDerivative(this.c.ys, L-3);
      let anglebefore = (Math.atan2(-y1stBefore, x1stBefore)*180/Math.PI + 360)%360;
      let angleafter = (Math.atan2(-y1stAfter, x1stAfter)*180/Math.PI + 360)%360;
      let da = ((anglebefore - angleafter) + 720 + 180)%360 - 180;
      if(Math.abs(da)>45 && !this.lastPointWasCorner) {
        if(this.c.tempx.length>=2 || 0) {
          this.c.sxs = this.c.sxs.concat(this.c.tempx.slice(-2));
          this.c.sys = this.c.sys.concat(this.c.tempy.slice(-2));
          this.c.sps = this.c.sps.concat(this.c.tempp.slice(-2));
        }
        else {
          this.c.sxs.push(this.c.tempx[0]);
          this.c.sys.push(this.c.tempy[0]);
          this.c.sps.push(this.c.tempp[0]);
        }
        this.lastPointWasCorner = true;
        this.c.tempx = [];
        this.c.tempy = [];
        this.c.tempp = [];
      }
    }
    
    //Test
    // var point = document.createElementNS('http://www.w3.org/2000/svg',"circle");
        // point.setAttribute("style", "stroke:none; fill:"+this.options.color+";");
        // point.setAttribute("cx", 5*x);
        // point.setAttribute("cy", 5*y);
        // point.setAttribute("r", 2);
        // this.svgcontainer.appendChild(point);
  }
  
  aggregate(a, L) {
    if(L>9)  return (-11*a[L-10] + /*0*a[L-9] +*/ 9*a[L-8] + 16*a[L-7] + 21*a[L-6] + 24*a[L-5] + 12.5*a[L-4])/143 + (3.5*a[L-4] + 6*a[L-3] + 3*a[L-2] - 2*a[L-1])/21; //assymmetrische Savitzky Golay
    if(L===1) return a[0];
    if(L===2) return 0.25*a[0] + 0.75*a[1];
    if(L===3) return 0.1*a[0] + 0.4*a[1] + 0.5*a[2];
    if(L===4) return 0.08*a[0] + 0.22*a[1] + 0.40*a[2] + 0.30*a[3];
    if(L===5) return 0.05*a[0] + 0.15*a[1] + 0.30*a[2] + 0.30*a[3] + 0.20*a[4];
    if(L===6) return (-2*a[0] + 3*a[1] + 6*a[2] + 3.5*a[3])/21 + (8.5*a[3] + 12*a[4] - 3*a[5])/35;
    if(L===7) return (-21*a[0] + 14*a[1] + 39*a[2] + 54*a[3] + 29.5*a[4])/231 + (8.5*a[4] + 12*a[5] - 3*a[6])/35;
    if(L===8) return (-36*a[0] + 9*a[1] + 44*a[2] + 69*a[3] + 84*a[4] + 44.5*a[5])/429 + (8.5*a[5] + 12*a[6] - 3*a[7])/35;
    if(L===9) return (-36*a[0] + 9*a[1] + 44*a[2] + 69*a[3] + 84*a[4] + 44.5*a[5])/429 + (3.5*a[5] + 6*a[6] + 3*a[7] - 2*a[8])/21;
  }
  
  get1stDerivative(arr, center) {
    return -0.2*arr[center-2] - 0.1*arr[center-1] + 0.1*arr[center+1] + 0.2*arr[center+2];
  }

  arraysToPressureContour(xs, ys, ps, thickness) {
    var coeff = thickness === undefined ? this.options.thickness : thickness;
    var L = xs.length;
    var points = xs.map(function(e, i) {return {x: xs[i], y: ys[i], t: coeff*(PRESSUREBASE + PRESSUREINCREASE*ps[i])};});
    let rs = {left: [], right: []};
    //cap inverted edges
    if(L>=3) {
      if((xs[L-2]-xs[L-1])*(xs[L-3]-xs[L-2]) + (ys[L-2]-ys[L-1])*(ys[L-3]-ys[L-2]) < 0) {
        points = points.slice(0, -1);
      }
      if(L >= 4 && (xs[1]-xs[0])*(xs[2]-xs[1]) + (ys[1]-ys[0])*(ys[2]-ys[1]) < 0) {
        points = points.slice(1);
      }
    }
    let getTurningEdge = function(role = "start||turn", edge, mid, inner) {
      if(!(edge&&mid&&inner)) log(edge, mid, inner)
      let a = getControlPoints(edge.x, edge.y, mid.x, mid.y, inner.x, inner.y, 0.4);
      let dx = edge.x - mid.x;
      let dy = edge.y - mid.y;
      let dsq = dx*dx + dy*dy;
      let tm = {x: a[0]-mid.x, y: a[1]-mid.y}; //Tangente an Mid
      let mt = {x: tm.x - 2*(tm.x*dx+tm.y*dy)/dsq * dx, y: tm.y - 2*(tm.x*dx+tm.y*dy)/dsq * dy}; //gespiegelte Tangente an Kurvensegment
      let mtl = Math.hypot(mt.x, mt.y);
      let pressthick = edge.t;
      let [corrx, corry] = [-pressthick * mt.x/mtl, -pressthick * mt.y/mtl];
      if(dsq==0) {warn(dsq, " GLEICH NULL!?? (Schaue in alter Version)");}
      let helperpoints = [];
      for(let step = 0; step < 9; step+=2) {
        let da = - 4/9*Math.PI + step/9*Math.PI; // + Math.atan2(mt.y, mt.x);
        helperpoints.push({x: edge.x + Math.cos(da) * corrx - Math.sin(da) * corry, y: edge.y + Math.cos(da) * corry + Math.sin(da) * corrx})
      }
      return {
        results: helperpoints
      }
    };
    let getTangents = function(p, i, arr) {
      if(i === arr.length-1) return; //last point
      let next = arr[i+1];
      let dx = next.x - p.x;
      let dy = next.y - p.y;
      let d = hypot(dx, dy);
      //log(dx, dy, d);
      if(i===0) {
        rs.left.push({x: p.x + dy / d * p.t, y: p.y - dx / d * p.t});
        rs.right.push({x: p.x - dy / d * p.t, y: p.y + dx / d * p.t});
        rs.left.push({x: next.x + dy / d * next.t, y: next.y - dx / d * next.t});
        rs.right.push({x: next.x - dy / d * next.t, y: next.y + dx / d * next.t});
        //log(rs);
        return;
      }
      let getpoint = function(side) {
        let sign = side === "left" ? +1 : -1;
        let start = {x: p.x + sign * dy / d * p.t, y: p.y - sign * dx / d * p.t};
        let end = {x: next.x + sign * dy / d * next.t, y: next.y - sign * dx / d * next.t};
        let rx = end.x - start.x, ry = end.y - start.y;
        let [prevstart, prevend] = rs[side].slice(-2);
        let ox = prevend.x - prevstart.x, oy = prevend.y - prevstart.y;
        
        if(ox*ry - oy*rx === 0) {
          //if(ox*rx+oy*ry < 0) warn("antiparallel");
          rs[side][rs[side].length - 1] = {x: 0.5 * (start.x + prevend.x), y: 0.5 * (start.y + prevend.y)};
        }
        else {
          let anglebefore = (Math.atan2(oy, ox)*180/Math.PI + 360)%360;
          let angleafter = (Math.atan2(ry, rx)*180/Math.PI + 360)%360;
          let da = ((anglebefore - angleafter) + 720 + 180)%360 - 180;
          if(Math.abs(da) > 60) {
            let loopover = da*sign > 0;
            let turnangle = loopover ? 360-Math.abs(da) : Math.abs(da);
            let numsteps = Math.floor(turnangle/30);
            let helperangle = turnangle / (numsteps + 1);
            let helperpoints = [];
            //log(side, da, loopover, turnangle, numsteps, helperangle);
            let rotx = start.x - p.x, roty = start.y - p.y;
            for(let step = 0; step<=numsteps; step++) {
              let a = - sign * helperangle*step/180*Math.PI;
              helperpoints.unshift({x: p.x + rotx * Math.cos(a) - roty * Math.sin(a), y: p.y + roty * Math.cos(a) + rotx * Math.sin(a)})
            }
            rs[side] = rs[side].concat(helperpoints)
          }
          else {
            let t = (start.x*ry - start.y*rx - prevstart.x * ry + prevstart.y * rx) / (ox*ry - oy*rx);
            if(t > 1) {
              rs[side][rs[side].length - 1] = {x: 0.5 * (start.x + prevend.x), y: 0.5 * (start.y + prevend.y)};
            }
            else if(t > 0) {
              rs[side][rs[side].length - 1] = {x: prevstart.x + t * ox, y: prevstart.y + t * oy};
            }
            else if(t < 0) {
              rs[side].pop();
            }
          }
        }
        rs[side].push(end);
      };
      getpoint("left");
      getpoint("right");
    };
    let results = [];
    if(points.length === 1) {
      for(let step = 0; step <= 11; step++) {
        let a = step * 30 / 180 * Math.PI;
        let p = points[0];
        results.push({x: p.x + p.t * Math.cos(a), y: p.y + p.t * Math.sin(a)})
      }
    }
    else {
      if(points.length === 2) {
        points = [points[0], {x: 0.5*(points[0].x+points[1].x), y: 0.5*(points[0].y+points[1].y), t: 0.5*(points[0].t+points[1].t)}, points[1]];
      }
      points.forEach(getTangents);
      let s = getTurningEdge("start", ...points.slice(0, 3));
      let t = getTurningEdge("turn", ...points.slice(-3).reverse());
      results = [...s.results, ...rs.left, ...t.results, ...rs.right.reverse()];
    }
    let path = results.length ? arraysToBezier(results.map(p=>p.x).concat([results[0].x]), results.map(p=>p.y).concat([results[0].y])) : "";
    return path;
  }
  
  checkForCorner() {
    let L = this.c.xs.length;
    if(L<8) return false;
    let x1stBefore = this.get1stDerivative(this.c.xs, L-6);
    let y1stBefore = this.get1stDerivative(this.c.ys, L-6);
    let x1stAfter = this.get1stDerivative(this.c.xs, L-3);
    let y1stAfter = this.get1stDerivative(this.c.ys, L-3);
    let anglebefore = (Math.atan2(-y1stBefore, x1stBefore)*180/Math.PI + 360)%360;
    let angleafter = (Math.atan2(-y1stAfter, x1stAfter)*180/Math.PI + 360)%360;
    let da = ((anglebefore - angleafter) + 720 + 180)%360 - 180;
    return Math.abs(da)>45;
  }

  draw() {
    requestAnimationFrame(_=>this.draw());
    if(!this.capturing) return;
    let {vx, vy, ax, ay, lpx, lpy, xs, ys} = this.c;
    let L = xs.length;
    if(L === this.drawn) return;
    this.drawn = L;
    let lag = 0.4;
    let factora = 0.2;
    var predictorsX = [1,2,3].map(t=>lpx + lag*vx*t + factora*t*t*ax);
    var predictorsY = [1,2,3].map(t=>lpy + lag*vy*t + factora*t*t*ay);
    //var qp = "M"+lpx+","+lpy+" L" +  // + " " + c.xs[c.xs.length-1]+","+ c.ys[c.ys.length-1];
    var dprof = ""; //this.c.xs[this.c.xs.length-1]+","+ this.c.ys[this.c.ys.length-1] + " " + this.c.xs[this.c.xs.length-1]+","+ this.c.ys[this.c.ys.length-1];
    //this.p.setAttribute("d", this.path);
    //this.prof.setAttribute("d", qp);
    if(this.c.tempx.length) {
      var d = this.distanceFromLastPoint();
      
      if(d>2*Math.sqrt(this.options.thickness)/this.zoomlevel) {
        if(d>4*this.c.tempx.length) {
          this.c.sxs = this.c.sxs.concat(this.c.tempx);
          this.c.sys = this.c.sys.concat(this.c.tempy);
          this.c.sps = this.c.sps.concat(this.c.tempp);
        }
        else {
          this.c.sxs.push(this.c.tempx.last());
          this.c.sys.push(this.c.tempy.last());
          this.c.sps.push(this.c.tempp.last());
        }
        this.c.tempx = [];
        this.c.tempy = [];
        this.c.tempp = [];
        this.lastPointWasCorner = false;
      }
    }
    if(!this.thickenStart && this.c.sps.length>=2) {
      this.thickenStart = "done";
      this.c.sps[0] = 0.9 * this.c.sps[1];
    }
    
    //@Temppath
    if(this.c.sxs.length - this.tempdone >= 15 && this.tempdone < 10*MAXTEMPPATH) {
      let ptemp = this.temppaths[Math.floor(this.tempdone/10)];
      ptemp.setAttribute("style", `stroke:none; stroke-width: 0; fill: ${this.options.color};`);
      let txs = this.tempdone === 0 ? this.c.sxs.slice(0,15) : this.c.sxs.slice(this.tempdone - 5, this.tempdone + 15);
      let tys = this.tempdone === 0 ? this.c.sys.slice(0,15) : this.c.sys.slice(this.tempdone - 5, this.tempdone + 15);
      let tps = this.tempdone === 0 ? this.c.sps.slice(0,15) : this.c.sps.slice(this.tempdone - 5, this.tempdone + 15);
      tps = this.tempdone === 0 ? tps : tps.map((p,i)=> i<5 ? p*(1-0.2*(5-i)) : p);
      tps = tps.map((p,i)=> i>tps.length-5 ? p*(1-0.2*(i-(tps.length-5))) : p);
      let ptemppath = this.arraysToPressureContour(txs, tys, tps);
      ptemp.setAttribute("d", ptemppath);
      this.tempdone = this.c.sxs.length-5;
    }
    let sxs = this.c.sxs.slice(Math.max(0, (this.tempdone||0)-5));
    let sys = this.c.sys.slice(Math.max(0, (this.tempdone||0)-5));
    let sps = this.c.sps.slice(Math.max(0, (this.tempdone||0)-5));
    sps = this.tempdone === 0 ? sps : sps.map((p,i)=> i<5 ? p*(1-0.2*(5-i)) : p);
    let path;
    if(!vx && !vy && !ax && !ay) {
      path = this.arraysToPressureContour(sxs, sys, sps);
    }
    else {
      path = this.arraysToPressureContour(sxs.concat([lpx, ...predictorsX]), sys.concat([lpy, ...predictorsY]), sps.concat([0,1,2,3].map(_=>this.c.ps.last())))
    }
    this.p.setAttribute("d", path);
    //localStorage.simplepainter__currentPath = path;
  }
  
  distanceFromLastPoint() {
    return Math.hypot(this.c.tempx.last()-this.c.sxs.last(), this.c.tempy.last()-this.c.sys.last())
  }

  saveItem() {
    if(Object.keys(this.options).filter(k=>this.lastOptions[k] !== this.options[k]).length) {
      let leaves = this.itemstore.getLeavesAsArray();
      var tree_parent = this.lastOptions.painter === null ? "" : (leaves.find(i=>i.painter === this.options.painter) || {}).tree_parent;
      var tree_pos = leaves.filter(i=>i.painter === this.options.painter && i.tree_parent === tree_parent).map(i=>+i.tree_pos).sort((u,v)=>v-u)[0] + 8 || 8;
      var obj = Object.assign({}, this.options, {group: true, tree_parent, tree_pos});
      this.itemstore.dDb.addItem("sketch", obj).then(idn=>this.lastGroup = idn).then(_=>this.saveStroke());
      this.lastOptions = this.options.clone();
    }
    else {this.saveStroke();}
  }

  saveStroke() {
    var tree_pos = this.itemstore.getLeavesAsArray().filter(i=>i.tree_parent === this.lastGroup).map(i=>+i.tree_pos).sort()[0] + 8 || 8;
    var obj = {c: this.deflate(this.c), tree_parent: this.lastGroup, tree_pos};
    this.itemstore.dDb.addItem("sketch", obj);
  }
  
  deflate(c) {
    const coorddeflator = x => base62.intToB62(x*64, 3);
    const pressuredeflator = x => base62.intToB62(x*60*64, 2);
    let {sxs, sys, sps} = c;
    let [xsd, ysd, psd] = [sxs.map(coorddeflator).join(""), sys.map(coorddeflator).join(""), sps.map(pressuredeflator).join("")];
    return {xsd, ysd, psd}  //xsdeflated etc
  }
  
  inflate(c) {
    const coordinflator = x => +base62.b62ToInt(x)/64;
    const pressureinflator = x => +base62.b62ToInt(x)/64/60;
    let {xsd, ysd, psd} = c;
    let [xsa, ysa, psa] = [xsd.match(/.{3}/g), ysd.match(/.{3}/g), psd.match(/.{2}/g)];
    let [sxs, sys, sps] = [xsa.map(coordinflator), ysa.map(coordinflator), psa.map(pressureinflator)];
    return {sxs, sys, sps};
  }
  
  loadStrokes() {
    var leaves = this.itemstore.getLeavesAsArray();
    leaves.filter(l=>l.tree_parent === "").sort((u,v)=>u.tree_pos>v.tree_pos?1:-1).forEach(l=>this.renderStrokeOrGroup(l.idn, leaves));
  }
  
  renderStrokeOrGroup(idn, leaves = this.itemstore.getLeavesAsArray() ) {
    leaves.filter(k => k.tree_parent === idn).sort((u,v)=>u.tree_pos>v.tree_pos?1:-1).forEach(k=>{
      if(k.group) {
        this.renderStrokeOrGroup(k.idn, leaves)
      }
      else {
        this.loadStroke(k.idn);
      }
    });
  }

  getSvgFromStroke(idn) {
    var item = this.itemstore.getItem(idn);
    if(item.group) return;
    var parent = this.itemstore.getItem(item.tree_parent);
    var p = document.createElementNS('http://www.w3.org/2000/svg', "path");
    if(!item.c.sxs) {
      Object.assign(item.c, this.inflate(item.c));
    }
    p.setAttribute("style", "stroke:none; stroke-width:0 ; fill:" + parent.color + ";");
    p.setAttribute("d", this.arraysToPressureContour(item.c.sxs, item.c.sys, item.c.sps, parent.thickness));
    return p;
  }
  
  loadStroke(idn) {
    let p = this.getSvgFromStroke(idn)
    p.setAttribute("data-idn", idn);
    this.svgcontainer.insertBefore(p, this.temppaths[0]);
//    for(let i=0; i<item.c.sxs.length; i++) {
//      let c = document.createElementNS('http://www.w3.org/2000/svg', "circle");
//      c.setAttribute("style", "stroke:none; stroke-width:0 ; fill:white;");
//      c.setAttribute("cx", item.c.sxs[i]);
//      c.setAttribute("cy", item.c.sys[i]);
//      c.setAttribute("r", "1");
//      this.svgcontainer.appendChild(c);
//    }
  }

  clear() {
    while (this.svgcontainer.firstChild && !this.svgcontainer.firstChild.classList.contains("temppath")) this.svgcontainer.removeChild(this.svgcontainer.firstChild);
  }
}

function getX(ev) {
  return ev.offsetX !== undefined ? ev.offsetX : warn("offsetX needs polyfill");
}

function getY(ev) {
  return ev.offsetY !== undefined ? ev.offsetY : warn("offsetY needs polyfill");
}

function getNeighbours(arr, i, len) {
  var L = len || arr.length;
  return {prev: arr[(L + i - 1) % L], next: arr[(i + 1) % L]};
}

function getControlPoints(x0, y0, x1, y1, x2, y2, t) {
  var d01 = Math.hypot(x1-x0, y1-y0);
  var d12 = Math.hypot(x2-x1, y2-y1);
  if(d01 + d12 === 0) {return [x1, y1, x1, y1]}
  var fa = t * d01 / (d01 + d12);   // scaling factor for triangle Ta
  var fb = t * d12 / (d01 + d12);   // ditto for Tb, simplifies to fb=t-fa
  var p1x = x1 - fa * (x2 - x0);    // x2-x0 is the width of triangle T
  var p1y = y1 - fa * (y2 - y0);    // y2-y0 is the height of T
  var p2x = x1 + fb * (x2 - x0);
  var p2y = y1 + fb * (y2 - y0);
  return [p1x.toFixed(2), p1y.toFixed(2), p2x.toFixed(2), p2y.toFixed(2)];
}

function arraysToBezier(xs, ys) {
  const rnd = x=>x.toPrecision(3);
  var p = "M" + xs[0] + "," + ys[0] + " ";
  p += " C" + (xs[0] + (xs[1] - xs[0]) / 3) + "," + (ys[0] + (ys[1] - ys[0]) / 3) + " ";
  for(var i = 1; i < xs.length - 1; i++) {
    var a = getControlPoints(xs[i - 1], ys[i - 1], xs[i], ys[i], xs[i + 1], ys[i + 1], 0.4);
    p += a[0] + "," + a[1] + " " + xs[i].toFixed(2) + "," + ys[i].toFixed(2) + " " + a[2] + "," + a[3] + " ";
  }
  //log(xs.last(), xs[xs.length - 2],  xs.last() + (xs[xs.length - 2] - xs.last()) / 3);
  p += (xs.last() + (xs[xs.length - 2] - xs.last()) / 3) + "," + (ys.last() + (ys[ys.length - 2] - ys.last()) / 3) + " ";
  p += xs.last() + "," + ys.last();
  return p;
}

//TODO
// //pousi
// function render(src){
// var image = new Image();
// image.onload = function(){
// var canvas = document.getElementsByClassName("pousi")[0];
// canvas.width = window.innerWidth;
// canvas.height = window.innerHeight;
// var ctx = canvas.getContext("2d");
// ctx.clearRect(0, 0, canvas.width, canvas.height);
// var scalefactor = Math.max(image.width/canvas.width, image.height/canvas.height);
// //      canvas.width = image.width;
// //      canvas.height = image.height;
// ctx.drawImage(image, 0, 0, image.width/scalefactor, image.height/scalefactor);
// };
// image.src = src;
// }
//
// function loadImage(src){
// //	Prevent any non-image file type from being read.
// log(src);
// if(!src || !src.type.match(/image.*/)){
//  console.log("The dropped file is not an image: ", src ? src.type : "no src");
//  return;
//}
//
////	Create our FileReader and run the results through the render function.
//var reader = new FileReader();
//reader.onload = function(e){
//  render(e.target.result);
//};
//reader.readAsDataURL(src);
//}
//
//var target = document.getElementById("mysvg");
//target.addEventListener("dragover", function(e){e.preventDefault(); e.dataTransfer.dropEffect = 'copy';}, false);
//target.addEventListener("drop", function(e){
//  e.preventDefault();
//  log(e);
//  log(e.dataTransfer.files[0]);
//  loadImage(e.dataTransfer.files[0]);
//}, true);
//
//document.onpaste = function (event) {
//      // use event.originalEvent.clipboard for newer chrome versions
//      var items = (event.clipboardData  || event.originalEvent.clipboardData).items;
//      console.log((event.clipboardData  || event.originalEvent.clipboardData).getData("text/plain"), items); // will give you the mime types
//      // find pasted image among pasted items
//      var blob = null;
//      for (var i = 0; i < items.length; i++) {
//        if (items[i].type.indexOf("image") === 0) {
//          blob = items[i].getAsFile();
//        }
//      }
////    // load image if there is a pasted image
////    if (blob !== null) {
////      var reader = new FileReader();
////      reader.onload = function(event) {
////        console.log(event.target.result); // data url!
////        document.getElementById("pastedImage").src = event.target.result;
////      };
////      reader.readAsDataURL(blob);
////    }
//    }




/***/ }),

/***/ 214:
/***/ (function(module, exports) {

var CHARACTER_SET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

/**
 *
 * @param {Number} int
 * @param {Number||Boolean} zeropad - Defaults to false
 * @returns {string}
 */
function intToB62(int, zeropad=false){
  var integer = Math.round(int);
  var s = integer===0 ? '0' : '';
  while (integer > 0) {
    s = CHARACTER_SET[integer % 62] + s;
    integer = Math.floor(integer/62);
  }
  return zeropad ? ("0000000000000000000000000000000"+s).slice(-zeropad) : s;
}

/**
 *
 * @param {string} base62String
 * @returns {Integer}
 */
function b62ToInt(base62String) {
  var val = 0;
  var base62Chars = base62String.split("").reverse();
  base62Chars.forEach(function(character, index){
    val += CHARACTER_SET.indexOf(character) * Math.pow(62, index);
  });
  return val;
}

/**
 *
 * @param {Date} date
 * @returns {String}
 */
function dateToB62(date = new Date()) {
  return intToB62(date.getTime(), 7);
}

/**
 *
 * @param {string} base62String
 * @returns {Date}
 */
function b62ToDate(base62String) {
  return new Date(b62ToInt(base62String));
}


module.exports = {intToB62, b62ToInt, dateToB62, b62ToDate};

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_modules_simplepainter__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_modules_domhelpers__ = __webpack_require__(101);




let s = window.s = new __WEBPACK_IMPORTED_MODULE_0_modules_simplepainter__["a" /* Simplepainter */]({onlypen: true, itemstore: false});


/***/ })

/******/ });
//# sourceMappingURL=test-bundle.js.map