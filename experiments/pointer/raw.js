import {Simplepainter} from "modules/simplepainter";
import {Store, Item} from "modules/itemstore";
import {on} from "modules/domhelpers";

let store = window.store = new Store("simplepainterdemo", {longpolling: false, logActions: false, dbcentral: "MEMORY"});
store.moveOldToArchive();

let s = window.s = new Simplepainter({onlypen: true, itemstore: store});

store.subscribe("ready", {onReady: _=> s.loadStrokes()});

store.subscribe("dbChange", {onDbChange: e=>{
  if(e.action!=="dbResponse") return;
  let [r, newIdvs, newDocs, oldIdvs, oldDocs] = e.args;
  newIdvs.forEach((idv, index)=>{
    let oldIdv = oldIdvs[index];
    let newItem = store.dbStates.versions.find(i=>i.idv===idv);
    s.clear();
    s.loadStrokes();
    if(!oldIdv || store.dbStates.versions.find(i=>i.idv===oldIdv).deleted) {
      console.log("(re)created");
      s.renderStrokeOrGroup(newItem.tree_parent || newItem.idn);
    }
    else if(newItem.deleted) {
      console.log("deleted");
      s.renderStrokeOrGroup(newItem.tree_parent || newItem.idn);
      s.renderStrokeOrGroup(newItem.tree_parent || newItem.idn);
    }
    else {
      console.log("updated");
      s.renderStrokeOrGroup(newItem.tree_parent || newItem.idn);
    }
  })
}});



// var $el = $(".simplepainter");
// var svgcontainer = document.createElementNS('http://www.w3.org/2000/svg', "svg");
// svgcontainer.setAttribute('class', "simplepainter__svgcontainer");
// $el.get(0).appendChild(svgcontainer);
// svgcontainer.setAttribute("width", $el.width());
// svgcontainer.setAttribute("height", $el.height());
//
// var p = document.createElementNS('http://www.w3.org/2000/svg', "path");
// p.setAttribute("style", `stroke:none; stroke-width: 0; fill: #000`);
// p.setAttribute("d", "");
// svgcontainer.appendChild(p);
//
// var pfix = "M100,100 C";
// for(let i=0; i<50; i++) {
//   var x = [0,0,0].map(_=>100+200*Math.random().toFixed(2));
//   var y = [0,0,0].map(_=>100+200*Math.random().toFixed(2));
//   pfix += `${x[0]},${y[0]} ${x[1]},${y[1]} ${x[2]},${y[2]} `;
// }
//
// function randomizePath() {
//   let path = pfix;
//   for(let i=0; i<50; i++) {
//     var x = [0,0,0].map(_=>100+200*Math.random().toFixed(2));
//     var y = [0,0,0].map(_=>100+200*Math.random().toFixed(2));
//     path += `${x[0]},${y[0]} ${x[1]},${y[1]} ${x[2]},${y[2]} `;
//   }
//   p.setAttribute("d", path);
//   requestAnimationFrame(randomizePath);
// }
//
// requestAnimationFrame(randomizePath);

on("svg", "click", "path", function(e) {
  if(!this || !this.dataset || !this.dataset.idn) return;
  log("click " + this.dataset.idn);
//  let c = store.getItem(this.dataset.idn).c;
//  log(c.sxs.map((x,i)=>`${x}\t${c.sys[i]}\t${c.sps[i]}\n`).join(""));
//  log("atp", s.arraysToPressureContour(c.sxs, c.sys, c.sps));
//  log(s.arraysToPressureContour(c.sxs, c.sys, c.sps, 10).replace(/[MC]/g, "").replace(/\s/g, "\n").replace(/,/g, "\t"))
  //store.dDb.deleteItem(this.dataset.idn);
  //Gruppe muss auch gelösch werden
});

//store.moveOldToArchive();