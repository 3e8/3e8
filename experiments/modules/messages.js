const msg = (html) => {
  let div = document.createElement("div");
  let overlay = document.createElement("div");
  overlay.classList.add("overlay");
  div.classList.add("message");
  div.innerHTML = html;
  document.body.appendChild(overlay);
  document.body.appendChild(div);
  let removeMe = e=>{
    document.body.removeChild(div);
    document.body.removeChild(overlay);
  };
  overlay.addEventListener("click", removeMe);
  div.addEventListener("click", removeMe);
};

const choosebox = (html, options, callback) => {
  var div = document.createElement("div");
  var overlay = document.createElement("div");
  var btn = document.createElement("button");
  overlay.classList.add("overlay");
  div.classList.add("message");
  div.classList.add("choosebox");
  let chooseMe = (i) => {
    document.body.removeChild(div);
    document.body.removeChild(overlay);
    callback(i);
  };
  div.innerHTML = html;
  options.forEach((o,i)=>{
    let btn = document.createElement("button");
    btn.classList.add("choosebox__option");
    btn.addEventListener("click", e=>chooseMe(i));
    btn.innerHTML = o;
    div.appendChild(btn);
  });
  document.body.appendChild(overlay);
  document.body.appendChild(div);
};

var css = `
.overlay {
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0%;
  left: 0%;
  background-color: #888;
  opacity: 0.2;
}

.message {
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 70vw;
  padding: 2em;
  background-color: white;
  opacity: 1;
}

.choosebox__option {
  display: block;
  width: 100%;
  background-color: #ddd;
  border: none;
  padding: 0;
  margin: 0.3em 0;
  text-align: left;
  cursor: pointer;
}

.choosebox__option:hover {
  color: #003366;
  background-color: #ccc;
}
`;
var style = document.createElement('style');
style.type = 'text/css';
if (style.styleSheet){
  style.styleSheet.cssText = css;
} else {
  style.appendChild(document.createTextNode(css));
}

document.head.appendChild(style);

export {msg, choosebox};
