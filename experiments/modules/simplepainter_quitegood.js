// TODO
/*
-einzelen Punkte und kleine Pfade
-Strichende, z.B. unten an m, r
-performance für lange Pfade
*/


class Simplepainter {
  constructor({selector = ".simplepainter", menu="auto", itemstore = false}={}) {
    this.$el = $(selector);
    this.$el.addClass("simplepainter");
    this.pousi = $("<canvas class='simplepainter__canvas pousi'></canvas>").appendTo(this.$el).get(0);
    this.svgcontainer = document.createElementNS('http://www.w3.org/2000/svg', "svg");
    this.svgcontainer.setAttribute('class', "simplepainter__svgcontainer");
    $(this.svgcontainer).appendTo(this.$el).get(0);
    this.svgcontainer.setAttribute("width", this.$el.width());
    this.svgcontainer.setAttribute("height", this.$el.height());
    if(menu == "auto") {
      this.menu = $(`
        <div class="toggleContent off simplepainter__menu" style="text-align: right; font-size: 2em;">
          <div class="toggler ifOn" style="color: grey;">...</div>
          <div class="toggler ifOff">...</div>
          <div class="ifOn"><input class="color opt_color" value="222266"></div>
          <div class="ifOn">
            Stiftdicke <select class="opt_thickness"><option>0.5</option><option>1</option><option>1.5</option><option>2</option><option selected>3</option><option>4</option><option>6</option><option>8</option><option>10</option></select><br>
            Details <select  class="opt_details"><option>2</option><option selected>4</option><option>6</option><option>8</option><option>10</option></select>
          </div>
        </div>
      `).appendTo(this.$el)
    }
    this.itemstore = itemstore;
    this.capturing = false;	// tracks in/out of canvas context
    this.onlyPen = true;
    this.setOptions(true);
    this.lastOptions = this.options.clone();
    this.lastGroup = null;
    this.p = null;
    this.path = "";
    this.x1 = 0;
    this.y1 = 0;
    this.p1 = 0;
    this.x2 = 0;
    this.y2 = 0;
    this.p2 = 0;
    this.n = 0;
//     this.lastP = 0.5;
    this.drawn = 0;
    
    //this.def = 0;
    this.zoomlevel = 1;
    this.zoomlevelcontrol = false;
    
    this.addEventListeners();
    this.draw();
    
    log(this.arraysToPressureContour([0,2,4,6,8,10,12,14,16], [0,2,4,6,8,10,12,14,16], [1,1,1,1,1,1,1,1,1], 3).replace(/[MC]/g, "").replace(/\s/g, "\n").replace(/,/g, "\t"))
  
  }
  
  addEventListeners() {
    this.svgcontainer.addEventListener("pointerdown", (e)=>this.pointerdown(e));
    this.svgcontainer.addEventListener("pointerup", (e)=>this.pointerup(e));
    this.svgcontainer.addEventListener("pointermove", (e)=>this.pointermove(e));
    this.svgcontainer.addEventListener("pointerleave", (e)=>this.pointerup(e));
    $(".toggler").on("click", function() {$(this).closest(".toggleContent").toggleClass("on off")});
  }

  setOptions(nullify) {
    this.options = {};
    this.options.thickness = nullify ? null : +$(".opt_thickness").find("option:selected").text();
    this.options.details = nullify ? null : +$(".opt_details").find("option:selected").text();
    this.options.color = nullify ? null : "#" + $(".opt_color").val();
    this.options.painter = nullify ? null : this.$el.data("painter");
    //localStorage.simplepainter__options = JSON.stringify(this.options);
    //localStorage.simplepainter__currentPathIdn = itemstore.dbStates.latest;
  }

  pointerdown(ev) {
    this.zoomlevelcontrol = {screenX: ev.screenX, clientX: ev.clientX};
    if(ev.pointerType == "pen" || !this.onlyPen) {
      this.setOptions();
      this.c = {
        stroke: this.options.color,
        fill:   false,
        xs:     [getX(ev)],
        ys:     [getY(ev)],
        ps:     [ev.pressure||0],
        //Simplified Points
        sxs:     [getX(ev)],
        sys:     [getY(ev)],
        sps:     [ev.pressure||0],
        //Temppoints
        tempx:   [],
        tempy:   [],
        tempp:   [],
        //other stuff
        lastX:  getX(ev),
        lastY:  getY(ev),
        vx: 0,
        vy: 0,
        ax: 0,
        ay: 0,
        lpx: getX(ev),
        lpy: getY(ev),
      };
      this.p = document.createElementNS('http://www.w3.org/2000/svg', "path");
      this.prof = document.createElementNS('http://www.w3.org/2000/svg', "path");
      //this.path = "M" +getX(ev) + "," + getY(ev) + " L" + getX(ev)+ "," + getY(ev) + " ";
      //this.path = "M" + getX(ev) + "," + getY(ev) + " L";
      //this.p.setAttribute("style", "stroke:#660000; stroke-width:"+this.options.thickness+" ; fill:none;");
      this.prof.setAttribute("style", "stroke:rgba(100,0,0,0.5); stroke-width:"+(this.options.thickness*0.2)+" ; fill:none;");
      this.p.setAttribute("style", `stroke:none; stroke-width: 0; fill: ${this.options.color};`);
      this.p.setAttribute("d", this.path);
      this.svgcontainer.appendChild(this.p);
      //this.svgcontainer.appendChild(this.prof);
      this.capturing = true;
      this.drawn = 0;
      this.thickenStart = false;
      this.lastPointWasCorner = false;
      this.n = 0;
    }
  }

  pointerup(ev) {
    if((ev.pointerType == "pen" || !this.onlyPen) && this.capturing) {
      
      let c = this.c;
      c.xs.push(getX(ev));
      c.ys.push(getY(ev));
      c.ps.push(c.ps.last() * 0.95);
      //this.path += c.xs.last() + "," + c.ys.last() + " ";
      
      if(c.tempx.length && !(c.tempx.last() == c.sxs.last() && c.tempy.last() == c.sys.last() )) {
        c.sxs.push(c.tempx.last());
        c.sys.push(c.tempy.last());
        c.sps.push(c.tempp.last());
      }
      
      if(c.xs.last() !== c.sxs.last() || c.ys.last() !== c.sys.last()) {
        c.sxs.push(c.xs.last());
        c.sys.push(c.ys.last());
        c.sps.push(c.sps.last()*0.94); //special case because pressure should not decrease linearly over several pixels
      }
      
      this.p.setAttribute("d", this.arraysToPressureContour(this.c.sxs, this.c.sys, this.c.sps));
      //this.svgcontainer.removeChild(this.prof);
      this.capturing = false;

      if(this.itemstore) {
        setTimeout(_=>this.saveItem(), 0);
      }
    }
  }

  pointermove(ev) {
    if(this.capturing && (ev.pointerType == "pen" || !this.onlyPen)) {
      let p = ev.pressure===undefined ? 0.5 : (ev.pressure === 0 ? this.c.ps.last() * 0.9 : ev.pressure);
      this.addPoint(getX(ev), getY(ev), p);
    }
    if(Math.abs(ev.clientX - this.zoomlevelcontrol.clientX) > 10) {
      this.zoomlevel = (ev.screenX - this.zoomlevelcontrol.screenX) / (ev.clientX - this.zoomlevelcontrol.clientX);
    }
  }

  addPoint(x,y,pressure) {
    if(x==this.c.xs.last() && y==this.c.ys.last()) {
      this.c.ps[this.c.ps.length - 1] = 0.5 * (this.c.ps.last() + pressure);
      return;
    }
    this.c.xs.push(x);
    this.c.ys.push(y);
    this.c.ps.push(pressure);
    let {vx, vy, ax, ay, lpx, lpy, xs, ys, ps} = this.c;
    let L = xs.length;
    var px = (17*xs[L-1] + 12*xs[L-2] - (3*xs[L-3]||3*xs[L-2]) ) / 26;
    var py = (17*ys[L-1] + 12*ys[L-2] - (3*ys[L-3]||3*ys[L-2]) ) / 26;
	  var nvx = L==3 ? (xs[L-1] - xs[L-2]) : 0.8 * vx + 0.2 * (px - lpx);
	  var nvy = L==3 ? (ys[L-1] - ys[L-2]) : 0.8 * vy + 0.2 * (py - lpy);
	  ax = 0.8 * ax + 0.2 * ( (xs[L-1] - xs[L-2]) - vx);
	  ay = 0.8 * ay + 0.2 * ( (ys[L-1] - ys[L-2]) - vy);
    Object.assign(this.c, {vx: nvx, vy: nvy, ax, ay, lpx: px, lpy: py})
    //this.path += px + "," + py + " ";
    
    this.c.tempx.push(this.aggregate(this.c.xs, L));
    this.c.tempy.push(this.aggregate(this.c.ys, L));
    this.c.tempp.push(this.aggregate(this.c.ps, L));
    
    //corner
    if(L>=8) {
      let x1stBefore = this.get1stDerivative(this.c.xs, L-6);
      let y1stBefore = this.get1stDerivative(this.c.ys, L-6);
      let x1stAfter = this.get1stDerivative(this.c.xs, L-3);
      let y1stAfter = this.get1stDerivative(this.c.ys, L-3);
      let anglebefore = (Math.atan2(-y1stBefore, x1stBefore)*180/Math.PI + 360)%360;
      let angleafter = (Math.atan2(-y1stAfter, x1stAfter)*180/Math.PI + 360)%360;
      let da = ((anglebefore - angleafter) + 720 + 180)%360 - 180;
      if(Math.abs(da)>45 && !this.lastPointWasCorner) {
        if(this.c.tempx.length>=2) {
          this.c.sxs = this.c.sxs.concat(this.c.tempx.slice(-2));
          this.c.sys = this.c.sys.concat(this.c.tempy.slice(-2));
          this.c.sps = this.c.sps.concat(this.c.tempp.slice(-2));
        }
        else {
          this.c.sxs.push(this.c.tempx[0]);
          this.c.sys.push(this.c.tempy[0]);
          this.c.sps.push(this.c.tempp[0]);
        }
        this.lastPointWasCorner = true;
        this.c.tempx = [];
        this.c.tempy = [];
        this.c.tempp = [];
      }
    }
    
    //Test
    // var point = document.createElementNS('http://www.w3.org/2000/svg',"circle");
        // point.setAttribute("style", "stroke:none; fill:"+this.options.color+";");
        // point.setAttribute("cx", 5*x);
        // point.setAttribute("cy", 5*y);
        // point.setAttribute("r", 2);
        // this.svgcontainer.appendChild(point);
  }
  
  aggregate(a, L) {
    if(L>9)  return (-11*a[L-10] + 0*a[L-9] + 9*a[L-8] + 16*a[L-7] + 21*a[L-6] + 24*a[L-5] + 12.5*a[L-4])/143 + (3.5*a[L-4] + 6*a[L-3] + 3*a[L-2] - 2*a[L-1])/21; //assymmetrische Savitzky Golay
    if(L==1) return a[0];
    if(L==2) return 0.25*a[0] + 0.75*a[1];
    if(L==3) return 0.1*a[0] + 0.4*a[1] + 0.5*a[2];
    if(L==4) return 0.08*a[0] + 0.22*a[1] + 0.40*a[2] + 0.30*a[3];
    if(L==5) return 0.05*a[0] + 0.15*a[1] + 0.30*a[2] + 0.30*a[3] + 0.20*a[4];
    if(L==6) return (-2*a[0] + 3*a[1] + 6*a[2] + 3.5*a[3])/21 + (8.5*a[3] + 12*a[4] - 3*a[5])/35;
    if(L==7) return (-21*a[0] + 14*a[1] + 39*a[2] + 54*a[3] + 29.5*a[4])/231 + (8.5*a[4] + 12*a[5] - 3*a[6])/35;
    if(L==8) return (-36*a[0] + 9*a[1] + 44*a[2] + 69*a[3] + 84*a[4] + 44.5*a[5])/429 + (8.5*a[5] + 12*a[6] - 3*a[7])/35;
    if(L==9) return (-36*a[0] + 9*a[1] + 44*a[2] + 69*a[3] + 84*a[4] + 44.5*a[5])/429 + (3.5*a[5] + 6*a[6] + 3*a[7] - 2*a[8])/21;
  }
  
  get1stDerivative(arr, center) {
    return -0.2*arr[center-2] - 0.1*arr[center-1] + 0.1*arr[center+1] + 0.2*arr[center+2];
  }

  arraysToPressureContour(xs, ys, ps, thickness) {
    const PRESSUREBASE = 0.18;
    const PRESSUREINCREASE = 0.82; //should add up to one
    var coeff = thickness === undefined ? this.options.thickness : thickness;
    var L = xs.length;
    var points = xs.map(function(e, i) {return {x: xs[i], y: ys[i], p: ps[i]};});
    
    //cap inverted edges
    if(L>=3) {
      if((xs[L-2]-xs[L-1])*(xs[L-3]-xs[L-2]) + (ys[L-2]-ys[L-1])*(ys[L-3]-ys[L-2]) < 0) {
        points = points.slice(0, -1);
      }
      if(L >= 4 && (xs[1]-xs[0])*(xs[2]-xs[1]) + (ys[1]-ys[0])*(ys[2]-ys[1]) < 0) {
        points = points.slice(1);
      }
    }
    let getTurningEdge = function(role = "start||turn", edge, mid, inner) {
      let a = getControlPoints(edge.x, edge.y, mid.x, mid.y, inner.x, inner.y, 0.4);
      let dx = edge.x - mid.x;
      let dy = edge.y - mid.y;
      let dsq = dx*dx + dy*dy;
      let tm = {x: a[0]-mid.x, y: a[1]-mid.y}; //Tangente an Mid
      let mt = {x: tm.x - 2*(tm.x*dx+tm.y*dy)/dsq * dx, y: tm.y - 2*(tm.x*dx+tm.y*dy)/dsq * dy}; //gespiegelte Tangente an Kurvensegment
      let mtl = Math.hypot(mt.x, mt.y);
      let pressthick = coeff * (PRESSUREBASE + PRESSUREINCREASE * edge.p);
      let [corrx, corry] = [-pressthick * mt.x/mtl, -pressthick * mt.y/mtl];
      if(dsq==0) {warn(dsq, " GLEICH NULL!?? (Schaue in alter Version)");}
      let p = [
        {x: edge.x + mt.x + corry , y: edge.y + mt.y - corrx}, //t1' from prev point
        {x: edge.x + corry, y: edge.y - corrx},
        {x: edge.x + 0.4*corrx + corry , y: edge.y + 0.4*corry - corrx}, //t1
        {x: edge.x + corrx + 0.4*corry , y: edge.y + corry - 0.4*corrx}, //t2
        {x: edge.x + corrx, y: edge.y + corry}, //Verlängerung von edge
        {x: edge.x + corrx - 0.4*corry , y: edge.y + corry + 0.4*corrx}, //t2'
        {x: edge.x + 0.4*corrx - corry , y: edge.y + 0.4*corry + corrx}, //t3
        {x: edge.x - corry, y: edge.y + corrx},
        {x: edge.x + mt.x - corry , y: edge.y + mt.y + corrx}, //t3' to next point
      ];
      let init = role == "start" ? `M${p[1].x},${p[1].y} C` : "";
      return {
        outgoing: init + `${p[2].x},${p[2].y} ${p[3].x},${p[3].y} ${p[4].x},${p[4].y} ${p[5].x},${p[5].y} ${p[6].x},${p[6].y} ${p[7].x},${p[7].y} ${p[8].x},${p[8].y} `,
        incoming: `${p[0].x},${p[0].y} ${p[1].x},${p[1].y} `,
        results: [p[7], p[4], p[1]]
      }
    };
    let getStartOfPath = function(arrOf3) {
      let [next, start, last] = arrOf3;
      let a = getControlPoints(last.x, last.y, start.x, start.y, next.x, next.y, 0.4);
      return "M" + start.x + "," + start.y + " C" + a[2] + "," + a[3] + " ";
    };
    let continuePath = function(oldpath, arrOf3, prepend=false) {
      let [next, self, prev] = arrOf3;
      let a = getControlPoints(prev.x, prev.y, self.x, self.y, next.x, next.y, 0.4);
      let p = a[0] + "," + a[1] + " " + self.x + "," + self.y + " " + a[2] + "," + a[3] + " ";
      p = prepend ? p + oldpath : oldpath + p;
      return p;
    };
    let getEndOfPath = function(oldpath, arrOf3) {
      let [next, start, last] = arrOf3;
      let a = getControlPoints(last.x, last.y, start.x, start.y, next.x, next.y, 0.4);
      return oldpath + a[0] + "," + a[1] + " " + start.x + "," + start.y;
    };
    var edgePoints = function(p, q, end = false) { //p is edge
      let dx = q.x - p.x;
      let dy = q.y - p.y;
      let d = hypot(dx, dy);
      let pressthick = coeff * (PRESSUREBASE + PRESSUREINCREASE * p.p);
      let [corrx, corry] = [pressthick * dx/d, pressthick * dy/d];
      if(d==0) {warn(d, " GLEICH NULL!?? (Schaue in alter Version)", xs, ys);}
      return [
        {x: p.x + 0.98*corry, y: p.y - 0.98*corrx},
        {x: p.x - 0.1*corrx + 0.8*corry, y: p.y - 0.1*corry - 0.8*corrx}, //um ca. 45° verschoben
        {x: p.x - 0.5*corrx, y: p.y - 0.5*corry},
        {x: p.x - 0.1*corrx - 0.8*corry, y: p.y - 0.1*corry + 0.8*corrx}, //um ca. -60° verschoben
        {x: p.x - 0.98*corry, y: p.y + 0.98*corrx}
      ];
    };
    var pointShiftToBorder = function(p, i, arr) {
      if(i==0 || i==arr.length-1) return false; //not for edgePoints
      var {prev, next} = getNeighbours(arr, i);
//       var dx = next.x - prev.x, dy = next.y - prev.y;
//       var d = hypot(dx, dy);
      
      var angle = (Math.atan2(next.y-p.y, next.x-p.x) - Math.atan2(p.y-prev.y, p.x-prev.x) + 5*Math.PI) % (2*Math.PI) - Math.PI;
      
      //wh berechnen
      var s1x = prev.x - p.x, s1y = prev.y - p.y;
      var s2x = next.x - p.x, s2y = next.y - p.y;
      var l1 = Math.hypot(s1x, s1y);
      var l2 = Math.hypot(s2x, s2y);
      if(l1==0 || l2==0) warn("null!?!");
      
      //        falls Gerade            ? Senkrechte :  Winkelhalbierende
      var whx = Math.abs(angle) < 1e-18 ?     s2y    :  s1x/l1 + s2x/l2;
      var why = Math.abs(angle) < 1e-18 ?    -s2x    :  s1y/l1 + s2y/l2;
      
      let whl = Math.hypot(whx, why);
      let whnormx = whx/whl, whnormy = why/whl;
      var pressthick = coeff * (PRESSUREBASE + PRESSUREINCREASE * p.p);
      
      var p1 = {x: p.x + whnormx * pressthick, y: p.y + whnormy * pressthick};
      var p2 = {x: p.x - whnormx * pressthick, y: p.y - whnormy * pressthick};
      return {left: (angle > 1e-18) ? p2 : p1, right: (angle > 1e-18 && angle) ? p1 : p2};
    };
    let results = [];
    let pathstart = "", pathend = "", path = "";
    if(L == 1 || L==2 || points.length < 3) { //TODO L==2
      results = [
        {x: xs[0] + 0.3*coeff, y: ys[0]},
        {x: xs[0], y: ys[0] + 0.3*coeff},
        {x: xs[0] - 0.3*coeff, y: ys[0]},
        {x: xs[0], y: ys[0] - 0.3*coeff},
        {x: xs[0] + 0.3*coeff, y: ys[0]},
      ];
      path = getStartOfPath([results.last(), results[0], results[1]]);
      path = continuePath(path, results.slice(0,3));
      path = continuePath(path, results.slice(1,4));
      path = continuePath(path, results.slice(2,5));
      path = getEndOfPath(path, [results.last(), results[0], results[1]]);
    }
    else {
      //Path is drawn starting from back!!;
      //results = results.concat(edgePoints(points.last(), points[points.length-2]));
      let s = getTurningEdge("start", ...points.slice(-3).reverse());
      pathstart = s.outgoing; //(1,4) if 5 edgepoints
      pathend = s.incoming; //(1,4) if 5 edgepoints
      results = s.results;
      for(let i = points.length-2; i>=1; i--) {
        let shifted = pointShiftToBorder(points[i], i, points);
        results = [shifted.right, ...results, shifted.left];
        if(i !== points.length - 2) { //wird schon in Edge abgehandelt
          pathstart = continuePath(pathstart, results.slice(0,3));
          pathend = continuePath(pathend, results.slice(-3), "prepend");
        }
      }
      let t = getTurningEdge("turn", ...points.slice(0, 3));
      results = [t.results[2], ...results, t.results[0]];
      pathstart = continuePath(pathstart, results.slice(0,3));
      pathend = continuePath(pathend, results.slice(-3), "prepend");
      path = pathstart + t.incoming + t.outgoing + pathend;
//       t.continuePath(pathstart, results.slice(0,3));
//       pathend = continuePath(pathend, results.slice(-7, -4), "prepend"); //only if 5 edgepoints
//       pathend = continuePath(pathend, results.slice(-6, -3), "prepend"); //only if 5 edgepoints
//       pathend = continuePath(pathend, results.slice(-5, -2), "prepend");
//       pathend = continuePath(pathend, results.slice(-4, -1), "prepend");
//       pathend = continuePath(pathend, results.slice(-3), "prepend");
//       pathstart = continuePath(pathstart, results.slice(-1).concat(results.slice(0,2)));
//       pathstart = continuePath(pathstart, results.slice(-2).concat([results[0]]));
//       path = pathstart + pathend;


      
//       points.forEach((p, i, arr) => {
//         var shifted = pointShiftToBorder(p, i, arr);
//         if(shifted) {
//           results = [shifted.left, ...results, shifted.right];
//         }
//       });
//       results = results.concat(edgePoints(points.last(), points[points.length-2], "end"));
    }

//     let t = document.createElementNS('http://www.w3.org/2000/svg', "text");
//     t.setAttribute("text-anchor", "middle");
//     t.setAttribute("font-size", "6px");
//     t.textContent= "S";
//     t.setAttribute("x", points[0].x);
//     t.setAttribute("y", points[0].y);
//     this.svgcontainer.appendChild(t);
//     t = document.createElementNS('http://www.w3.org/2000/svg', "text");
//     t.setAttribute("text-anchor", "middle");
//     t.setAttribute("font-size", "6px");
//     t.textContent= "E";
//     t.setAttribute("x", points.last().x);
//     t.setAttribute("y", points.last().y);
//     this.svgcontainer.appendChild(t);
    //log(path, this.arraysToPressureContour1(xs, ys, ps, thickness));
    if(!this.done)
    //log(results.map(r=>r.x+"\t"+r.y).join("\n"));
    this.done = 1;
    return path;
    //return results.length ? arraysToBezier(results.map(p=>p.x).concat([results[0].x]), results.map(p=>p.y).concat([results[0].y])) : "";
  }
  
  checkForCorner() {
    let L = this.c.xs.length;
    if(L<8) return false;
    let x1stBefore = this.get1stDerivative(this.c.xs, L-6);
    let y1stBefore = this.get1stDerivative(this.c.ys, L-6);
    let x1stAfter = this.get1stDerivative(this.c.xs, L-3);
    let y1stAfter = this.get1stDerivative(this.c.ys, L-3);
    let anglebefore = (Math.atan2(-y1stBefore, x1stBefore)*180/Math.PI + 360)%360;
    let angleafter = (Math.atan2(-y1stAfter, x1stAfter)*180/Math.PI + 360)%360;
    let da = ((anglebefore - angleafter) + 720 + 180)%360 - 180;
    return Math.abs(da)>45;
  }

  draw() {
    requestAnimationFrame(_=>this.draw());
    if(!this.capturing) return;
    let {vx, vy, ax, ay, lpx, lpy, xs, ys} = this.c;
    let L = xs.length;
    if(L == this.drawn) return;
    this.drawn = L;
    let lag = 0.4;
    let factora = 0.2;
    var predictorsX = [1,2,3].map(t=>lpx + lag*vx*t + factora*t*t*ax);
    var predictorsY = [1,2,3].map(t=>lpy + lag*vy*t + factora*t*t*ay);
    //var qp = "M"+lpx+","+lpy+" L" +  // + " " + c.xs[c.xs.length-1]+","+ c.ys[c.ys.length-1];
    var dprof = ""; //this.c.xs[this.c.xs.length-1]+","+ this.c.ys[this.c.ys.length-1] + " " + this.c.xs[this.c.xs.length-1]+","+ this.c.ys[this.c.ys.length-1];
    //this.p.setAttribute("d", this.path);
    //this.prof.setAttribute("d", qp);
    if(this.c.tempx.length) {
      var d = this.distanceFromLastPoint();
      
      if(d>2*Math.sqrt(this.options.thickness)/this.zoomlevel) {
        if(d>4*this.c.tempx.length) {
          this.c.sxs = this.c.sxs.concat(this.c.tempx);
          this.c.sys = this.c.sys.concat(this.c.tempy);
          this.c.sps = this.c.sps.concat(this.c.tempp);
        }
        else {
          this.c.sxs.push(this.c.tempx.last());
          this.c.sys.push(this.c.tempy.last());
          this.c.sps.push(this.c.tempp.last());
        }
        this.c.tempx = [];
        this.c.tempy = [];
        this.c.tempp = [];
        this.lastPointWasCorner = false;
      }
    }
    if(!this.thickenStart && this.c.sps.length>=2) {
      this.thickenStart = "done";
      this.c.sps[0] = 0.9 * this.c.sps[1];
    }
    let path;
    if(!vx && !vy && !ax && !ay) {
      path = this.arraysToPressureContour(this.c.sxs, this.c.sys, this.c.sps);
    }
    else {
      path = this.arraysToPressureContour(this.c.sxs.concat([lpx, ...predictorsX]), this.c.sys.concat([lpy, ...predictorsY]), this.c.sps.concat([0,1,2,3].map(_=>this.c.ps.last())))
    }
    this.p.setAttribute("d", path);
    //localStorage.simplepainter__currentPath = path;
  }
  
  distanceFromLastPoint() {
    return Math.hypot(this.c.tempx.last()-this.c.sxs.last(), this.c.tempy.last()-this.c.sys.last())
  }

  saveItem() {
    if(Object.keys(this.options).filter(k=>this.lastOptions[k] !== this.options[k]).length) {
      let leaves = this.itemstore.getLeavesAsArray();
      var tree_parent = this.lastOptions.painter === null ? "" : (leaves.filter(i=>i.painter === this.options.painter)[0] || {}).tree_parent;
      var tree_pos = leaves.filter(i=>i.painter === this.options.painter && i.tree_parent === tree_parent).map(i=>+i.tree_pos).sort()[0] + 8 || 8;
      var obj = Object.assign({}, this.options, {group: true, tree_parent, tree_pos});
      this.itemstore.dDb.addItem("sketch", obj).then(idn=>this.lastGroup = idn).then(_=>this.saveStroke());
      this.lastOptions = this.options.clone();
    }
    else {this.saveStroke();}
  }

  saveStroke() {
    var tree_pos = this.itemstore.getLeavesAsArray().filter(i=>i.tree_parent === this.lastGroup).map(i=>+i.tree_pos).sort()[0] + 8 || 8;
    var obj = {c: this.c.extractKeys("sxs", "sys", "sps"), tree_parent: this.lastGroup, tree_pos};
    this.itemstore.dDb.addItem("sketch", obj);
  }
  
  loadStrokes() {
    var leaves = this.itemstore.getLeavesAsArray();
    const renderKids = (idn) => leaves.filter(k => k.tree_parent == idn).forEach(k=>{
      if(k.group) {
        renderKids(k.idn)
      }
      else {
        this.loadStroke(k.idn);
      }
    });
    leaves.filter(l=>l.tree_parent=="").forEach(l=>renderKids(l.idn));
  }

  loadStroke(idn) {
    var item = this.itemstore.getItem(idn);
    if(item.group) return;
    var parent = this.itemstore.getItem(item.tree_parent);
    var p = document.createElementNS('http://www.w3.org/2000/svg', "path");
    p.setAttribute("style", "stroke:none; stroke-width:0 ; fill:" + parent.color + ";");
    p.setAttribute("d", this.arraysToPressureContour(item.c.sxs, item.c.sys, item.c.sps, parent.thickness));
    p.setAttribute("data-idn", idn);
    this.svgcontainer.appendChild(p);
//     for(let i=0; i<item.c.sxs.length; i++) {
//       let c = document.createElementNS('http://www.w3.org/2000/svg', "circle");
//       c.setAttribute("style", "stroke:none; stroke-width:0 ; fill:white;");
//       c.setAttribute("cx", item.c.sxs[i]);
//       c.setAttribute("cy", item.c.sys[i]);
//       c.setAttribute("r", "1");
//       this.svgcontainer.appendChild(c);
//     }
    
  }

  clear() {
    $(this.svgcontainer).empty();
  }
}

function getX(ev) {
  return ev.offsetX !== undefined ? ev.offsetX : warn("offsetX needs polyfill");
}

function getY(ev) {
  return ev.offsetY !== undefined ? ev.offsetY : warn("offsetY needs polyfill");
}

function getNeighbours(arr, i, len) {
  var L = len || arr.length;
  return {prev: arr[(L + i - 1) % L], next: arr[(i + 1) % L]};
}

function getControlPoints(x0, y0, x1, y1, x2, y2, t) {
  var d01 = Math.hypot(x1-x0, y1-y0);
  var d12 = Math.hypot(x2-x1, y2-y1);
  if(d01 + d12 === 0) {return [x1, y1, x1, y1]}
  var fa = t * d01 / (d01 + d12);   // scaling factor for triangle Ta
  var fb = t * d12 / (d01 + d12);   // ditto for Tb, simplifies to fb=t-fa
  var p1x = x1 - fa * (x2 - x0);    // x2-x0 is the width of triangle T
  var p1y = y1 - fa * (y2 - y0);    // y2-y0 is the height of T
  var p2x = x1 + fb * (x2 - x0);
  var p2y = y1 + fb * (y2 - y0);
  return [p1x.toFixed(2), p1y.toFixed(2), p2x.toFixed(2), p2y.toFixed(2)];
}

function arraysToBezier(xs, ys) {
  var p = "M" + xs[0] + "," + ys[0] + " ";
  p += " C" + (xs[0] + (xs[1] - xs[0]) / 3) + "," + (ys[0] + (ys[1] - ys[0]) / 3) + " ";
  for(var i = 1; i < xs.length - 1; i++) {
    var a = getControlPoints(xs[i - 1], ys[i - 1], xs[i], ys[i], xs[i + 1], ys[i + 1], 0.4);
    p += a[0] + "," + a[1] + " " + xs[i] + "," + ys[i] + " " + a[2] + "," + a[3] + " ";
  }
  //log(xs.last(), xs[xs.length - 2],  xs.last() + (xs[xs.length - 2] - xs.last()) / 3);
  p += (xs.last() + (xs[xs.length - 2] - xs.last()) / 3) + "," + (ys.last() + (ys[ys.length - 2] - ys.last()) / 3) + " ";
  p += xs.last() + "," + ys.last();
  return p;
}

//TODO
// //pousi
// function render(src){
// var image = new Image();
// image.onload = function(){
// var canvas = document.getElementsByClassName("pousi")[0];
// canvas.width = window.innerWidth;
// canvas.height = window.innerHeight;
// var ctx = canvas.getContext("2d");
// ctx.clearRect(0, 0, canvas.width, canvas.height);
// var scalefactor = Math.max(image.width/canvas.width, image.height/canvas.height);
// //      canvas.width = image.width;
// //      canvas.height = image.height;
// ctx.drawImage(image, 0, 0, image.width/scalefactor, image.height/scalefactor);
// };
// image.src = src;
// }
//
// function loadImage(src){
// //	Prevent any non-image file type from being read.
// log(src);
// if(!src || !src.type.match(/image.*/)){
//  console.log("The dropped file is not an image: ", src ? src.type : "no src");
//  return;
//}
//
////	Create our FileReader and run the results through the render function.
//var reader = new FileReader();
//reader.onload = function(e){
//  render(e.target.result);
//};
//reader.readAsDataURL(src);
//}
//
//var target = document.getElementById("mysvg");
//target.addEventListener("dragover", function(e){e.preventDefault(); e.dataTransfer.dropEffect = 'copy';}, false);
//target.addEventListener("drop", function(e){
//  e.preventDefault();
//  log(e);
//  log(e.dataTransfer.files[0]);
//  loadImage(e.dataTransfer.files[0]);
//}, true);
//
//document.onpaste = function (event) {
//      // use event.originalEvent.clipboard for newer chrome versions
//      var items = (event.clipboardData  || event.originalEvent.clipboardData).items;
//      console.log((event.clipboardData  || event.originalEvent.clipboardData).getData("text/plain"), items); // will give you the mime types
//      // find pasted image among pasted items
//      var blob = null;
//      for (var i = 0; i < items.length; i++) {
//        if (items[i].type.indexOf("image") === 0) {
//          blob = items[i].getAsFile();
//        }
//      }
////    // load image if there is a pasted image
////    if (blob !== null) {
////      var reader = new FileReader();
////      reader.onload = function(event) {
////        console.log(event.target.result); // data url!
////        document.getElementById("pastedImage").src = event.target.result;
////      };
////      reader.readAsDataURL(blob);
////    }
//    }

export {Simplepainter};
