//import * as Immutable from "Immutable";

//var Immutable = require("Immutable");
//var s = new Store();
//var store = Immutable.fromJS({a:1, b: [1,2,3]});
//var List = Immutable.List.of(...[0,1,2]);
//var q = store.merge({a:0, c:5});
//log(store.get("b"), q.toJS());


import {post} from "modules/ajaxhelpers";

//var uq = newP();
//setTimeout(e=>uq.then(r=>log(1, r) || r), 1000);
//setTimeout(e=>uq.then(r=>{log(2, r); setTimeout(e=>uq.then(r=>log(31, r) || r), 1000); return r}), 2000);
//setTimeout(e=>uq.then(r=>log(32, r) || r), 3000);

//log(post("//localhost/dev/build/server/mydb.php", {}));

var defaultOptions = {
  db: {
    central: "db",
    name: "test"
  },
  settings: {
    leavesonly: false,
    longpolling: true
  },
  keysByType: {}
};

//  globalHistory: {show: false, nowline: null, undoneIdvs: [], maxItems: 10},
//shelfnames: {},
//leaves: [],
//    versiongroups: [],
//    versions: [],
//    latest: 0,
//    ready: false,
//  updatePipe: {},
// actionPipe: [],
//updateTimer: null,
//    lastTimestamp: 0,

class Store {
  constructor(name, options = {}) {
    this.name = name;
    this.options = Object.assign({}, defaultOptions, options);
    this.state = new State(this);
    this.loadItems();
    this.eventEmitter = document.createElement("div");
    document.body.appendChild(this.eventEmitter);
    this.ready = false;
  }
  loadItems() {
    return post(this.options.db.central, {dbname: this.options.db.name, task: this.options.settings.leavesonly ? "getLeaves" : "getAll"})
    .then(o=>{
      this.itemCollection = new ItemCollection(o, this.options);
      //var idn = this.itemCollection.addItem("test", {val:0, b: 0});
      //setTimeout(()=>this.itemCollection.updateItem(idn, {val:2}), 1000);
      //for(let i = 1; i<5; i++) {
      //  setTimeout(()=>this.itemCollection.debouncedUpdate(idn, {val:i}), i*100);
      //  setTimeout(()=>this.itemCollection.debouncedUpdate(idn, {b:i}), i*130);
      //}
      this.ready = true;
      this.options.keysByType = Object.assign(this.options.keysByType, this.getKeysByType(this.itemCollection.leaves.toArray()));
      this.eventEmitter.dispatchEvent(new CustomEvent("ready", {detail: "ready"}));

// Dispatch/Trigger/Fire the event
      //let versions = Object.keys(o).map(idv => (new Item(idv, JSON.parse(o[idv])) ));
      //this.dispatch(dbInitialLoad(versions));

      //this.dispatch(setValue("keysByType", this.getKeysByType(this.state.leaves)));
    })
    //.then(()=>this.state.settings.longpolling ? this.poll() : false)
    .catch(e=>warn(e));
  }
  getKeysByType(versions) {
    var keysByTypeFomProps = this.options.keysByType || {};
    var keysByType = {};
    var types = versions.map(v=>v.type).unique();
    types.forEach(type=>{
      keysByTypeFomProps[type] = keysByTypeFomProps[type] || {};
      keysByType[type] = {};
      let itemsOfType = versions.filter(v=>v.type == type);
      let keys = Object.keys(keysByTypeFomProps[type]).concat(itemsOfType.map(v=>Object.keys(v.doc).join(",")).join(",").split(",")).unique(); //muss so kompliziert sein wegen Reihenfolge
      keys.forEach(key => {
        keysByTypeFomProps[type][key] = keysByTypeFomProps[type][key] || {};
        keysByType[type][key] = {
          key: key,
          isPublic: !~key.indexOf("_"),
          isNumeric: itemsOfType.map(v=>v.doc[key]).filter(x=>!!x).every(x=>parseFloat(x)==x)
        };
        keysByType[type][key] = Object.assign(keysByType[type][key], keysByTypeFomProps[type][key]);
      });
    });
    return keysByType;
  }
}

class ItemCollection {
  constructor(raw, options) {
    this.options = options;
    this.versions = Object.keys(raw).map(idv => (new Item(idv, JSON.parse(raw[idv])) ));
    this.versions.sort((a,b)=>a.idv<b.idv?1:-1);
    this.versiongroups = this.versions.reduce((o, el)=>Object.assign(o, {[el.idn]: (o[el.idn] || []).concat([el.idv])} ), {}); //Versionsgroup: {idn-XY : [idv4, idv3, idv2]}
    this.leaves = new LeafCollection(Object.keys(this.versiongroups).map(v=>this.versions.pick({idv: this.versiongroups[v][0]})));
    this.dirtyIdns = [];
    this.pendingIdvs = [];
    this.debouncedIdns = {};
    this.updateQueue = newP();
    this.latest = this.versions.reduce((latestMod, nextItem) => nextItem.modified > latestMod ? nextItem.modified : latestMod, "0");
  }
  logAction(type, a) {
    console.groupCollapsed(type);
    [].slice.call(a).forEach(arg=>console.log(arg));
    setTimeout(()=>console.groupEnd(), 0);
  }
  addItem(itemtype, newDoc) {
    this.logAction("addItem", arguments);
    var idn = itemtype + "-" + this.getTimestamp() + "-" + this.getUsr();
    var idv = idn + "-" + this.lastTimestamp + "-" + this.getUsr() + ".json";
    this.addVersions([idv], [newDoc], [""]);
    return idn;
  }
  debouncedUpdate(idn, obj) {
    this.logAction("debouncedUpdate", arguments);
    if(Object.keys(obj).length !== 1) throw new Error("Debounced update must only change one key!");
    var key = Object.keys(obj)[0];
    var val = obj[key];
    var item = this.leaves[idn];
    var getTimeout = () => setTimeout(()=>{this.dispatchDebouncedUpdate(idn)}, 2000);
    if(this.debouncedIdns[idn]) {
      if(this.debouncedIdns[idn].key === key) {
        clearTimeout(this.debouncedIdns[idn].timeout);
        this.debouncedIdns[idn] = Object.assign(this.debouncedIdns[idn], {val, timeout: getTimeout()})
      }
      else {
        this.dispatchDebouncedUpdate(idn);
        this.debouncedUpdate(idn, {[key]: val});
      }
    }
    else {
      this.debouncedIdns[idn] = {key, val, originalval: item[key], timeout: getTimeout()};
    }
  }
  dispatchDebouncedUpdate(idn) {
    clearTimeout(this.debouncedIdns[idn].timeout);
    var {key, val, originalval} = this.debouncedIdns[idn];
    delete this.debouncedIdns[idn];
    if(JSON.stringify(originalval) == JSON.stringify(val)) {
      return log("item was changed back to original value, no update");
    }
    return this.updateItem(idn, {[key]: val});
  }
  //Single item
  deleteItem(idn)   { return this.deleteItems([idn]); }
  undeleteItem(idn) { return this.undeleteItems([idn]); }
  updateItem(idn, obj) { return this.updateItems([idn], [obj]); }
  replaceItem(idn, newDoc) { return this.replaceItems([idn], [newDoc]);}
  //Array of items
  deleteItems(idns)   { return this.replaceItems(idns, false, "delete");  }
  undeleteItems(idns) { return this.replaceItems(idns, false, "undelete"); }
  updateItems(idns, objs) {return this.replaceItems(idns, objs, "updateKeys"); }
  replaceItems(idns, newDocsOrObjs, task = "update") {
    this.logAction("replaceItems", arguments);
    idns.filter(idn=>this.debouncedIdns[idn]).forEach(idn=>this.dispatchDebouncedUpdate(idn));
    let timestamp = this.getTimestamp();
    let prevItems = idns.map(idn => this.leaves[idn]);
    let prevIdvs = prevItems.map(item => item.idv);  // ?? var versionsToChange = action.previousIdvs.map(idv=>n.versions.pick({idv}));
    let prevDocs = prevItems.map(item => item.doc);  // ?? var prevDocs = versionsToChange.map(v=>v.doc);
    let newDocs = newDocsOrObjs;
    if(!newDocs) newDocs = prevDocs;
    if(task == "updateKeys") newDocs = newDocsOrObjs.map((obj, index) => Object.assign({}, this.leaves[idns[index]].doc, obj));
    let idvs = idns.map( (idn, index) => idn+ "-" + timestamp + "-" + this.getUsr() + this.getNewFlags(prevItems[index], task) + ".json");
    return this.addVersions(idvs, newDocs, prevIdvs);
  }
  getNewFlags(olditem, task) {
    var newFlagsArray = olditem.flags.slice(0);
    if(task=="delete") newFlagsArray = newFlagsArray.map(f=> ~f.indexOf("x") ? f : "x"+f);
    if(task=="undelete") newFlagsArray = newFlagsArray.map(f=> f.replace(/x/ig, ""));
    return newFlagsArray.length ? "-"+newFlagsArray.join("") : "";
  }
  addVersions(idvs, newDocs, prevIdvs, fromLocal=true) {
    if(!fromLocal) {alert("Noch nicht implementiert: MultiUser")} //@TODO
    idvs.forEach((idv, index) => {
      var item = new Item(idv, newDocs[index]);
      var idn = item.idn;
      this.versions.unshift(item);
      this.leaves[idn] = item;
      this.dirtyIdns.push(idn);
      this.pendingIdvs.push(idv);
      this.versiongroups[idn] = [idv].concat(this.versiongroups[idn]);
    });
    return this.sendVersionsToDb(idvs, newDocs, prevIdvs);
  }
  sendVersionsToDb(newIdvs, newDocs, previousIdvs) {
    return this.updateQueue.then(r=>post(this.options.db.central,
        {dbname: this.options.db.name, task: "addUpdatedVersions", idvs: newIdvs, previousIdvs: previousIdvs, data: newDocs.map(newDoc=>JSON.stringify(newDoc))}
    ).then(r=>this.dbResponse(r, newIdvs, newDocs /*, prevDocs*/))).then(r=>log(r)); //{type: "dbResponse", r, idvs: newIdvs, docs: newIdvs.map(idv=>n.versions.pick({idv}).doc), prevDocs})});
  }
  dbResponse(r, newIdvs, newDocs, prevIdvs, prevDocs) {
    this.logAction("dbResponse", arguments);
    //log(r, newIdvs, newDocs, prevIdvs, prevDocs);
    if(r === "updates ok!") {
      newIdvs.forEach(idv => {this.pendingIdvs = this.pendingIdvs.filter(i=>i!==idv);} )
    }
    if(r.indexOf("Konflikte mit Eintrag von anderswo:") === 0) {
      var conflictingWrap = JSON.parse(action.r.slice("Konflikte mit Eintrag von anderswo:".length));
      conflictingWrap.forEach( (c, index) => {
        if(!c) {return}
        var conflicting = Object.keys(c)[0];
        var confl = JSON.parse(c[conflicting]);
        var conflictingItem = new Item(conflicting, confl);
        n.versions = n.versions.replaceAt(n.versions.indexOf(n.versions.pick({idv: action.idvs[index]})), conflictingItem);
        n = Object.assign(n, arrangeItems(n.versions));
        var prev = action.prevDocs[index].clone();
        var self = action.newDocs[index].clone();
        var merged = prev.clone();
        for(let key in confl) {
          if(self[key]===undefined && prev[key]===undefined)      {merged[key] = confl[key]; log(`Conflicting added new key ${key}`);}
          if(self[key] == prev[key] && prev[key] !== confl[key])  {merged[key] = confl[key]; log(`Conflicting changed key ${key}, self didn't.`);}
          if(self[key] !== prev[key] && self[key] === confl[key]) {merged[key] = confl[key]; log(`Both changed key ${key} to same value.`);}
          if(self[key] == prev[key] && prev[key] === confl[key])  {merged[key] = confl[key]; log(`Both did not change key ${key}.`);}
        }
        for(let key in self) {
          if(confl[key]===undefined && prev[key]===undefined)     {merged[key] = self[key];  log(`Self added new key ${key}`);}
          if(confl[key] == prev[key] && prev[key] !== self[key])  {merged[key] = self[key];  log(`Self changed key ${key}, conflicting didn't.`);}
          if(confl[key] !== prev[key] && prev[key] !== self[key] && confl[key] !== self[key]) {merged = false; log(`Both changes key ${key} to different value.`); break;}
        }
        if(merged!==false) {
          log("item was merged with conflicting item:", merged);
          return merged;
        }
        else {
          warn("item could not be merged with conflicing item!");
          return "error";
        }
      });
    }

// if(action.r.indexOf("Konflikte mit Eintrag von anderswo:") === 0) {
// var conflictingWrap = JSON.parse(action.r.slice("Konflikte mit Eintrag von anderswo:".length));

// if(!newDocsNextTry.some(e=>e=="error")) {
// setTimeout(()=>action.dispatch(dbUpdateItems(action.idvs.map(Item.getIdnFromIdv), newDocsNextTry)), 0);
// }
// }
// }
  }
  getTimestamp(lastTimestamp) {
    var time = new Date().getTime()+31536000000;
    var timestamp = time.toString(36);
    if(timestamp === this.lastTimestamp) {
      timestamp = (time+1).toString(36)
    }
    this.lastTimestamp = timestamp;
    return timestamp;
  }
  getUsr() {
    return "l"; //@TODO
  }
  static timestampToDate(t) {
    return new Date(parseInt(t, 36) - 31536000000);
  }
}

class State {
  constructor(store) {
    this.store = store;
    this.state = {
      shelfnames: {}
    };
  }
  mountShelf(shelfname, values) {
    this.state.shelfnames[shelfname] = values;
    this.saveStates();
  }
  changeShelfStates(shelfname, newStates) {
    this.state.shelfnames[shelfname] = Object.assign({},  this.state.shelfnames[shelfname], newStates);
    this.saveStates();
  }
  setShelfStateOfIdn(shelfname, itemstate, idn, value) {
    if(value === true) {
      this.state.shelfnames[shelfname][itemstate] = this.state.shelfnames[shelfname][itemstate].addUnique(idn);
    }
    else {
      this.state.shelfnames[shelfname][itemstate] = this.state.shelfnames[shelfname][itemstate].deleteWhere(i=>i == idn);
    }
    this.saveStates();
  }
  toggle(pathArrayOrString) {
    this.setValue(pathArrayOrString, null, true);
  }
  setValue(pathArrayOrString, val, toggle=false) {
    let pathToVariable = this.state;
    let patharray = pathArrayOrString instanceof Array ? pathArrayOrString : pathArrayOrString.split(".");
    patharray.slice(0,-1).forEach(p => pathToVariable = pathToVariable[p]);
    let nameOfVariable =  patharray.slice(-1)[0];
    pathToVariable[nameOfVariable] = toggle ? !pathToVariable[nameOfVariable] : val;
    this.saveStates();
  }
  saveStates() {
    localStorage[this.store.name+"_state_"] = JSON.stringify(this.state);
    this.store.eventEmitter.dispatchEvent(new CustomEvent("statechange"));
  }
//if(action.type=="shelfMount") {n.shelfnames = Object.assign({}, {[action.shelfname]: action.values}, n.shelfnames)}
//if(action.type=="shelfChangeState") {n.shelfnames[action.shelfname] = Object.assign({}, n.shelfnames[action.shelfname], action.newStates)}
//if(action.type=="shelfSetStateOfIdn") {
//  var {shelfname, itemstate} = action;
//  if(action.value === true) {
//    n.shelfnames[shelfname][itemstate] = n.shelfnames[shelfname][itemstate].addUnique(action.idn);
//  }
//  else {
//    n.shelfnames[shelfname][itemstate] = n.shelfnames[shelfname][itemstate].deleteWhere(i=>i == action.idn);
//  }
//}
//if(action.type.slice(0,5) == "shelf") { //saveStates, or if(JSON.stringify(n.shelfnames !== JSON.stringify(state.shelfnames)) ...;
//  log(n.shelfnames);
//  localStorage[(action.itemworld.props.name||"")+"_states_"+action.shelfname] = JSON.stringify(n.shelfnames[action.shelfname]);
//}
}

class LeafCollection {
  constructor(itemarray) {
    itemarray.forEach(i=>this[i.idn] = i);
  }
  map(fn) {
    return this.toArray().map(fn);
  }
  toArray() {
    return Object.keys(this).map(k=>this[k]);
  }
}



//ychoufe-12345678-98765432.json
//<type>-<created>-<modified>.<extension>
//<----------idv----------------------->
//<-----version------------->
//<----idn-------> (identifier)

class Item {
  constructor(idv, doc) {
    this.idv = idv;
    this.version   = idv.split(".")[0];
    this.extension = idv.split(".")[1];
    this.type     = this.version.split("-")[0];
    this.created  = this.version.split("-")[1];
    this.creator  = this.version.split("-")[2];
    this.modified = this.version.split("-")[3];
    this.modifier = this.version.split("-")[4];
    this.flags    = this.version.split("-")[5] || "";
    this.deleted  = this.flags.indexOf("x") > -1;
    this.idn      = this.type+"-"+this.created+"-"+this.creator;
    this.doc = doc;
    this.createGetSet();
  };
  createGetSet() {
    for(let prop in this.doc) {
      if(prop in Item.prototype) continue;
      Object.defineProperty(Item.prototype, prop, {
        get: function getProp() {return this.doc[prop];},
        set: function setProp(val) {this.doc[prop] = val;}
      });
    }
  };
  static getIdnFromIdv(idv) {return idv.split("-").slice(0,2).join("-");}
  get(prop) {
    let p = this[prop] !== undefined ? this[prop] : (this.doc[prop] !== undefined ? this.doc[prop] : undefined);
    return (typeof p == "function") ? p.bind(this)() : p;
  };
  getPublicKeys() {
    return Object.keys(this.doc).filter(k=>!~k.indexOf("_"));
  }
  hasType(typegroup) {
    if(typegroup === "all") return true;
    if(typegroup === "deleted") return this.deleted === true;
    return this.type === typegroup;
  };
  //Tree Methods
  isTreeItem() {return this.tree_pos !== undefined;}
  getParent() {
    return this.tree_parent;
  }
}



// if(action.type=="dbGotRemoteItems"){
// n.versions = n.versions.concat(action.newVersions);
// n = Object.assign(n, arrangeItems(n.versions));
// }


// if(action.fromUndo && !n.globalHistory.nowline) {
// n.globalHistory.nowline = n.lastTimestamp;
// //setTimeout(()=>action.dispatch(setValue("globalHistory.nowline", n.lastTimestamp)), 0);
// }
// if(action.type.slice(0,10) == "dbResponse" && action.originalaction.fromUndo) {
// n.globalHistory.undoneIdvs = n.globalHistory.undoneIdvs.concat(action.originalaction.previousIdvs);
// //setTimeout(()=>action.dispatch(setValue("globalHistory.undoneIdvs", action.itemworld.state.globalHistory.undoneIdvs.concat(action.originalaction.previousIdvs))), 0);
// }
// if(action.type.slice(0,2)=="db" && /*action.type!=="dbDebouncedUpdate" &&*/ !action.fromUndo && !(action.originalaction && action.originalaction.fromUndo)) {
//  n.globalHistory.nowline = "";
//  n.globalHistory.undoneIdvs = [];
//  //setTimeout(()=>action.dispatch(setValue("globalHistory.nowline", "")), 0);
//  //setTimeout(()=>action.dispatch(setValue("globalHistory.undoneIdvs", [])), 0);
//}
//
//

//console.groupEnd();
//return n;
//};

//class Itemworld extends React.Component {
//  constructor(props) {
//    super(props);
//    this.store = Object.assign({}, defaultStore, this.props.store);
//    this.state = this.store;
//    this.dispatch = this.dispatch.bind(this);
//    this.keydown = this.keyDown.bind(this)
//  }
//  dispatch(action) {
//    action = Object.assign(action, {dispatch: this.dispatch, itemworld: this});
//    this.store = reducer(this.store, action);
//    this.setState(this.store);
//  }
//  componentDidMount() {
//    this.loadItems();
//    document.addEventListener("keydown", this.keydown);
//  }
//  componentWillUnmount() {
//    this.willunmount = true;
//    document.removeEventListener("keydown", this.keydown);
//  }
//  loadItems() {
//    return $.post(this.state.db.central, {dbname: this.state.db.name, task: this.state.settings.leavesonly ? "getLeaves" : "getAll"})
//    .then(r=>JSON.parse(r))
//    .then(o=>{
//      let versions = Object.keys(o).map(idv => (new Item(idv, JSON.parse(o[idv])) ));
//      this.dispatch(dbInitialLoad(versions));
//      this.dispatch(setValue("keysByType", this.getKeysByType(this.state.leaves)));
//    })
//    .then(()=>this.state.settings.longpolling ? this.poll() : false)
//    .catch(e=>log(e));
//  }

//
//
//  getItem(idn) {
//    return this.state.leaves.filter(i=>i.idn==idn)[0];
//  }
//  poll() {
//    if(document.hidden) {
//      setTimeout(()=>this.poll(), 1000);
//    }
//    else {
//      log("polling...");
//      newP().then(()=>$.ajax(this.state.db.central, {type: "POST", data: {dbname: this.state.db.name, task: "pollForChanges", since: this.state.latest}, timeout: 1e6})).then((r)=>{
//        if(!this.willunmount) {
//          if(String(r.slice(0)) === String("nochanges")) {
//            log("nochanges");
//          }
//          else {
//            var remoteVersions = JSON.parse(r);
//            var external = Object.keys(remoteVersions).filter(newv => this.state.versions.filter(v=>v.idv == newv).length==0);
//            let newVersions = external.map(idv => (new Item(idv, JSON.parse(remoteVersions[idv])) ));
//            if(newVersions.length) {
//              this.dispatch(dbGotRemoteItems(newVersions));
//            }
//          }
//          setTimeout(()=>this.poll(), 1000);
//        }
//      }).catch(e=>{log(e); setTimeout(()=>this.poll(), 1000);});
//    }
//  }
//  moveOldToArchive() {
//    $.post(this.state.db.central, {dbname: this.state.db.name, task: "moveOldToArchive"}).then(r=>log(r));
//  }
//  keyDown(e) {
//    if(e.which == 90 && e.shiftKey && e.ctrlKey) {
//      this.dispatch(toggle("globalHistory.show"));
//    }
//  }
//  createShelf(type, {shelfcomponent = Shelf, name = type, filterfunction = ()=>true, noEdit=false, purpose="", moreprops={}} = {}) {
//    return React.createElement(shelfcomponent,  Object.assign({
//      itemworld: this,
//      dispatch: this.dispatch,
//      type,
//      name,
//      leaves: this.state.leaves,
//      states: this.state.shelfnames[name]||[],
//      keysByType: this.state.keysByType,
//      filterfunction,
//      noEdit,
//      purpose
//    }, moreprops), null);
//  }
//  createTree(type, options = {}) {
//    return this.createShelf(type, Object.assign(options, {shelfcomponent: Tree, purpose: options.purpose || "manageTree"}));
//  }
//  createGlobalhistory() {
//    return <Globalhistory itemworld={this} {...this.state.globalHistory} versions={this.state.versions} versiongroups={this.state.versiongroups}/>;
//  }
//  render() {
//    return <div>{this.createGlobalhistory()} {this.createShelf("all")} </div>;
//  }
//}

module.exports = {Store};