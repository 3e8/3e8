const http = require('http'),
      url = require('url'),
      path = require('path'),
      fs = require('fs');

const express = require('express'),
      bodyParser = require('body-parser');

const PORT = 1337;

const tasks = {
  inventory: require('../metamodules/inventory').inventory
};

const mimeTypes = {
  "html": "text/html",
  "jpeg": "image/jpeg",
  "jpg": "image/jpeg",
  "png": "image/png",
  "js": "text/javascript",
  "css": "text/css"
};

app.use(bodyParser.urlencoded({ extended: true, limit:1024*1024*5, parameterLimit: 1000000  })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json({limit:1024*1024*5, type:'application/json'})); // to support JSON-encoded bodies
    
http.createServer(function(req, res) {
  const uri = url.parse(req.url).pathname;
  const filename = path.join(process.cwd(), uri);
  for(let t in tasks) {
    if(uri.split("/")[1] == t) {
      return tasks[t](req, res);
    }
  }
  fs.exists(filename, function(exists) {
    if(!exists) {
      console.log("not exists: " + filename);
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.write('404 Not Found\n');
      res.end();
      return;
    }
    const mimeType = mimeTypes[path.extname(filename).split(".")[1]];
    res.writeHead(200, {'Content-Type':mimeType});

    const fileStream = fs.createReadStream(filename);
    fileStream.pipe(res);
  });
}).listen(PORT);

console.log("Server running on localhost:" + PORT);