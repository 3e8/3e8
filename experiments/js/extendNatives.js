//ARRAY
Object.defineProperty(Array.prototype, "searchAll", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    var filtered = this.slice(0); //faster than (deep) clone
    for(var prop in compareObj) {
      filtered = filtered.filter(obj => obj[prop] === compareObj[prop]);
    }
    return filtered;
  }
});

Object.defineProperty(Array.prototype, "has", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    return this.searchAll(compareObj).length>0;
  }
});

Object.defineProperty(Array.prototype, "pick", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    var m = this.searchAll(compareObj);
    if(m.length > 1) {warn("Pick Element: More than 1 match, first one was picked", compareObj, m); return m[0];}
    if(m.length == 0) {warn("Pick Element: not found", compareObj); return false;}
    return m[0];
  }
});

//Object.defineProperty(Array.prototype, "searchItems", {
//  enumerable: false,
//  writable: true,
//  value: function(compareObj) {
//    var filtered = this.slice(0); //faster than (deep) clone
//    for(var prop in compareObj) {
//      filtered = filtered.filter(obj => obj.get(prop) === compareObj[prop]);
//    }
//    return filtered;
//  }
//});

Object.defineProperty(Array.prototype, "unique", {
  enumerable: false,
  writable: true,
  value: function() {
    let unicates = this.filter(function(el, i, ar) {
      return typeof el === "object" ? !ar.slice(0,i).has(el) : ar.indexOf(el) === i;
    });
    return unicates;
  }
});

//Object.defineProperty(Array.prototype, "pushUnique", {
//  enumerable: false,
//  writable: true,
//  value: function(el, byValue = true) {
//    let exists = el === "object" && byValue ? this.has(el) : this.indexOf(el) > -1;
//    if(!exists) this.push(el);
//    return this;
//  }
//});

Object.defineProperty(Array.prototype, "addUnique", {
  enumerable: false,
  writable: true,
  value: function(el, byValue = true) {
    var arr = [...this];
    let exists = (el === "object" && byValue) ? this.has(el) : this.indexOf(el) > -1;
    if(!exists) arr = [...arr, el];
    return arr;
  }
});

Object.defineProperty(Array.prototype, "deleteWhere", {
  enumerable: false,
  writable: true,
  value: function(compareObj) {
    var arr = [...this];
    let matches = typeof compareObj == "function" ? this.filter(compareObj) : this.searchAll(compareObj);
    matches.forEach(m => arr = [...arr.slice(0,this.indexOf(m)), ...arr.slice(this.indexOf(m) + 1)]);
    return arr;
  }
});

Object.defineProperty(Array.prototype, "replaceAt", {
  enumerable: false,
  writable: true,
  value: function(index, newItem) {
    return [...this.slice(0, index), newItem, ...this.slice(index+1)];
  }
});

Object.defineProperty(Array.prototype, "last", {
  enumerable: false,
  writable: true,
  value: function() {return this[this.length-1];}
});

Object.defineProperty(Array.prototype, "sum", {
  enumerable: false,
  writable: true,
  value: function() {return this.reduce((o,n)=>o+n, 0);}
});

Object.defineProperty(Array.prototype, "avg", {
  enumerable: false,
  writable: true,
  value: function() {if(this.length) return this.sum()/this.length;}
});

/**
 * Works for numbers and strings!
 * Pass in Array ["key", "desc"] for descending sort
 */
Object.defineProperty(Array.prototype, "sortByMultipleKeys", {
  enumerable: false,
  writable: true,
  value: function(...keys) {
    this.sort((a,b) => {
      for (let prop of keys) {
        let [p, ...options] = prop instanceof Array ? prop : [prop];
        let desc = options.indexOf("desc") > -1;
        let propsequence = p.split(".");
        var o1 = a, o2 = b;
        for(var j in propsequence) {o1 = o1[propsequence[j]]; o2 = o2[propsequence[j]];}
        if(replaceUml(o1) > replaceUml(o2)) return desc?-1:1;
        if(replaceUml(o1) < replaceUml(o2)) return desc?1:-1;
      }
    });
    return this;
  }
});

//Object

Object.defineProperty(Object.prototype, "extractKeys", {
  enumerable: false,
  writable: true,
  value: function(...keys) {
    let extracted = {};
    keys.forEach(prop => extracted[prop] = this[prop]);
    return extracted;
  }
});


Object.defineProperty(Object.prototype, "values", {
  enumerable: false,
  writable: true,
  value: function() {
    return Object.keys(this).map(key => this[key]);
  }
});

Object.defineProperty(Object.prototype, "clone", {
  enumerable: false,
  writable: true,
  value: function() {
    return JSON.parse( JSON.stringify( this ) );
  }
});

//Element.closest polyfill
if (window.Element && !Element.prototype.closest) {
  Element.prototype.closest =
    function(s) {
      var matches = (this.document || this.ownerDocument).querySelectorAll(s),
          i,
          el = this;
      do {
        i = matches.length;
        while (--i >= 0 && matches.item(i) !== el) {};
      } while ((i < 0) && (el = el.parentElement));
      return el;
    };
}

//Element.matches polyfill
if (!Element.prototype.matches) {
  Element.prototype.matches =
      Element.prototype.matchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector ||
      Element.prototype.oMatchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
            i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) {}
        return i > -1;
      };
}