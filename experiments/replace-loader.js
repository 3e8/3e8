//This Loader will replace "World" with "Planet"

module.exports = function(source, map) {
  this.cacheable();
  let result = source.replace(/<%dirname%>/g, __dirname);
  this.callback(null, result, map);
};