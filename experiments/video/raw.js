const video = document.querySelector(".player");
const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');
const strip = document.querySelector('.strip');
const snap = document.querySelector('.snap');

const hideVideo = document.querySelector('.hideVideo');
hideVideo.addEventListener("click", _ => video.style.display = "none")

function getVideo() {
  navigator.mediaDevices.getUserMedia({video: {facingMode: "user", width: 120, height: 90}, audio: false})
      .then(stream => {
        video.src = URL.createObjectURL(stream);
        video.play();
      });
}

function paintToCanvas() {
  const width = video.videoWidth;
  const height = video.videoHeight;
  canvas.width = width;
  canvas.height = height;
  
  return setInterval(() => {
    ctx.drawImage(video, 0, 0, width, height);
    // take the pixels out
    let pixels = ctx.getImageData(0, 0, width, height);
    // mess with them
    pixels = modify(pixels);

    //Ghost effect
    //ctx.globalAlpha = 0.05;

    // put them back
    ctx.putImageData(pixels, 0, 0);
  }, 16);
}

getVideo();
video.addEventListener('canplay', paintToCanvas);

function modify(pixels) {
  const levels = {};
  
  document.querySelectorAll('.rgb input').forEach((input) => {
    levels[input.name] = input.value;
  });
  
  for (i = 0; i < pixels.data.length; i = i + 4) {
    red = pixels.data[i + 0];
    green = pixels.data[i + 1];
    blue = pixels.data[i + 2];
    alpha = pixels.data[i + 3];
    
//     if (red >= levels.rmin
//         && green >= levels.gmin
//         && blue >= levels.bmin
//         && red <= levels.rmax
//         && green <= levels.gmax
//         && blue <= levels.bmax) {
//       // take it out!
//       pixels.data[i + 3] = 0;
//     }
    let score = red + red - 1.5* (green + blue);
    pixels.data[i + 3] = Math.min(255, score);
  }
  
  return pixels;
}

navigator.mediaDevices.enumerateDevices()
    .then(function(devices) {
      devices.forEach(function(device) {
        console.log(device.kind + ": " + device.label +
            " id = " + device.deviceId);
      });
    })