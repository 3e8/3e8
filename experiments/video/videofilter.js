var SRC = "nasa.mp4";  //"WEBCAM" oder Name des Videos

function modify(pixels, newpixels) {

  for (let i = 0; i < pixels.data.length; i = i + 4) {
    let red   = pixels.data[i + 0];
    let green = pixels.data[i + 1];
    let blue  = pixels.data[i + 2];
    let alpha = pixels.data[i + 3];
  
    //nichts tun
//     newpixels.data[i+0] = green;
//     newpixels.data[i+1] = red;
//     newpixels.data[i+2] = blue;
//     newpixels.data[i+3] = alpha;
    
    //aufhellen
//     newpixels.data[i+0] = 255 - 0.5*(255-red);
//     newpixels.data[i+1] = 255 - 0.5*(255-green);
//     newpixels.data[i+2] = 255 - 0.5*(255-blue);
//     newpixels.data[i+3] = alpha;
  
    // Transparenz falls schwarz
    newpixels.data[i+0] = red;
    newpixels.data[i+1] = green;
    newpixels.data[i+2] = blue;
    newpixels.data[i+3] = Math.min(255, (red + blue + green) * 4);
    
    //Abdunkeln
    //Kontrast erhöhen
    //Blue Screen
    //Unscharf machen
    //Ghost effekt
    
  }
  
  return newpixels;
}
