express = require('express');  //web server
app = express();
server = require('http').createServer(app);
io = require('socket.io').listen(server);	//web socket server

require("../metamodules/processSetErrorhandlers");
var {log, logError} = require("../metamodules/logToOpener.js");

var serversettings = require("../entrance/serversettings.js");

app.use('/', express.static(__dirname + '/'));

server.listen(serversettings.physics.internalport);

var brightness=1;

io.sockets.on('connection', function (socket) { //gets called whenever a client connects
  socket.emit('led', {value:brightness }); //send the new client the current brightness
  
  socket.on('led', function (data) { //makes the socket react to 'led'
    console.log(data);
    brightness = data.value;  //updates brightness from the data object
    io.sockets.emit('led', {value: brightness, edit: data.edit}); //sends the updated brightness to all connected clients
  });
});

log("Sensor server listening on port " + serversettings.physics.internalport);