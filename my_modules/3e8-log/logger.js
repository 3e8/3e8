const fs = require('fs');
const rootdir = require('find-parent-dir').sync(__dirname, "isRoot.txt");

const logger = require('tracer').console({
  format : "{{timestamp}} - {{message}} ({{title}} in {{file}}:{{line}})",
  dateformat : "yyyy-mm-dd HH:MM:ss.L",
  transport : function({output, timestamp, message, title, line, pos, level, path, method, stack}) {
    console.log(output);
    let html = `<div class='${title}' data-time=${timestamp.replace(/[-:.\s]/g, "")}>${escape(output)}</div>`;
    fs.appendFileSync(rootdir+'logs/local_logs/logs.html', html + '\n', (err) => {
      if (err) throw err;
    });
  }
});

if(!process.has3e8logger) {
  process.has3e8logger = true;
  process.on("uncaughtException", err => {
    logger.error(err);
    process.exit(1);
  });
}

if(!process.has3e8rejector) {
  process.has3e8rejector = true;
  process.on('unhandledRejection', (reason, p) => {
    //let err = new Error('Unhandled Rejection: ' + typeof reason === "string" ? reason : JSON.stringify(reason));
    logger.error(reason);
    process.exit(1);
  });
}

module.exports = {
  log: logger.log,
  logError: logger.error,
  warn: logger.warn,
  logger,
};

function escape(s) {
  return String(s).replace(/[<>&"']/g, function(c) {
    return "&#" + c.charCodeAt(0) + ";";
  });
}

// logger.log('hello');
// logger.trace('hello', 'world');
// logger.debug('hello %s', 'world', 123);
// logger.info('hello %s %d', 'world', 123, {foo : 'bar'});
// logger.warn('hello %s %d %j', 'world', 123, {foo : 'bar'});
// logger.error('hello %s %d %j', 'world', 123, {foo : 'bar'}, [ 1, 2, 3, 4 ], Object);