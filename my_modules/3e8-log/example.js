const {log, logError, warn} = require("./logger.js");

log(111, 66, 66878);
warn("Attention!");
logError(222, 33, [66,88], {a: function(f) {return 7}, c: 8});

Promise.reject("ooooops");
throw new Error("ooooopsiiiie!");
//Logging happens later beacuse of the event queue

class A {
  bark() {
    log("AAA");
    logError("shit");
  }
}

let a = new A();

a.bark();