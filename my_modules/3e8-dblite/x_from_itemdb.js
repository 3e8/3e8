const fs = require('fs');
const url = require('url');
const path = require('path');

//********dbHandler

// var rp = path.relative(process.cwd(), __dirname);
// var relativePathToWd = rp ? rp + "/" : "";
// console.log(relativePathToWd);

function dbHandler(req, res) {
  const uri = url.parse(req.originalUrl).pathname;
  var rb = req.body;
  console.log(rb);
  var task = rb.task;
  var dbname = rb.dbname;
  var dbpath = uri.replace("/db", "").replace(/^\//g, "");
  var dbdir = rb.dbdir || (dbpath + "/" + dbname);

  if(task === "getAll") {
    var allFiles = {};
    getValidFilenames(dbdir).forEach(function(f) {allFiles[f] = fs.readFileSync(dbdir + "/" + f, 'utf8')});
    res.json(allFiles);
  }

  if(task === "getLeaves") {
    var leaves = {};
    var itemgroups = getListWithNewestPerIdn(dbdir);
    Object.keys(itemgroups).forEach(function(idn) {
      var idv = itemgroups[idn];
      if(!~(idv.split(".")[0].split("-")[5] || "").indexOf("x")) { //exlude deleted items
        leaves[idv] = fs.readFileSync(dbdir + "/" + idv, 'utf8');
      }
    });
    res.json(leaves);
  }

  if(task === "addItem") {
    var previous = rb.previous;
    var idv = rb.idv;
    var idn = idv.split("-").slice(0,3).join("-");
    var data = rb.data;
    //if(get_magic_quotes_gpc()) $data = stripslashes($data);
    var existing = getValidFilenames(dbdir).filter(function(filename) {return filename.indexOf(idn) == 0;}).sort();
    if(previous === "" && existing.length > 0) {
      res.set('Content-Type', 'text/plain').send("Namenskonflikt beim Hinzufügen!");
    }
    else if(previous !== "" && previous !== existing[existing.length-1] ) {
      let conflicting = {};
      conflicting[existing[existing.length-1]] = fs.readFileSync(dbdir + "/" + existing[existing.length-1], 'utf8');
      res.set('Content-Type', 'text/plain').send("Konflikt mit Eintrag von anderswo:" + JSON.stringify(conflicting));
    }
    else {
      fs.writeFileSync(dbdir + "/" + idv, data);
      res.set('Content-Type', 'text/plain').send("insert ok!");
    }
  }

  if(task === "addUpdatedVersions") {
    var previousIdvs = rb.previousIdvs;
    var idvs = rb.idvs;
    let data = rb.data;
    var filenames = getValidFilenames(dbdir);

    var conflicting = [];
    var ok = {};
    var allok = true;

    data.forEach(function(doc, i) {
      var idv = idvs[i];
      var idn = idv.split("-").slice(0,3).join("-");
      var existing = getValidFilenames(dbdir).filter(function(filename) {return filename.indexOf(idn) === 0;}).sort();
      if(previousIdvs[i] !== "" && previousIdvs[i] !== existing[existing.length-1]) {
        conflicting[i] = {};
        conflicting[i][existing[existing.length-1]] = fs.readFileSync(dbdir + "/" + existing[existing.length-1], 'utf8');
        allok = false;
      }
      else {
        conflicting[i] = false;
        ok[idv] = doc;
      }
    });
    if(allok) {
      for(var idvToSave in ok) {
        fs.writeFileSync(dbdir + "/" + idvToSave, ok[idvToSave]);
      }
      console.log("SendUpdate");
      res.set('Content-Type', 'text/plain').send("updates ok!");
    }
    else {
      res.set('Content-Type', 'text/plain').send("Konflikte mit Eintrag von anderswo:" + JSON.stringify(conflicting));
    }
  }

  if(task === "moveOldToArchive") {
    let itemgroups = getListWithNewestPerIdn(dbdir);
    var old = 0, deleted = 0;
    getValidFilenames(dbdir).forEach(function(f) {
      var idn = f.split("-").slice(0,3).join("-");
      if(itemgroups[idn] !== f) {
        fs.renameSync(dbdir+"/"+f, dbdir+"/archive/"+f);
        old++;
      }
      else if(~(f.split(".")[0].split("-")[5] || "").indexOf("x")) {
        fs.renameSync(dbdir+"/"+f, dbdir+"/archive/"+f);
        deleted++;
      }
    });
    res.set('Content-Type', 'text/plain').send("moved "+old+" old and "+deleted+" deleted files to archive.");
  }

  if(task === "pollForChanges") {
    var since = rb.since;
    var i = 0;
    var checkForChanges = function(counter, start, responseLink) {
      var newFiles = {};
      var files = getValidFilenames(dbdir);
      files.forEach(function(f) {
        if(getFileModificationDate(f) > start) {
          newFiles[f] = fs.readFileSync(dbdir + "/" + f, 'utf8');
        }
      });
      if(Object.keys(newFiles).length) {
        console.log(JSON.stringify(newFiles));
        responseLink.json(newFiles);
      }
      else {
        if(counter++ < 10) {
          setTimeout(checkForChanges.bind(null, counter, start, responseLink), 1000);
        }
        else responseLink.set('Content-Type', 'text/plain').send("nochanges");
      }
    };
    setTimeout(checkForChanges.bind(null, i, since, res), 1000);
  }
}

function getValidFilenames(dbdir) {
  return fs.readdirSync(dbdir).filter(function(filename) {return ~filename.indexOf(".")});
}

function getListWithNewestPerIdn(dbdir) {
  var itemgroups = {};
  getValidFilenames(dbdir).forEach(function(f) {
    var parts = f.split("-");
    var idn = parts.slice(0,3).join("-");
    var newestUntilNow = itemgroups[idn] || "";
    var partsOfNewest = newestUntilNow.split("-");
    if(!itemgroups[idn] || parts[3] > partsOfNewest[3]) {
      itemgroups[idn] = f;
    }
  });
  return itemgroups;
}

function getFileModificationDate(filename) {
  return filename.split(".")[0].split("-")[3] || "0";
}

function pollForDevChanges(req, res) {
  var rb = req.body;
  //console.log(rb);
  //console.log("DC", relativePathToWd + 'build' + rb.file.slice(rb.file.indexOf("/dev") + 4));
  var mod = fs.statSync(relativePathToWd + 'build' + rb.file.slice(rb.file.indexOf("/dev") + 4)).mtime.getTime();
  res.send(""+mod);
}

module.exports = {dbHandler};