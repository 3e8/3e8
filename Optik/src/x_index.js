import React from 'react';
import ReactDOM from 'react-dom';

let mainstate = {
  shapes: []
};
let statesetter;

let selectShape = function(e) {
  let shape = findShape(e.currentTarget.dataset);
  // if(mainstate.activeTask==="addSegment" && shape.type === "point") {
  //   if(mainstate.shapePoints.length === 0) {
  //     statesetter({shapePoints: [shape]});
  //   }
  //   else {
  //     let sp = [...mainstate.shapePoints, shape];
  //     statesetter({shapes: [...mainstate.shapes, createSegment(sp)], shapePoints: []});
  //   }
  // }
  if(mainstate.activeTask==="addSegment") {
    if(shape.type==="point") {
      if (!mainstate.deps || !mainstate.deps.find(s => s.relation === "from")) {
        statesetter({deps: [...mainstate.deps||[], {relation: "from", obj: shape}]});
      }
      if (mainstate.deps && mainstate.deps.find(s => s.relation === "from")) {
        statesetter({deps: [...mainstate.deps, {relation: "to", obj: shape}]});
        statesetter({shapes: [...mainstate.shapes, createSegment(mainstate.deps)], deps: []});
      }
    }
    if(hasType(shape, "segment")) {
      if (!mainstate.deps || !mainstate.deps.find(s => s.relation === "parallel")) {
        statesetter({deps: [...mainstate.deps||[], {relation: "parallel", obj: shape}]});
      }
    }
  }
  console.log(shape);
  e.stopPropagation();
};

function findShape({name, type}) {
  return mainstate.shapes.find(s=>s.type===type && s.name===name);
}

class Optiktask extends React.Component {
	constructor(props) {
    super(props);
    this.state = {
      shapes: []
    };
    statesetter = update => {
      this.setState(update);
      mainstate = {...mainstate, ...update}
    }
  }
	render() {
    return (
    <div style={{position: "relative"}}>
			<Area mainstate={this.state}></Area>
      <Menu mainstate={this.state}></Menu>
      <Protocol mainstate={this.state}></Protocol>
    </div>
    );
	}
}

class Menu extends React.Component {
	constructor(props) {
    super(props);
  }
  addPoint = e => statesetter({activeTask: "addPoint"});
  addSegment = e => statesetter({activeTask: "addSegment", shapePoints: []});

  inputChange = e => {
    statesetter({["input_"+e.target.dataset.name]: e.target.value})
  };
  addManualPoint = e => {
    let {shapes, input_newPointX, input_newPointY} = mainstate;
    statesetter({shapes: [...shapes, createPoint([{relation: "manual", coords: {x: +input_newPointX, y: +input_newPointY}}])]});
  };

	render() {
    let {activeTask, shapes, coords, input_newPointX, input_newPointY} = mainstate;
    return (
      <div>
        <button onClick={this.addPoint}>Punkt</button>
        <button onClick={this.addSegment}>Strecke</button>
        <div>
          {activeTask === "addPoint" &&
          <div>
            X: <input data-name="newPointX" onChange={this.inputChange} value={coords ? coords.x : input_newPointX || ""}></input>
            Y: <input data-name="newPointY" onChange={this.inputChange} value={coords ? coords.y : input_newPointY || ""}></input>
            {input_newPointX && input_newPointY && <button onClick={this.addManualPoint}>Add</button>}
          </div>}
          {activeTask === "addSegment" && <div>P1: {mainstate.shapePoints[0] && mainstate.shapePoints[0].name} P2: <input></input></div>}
        </div>
      </div>);
  }
}

class Area extends React.Component {
	
	constructor(props) {
    super(props);
    this.selectShape = selectShape.bind(this);
  }
  mouseMove = e => {
    statesetter({coords: getSvgCoords(e)})
  };
  mouseLeave = e => {
    statesetter({coords: false})
  };
  click = e => {
    if(mainstate.activeTask === "addPoint") {
      statesetter({shapes: [...mainstate.shapes, createPoint([{relation: "manual", coords: getSvgCoords(e)}])]});
    }
  };
	render() {
	  let {mainstate} = this.props;
		return (
    <div style={{position: "relative"}}>
      <svg width="100%" viewBox="0 0 200 100" style={{backgroundColor: "#cde", transform: "scaleY(-1)"}} onMouseMove={this.mouseMove} onMouseLeave={this.mouseLeave} onClick={this.click}>
        {mainstate.shapes.filter(s=>s.type=="segment"&&!s.hidden).map(s=><line onClick={this.selectShape} key={s.name} x1={coords(from(s)).x} y1={coords(from(s)).y} x2={coords(to(s)).x} y2={coords(to(s)).y}  stroke="#eea" title={s.name} data-name={s.name} data-type={s.type}><title>{s.name}</title></line>)}
        {mainstate.shapes.filter(s=>s.type=="point"&&!s.hidden).map(s=><circle onClick={this.selectShape} key={s.name} r={1} cx={coords(s).x} cy={coords(s).y} data-name={s.name} data-type={s.type}><title>{s.name}</title></circle>)}
      </svg>
    </div>
    );
	}
}

class Protocol extends React.Component {
	
	constructor(props) {
    super(props);
    this.selectShape = selectShape.bind(this);
  }
	render() {
    let {mainstate} = this.props;
		return (
    <div>
      {mainstate.shapes.filter(s=>s.type==="point").map(s=><Protocolshape key={s.name} shape={s} mainstate></Protocolshape>)}
      {mainstate.shapes.filter(s=>s.type==="segment").map(s=><div key={s.name} data-name={s.name} data-type={s.type} onClick={this.selectShape}>{s.type} {s.name} {s.shapepoints[0].name} {s.shapepoints[1].name} {dir(s.shapepoints)|0}° {dist(s.shapepoints)|0}</div>)}
    </div>
    );
	}
}

class Protocolshape extends React.Component {
  constructor(props) {
    super(props);
    this.selectShape = selectShape.bind(this);
  }
  toggleHide = function(e){
    let shape = findShape(e.currentTarget.closest("div").dataset);
    mainstate.shapes[mainstate.shapes.indexOf(shape)] = {...shape, hidden: !shape.hidden};
    statesetter({shapes: [...mainstate.shapes]})
  };
  render() {
    let s = this.props.shape;
    return (<div
      data-name={s.name}
      data-type={s.type}
      onClick={this.selectShape}>
      <button onClick={this.toggleHide}>hide</button>
      <button>delete</button>
      {s.type}
      {s.name}
      {coords(s).x|0}
      {coords(s).y|0}
      </div>);
  }
}

let pointNum = 0;
function createPoint(dependencies) {
  let point = {};
  point.name = String.fromCharCode(65 + pointNum%26) + (pointNum>25?pointNum/26|0:"");
  point.type = "point";
  point.dependencies = dependencies;
  pointNum++;
  return point;
}
function coords(shape) {
  if(shape.type==="point" && shape.dependencies[0].relation === "manual") {
    return shape.dependencies[0].coords;
  }
  else console.warn("coords not yet defined");
}

function from(shape) {
  return shape.dependencies.find(s=>s.relation === "from");
}

function to(shape) {
  let to = shape.dependencies.find(s=>s.relation === "to");
  if(to) return to;
  let from = from(shape);
}

let lineNum = 0;
function createSegment(dependencies) {
  let shape = {};
  shape.name = String.fromCharCode(97 + lineNum%26) + (lineNum>25?lineNum/26|0:"");
  shape.type = "segment";
  shape.dependencies = dependencies;
  lineNum++;
  return shape;
}
function createLine(dependencies) {
  let shape = {};
  shape.name = String.fromCharCode(97 + lineNum%26) + (lineNum>25?lineNum/26|0:"");
  shape.type = "line";
  shape.dependencies = dependencies;
  debugger;
  lineNum++;
  return shape;
}

function diff(p0, p1) {
  return {dx: p1.x - p0.x, dy: p1.y - p0.y};
}

function angleFromPoints(p0, p1) {
  let {dx, dy} = diff(coords(p0), coords(p1));
  return Math.atan2(dy, dx) * 180 / Math.PI;
}

function dir(shape) {
  if(["segment", "ray", "line"].includes(shape.type)) {
    if(shape.dependencies.every(d => d.type === "point")) {
      return angleFromPoints(...shape.dependencies);
    }
    if(shape.dependencies.some(d => d.type === "point")) {
      return angleFromPoints(...shape.dependencies);
    }
  }
  let {dx, dy} = diff(coords(points[0]), coords(points[1]));
  return Math.atan2(dy, dx) * 180 / Math.PI;
}

function dist(points) {
  let {dx, dy} = diff(coords(points[0]), coords(points[1]));
  return Math.hypot(dy, dx);
}

function hasType(shape, type) {
  if(shape.type === type) return true;
  if(type === "linear" || type === "direction") {
    if(["segment", "line", "ray", "vector"].includes(shape.type)) {
      return true;
    }
  }
  if(type === "distance") {
    if(["segment", "vector"].includes(shape.type)) {
      return true;
    }
  }
}

class Ray {
  constructor(shapepoints) {
    this.name = String.fromCharCode(97 + lineNum%26) + (lineNum>25?lineNum/26|0:"");
    lineNum++;
    this.type = "ray";
    this.shapepoints = shapepoints;
  }
}
class Line {
  constructor(shapepoints) {
    this.name = String.fromCharCode(97 + lineNum%26) + (lineNum>25?lineNum/26|0:"");
    lineNum++;
    this.type = "line";
    this.shapepoints = shapepoints;
  }
}

ReactDOM.render(
  <Optiktask></Optiktask>,
  document.getElementById('app')
);

function getSvgCoords(e) {
  let m = e.currentTarget.getScreenCTM();
  let p = e.currentTarget.createSVGPoint(); 
  p.x = e.clientX;
  p.y = e.clientY;
  p = p.matrixTransform(m.inverse());
  return {x: p.x, y: p.y}
}

module.hot.accept();