const path = require('path').posix;
const Database = require("better-sqlite3");
const {readDir, readFile, readStat, copyFile, unlink, open, close, writeFile} = require("prms-fs");

let db = new Database("./localhistory.db");

async function mig() {
  let syncsettings = require("./syncsettings.json");
  let ignoreRegexes = syncsettings.ignore.map(pattern=>new RegExp(pattern, "g"));
  let r = db.prepare("SELECT action, version, path FROM history").all();
  r.forEach(async i=>{
    if(ignoreRegexes.some(rgx=>i.path.search(rgx)!==-1)) return;
    let newfile = await open("localhistory/" + encodeVersion(i), 'w');
    await close(newfile);
  });
}

mig();

/**
 * decodePath
 * @param {string} p
 */
function decodePath(p) {
  return p.replace(/-PARFOL-/g, "..").replace(/-DOT-/g, ".").replace(/_/g, "/").replace(/-UNDSC-/g, "_");
}

/**
 * encodePath
 * @param {string} p
 */
function encodePath(p) {
  let {dir, name, ext} = path.parse(p);
  return (dir+"/"+name+ext).replace(/_/g, "-UNDSC-").replace(/[\/\\]/g, "_").replace(/\.\./g, "-PARFOL-").replace(/\./g, "-DOT-");
}

function encodeVersion(obj) {
  return obj.action + "_" + obj.version + "_" + encodePath(obj.path) + ".x";
}