const fs = require("fs");

const liveLinkToJsonFile = function(obj, path, interval) {
  let lastUpdate = fs.statSync(path).mtime;
  obj.data = JSON.parse(fs.readFileSync(path, {encoding: "UTF-8"}));
  
  const check = function() {
    fs.stat(path, (err, stat) => {
      if(stat.mtime > lastUpdate) {
        obj.data = JSON.parse(fs.readFileSync(path, {encoding: "UTF-8"}));
        lastUpdate = stat.mtime;
      }
    })
  };
  
  setInterval(check, interval);
};

module.exports = {liveLinkToJsonFile};