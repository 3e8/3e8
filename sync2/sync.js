//start with $ .\node_modules\.bin\electron .
const {app, Tray, Menu, BrowserWindow} = require('electron');
const {log, logError, warn, logger} = require("../my_modules/3e8-log");

const path = require('path');
const iconPath = path.join(__dirname, 'ok.ico');
let tray = null;
let win = null;

app.on('ready', _=>{
  setTimeout(_=>startSynccheck(), 1000);
});

function startSynccheck(){
  win = new BrowserWindow({show: false});
  win.show();
  win.maximize();
  win.toggleDevTools();
  
  win.loadURL(`file://${__dirname}/sync.html`);
  win.on("close", e => {e.preventDefault(); win.hide();} );
  tray = new Tray(iconPath);
  tray.setImage(path.join(__dirname, 'warn.ico'));
  tray.on("click", _=> win.show() );
  
  // Send async message to main process
  const {ipcMain} = require('electron');
  ipcMain.on('my-channel', (event, data) => {
    console.log(data);
    tray.setImage(path.join(__dirname, data.warn ? 'warn.ico' : (data.error ? 'error.ico' : 'ok.ico')));
    tray.setToolTip(data.msg);
  });
  
  var contextMenu = Menu.buildFromTemplate([
    {
      label: 'show State',
      click: function() {
        win.show();
        win.toggleDevTools();
      }
    },
    { label: 'Quit',
      accelerator: 'Command+Q',
      selector: 'terminate:',
      click: function() {
        tray.destroy();
        app.exit();
      }
    }
  ]);
  tray.setToolTip('Sync');
  tray.setContextMenu(contextMenu);
}