let rdd = require("./synchelpers").readDirDeep;

async function start() {
  let s = Date.now();
  let d = await rdd("../", [/node_modules/]);
  console.log(Date.now()-s);
  console.log(d.length);
}

start();