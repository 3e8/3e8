const path = require('path').posix;
const Database = require("better-sqlite3");
const {readDir, readFile, readStat, copyFile, unlink, open, close} = require("prms-fs");

let synchelpers = {};
const actions = {created: "C", modified: "M", deleted: "X"};
synchelpers.actions = actions;

/**
 * @param {String} dir
 * @param {Array?} ignoreRegexes
 * @param {Boolean?} onlyFolders
 * @returns {Promise.<Array>}
 */
synchelpers.readDirDeep = async function(dir, ignoreRegexes=[], onlyFolders = false) {
  let allFiles = [];
  if(ignoreRegexes.some(rgx=>dir.search(rgx)!==-1)) return allFiles;
  try {
    var files = await readDir(dir);
    const validNewPaths = files.map(f => path.join(dir, f)).filter(f => !ignoreRegexes.some(rgx=>f.search(rgx)!==-1));
    let kids = await Promise.all(validNewPaths.map(async f => {
      try {
        let stat = await readStat(f);
        return stat.isDirectory() ? [f, ...(await synchelpers.readDirDeep(f, ignoreRegexes, onlyFolders))] : (onlyFolders ? [] : [f]);
      }
      catch(e) {
        if(e.code === "ENOENT") return [];
        else logError(e);
      }
    }));
    return allFiles.concat(...kids);
  }
  catch(e) {
    if(e.code === "ENOENT") return allFiles;
    else logError(e);
  }
};

/**
 * get valid (not ignored) entries from all roots
 * @param {Object} syncsettings
 * @returns {Promise}
 */
synchelpers.getLocalFiles = async function(syncsettings) {
  let ignoreRegexes = syncsettings.ignore.map(pattern=>new RegExp(pattern, "g"));
  let rootEntries = await Promise.all(syncsettings.syncroots.map(r => synchelpers.readDirDeep(r, ignoreRegexes)));
  let entries = [].concat(...rootEntries); //flatten
  //noinspection EqualityComparisonWithCoercionJS
  //let filesToSync = files.filter(p=>ignoreRegexes.every(rgx=>p.search(rgx)==-1));
  let filedataOnDisk = await Promise.all(entries.map(async p=>{
    let stats = await readStat(p);
    let obj = {path: p, version: getVersion(stats.mtime)};
    if(!!~path.parse(p).base.indexOf(".") === !!stats.isDirectory()) {
      obj.isDir = stats.isDirectory(); //nur falls es mit dem Punkt im Dateinamen nicht eindeutig ist.
    }
    return obj;
  }));
  return filedataOnDisk;
};

/**
 * getHistoryVersions
 */
synchelpers.getHistoryVersions = async function(syncsettings) {
  const db = new Database("./localhistory.db");
  let ignoreRegexes = syncsettings.ignore.map(pattern=>new RegExp(pattern, "g"));
  let versions = db.prepare("SELECT action, version, path FROM history").all()
    .filter(f => !ignoreRegexes.some(rgx=>f.path.search(rgx)!==-1));
  db.close();
  return versions;
};

/**
 * get Array that needsUpdate
 * @param oldfiles
 * @param newfiles
 * @returns {Array}
 */
synchelpers.diff = function(oldfiles, newfiles) {
  let needsUpdate = [];
  newfiles.forEach(f=>{
    var l = oldfiles.find(lf=>lf.path===f.path);
    if(f.action!==actions.deleted && (!l || l.action===actions.deleted)) {
      needsUpdate.push(Object.assign({} ,f, {action: actions.created}));
    }
    else if(f.action===actions.deleted && !l) {
      // ignore items that are deleted elsewhere and don't exist here
    }
    else if(l.version !== f.version && !synchelpers.isDir(l) && !(f.action===actions.deleted && l.action===actions.deleted)) {
      needsUpdate.push(Object.assign({}, f, {action: actions.modified, prev: l.version}));
    }
  });
  oldfiles.forEach(l=>{
    var f = newfiles.find(fod=>fod.path===l.path);
    if( (!f && l.action !== actions.deleted) || (f && f.action === actions.deleted && l.action !== actions.deleted && f.version >= l.version) ) {
      needsUpdate.unshift(Object.assign({}, l, {action: actions.deleted}));
    }
  });
  return needsUpdate;
};

/**
 * updateLocaldata on item change
 * @param newitem
 */
synchelpers.updateLocaldata = async function(newitem) {
  //let syncsettings = JSON.parse(await readFile("syncsettings.json"));
  //let old = await synchelpers.getHistoryVersions(syncsettings);
  const db = new Database("./localhistory.db");
  //let old = db.prepare("SELECT action, version, path FROM history").all();
  let a = db.prepare(`DELETE FROM history WHERE path = ?`).run(newitem.path);
  let b = db.prepare(`INSERT INTO history (action, path, version) VALUES (@action, @path, @version)`).run(newitem);
  db.close();
  // let oldOfItem = old.find(o=>o.path === newitem.path);
  // if(oldOfItem) {
  //   await unlink("localhistory/" + encodeVersion(oldOfItem));
  // }
  // let newfile = await open("localhistory/" + encodeVersion(newitem), 'w');
  // await close(newfile);
};

/**
 * checks if obj.path is dir
 * @param obj
 */
synchelpers.isDir = function(obj) {
  if(obj.isDir !== undefined) return obj.isDir;
  let {base} = path.parse(obj.path);
  return base.indexOf(".")===-1;
};

/**
 * datastream to json
 * @param req
 * @returns {Promise}
 */
synchelpers.parseJsonBody = async function(req) {
  return new Promise((resolve, reject)=>{
    let body = [];
    req.on('data', function(chunk) {
      body.push(chunk);
    }).on('end', function() {
      let bodystr = Buffer.concat(body).toString();
      try {
        let b = JSON.parse(bodystr);
        resolve(b);
      }
      catch(e) {
        logError(e);
        logError(`invalid JSON was: ${bodystr}`);
        reject(e);
      }
    });
  });
};

/**
 * pathToArchiveModifiedOrDeletedFiles
 * @param obj
 */
synchelpers.getArchivePath = function(obj) {
  let {dir, name, ext} = path.parse(obj.path);
  return "localarchive/" + (dir+"/"+name).replace(/\.\./g, "-PARFOL-").replace(/_/g, "-UNDSC-").replace(/[\/\\]/g, "_").replace(/\.\./g, "--") + "_" + obj.version + ext;
};

// /**
//  * decodePath
//  * @param {string} p
//  */
// function decodePath(p) {
//   return p.replace(/-PARFOL-/g, "..").replace(/-DOT-/g, ".").replace(/_/g, "/").replace(/-UNDSC-/g, "_");
// }

// /**
//  * encodePath
//  * @param {string} p
//  */
// function encodePath(p) {
//   let {dir, name, ext} = path.parse(p);
//   return (dir+"/"+name+ext).replace(/_/g, "-UNDSC-").replace(/[\/\\]/g, "_").replace(/\.\./g, "-PARFOL-").replace(/\./g, "-DOT-");
// }

// /**
//  * encodeVersion
//  * @param obj
//  */
// function encodeVersion(obj) {
//   return obj.action + "_" + obj.version + "_" + encodePath(obj.path) + ".x";
// }

/**
 * Version (e.g "oq3282") from Date
 * @param {Date} mtime
 * @returns {string}
 */
function getVersion(mtime) {
  return Math.floor(mtime.getTime()/1000).toString(36);
}

/**
 * Date from version (e.g "oq3282")
 * @param {string} version
 * @returns {Date}
 */
synchelpers.versionToTime = function (version) {
  return  new Date(parseInt(version, 36)*1000);
};
// function sanityChecks(syncsettings, localdata) {
//   syncsettings.syncroots.forEach(syncroot => !fs.existsSync(syncroot) && fs.mkdirSync(syncroot) );
//   let old = JSON.stringify(localdata);
//   localdata = matchWithSettings(syncsettings, localdata);
//   if(old !== JSON.stringify(localdata)) {
//     log("slow save necessary");
//     saveLocaldata(localdata);
//   }
//   return localdata;
// }

module.exports = synchelpers;

