const path = require('path').posix;
const Database = require("better-sqlite3");
const {readDir, readFile, readStat, copyFile, unlink, open, close} = require("prms-fs");

let db = new Database("./localhistory.db");

db.prepare(`CREATE TABLE IF NOT EXISTS history (
  id INTEGER PRIMARY KEY ASC,
  action text NOT NULL,
  version text NOT NULL,
  path text NOT NULL
)`).run();

async function mig() {
  let syncsettings = require("./syncsettings.json");
  let ignoreRegexes = syncsettings.ignore.map(pattern=>new RegExp(pattern, "g"));
  let paths = await readDir("localhistory");
  let versions = (await Promise.all(paths.map(async p=>{
    let parts = p.slice(0, -(".x".length)).split("_");
    return {action: parts[0], version: parts[1], path: decodePath(parts.slice(2).join("_"))};
  }))).filter(f => !ignoreRegexes.some(rgx=>f.path.search(rgx)!==-1));
  return versions;
}

mig().then(d=>{
  let s = d.map(i=>`("${i.path}","${i.version}","${i.action}")`).join(",");
  db.prepare(`INSERT INTO history (path, version, action) VALUES ${s}`).run();
  let r = db.prepare("SELECT action, version, path FROM history").all();
  console.log(r);
});





/**
 * decodePath
 * @param {string} p
 */
function decodePath(p) {
  return p.replace(/-PARFOL-/g, "..").replace(/-DOT-/g, ".").replace(/_/g, "/").replace(/-UNDSC-/g, "_");
}

/**
 * encodePath
 * @param {string} p
 */
function encodePath(p) {
  let {dir, name, ext} = path.parse(p);
  return (dir+"/"+name+ext).replace(/_/g, "-UNDSC-").replace(/[\/\\]/g, "_").replace(/\.\./g, "-PARFOL-").replace(/\./g, "-DOT-");
}