/**
 * @typedef {Object} syncsettings
 * @typedef {Array} syncsettings.syncroots
 * @typedef {Array} syncsettings.ignore
 * @typedef {Number} syncsettings.PORT
 * @typedef {Number} syncsettings.PORTINTERN
 */

process.chdir(__dirname);

const path = require('path').posix;
const {readDir, readFile, readStat, copyFile, rename, utimes} = require("prms-fs");
const http = require('http');
const fs = require('fs');
const {log, logError, warn} = require("../my_modules/3e8-log");

const synchelpers = require("./synchelpers");

async function start() {
	let syncsettings = JSON.parse(await readFile("syncsettings.json"));
	let localdata = await synchelpers.getHistoryVersions(syncsettings);
	let lastUpdates = JSON.parse(await readFile("localLastUpdates.json"));
	let user = readFile("localuser.txt", "UTF-8");
	let clientsOnline = [user];
	let server = http.createServer(httpRequestHandler);
  let pending = {};
	server.listen(syncsettings.PORTINTERN);
	log("Syncmaster is on " + syncsettings.PORTINTERN);
	
	async function httpRequestHandler(req, res) {
    if(req.method.toUpperCase()==="POST") {
      if(req.url.includes("/getmasterinfo")) {await getMasterData(req, res);}
      if(req.url.includes("/getfile")) {await getFile(req, res);}
      if(req.url.includes("/prepareUpload")) {await prepareUpload(req, res);}
      if(req.url.includes("/purgeHistory")) {let n = await synchelpers.purgeHistory(); res.end(String(n))}
    }
    else if(req.method.toUpperCase()==="PUT") {
      if(req.url.includes("/uploadfile")) {await uploadFile(req, res);}
    }
    else if(req.method.toUpperCase()==="DELETE") {
      if(req.url.includes("/deletefileorfolder")) {await deleteFile(req, res);}
    }
    else respondWithJson(res, {ok: 0});
	}
  async function getMasterData(req, res) {
    let syncsettings = JSON.parse(await readFile("syncsettings.json"));
    let versions = await synchelpers.getHistoryVersions(syncsettings);
    let localdata = await synchelpers.getLocalFiles(syncsettings);
    let localChanges = synchelpers.diff(versions, localdata);
    for(let item of localChanges) {
      await synchelpers.updateLocaldata(item);
    }
    synchelpers.parseJsonBody(req).then(reqjson=>{
      respondWithJson(res, localdata);
    });
  }
  async function getFile(req, res) {
    let {path} = await synchelpers.parseJsonBody(req);
    if(fs.existsSync(path)) {
      res.writeHead(200, {"Content-Type": "application/octet-stream"});
      fs.createReadStream(path).pipe(res);
    }
    else {
      res.writeHead(400, {"Content-Type": "text/plain"});
      res.end("Error: file does not exists");
    }
  }
  async function deleteFile(req, res) {
    let reqjson = await synchelpers.parseJsonBody(req);
    if(!fs.existsSync(reqjson.obj.path)) {
      log("Object to delete did not exist!!!");
      await synchelpers.updateLocaldata(reqjson.obj, "noSave");
      respondWithJson(res, {deleted: true});
      return;
    }
    await rename(reqjson.obj.path, synchelpers.getArchivePath(reqjson.obj));
    await synchelpers.updateLocaldata(reqjson.obj, "noSave");
    respondWithJson(res, {deleted: true});
    //informClientsExcept(reqjson.obj, reqjson.actor);
  }
  async function prepareUpload(req, res) {
    let reqjson = await synchelpers.parseJsonBody(req);
    if(synchelpers.isDir(reqjson.obj)) {
      if (!fs.existsSync(reqjson.obj.path)){
        fs.mkdirSync(reqjson.obj.path);
      }
      fs.utimesSync(reqjson.obj.path, reqjson.atime, reqjson.mtime);
      await synchelpers.updateLocaldata(reqjson.obj, "noSave");
      respondWithJson(res, {ok: true});
      //informclientsexcept(reqjson.obj, reqjson.actor);
    }
    else {
      let key = Date.now();
      pending[key] = reqjson;
      respondWithJson(res, {key});
    }
  }
  async function uploadFile(req, res) {
    let key = req.url.slice("/uploadfile/".length);
    if(pending[key].obj.action === synchelpers.actions.modified && fs.existsSync(pending[key].obj.path)) {
      await rename(pending[key].obj.path, synchelpers.getArchivePath(pending[key].obj));
    }
    let writer = fs.createWriteStream(pending[key].obj.path);
    req.on("data", chunk => writer.write(chunk));
    req.on("end", _ => {
      writer.end(async _=>{
        await utimes(pending[key].obj.path, pending[key].atime, pending[key].mtime);
        await synchelpers.updateLocaldata(pending[key].obj, "noSave");
        delete pending[key];
        respondWithJson(res, {ok: true});
      });
    });
  }
}


start().catch(e=>console.log(e));

function respondWithJson(res, jsonObj) {
  res.writeHead(200, {"Content-Type": "application/json"});
  res.write(JSON.stringify(jsonObj));
  res.end();
}

// function masterCheck(fromPoll = false) {
  // synchelpers.checkForLocalChangesPromise(syncsettings, localdata).then(updates=>{
    // if(updates.some(o=>Object.keys(pending).map(k=>pending[k].obj.path).includes(o.path))) {
      // clearTimeout(willSync);
      // willSync = setTimeout(masterCheck, 400);
      // return;
    // }
    // updates.forEach(updateLocaldata);
    // if(wss) {
      // updates.forEach(obj=>informClientsExcept(obj, user));
    // }
    // clearTimeout(willSync);
    // willSync = setTimeout(_=>masterCheck(fromPoll ? Math.min(2*fromPoll, 60000) : 4000), fromPoll || 2000)
  // }).catch(logError);
// }

//Global variables


// var lastUpdates = JSON.parse(fs.readFileSync("localLastUpdates.json"));
// var user = fs.readFileSync("localuser.txt", "UTF-8");
// var clientsOnline = [user];
// var pending = {};
// var server = http.createServer(httpRequestHandler);
// var willSync = false;

// //var watcher;

// masterCheck();

// //start servers
// server.listen(syncsettings.PORTINTERN);



// var wss = startWebsocketServer();
// //startWatching();
// log("Sync Server and Websocket Listening on port "+syncsettings.PORTINTERN+"...");

// // function startWatching() {
// //   let ww = synchelpers.resetWatcher(syncsettings, watcher, willSync, localdata, masterCheck);
// //   watcher = ww.watcher;
// //   willSync = ww.willSync;
// // }



// function updateLocaldata(newitem) {
  // synchelpers.updateLocaldata(newitem, localdata);
// }

// function saveLastUpdates() {
  // fs.writeFileSync("localLastUpdates.json", JSON.stringify(lastUpdates));
// }

// function startWebsocketServer() {
  // /** @typedef {Function} io.on */
  // let io = require('socket.io')(server);
  // io.on("connection", function(ws) {
    // var client = "";
    // ws.on("login", data=> {
      // client = data.actor;
      // clientsOnline = clientsOnline.filter(c=>c!==client).concat([client]);
      // lastUpdates[client] = lastUpdates[user];
      // if(client) log(client + " logged in");
      // saveLastUpdates();
    // });
    // ws.on("disconnect", _=> {
      // clientsOnline = clientsOnline.filter(c=>c!==client);
      // ws.disconnect(true);
      // if(client) log(client + " logged out, remaining: " + clientsOnline.join(","));
    // });
    // ws.on("cleanLogs", _=>{
      // log("cleanLogs");
      // synchelpers.cleanLogs();
    // });
  // });
  // return io;
// }

// function informClientsExcept(obj, actor) {
  // wss.emit("filechange", {obj, actor});
  // var updatestamp = synchelpers.getVersion(new Date());
  // clientsOnline.forEach(c=>{
    // lastUpdates[c] = updatestamp;
  // });
  // saveLastUpdates();
// }



// function httpRequestHandler(req, res) {
  // if(req.method==="POST" && req.url.indexOf("/prepareUpload")===0) { //prepareUpload or folder update
    // synchelpers.parseJsonBody(req).then(reqjson=>{
      // log(reqjson.actor + " " + reqjson.obj.action + " "+ reqjson.obj.path);
      // if(synchelpers.isDir(reqjson.obj)) {
        // if (!fs.existsSync(reqjson.obj.path)){
          // fs.mkdirSync(reqjson.obj.path);
        // }
        // fs.utimesSync(reqjson.obj.path, reqjson.atime, reqjson.mtime);
        // updateLocaldata(reqjson.obj);
        // respondWithJson(res, {ok: true});
        // informClientsExcept(reqjson.obj, reqjson.actor);
      // }
      // else {
        // let key = Date.now();
        // pending[key] = reqjson;
        // respondWithJson(res, {key});
      // }
    // }).catch(logError);
  // }
  // else if(req.method==="POST" && req.url.indexOf("/getmasterinfo")===0) {
    // synchelpers.parseJsonBody(req).then(reqjson=>{
      // respondWithJson(res, localdata);
    // });
  // }
  // else if(req.method==="POST" && req.url.indexOf("/getfile")===0) {
    // synchelpers.parseJsonBody(req).then(obj=>{
      // if(fs.existsSync(obj.path)) {
        // res.writeHead(200, {"Content-Type": "application/octet-stream"});
        // fs.createReadStream(obj.path).pipe(res);
      // } else {
        // res.writeHead(400, {"Content-Type": "text/plain"});
        // res.end("ERROR File does NOT Exists");
      // }
    // });
  // }
  // else if(req.method==="PUT") {
    // let key = req.url.slice("/uploadfile/".length);
    // if(pending[key].obj.action === synchelpers.actions.modified && fs.existsSync(pending[key].obj.path)) {
      // fs.renameSync(pending[key].obj.path, synchelpers.getArchivePath(pending[key].obj));
    // }
    // let writer = fs.createWriteStream(pending[key].obj.path);
    // req.on("data", chunk => writer.write(chunk));
    // req.on("end", _ => {
      // writer.end(_=>{
        // fs.utimes(pending[key].obj.path, pending[key].atime, pending[key].mtime, _=>{
          // updateLocaldata(pending[key].obj);
          // respondWithJson(res, {ok: true});
          // informClientsExcept(pending[key].obj, pending[key].actor);
          // delete pending[key];
        // });
      // });
    // });
  // }
  // else if(req.method==="DELETE") {
    // synchelpers.parseJsonBody(req).then(reqjson=>{
      // log(reqjson.actor + " " + reqjson.obj.action + " "+ reqjson.obj.path);
      // if(!fs.existsSync(reqjson.obj.path)) {
        // log("Object to delete did not exist!!!");
        // updateLocaldata(reqjson.obj);
        // respondWithJson(res, {deleted: true});
        // return;
      // }
      // fs.renameSync(reqjson.obj.path, synchelpers.getArchivePath(reqjson.obj));
      // //if(!synchelpers.isDir(reqjson.obj)) {
      // //  fs.renameSync(reqjson.obj.path, synchelpers.getArchivePath(reqjson.obj));
      // //}
      // //else {
      // //  fs.rmdirSync(reqjson.obj.path);
      // //}
      // updateLocaldata(reqjson.obj);
      // respondWithJson(res, {deleted: true});
      // informClientsExcept(reqjson.obj, reqjson.actor);
    // });
  // }
  // else {
    // log("invalid request from " + (req.headers['x-forwarded-for'] || req.connection.remoteAddress));
    // respondWithJson(res, {ok: false})
  // }
// }