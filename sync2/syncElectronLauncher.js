const {spawn} = require("child_process");
const {log, logError, warn, logger} = require("../my_modules/3e8-log");

const p = spawn("node_modules\\.bin\\electron.cmd", ["."], {detached: false});

process.on("message", msg => {
  if(msg==="quit") {
    process.exit(0);
  }
});

p.on("exit", exitcode=>{
  log("SyncElectron finished with code: " + exitcode);
  process.exit(0);
});