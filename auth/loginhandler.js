window.loginState = false;

let forcelogin = document.currentScript.dataset.forcelogin === "false" ? false : !!document.currentScript.dataset.forcelogin;
console.log(forcelogin);

window.addEventListener("load", function() {
  const iframe = document.createElement('iframe');
  //iframe.style.display = "none";
  iframe.src = "http://localhost:11322/auth";
  document.body.appendChild(iframe);
  window.addEventListener ("message", e => {
    if(!["localhost", "3e8.ch", "loooping.ch", "gymburgdorf.ch"].some(o=>e.origin.includes(o))) return console.warn("not authorized!");
    if(e.data.state === undefined) return;
    window.loginState = localStorage.uid = e.data.state;
    if(e.data.state === false && forcelogin) {
      console.log("FL", forcelogin);
      location = "http://localhost:11322/login#" + encodeURI(location.href);
    }
    document.dispatchEvent(new CustomEvent("loginStateChange", {detail: {loginState: e.data.state}}));
  });
});

async function getUserData() {
  return new Promise((resolve, reject)=> {
    document.addEventListener("loginStateChange", e => {
      if(e.detail.loginState) {
        resolve(fetchUserData(e.detail.loginState));
      }
      else reject("notOnline");
    }, {once: true});
  });
}

// async function saveItem(item, value, uid=localStorage.uid) {
//   return await postCors("https://auth.3e8.ch/saveUserItem", {uid, item, value});
// }
//
// async function loadItem(item, uid = localStorage.uid) {
//   return await postCors("https://auth.3e8.ch/loadUserItem", {uid, item});
// }
//
// async function loadTemplate(item) {
//   return await postCors("https://auth.3e8.ch/loadTemplate", {item});
// }
//
// async function fetchUserData(uid = localStorage.uid) {
//   return await postCors("https://auth.3e8.ch/getUserData", {which: "basic", uid: uid});
// }

/**
 *
 * @param url
 * @param data
 * @returns {Promise}
 */
async function postCors(url, data) {
  return window.fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json, application/x-www-form-urlencoded, text/plain, text/json, */*',
      'Content-Type': 'application/x-www-form-urlencoded' // instead of application/json because of cors
    },
    body: JSON.stringify(data)
  })
    .then(checkStatus)
    .then(extractContent)
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response);
  } else {
    console.log(response);
    return Promise.reject(new Error("invalid status text: " + response.statusText))
  }
}

function extractContent(r) {
  const ct = r.headers.get("content-type");
  if(~ct.indexOf("json") || ~ct.indexOf("x-www-form-urlencoded")) return r.json();
  if(~ct.indexOf("text")) return r.text();
  return r;
}

//export {getUserData, saveItem, loadItem, loadTemplate};