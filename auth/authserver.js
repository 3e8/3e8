const http = require('http'),
  url = require('url'),
  path = require('path'),
  fs = require('fs'),
  b62 = require("../metamodules/base62"),
  crypto = require("crypto");
const {log, logError, warn, logger} = require("../my_modules/3e8-log");
const {writeFile, readFile, readDir} = require("prms-fs");

let GoogleAuth = require('google-auth-library');
let auth = new GoogleAuth;
let client = new auth.OAuth2("1027611627583-dd0ka88am0f4rkeaaa22jd822c8tnj83.apps.googleusercontent.com", '', '');
// wo braucht es den: client-key hf2IgAzy8bzn2CGSfKTGI9r_

const PORT = 11322; //require("../entrance/serversettings").auth.internalport;

async function startServer() {
  let app = http.createServer(async (req, res)=>{
    if(req.url === "/checktoken") {
      let body = '';
      req.on('data', data => body += data);
      req.on('end', _=>{
        client.verifyIdToken(
          body,
          "1027611627583-dd0ka88am0f4rkeaaa22jd822c8tnj83.apps.googleusercontent.com", //or client_id array of client_ids
          async function(e, login) {
            if (e) return logError(e);
            let payload = login.getPayload();
            console.log(payload);
            let {sub: id, email, name, hd: domain, picture} = payload;
            let user = email.replace("@gymburgdorf.ch", "").toLowerCase().replace(/\./g, "_");
            //let uid = encode(user);
            // if (user && fs.existsSync(`users/${user}`)) {
            //   await writeFile(`users/${user}/lastlogin.txt`, uid.split("*")[0]);
            // }
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(email);
          }
        );
      });
    }
    else if(req.url === "/checkuid") {
      res.writeHead(200, {'Content-Type': 'text/html'});
      let body = '';
      req.on('data', data => body += data);
      req.on('end', _=>{
        res.end(decodeUid(body).valid ? "1" : "0");
      });
    }
    else if(req.url === "/getGymburgdorfUser") {
      try {
        let data = await parseJsonBody(req);
        log(data);
        let user = getUserFromUid(data.uid);
        res.end(user);
      } catch(e) {logError(e);}
    }
    else if(req.url === "/getUserData") {
      try {
        let data = await parseJsonBody(req);
        let user = getUserFromUid(data.uid);
        respondWithJson(res, user ? await getBasicData(user) : {error: "user not found"}, req.headers.origin);
      } catch(e) {logError(e);}
    }
    else if(req.url === "/loadUserItem") {
      try {
        let data = await parseJsonBody(req);
        let user = getUserFromUid(data.uid);
        log(data);
        respondWithJson(res, user ? await loadUserItem(user, data.item) : {error: "user not found"}, req.headers.origin);
      } catch(e) {
        if(e.code === "ENOENT") {
          respondWithJson(res, {error: "item not found"}, req.headers.origin);
        }
        else {
          logError(e);
        }
      }
    }
    else if(req.url === "/loadTemplate") {
      try {
        let data = await parseJsonBody(req);
        respondWithJson(res, await loadTemplate(data.item), req.headers.origin);
      } catch(e) {logError(e);}
    }
    else if(req.url === "/saveUserItem") {
      try {
        let data = await parseJsonBody(req);
        let user = getUserFromUid(data.uid);
        respondWithJson(res, user ? await saveUserItem(user, data) : {error: "user or item not found"}, req.headers.origin);
      } catch(e) {logError(e);}
    }
    else if(["/auth", "/z_auth", "/login", "/z_login", "/softlogin", "/example", "/exampleMayLogin", "/requireLogin.js", "/mayLogin.js"].some(p=>req.url.startsWith(p)) ) {
      let [pathWithoutQuery, query] = req.url.split("?");
      let path = pathWithoutQuery.includes(".") ? pathWithoutQuery.slice(1) : (pathWithoutQuery.slice(1) + ".html");
      fs.readFile(path, function(err, data) {
        if(err) {
          res.writeHead(404, {'Content-type': 'text/plain'});
          res.write('Page Was Not Found');
          res.end();
        }
        else {
          res.writeHead(200, {
            'Content-type': 'text/' + (path.endsWith(".js") ? "javascript" : "html"),
            'Content-Security-Policy': "frame-ancestors *.gymburgdorf.ch *.loooping.ch loooping.ch 3e8.ch *.3e8.ch http://localhost:* http://*.localhost:* http://*.localhost.tv"
          });
          res.write(data);
          res.end();
        }
      });
    }
    else res.end("not found");
  });

  app.listen(PORT, function () {
    log('ZAuth Server listening on port ' + PORT);
  });
}

startServer();

function hash(data) {
  return crypto.createHash("sha256").update(data+"salzindersuppe").digest("base64").slice(0,42);
}

function encode(user, now = b62.dateToB62()) {
  return [now, encrypt(user), hash(user+now)].join("*");
}

function encrypt(text){
  var cipher = crypto.createCipher('aes-256-cbc',"h594b9zt84m985z94h7698ncZBU");
  return cipher.update(text,'utf8','hex') + cipher.final('hex');
}

function decrypt(text){
  var decipher = crypto.createDecipher('aes-256-cbc',"h594b9zt84m985z94h7698ncZBU");
  return decipher.update(text,'hex','utf8') + decipher.final('utf8');
}

function decodeUid(uid) {
  let [b62date, encrypteduser, hashed] = uid.split("*");
  if(!encrypteduser) log(uid);
  let user = decrypt(encrypteduser);
  return {date: b62.b62ToDate(b62date), user, valid: hash(user+b62date) === hashed};
}

function getUserFromUid(uid) {
  let {date, user, valid} = decodeUid(uid);
  return valid && user;
}

async function getBasicData(user) {
  let profile = await parseJsonFile(`./users/${user}/profile.json`);
  let lastlogin = await readFile(`./users/${user}/lastlogin.txt`, 'utf8');
  return Object.assign(profile, {user, lastlogin});
}

async function loadUserItem(user, item) {
  return await parseJsonFile(`./users/${user}/${item}.json`);
}

async function loadTemplate(item) {
  let raw = await parseJsonFile(`./users/diskstation/${item}.json`)
  if(raw.code) {
    raw.code = raw.code.replace(/\/\/\s*only\s*solution\s*start[\s\S]*?\/\/\s*only\s*solution\s*end/ig, "");
    raw.code = raw.code.replace(/\/\/\s*not\s*solution\s*start.*/ig, "").replace(/\/\/\s*not\s*solution\s*end.*/ig, "");
  }
  return raw;
}

async function saveUserItem(user, {item, value}) {
  try {
    await writeFile(`./users/${user}/${item}.json`, JSON.stringify(value));
    return {ok: true};
  }
  catch(e) {logError(e); return e;}
}

async function parseJsonFile(path) {
  try {
    return JSON.parse(await readFile(path, "UTF-8"));
  } catch(e) {throw(e);}
}

/**
 * datastream to json
 * @param req
 * @returns {Promise}
 */
function parseJsonBody(req) {
  return new Promise((resolve, reject)=>{
    var body = [];
    req.on('data', function(chunk) {
      body.push(chunk);
    }).on('end', function() {
      let bodystr = Buffer.concat(body).toString();
      try {
        resolve(JSON.parse(bodystr || "{}"));
      }
      catch(e) {
        logError(e);
        reject(e);
        process.exit(1);
      }
    });
  });
}

/**
 *
 * @param res
 * @param jsonobj
 * @param origin req.headers.origin
 */
function respondWithJson(res, jsonobj, origin = "*") {
  if(checkOrigin(origin)) {
    res.writeHead(200, {"Content-Type": 'application/x-www-form-urlencoded; charset=UTF-8', 'Access-Control-Allow-Origin': origin});
    res.write(JSON.stringify(jsonobj));
    res.end();
  }
  else {
    res.writeHead(403, {"Content-Type": 'application/x-www-form-urlencoded; charset=UTF-8', 'Access-Control-Allow-Origin': "*"});
    res.end('{"answer": "not allowed!"}');
  }
}

function checkOrigin(origin) {
  return ["gymburgdorf.ch", "localhost", "loooping.ch", "3e8.ch", "looopy.noip.me", "phy.hopto.org"].some(o=>origin.includes(o));
}