/**
 *
 * @param {number} integer
 * @param {number||Boolean} zeropad - Defaults to false
 * @returns {string}
 */
function intToB36(integer, zeropad=false){
  var s = integer.toString(36);
  return zeropad ? ("0000000000000000000000000000000"+s).slice(-zeropad) : s;
}

/**
 *
 * @param {string} base36String
 * @returns {Number}
 */
function b36ToInt(base36String) {
  return parseInt(base36String, 36);
}

/**
 *
 * @param {Date} date
 * @returns {String}
 */
function dateToB36(date = new Date()) {
  return intToB36(date.getTime(), 8 ); /*reicht bis ins Jahr 2059*/
}

/**
 *
 * @param {string} base36String
 * @returns {Date}
 */
function b36ToDate(base36String) {
  return new Date(b36ToInt(base36String));
}

module.exports = {intToB36, b36ToInt, dateToB36, b36ToDate};