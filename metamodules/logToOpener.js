function logError(err) {
  let {message, stack} = err;
  console.log(err);
  if(process.send) process.send({error: true, message, stack});
}

function log(message) {
  console.log(message);
  if(process.send) process.send({log: true, message});
}

module.exports = {logError, log};