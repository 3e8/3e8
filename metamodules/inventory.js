const url = require('url');
const fs = require('fs');
const path = require('path');

var {log, logError} = require("../metamodules/logToOpener.js");
let inventory = (root=".") => {
  return function(req, res) {
    const uri = url.parse(req.originalUrl).pathname;
    if(uri.indexOf("/inventory/") !== 0) {
      log("ERROR inventory")
    }
    fs.readdir(path.join(root, uri.slice("/inventory/".length)), (err, files)=>{
      if(err) return logError(err);
      res.json(files);
    });
  };
};

module.exports = {inventory};