var {log, logError} = require("./logToOpener.js");

process.on("uncaughtException", err => {
  logError(err);
  throw err;
});

process.on('unhandledRejection', (reason, p) => {
  logError(new Error('Unhandled Rejection: ' + JSON.stringify(reason)));
});