var fs = require("fs");
var path = require("path");
var {dateToB36} = require("../metamodules/base36");

function createLogger(folder, type="log") {
  return function(text, t=type) {
    fs.writeFile(path.join(folder, dateToB36()+"_"+t+".log"), text, err => {});
  }
}

function createErrorLogger(folder, type="error") {
  return function(error, t=type) {
    let {message, stack} = error;
    fs.writeFile(path.join(folder, dateToB36()+"_"+t+".log"), message+"\n"+stack, err => {});
  }
}

module.exports = {createLogger, createErrorLogger};