//neu schon direkt in requireLogin enthalten

import {postCors} from "./ajaxhelpers";

async function getUserData() {
  return new Promise((resolve, reject)=> {
    document.addEventListener("loginStateChange", e => {
      if(e.detail.loginState) {
        resolve(fetchUserData(e.detail.loginState));
      }
      else reject("notOnline");
    }, {once: true});
  });
}

async function saveItem(item, value, uid=localStorage.uid) {
  return await postCors("https://auth.3e8.ch/saveUserItem", {uid, item, value});
}

async function loadItem(item, uid = localStorage.uid) {
  return await postCors("https://auth.3e8.ch/loadUserItem", {uid, item});
}

async function loadTemplate(item) {
  return await postCors("https://auth.3e8.ch/loadTemplate", {item});
}

async function fetchUserData(uid = localStorage.uid) {
  return await postCors("https://auth.3e8.ch/getUserData", {which: "basic", uid: uid});
}

export {getUserData, saveItem, loadItem, loadTemplate};