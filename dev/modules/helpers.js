const _ = {};

/**
 *
 * @param array
 * @returns {*}
 */
_.last = function(array) {return array[array.length-1];};

_.log = console.log.bind(console);
_.warn = console.warn.bind(console);
_.newP = _=>Promise.resolve();

if(window) Object.assign(window, _);

module.exports = _;

//(function () {
//  var noop = function () { };
//  var methods = [
//    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
//    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
//    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
//    'timeStamp', 'trace', 'warn'
//  ];
//  var console = (window.console = window.console || {});
//  for(var i = methods.length - 1; i>=0; i--) {
//    console[methods[i]] = console[methods[i]] || noop; // Only stub undefined methods.
//  }
//
//  if (Function.prototype.bind && 0) {
//    window.log = Function.prototype.bind.call(console.log, console);
//  }
//  else {
//    window.log = function() {
//      if(!$(".log").length) $("body").append("<div class='log'></div>");
//      $(".log").empty().append("<p>"+arguments[0]+"</p>");
//      Function.prototype.apply.call(console.log, console, arguments);
//    };
//  }
//})();

/**

var self = function(s) {log(s); return s;}

function replaceUml(a) {
  if(typeof a !== "string") return a;
  a = a.toLowerCase();
  a = a.replace("ä", "a");
  a = a.replace("ö", "o");
  a = a.replace("ü", "u");
  a = a.replace("à ", "a");
  a = a.replace("é", "e");
  a = a.replace("è", "e");
  a = a.replace("ë", "e");
  a = a.replace("ï", "i");
  a = a.replace("ñ", "n");
  return a;
}

function htmlEscape(str) {
  return String(str)
  .replace(/&/g, '&amp;')
  .replace(/"/g, '&quot;')
  .replace(/'/g, '&#39;')
  .replace(/</g, '&lt;')
  .replace(/>/g, '&gt;');
}

function htmlUnescape(value){
  return String(value)
  .replace(/&quot;/g, '"')
  .replace(/&#39;/g, "'")
  .replace(/&lt;/g, '<')
  .replace(/&gt;/g, '>')
  .replace(/&amp;/g, '&');
}

function areShallowEqual(a, b) {
  return !Object.keys(a).concat(Object.keys(b)).some(k=> !(k in a) || !(k in b) || a[k] !== b[k]);
}

/*Lodash
array
    .diff
    .chunk
    .fill(value, [start=0], [end=array.length])
    .last
    .first
    .flatten([isDeep])
    .union
    .unique
    .groupBy([4.2, 6.1, 6.4], function(n) {return Math.floor(n);}); // → { '4': [4.2], '6': [6.1, 6.4] }
    .pluck(path)
    .sample([n=1]) // n elemente per Zufall auswählen
    .shuffle()
    .sortByAll
    .sortByOrder
    .curry
    .
    

// aliasing important functions into global namespace
var sin     = Math.sin;
var cos     = Math.cos;
var tan     = Math.tan;
var arctan  = Math.atan;
var atan2   = Math.atan2;
var arcsin  = Math.asin;
var arccos  = Math.acos;

var exp     = Math.exp;
var ln      = Math.log;
var log10   = Math.log10 || function(x) {return Math.log(x) / Math.LN10;}
var pow     = Math.pow;
var sqrt    = Math.sqrt;
var cbrt    = Math.cbrt;

var abs     = Math.abs;
var round   = Math.round;
var ceil    = Math.ceil;
var floor   = Math.floor;
var max     = Math.max;
var min     = Math.min;

var sign    = Math.sign || function(x) {if(typeof x !== "number") {return Math.NaN;} return x>0?1:x<0?-1:0;};

var rand    = Math.random;

var hypot   = Math.hypot || function(a,b) {return sqrt(a*a+b*b);};
var cath    = function(c,b) {return sqrt(c*c-b*b);};

var dist    = function(p1, p2) {hypot(p2.x-p1.x, p2.y-p1.y);};

var constrain = function(value, min, max) {
  if(value < Math.min(min, max)) return Math.min(min, max);
  if(value > Math.max(min, max)) return Math.max(min, max);
  return value;
};

//CONSTANT NUMBERS
var PI      = Math.PI;
var E       = Math.E;
var SQRT1_2 = Math.SQRT1_2;
var SQRT2   = Math.SQRT2;
var LN2     = Math.LN2;

var cblog = function(msg) {return function(data) {log(msg, data);};};

var g = 9.81;
var g_round = 10;
var euler = Math.E;
var pi = Math.PI;
var q_e = 1.6e-19;
var G = 6.67e-11;
var c = 299792458;
var c_round = 3e8;

var globalConstants = {
  g : {val:  9.81, unit: "m/s^2"},
  g_round : {val:  10, unit: "m/s^2"},
  euler : {val:  2.71, unit: false},
  pi : {val:  3.1415926535, unit: false},
  q_e : {val:  1.6e-19, unit: "C"},
  G : {val:  6.67e-11, unit: "Nm^2/kg^2"},
  c : {val:  299792458, unit: "m/s"},
  c_round : {val:  3e8, unit: "m/s"},
};

var newP = ()=>Promise.resolve(1); //better implement it with a real Promise Request instead of jQuery.post

var pointerX = e => e.pageX || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).pageX;
var pointerY = e => e.pageY || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).pageY;

var pointerClientX = e => e.clientX || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).clientX;
var pointerClientY = e => e.clientY || (e.originalEvent.touches[0] || e.originalEvent.changedTouches[0]).clientY;

window.onload=function() {
  if(location.host.indexOf("localhost") !== 0) return;
  var scripts = document.getElementsByTagName("script");
  var scriptlengths = new Map();
  var ct = 'application/json';
  var h = {"Accept": ct, "Content-Type": ct};
  for (let i=0;i<scripts.length;i++) {
    if (scripts[i].src) {
      window.fetch("dev/pfdc111", {method: 'POST', headers: h, body: JSON.stringify({file: scripts[i].src})})
      .then(r=>r.text()).then(r=>{scriptlengths.set(scripts[i].src, r)});
      setInterval(function() {
        if(document.hidden) return;
        window.fetch("dev/pfdc111", {method: 'POST', headers: h, body: JSON.stringify({file: scripts[i].src})})
        .then(r=>r.text()).then(r=>{if(scriptlengths.get(scripts[i].src) !== r) location.reload();});
      }, 2000);
    }
  }
};*/