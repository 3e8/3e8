function on(elSelector, eventName, selector, fn) {
  let elements = document.querySelectorAll(elSelector);
  
  for (let i = 0; i < elements.length; ++i) {
    let element = elements[i];
    element.addEventListener(eventName, function(event) {
      let possibleTargets = element.querySelectorAll(selector);
      let target = event.target;
    
      for(let j = 0, l = possibleTargets.length; j < l; j++) {
        let el = target;
        let p = possibleTargets[j];
        while(el && el !== element) {
          if(el === p) {
            return fn.call(p, event);
          }
          el = el.parentNode;
        }
      }
    });
  }
}

function getPosition(el) {
  let xPos = 0;
  let yPos = 0;
  
  while (el) {
    if (el.tagName === "BODY") {
      // deal with browser quirks with body/window/document and page scroll
      var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
      var yScroll = el.scrollTop || document.documentElement.scrollTop;
      
      xPos += (el.offsetLeft - xScroll + el.clientLeft);
      yPos += (el.offsetTop - yScroll + el.clientTop);
    } else {
      // for all other non-BODY elements
      xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
      yPos += (el.offsetTop - el.scrollTop + el.clientTop);
    }
    
    el = el.offsetParent;
  }
  return {
    x: xPos,
    y: yPos
  };
}

/**
 * @param {String} html representing a single element
 * @return {Node}
 */
function makeNode(html) {
  var template = document.createElement('template');
  template.innerHTML = html;
  return template.content.firstChild;
}

/**
 * @param {String} html representing any number of sibling elements
 * @return {NodeList}
 */
function makeNodes(html) {
  var template = document.createElement('template');
  template.innerHTML = html;
  return template.content.childNodes;
}

/**
 *
 * @param {Element} node
 * @returns {Element}
 */
function empty(node) {
  node.innerHTML = "";
  return node;
}


export {on, getPosition, makeNode, makeNodes, empty};