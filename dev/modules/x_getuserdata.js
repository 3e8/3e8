import {post} from "./ajaxhelpers";

async function getUserData() {
  return new Promise((resolve, reject)=> {
    document.addEventListener("loginStateChange", e => {
      if(e.detail.loginState) {
        resolve(fetchUserData(e.detail.loginState));
      }
      else reject("notOnline");
    }, {once: true});
  });
}

async function fetchUserData(uid = localStorage.uid) {
  console.log(uid);
  return await post("https://log.loooping.ch/getUserData", {which: "basic", uid: uid});
}

export {getUserData};