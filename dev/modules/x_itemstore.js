//var Immutable = require("Immutable");
//var store = Immutable.fromJS({a:1, b: [1,2,3]});

import {post} from "modules/ajaxhelpers";
import {msg} from "modules/messages";

var defaultSettings = {
  dbcentral: "db",
  dbname: "test",
  leavesonly: false,
  longpolling: true,
  keysByType: {}
};

var defaultGlobal = {
  globalHistory: {show: false, nowline: null, undoneIdvs: [], redoneIdvs: [], maxItems: 10}
};

class Store {
  constructor(name, settings = {}) {
    this.name = name;

    this.settings = Object.assign({}, defaultSettings, settings);
    this.namedStates = {};
    this.namedStates.global = Object.assign({}, defaultGlobal, JSON.parse(localStorage[this.name + "_state_global"] || "{}")); //loadStates;
    this.dbStates = {};

    this.createDispatchers();

    this.loadItems();
    this.ready = false;
    this.fromUndo = false;

    this.channels = {}; //ready, stateChange, dbChange, instantchange, ...
  }
  subscribe(channel, target, subchannel = "", methodname) {
    if(!this.channels[channel]) this.channels[channel] = [];
    let mn = methodname || ("on"+channel.slice(0,1).toUpperCase()+channel.slice(1));
    var listener = {target, subchannel, methodname: mn};
    this.channels[channel].push(listener);
    return listener;
  }
  publish(channel, msg, subchannel = "") {
    if(!this.channels[channel]) return;
    this.channels[channel].forEach(listener => {
      if(listener.target && listener.subchannel == subchannel) listener.target[listener.methodname](msg);
    });
  }
  unsubscribe(targetOrListener) {
    for(let i in this.channels) {
      this.channels[i] = this.channels[i].filter(l => l.target !== targetOrListener && l !== targetOrListener);
    }
  }
  loadItems() {
    return post(this.settings.dbcentral, {dbname: this.settings.dbname, task: this.settings.leavesonly ? "getLeaves" : "getAll"})
    .then(o=>{
      this.handleItems.init();
      this.handleItems.addItems(Object.keys(o).map(idv => (new Item(idv, JSON.parse(o[idv])) )));
      this.ready = true;
      //use dispatcher?
      this.settings.keysByType = Object.assign(this.settings.keysByType, this.getKeysByType(this.getLeavesAsArray()));
      this.publish("ready");
      document.body.addEventListener("keydown", e => {
        if(e.which == 90 && e.shiftKey && e.ctrlKey) {
          this.dNs.toggle("global", "globalHistory.show");
        }
      });
      //var idn = this.addItem("test", {val:1, b: 0});
    })
    .then(()=>this.settings.longpolling ? this.poll() : false)
    //.catch(e=>warn(e));
  }
  poll() {
    if(document.hidden) {
      setTimeout(()=>this.poll(), 1000);
    }
    else {
      log("polling...");
      post(this.settings.dbcentral, {dbname: this.settings.dbname, task: "pollForChanges", since: this.dbStates.latest}).then(r=>{
        if(!this.willunmount) {
          if(r == "nochanges") {
            log("nochanges");
          }
          else {
            var external = Object.keys(r).filter(newv => this.dbStates.versions.filter(v=>v.idv == newv).length==0);
            let newVersions = external.map(idv => (new Item(idv, JSON.parse(r[idv])) ));
            if(newVersions.length) {
              this.dDb.gotRemoteItems(newVersions);
            }
          }
          setTimeout(()=>this.poll(), 1000);
        }
      }).catch(e=>{warn(e); setTimeout(()=>this.poll(), 1000);});
    }
  }
  moveOldToArchive() {
    post(this.settings.dbcentral, {dbname: this.settings.dbname, task: "moveOldToArchive"}).then(r=>log(r));
  }
  createDispatchers() {
    this.dispatchSettings = {
      //this.eventEmitter.dispatchEvent(new CustomEvent("ready", {detail: "ready"}));
    };
    this.dispatchNamed = {
      mountShelf: (shelfname, values) => {
        //loadStates
        var shelfstate = Object.assign({}, values, JSON.parse(localStorage[this.name+"_state_"+shelfname] || "{}")); //loadStates
        this.namedStates[shelfname] = shelfstate;
      },
      changeShelfStates: (shelfname, newStates) => {
        this.namedStates[shelfname] = Object.assign({},  this.namedStates[shelfname], newStates);
      },
      setShelfStateOfIdn: (shelfname, itemstate, idn, value) => {
        let oldItemstate = this.namedStates[shelfname][itemstate];
        let newItemstate = value === true ? oldItemstate.addUnique(idn) : oldItemstate.deleteWhere(i=>i == idn);
        this.namedStates[shelfname] = Object.assign({},  this.namedStates[shelfname], {[itemstate]: newItemstate});
      },
      toggle: (shelfname, pathArrayOrString) => {
        this.dispatchNamed.setValue(shelfname, pathArrayOrString, null, true);
      },
      setValue: (shelfname, pathArrayOrString, val, toggle=false) => {
        var old = this.namedStates[shelfname];
        this.namedStates[shelfname] = Object.assign({}, this.namedStates[shelfname]);
        let pathToVariable = this.namedStates[shelfname];
        let patharray = pathArrayOrString instanceof Array ? pathArrayOrString : pathArrayOrString.split(".");
        patharray.slice(0,-1).forEach(p => {
          pathToVariable = Object.assign({}, pathToVariable);
          pathToVariable = pathToVariable[p];
        });
        let nameOfVariable =  patharray.slice(-1)[0];
        pathToVariable[nameOfVariable] = toggle ? !pathToVariable[nameOfVariable] : val;
      }
    };
    this.dispatchDb = {
      addItem: (itemtype, newDoc) => {
        var idn = itemtype + "-" + this.getTimestamp() + "-" + this.getUsr();
        var idv = idn + "-" + this.lastTimestamp + "-" + this.getUsr() + ".json";
        this.dispatchDb.addVersions([idv], [newDoc], [""], [""]);
        return idn;
      },
      debouncedUpdate: (idn, obj) => {
        if(Object.keys(obj).length !== 1) throw new Error("Debounced update must only change one key!");
        var key = Object.keys(obj)[0];
        var val = obj[key];
        var item = this.dbStates.leaves[idn];
        var getTimeout = () => setTimeout(()=>{this.dDb.dispatchDebouncedUpdate(idn)}, 2000);
        if(item.debounced) {
          if(item.debounced.key === key) {
            clearTimeout(item.debounced.timeout);
            this.handleItems.updateDebouncedIdn(idn, {val, timeout: getTimeout()});
          }
          else {
            setTimeout(()=> {
              this.dDb.dispatchDebouncedUpdate(idn);
              this.dispatchDb.debouncedUpdate(idn, {[key]: val});
            }, 0);
          }
        }
        else {
          this.handleItems.updateDebouncedIdn(idn, {key, val, originalval: item[key], timeout: getTimeout()});
        }
      },
      dispatchDebouncedUpdate: (idn) => {
        let item = this.dbStates.leaves[idn];
        clearTimeout(item.debounced.timeout);
        var {key, val, originalval} = item.debounced;
        this.handleItems.updateDebouncedIdn(idn, "delete");
        if(JSON.stringify(originalval) == JSON.stringify(val)) {
          return log("changed back to original value, no update");
        }
        return this.dispatchDb.updateItem(idn, {[key]: val});
      },
      //Single item
      deleteItem:   (idn) => this.dispatchDb.deleteItems([idn]),
      undeleteItem: (idn) => this.dispatchDb.undeleteItems([idn]),
      updateItem: (idn, obj) => this.dispatchDb.updateItems([idn], [obj]),
      replaceItem: (idn, newDoc) => this.dispatchDb.replaceItems([idn], [newDoc]),
      //Array of items
      deleteItems:   (idns) => this.dispatchDb.replaceItems(idns, false, "delete"),
      undeleteItems: (idns) => this.dispatchDb.replaceItems(idns, false, "undelete"),
      updateItems: (idns, objs) => this.dispatchDb.replaceItems(idns, objs, "updateKeys"),
      replaceItems: (idns, newDocsOrObjs, task = "update") => {
        let prevItems = idns.map(idn => this.dbStates.leaves[idn]);
        if(prevItems.some(i=>i.debounced)) {
          return Promise.all(
            prevItems.filter(i=>i.debounced).map(i=>this.dispatchDb.dispatchDebouncedUpdate(i.idn))
          ).then(_=>this.dispatchDb.replaceItems(idns, newDocsOrObjs, task));
        }
        let timestamp = this.getTimestamp();
        let prevIdvs = prevItems.map(item => item.idv);  // ?? var versionsToChange = action.previousIdvs.map(idv=>n.versions.pick({idv}));
        let prevDocs = prevItems.map(item => item.doc);  // ?? var prevDocs = versionsToChange.map(v=>v.doc);
        let newDocs = newDocsOrObjs;
        if(!newDocs) newDocs = prevDocs;
        if(task == "updateKeys") newDocs = newDocsOrObjs.map((obj, index) => Object.assign({}, this.dbStates.leaves[idns[index]].doc, obj));
        if(task!=="undelete" && task!=="delete") {
          if(newDocs.every((doc, i)=>JSON.stringify(doc)==JSON.stringify(prevDocs[i]))) {
            log("no items changed");
            return "no items changed"
          }
        }
        let getNewFlags = (olditem, task) => {
          var newFlags = olditem.flags;
          if(task=="delete") newFlags = ~newFlags.indexOf("x") ? newFlags : "x"+newFlags;
          if(task=="undelete") newFlags = newFlags.replace(/x/ig, "");
         return newFlags ? "-"+newFlags : "";
        };
        let idvs = idns.map( (idn, index) => idn+ "-" + timestamp + "-" + this.getUsr() + getNewFlags(prevItems[index], task) + ".json");
        return this.dispatchDb.addVersions(idvs, newDocs, prevIdvs, prevDocs);
      },
      addVersions: (idvs, newDocs, prevIdvs, prevDocs, fromLocal=true) => {
        if(!fromLocal) {alert("Noch nicht implementiert: MultiUser")} //@TODO
        idvs.forEach((idv, index) => {
          var item = new Item(idv, newDocs[index]);
          var idn = item.idn;
          this.handleItems.addItems([item]);
          //this.handleItems.addValueToArray(idn, "dirtyIdns");
          this.handleItems.addValueToArray(idv, "pendingIdvs");
        });
        //UndoRedo
        this.dNs.setValue("global", "globalHistory", Object.assign({}, this.namedStates.global.globalHistory, {
          nowline: this.fromUndo ? this.namedStates.global.globalHistory.nowline || this.lastTimestamp : "",
          undoneIdvs: this.namedStates.global.globalHistory.undoneIdvs.concat(this.fromUndo ? [this.fromUndo] : []), //this.fromUndo ? this.namedStates.global.globalHistory.undoneIdvs.concat([this.fromUndo]||[]) : [],
          redoneIdvs: this.fromUndo ? this.namedStates.global.globalHistory.redoneIdvs.concat(this.fromRedo ? idvs : []) : [],
        }));
        return this.dispatchDb.sendVersionsToDb(idvs, newDocs, prevIdvs, prevDocs);
      },
      sendVersionsToDb: (newIdvs, newDocs, prevIdvs, prevDocs) => {
        return this.dbStates.updateQueue.then(r=>post(this.settings.dbcentral,
            {dbname: this.settings.dbname, task: "addUpdatedVersions", idvs: newIdvs, previousIdvs: prevIdvs, data: newDocs.map(newDoc=>JSON.stringify(newDoc))}
        ).then(r=>this.dDb.dbResponse(r, newIdvs, newDocs, prevIdvs, prevDocs))); //{type: "dbResponse", r, idvs: newIdvs, docs: newIdvs.map(idv=>n.versions.pick({idv}).doc), prevDocs})});
      },
      dbResponse: (r, newIdvs, newDocs, prevIdvs, prevDocs) => {
        if(r === "updates ok!") {
          newIdvs.forEach(idv => {this.handleItems.removeValueFromArray(idv, "pendingIdvs")} );
          return newIdvs;
        }
        else if(r.indexOf("Konflikte mit Eintrag von anderswo:") === 0) { //@TODO
          console.groupCollapsed("Konflikte mit Eintrag von anderswo:");
          var conflictingWrap = JSON.parse(r.slice("Konflikte mit Eintrag von anderswo:".length));
          var newDocsNextTry = conflictingWrap.map((c, index) => {
            this.handleItems.removeItems([this.dbStates.versions.find(i=>i.idv==newIdvs[index])]); //delete also the pending even if no error
            if(!c) {
              return newDocs[index];
            }
            var conflicting = Object.keys(c)[0];
            var confl = JSON.parse(c[conflicting]);
            var conflictingItem = new Item(conflicting, confl);
            this.handleItems.addItems([conflictingItem]);
            var prev = prevDocs[index].clone();
            var self = newDocs[index].clone();
            var merged = prev.clone();
            var errors = [];
            for(let key in confl) {
              if(self[key]===undefined && prev[key]===undefined)      {merged[key] = confl[key]; log(`Conflicting added new key ${key}`);}
              if(self[key] == prev[key] && prev[key] !== confl[key])  {merged[key] = confl[key]; log(`Conflicting changed key ${key}, self didn't.`);}
              if(self[key] !== prev[key] && self[key] === confl[key]) {merged[key] = confl[key]; log(`Both changed key ${key} to same value.`);}
              if(self[key] == prev[key] && prev[key] === confl[key])  {merged[key] = confl[key]; log(`Both did not change key ${key}.`);}
            }
            for(let key in self) {
              if(confl[key]===undefined && prev[key]===undefined)     {merged[key] = self[key];  log(`Self added new key ${key}`);}
              if(confl[key] == prev[key] && prev[key] !== self[key])  {merged[key] = self[key];  log(`Self changed key ${key}, conflicting didn't.`);}
              if(confl[key] !== prev[key] && prev[key] !== self[key] && confl[key] !== self[key]) {merged = false; errors.push({key, self: self[key], confl: confl[key]}); log(`Both changes key ${key} to different value.`); break;}
            }
            if(merged!==false) {
              log("item was merged with conflicting item:", merged);
              console.groupEnd();
              return merged;
            }
            else {
              console.groupEnd();
              msg("<h3>Aktion konnte nicht abgeschlossen werden!</h3><div>Eine andere Quelle hat den Eintrag ebenfalls geändert.</div>"
                  + errors.map(e=>`<div>Ihr Vorschlag für ${e.key}:</div><div>${e.self}</div><div>Externer Vorschlag:</div><div>${e.confl}</div>`)
              + "<div>Bitte beheben Sie den Konflikt.</div>");
              return "error";
            }
          });
          if(!newDocsNextTry.some(e=>e=="error")) {
            return this.dispatchDb.replaceItems(newIdvs.map(Item.getIdnFromIdv), newDocsNextTry);
          }
          else return false;
        }
      },
      gotRemoteItems(newVersions) {
        newVersions.forEach(item => {
          if(this.dbStates.leaves[item.idn].debounced) warn("check what happens if remote updates debounced");
          this.handleItems.addItems([item]);
        });
      }
    };
    this.dDb = {};
    for(let key in this.dispatchDb) {
      this.dDb[key] = (...args) => {
        console.groupCollapsed(key);
        log(args);
        this.fromUndo = args.some(a=>String(a).slice(0,8)=="fromUndo") ? args.find(a=>String(a).slice(0,8)=="fromUndo").split("=")[1] : false; //UndoRedo
        this.fromRedo = args.includes("redo");
        setTimeout(()=>{
          this.publish("dbChange", {action: key});
          console.groupEnd();
        }, 0);
        return this.dispatchDb[key].apply(this, args);
      }
    }
    this.dNs = {};
    for(let key in this.dispatchNamed) {
      this.dNs[key] = (...args) => {
        console.groupCollapsed(key);
        log(args);
        var shelfname = args[0];
        let old = this.namedStates[shelfname] || {};
        setTimeout(()=>{
          //save states
          localStorage[this.name+"_state_"+shelfname] = JSON.stringify(this.namedStates[shelfname] || {});
          this.publish("stateChange", {action: key, old, current: this.namedStates[shelfname]}, shelfname);
          console.groupEnd();
        }, 0);
        return this.dispatchNamed[key].apply(this, args);
      }
    }
    // Helpers to make dbStates Immutable
    this.handleItems = {
      init: () => {
        this.dbStates = {
          versions: [],
          versiongroups: {},
          leaves: {},
          latest: "0",
          pendingIdvs: [],
          updateQueue: newP()
        }
      },
      addItems: (items)=> {
        var a = this.dbStates;
        var dbs = this.dbStates = Object.assign({}, this.dbStates);
        items.forEach(i=> {
          let pos = dbs.versions.findIndex(v=>v.modified <= i.modified);
          pos = pos == -1 ? dbs.versions.length : pos;
          dbs.versions = [...dbs.versions.slice(0, pos), i, ...dbs.versions.slice(pos)];
          dbs.versiongroups = Object.assign({}, dbs.versiongroups);
          dbs.versiongroups[i.idn] = [i.idv].concat(dbs.versiongroups[i.idn]||[]);
          dbs.versiongroups[i.idn].sort((a,b)=>a>b?-1:a<b?+1:0);
          dbs.leaves = Object.assign({}, dbs.leaves, {[i.idn]: dbs.versions.find(v=>v.idn == i.idn)});
          dbs.latest = dbs.versions.length ? dbs.versions[0].modified : "0";
        });
      },
      removeItems: (items)=> {
        var dbs = this.dbStates = Object.assign({}, this.dbStates);
        items.forEach(i=> {
          dbs.versions = dbs.versions.filter(v=>v.idv !== i.idv);
          dbs.versiongroups = Object.assign({}, dbs.versiongroups);
          dbs.leaves = Object.assign({}, dbs.leaves);
          if(dbs.versiongroups[i.idn].length ==1) {
            delete dbs.versiongroups[i.idn];
            delete dbs.leaves[i.idn];
          }
          else {
            dbs.versiongroups[i.idn] = dbs.versiongroups[i.idn].filter(v=>v!==i.idv);
            dbs.leaves[i.idn] = dbs.versions.find(v=>v.idn == i.idn);
          }
          if(dbs.pendingIdvs.includes(i.idv)) {dbs.pendingIdvs = dbs.pendingIdvs.filter(idv=>idv!==i.idv);}
        });
      },
      updateDebouncedIdn: (idn, obj) =>{
        let item = this.dbStates.leaves[idn];
        let clonedItem = Object.assign(new Item(item.idv, item.doc), item);
        if(obj=="delete") {
          delete clonedItem.debounced;
        }
        else {
          clonedItem.debounced = Object.assign({}, clonedItem.debounced, obj);
        }
        this.handleItems.removeItems([item]);
        this.handleItems.addItems([clonedItem]);
        if(obj!=="delete") {
          this.publish("instantchange", {old: item, current: clonedItem}, clonedItem.idn);
        }
      },
      addValueToArray: (val, array) => {
        this.dbStates = Object.assign({}, this.dbStates, {[array]: this.dbStates[array].concat([val]).unique()});
      },
      removeValueFromArray: (val, array) => {
        this.dbStates = Object.assign({}, this.dbStates, {[array]: this.dbStates[array].filter(v=>v!==val)});
      }
    };
    this.isLeaf = v => this.dbStates.versiongroups[v.idn][0] == v.idv;
    this.getLeavesAsArray = () => this.dbStates.versions.filter(v=>!v.deleted && this.isLeaf(v));
    this.getTimestamp = (lastTimestamp) => {
      var time = new Date().getTime()+31536000000;
      var timestamp = time.toString(36);
      while(timestamp === this.lastTimestamp || timestamp <= this.dbStates.latest) {
        timestamp = (time+1).toString(36)
      }
      this.dbStates.latest = timestamp;
      this.lastTimestamp = timestamp;
      return timestamp;
    };
    this.getUsr = () => "l"; //@TODO
    this.getItem = (idn) => this.dbStates.leaves[idn];
    this.timestampToDate = (t) => new Date(parseInt(t, 36) - 31536000000);
  }
  getKeysByType(versions) {
    var keysByTypeFomProps = this.settings.keysByType || {};
    var keysByType = {};
    var types = versions.map(v=>v.type).unique();
    types.forEach(type=>{
      keysByTypeFomProps[type] = keysByTypeFomProps[type] || {};
      keysByType[type] = {};
      let itemsOfType = versions.filter(v=>v.type == type);
      let keys = Object.keys(keysByTypeFomProps[type]).concat(itemsOfType.map(v=>Object.keys(v.doc).join(",")).join(",").split(",")).unique(); //muss so kompliziert sein wegen Reihenfolge
      keys.forEach(key => {
        keysByTypeFomProps[type][key] = keysByTypeFomProps[type][key] || {};
        keysByType[type][key] = {
          key: key,
          isPublic: !~key.indexOf("_"),
          isNumeric: itemsOfType.map(v=>v.doc[key]).filter(x=>!!x).every(x=>parseFloat(x)==x)
        };
        keysByType[type][key] = Object.assign(keysByType[type][key], keysByTypeFomProps[type][key]);
      });
    });
    return keysByType;
  }

  //Tree methods
  getPathOf(item) {
    return item.tree_parent ? this.getPathOf(this.dbStates.leaves[item.tree_parent]).concat([item.idn]) : [item.idn];
  }
  sortByTreePos(a,b) { //MUSS WOHL IN SHELF!!!
    var p1 = this.getPathOf(a).map(idn=>this.dbStates.leaves[idn].tree_pos);
    var p2 = this.getPathOf(b).map(idn=>this.dbStates.leaves[idn].tree_pos);
    var i = 0;
    while(p1[i] === p2[i]) i++;
    log(a.nr, b.nr, p1, p2, (+p1[i]||0) > (+p2[i]||0) ? 1 : -1);
    return (+p1[i]||0) > (+p2[i]||0) ? 1 : -1;
  };
  findAll(relation, idnOrItem) {
    var item = typeof idnOrItem == "string" ? this.dbStates.leaves[idnOrItem] : idnOrItem;
    var candidates = this.getLeavesAsArray().filter(i=>i.type==item.type);
    var filterfunction;
    if(relation == "siblings")    filterfunction = i => i.tree_parent===item.tree_parent;
    if(relation == "kids")        filterfunction = i => i.tree_parent===item.idn;
    if(relation == "ancestors")   filterfunction = i => ~this.getPathOf(item).slice(0,-1).indexOf(i.idn);
    if(relation == "descendants") filterfunction = i => ~this.getPathOf(i).slice(0,-1).indexOf(item.idn);
    return candidates.filter(filterfunction).filter(i=>!i.deleted).sort((a,b)=>a.tree_pos - b.tree_pos).map(i=>i.idn);
  }
  isThatMy(relation, selfIdnOrItem, otherIdnOrItem) {
    return ~this.findAll(relation + "s", selfIdnOrItem, false).indexOf(otherIdnOrItem.idn || otherIdnOrItem);
  }
}

//ychoufe-12345678-l-98765432-l.json
//<type>-<created>-<modified>.<extension>
//<----------idv----------------------->
//<-----version------------->
//<----idn-------> (identifier)

class Item {
  constructor(idv, doc) {
    this.idv = idv;
    this.version   = idv.split(".")[0];
    this.extension = idv.split(".")[1];
    this.type     = this.version.split("-")[0];
    this.created  = this.version.split("-")[1];
    this.creator  = this.version.split("-")[2];
    this.modified = this.version.split("-")[3];
    this.modifier = this.version.split("-")[4];
    this.flags    = this.version.split("-")[5] || "";
    this.deleted  = this.flags.indexOf("x") > -1;
    this.idn      = this.type+"-"+this.created+"-"+this.creator;
    this.doc = doc;
    this.createGetSet();
  };
  createGetSet() {
    for(let prop in this.doc) {
      if(prop in Item.prototype) continue;
      Object.defineProperty(Item.prototype, prop, {
        get: function getProp() {return (this.debounced && this.debounced.key == prop) ? this.debounced.val : this.doc[prop];},
        set: function setProp(val) {this.doc[prop] = val;}
      });
    }
  };
  static getIdnFromIdv(idv) {return idv.split("-").slice(0,3).join("-");}
  get(prop) {
    let p = this[prop] !== undefined ? this[prop] : (this.doc[prop] !== undefined ? this.doc[prop] : undefined);
    if(this.debounced && this.debounced.key == prop) p = this.debounced.val;
    return (typeof p == "function") ? p.bind(this)() : p;
  };
  getPublicKeys() {
    return Object.keys(this.doc).filter(k=>!~k.indexOf("_"));
  }
  hasType(typegroup) {
    if(typegroup === "all") return true;
    if(typegroup === "deleted") return this.deleted === true;
    return this.type === typegroup;
  };
  //Tree Methods
  isTreeItem() {return this.tree_pos !== undefined;}
  getParent() {
    return this.tree_parent;
  }

}

export {Store, Item};