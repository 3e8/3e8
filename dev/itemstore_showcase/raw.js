import {Store} from "modules/itemstore";
import {_} from "modules/helpers";

window.store = new Store("showcase", {
  dbcentral: "mydb/db", // "/db" wird entfernt, Rest sollte der Pfad zum Ordner sein
  dbname: "showcase",
  leavesonly: false,
  longpolling: true,
  keysByType: {}
});

store.dNs.mountShelf("demo", {checked: []})

log(store);

let anyObserver = {
  onReady() {
    log(store.getLeavesAsArray());
    this.refreshView();
  },
  onDbChange() {
    this.refreshView();
  },
  onInstantchange() {
    log("instantchange");
    this.refreshView();
  },
  refreshView() {
    let html = store.getLeavesAsArray().map(i=>(`
      <div class='item ${store.dbStates.pendingIdvs.includes(i.idv)?"pending":""}'>
        <input type="checkbox" ${store.namedStates["demo"].checked.includes(i.idn) ? "checked" : ""} onchange="toggleCheck('${i.idn}')">
        ${i.title} ${i.subtitle}
        <button onclick="modify('${i.idn}')">Modify</button>
        <button onclick="debounced('${i.idn}', '${i.subtitle}')">Debounced</button>
        <button onclick="trash('${i.idn}')">Delete</button>
      </div>
    `)).join("");
    document.querySelector(".list").innerHTML = `<div>${store.dbStates.latest}</div>` + html;
    
  }
};

window.toggleCheck = function(idn) {
  store.dNs.toggleShelfStateOfIdn("demo", "checked", idn)
};

store.subscribe("ready", anyObserver);
store.subscribe("dbChange", anyObserver);
store.subscribe("instantchange", anyObserver);

window.modify = function(idn) {
  store.dDb.updateItem(idn, {title: Date.now().toString(36)})
};

window.debounced = function(idn, oldtitle) {
  store.dDb.debouncedUpdate(idn, {subtitle: oldtitle+"a"})
};


window.trash = function(idn) {
  store.dDb.deleteItem(idn);
};

window.addRandomItem = function() {
  store.dDb.addItem("testtype", {title: Date.now().toString(36), subtitle: Date.now().toString(24)}).then(r=>log(r));
};

window.moveold = function() {
  store.moveOldToArchive();
};

function blink(sel) {
  let el = document.querySelector(sel);
  el.style.outline = "4px solid blue";
  setTimeout(_=>el.style.outline = "none", 1000);
  setTimeout(_=>el.style.outline = "4px solid blue", 2000);
  setTimeout(_=>el.style.outline = "none", 3000);
  setTimeout(_=>el.style.outline = "4px solid blue", 4000);
  setTimeout(_=>el.style.outline = "none", 5000);
  setTimeout(_=>el.style.outline = "4px solid blue", 6000);
  setTimeout(_=>el.style.outline = "none", 7000);
}

store.subscribe("ready", _ => blink(".ready") );

let target = {
  el: document.querySelector(".dbChange"),
  onDbChange: function({action, args}) {
    this.el.style.outline = "2px solid red";
    setTimeout(_=>this.el.style.outline = "none", 1000);
  },
  green: function({action, args}) {
    this.el.style.outline = "2px solid green";
    setTimeout(_=>this.el.style.outline = "none", 1000);
  }
};
store.subscribe("dbChange", target);
store.subscribe("stateChange", target, "demo", "green");
store.subscribe("stateChange", _=>console.log(111), "demo");