import React from 'react';
import ReactDOM from 'react-dom';

console.clear();

let mainstate = {
  shapes: []
};
let statesetter;
let W = 200, H = 100;

let globalid = 0;
let pointNum = 0;
let lineNum = 0;

class Shape {
  constructor(type, dependencies, id) {
    this.type = type;
    this.dependencies = dependencies;
    this.id = id || String(globalid++);
    if(!id) {
      if(this.hasType("linear")) {
        this.name = String.fromCharCode(97 + lineNum%26) + (lineNum>25?lineNum/26|0:"");
        lineNum++;
      }
      else {
        this.name = String.fromCharCode(65 + pointNum%26) + (pointNum>25?pointNum/26|0:"");
        pointNum++;
      }
    }
  }
  hasType(type) {
    if(this.type === type) return true;
    if(type === "linear" || type === "direction") {
      if(["segment", "line", "ray", "vector"].includes(this.type)) {
        return true;
      }
    }
    if(type === "distance") {
      if(["segment", "vector"].includes(this.type)) {
        return true;
      }
    }
  }
  get coords() {
    if(this.type==="point" && this.dependencies[0].relation === "manual") {
      return this.dependencies[0].coords;
    }
    else console.warn("coords not yet defined");
  }
  dep(relation) {
    return this.dependencies.find(s=>s.relation === relation);
  }
  depObj(relation) {
    return findShapeById(this.dep(relation).id);
  }
  // get depObjs() {
  //   return this.dependencies.map(d=>d.id).map(findShapeById);
  // }
  get from() {
    if(this.dep("from")) return this.depObj("from");
    if(this.dep("fromThrough")) {
      let dx = -Math.cos(this.dir / 180 * Math.PI);
      let dy = -Math.sin(this.dir / 180 * Math.PI);
      let f = Math.max(Math.abs(W / dx), Math.abs(H / dy));
      dx = f * dx;
      dy = f * dy;
      let c = {x: this.depObj("fromThrough").coords.x + dx, y: this.depObj("fromThrough").coords.y + dy};
      return new Shape("point", [{relation: "manual", coords: c}], "temp");
    }
    console.warn("from not implemented")
  }
  get to() {
    if(this.dep("to")) return this.depObj("to");
    if(this.dep("parallel") && this.dep("from")) {
      let {dx, dy} = diff(this.depObj("parallel").from.coords, this.depObj("parallel").to.coords);
      if(this.type==="ray") {
        let f = Math.max(Math.abs(W/dx), Math.abs(H/dy));
        dx = f * dx;
        dy = f * dy;
      }
      let c = {x: this.depObj("from").coords.x + dx, y: this.depObj("from").coords.y + dy};
      return new Shape("point", [{relation: "manual", coords: c}], "temp");
    }
    if(this.dep("parallel") && this.dep("fromThrough")) {
      let {dx, dy} = diff(this.depObj("parallel").from.coords, this.depObj("parallel").to.coords);
      let f = Math.max(Math.abs(W/dx), Math.abs(H/dy));
      dx = f * dx;
      dy = f * dy;
      let c = {x: this.depObj("fromThrough").coords.x + dx, y: this.depObj("fromThrough").coords.y + dy};
      return new Shape("point", [{relation: "manual", coords: c}], "temp");
    }
    if(this.dep("through") && (this.dep("fromThrough") || this.dep("from"))) {
      let s = this.dep("from") ? this.depObj("from") : this.depObj("fromThrough") ;
      let {dx, dy} = diff(s.coords, this.depObj("through").coords);
      let f = Math.max(Math.abs(W/dx), Math.abs(H/dy));
      dx = f * dx;
      dy = f * dy;
      let c = {x: s.coords.x + dx, y: s.coords.y + dy};
      return new Shape("point", [{relation: "manual", coords: c}], "temp");
    }
  }
  get dir() {
    if(this.hasType("direction")) {
      if(this.dep("parallel")) return this.depObj("parallel").dir;
      if(this.dep("perpendicular")) return (this.depObj("parpendicular").dir + 90) % 360;
      if(this.dep("from") && this.dep("to")) {
        let {dx, dy} = diff(this.depObj("from").coords, this.depObj("to").coords);
        return Math.atan2(dy, dx) * 180 / Math.PI;
      }
      if(this.dep("from") && this.dep("through")) {
        let {dx, dy} = diff(this.depObj("from").coords, this.depObj("through").coords);
        return Math.atan2(dy, dx) * 180 / Math.PI;
      }
      if(this.dep("fromThrough") && this.dep("through")) {
        let {dx, dy} = diff(this.depObj("fromThrough").coords, this.depObj("through").coords);
        return Math.atan2(dy, dx) * 180 / Math.PI;
      }
    }
  }
  get len() {
    return dist(this.from, this.to);
  }
  render(svgarea) {
    let s = this;
    return (<g
      onClick={svgarea.selectShape}
      key={s.id}
      data-id={s.id}
      data-hovered={s.hovered}
      onMouseEnter={setHovered}
      onMouseLeave={unsetHovered}>
      <title>{s.name}</title>
      {s.type==="point" && <g>
        <circle r={2} cx={s.coords.x} cy={s.coords.y} fill="#aaa" className="shade"></circle>
        <circle r={1} cx={s.coords.x} cy={s.coords.y}></circle>
      </g>}
      {s.hasType("linear") && <g>
        <line x1={s.from.coords.x} y1={s.from.coords.y} x2={s.to.coords.x} y2={s.to.coords.y} stroke="#aaa" strokeWidth="3" className="shade"></line>
        <line x1={s.from.coords.x} y1={s.from.coords.y} x2={s.to.coords.x} y2={s.to.coords.y} stroke="#eea"></line>
      </g>}
    </g>);
  }
}

window.A = Shape;

function findShapeById(id) {
  return mainstate.shapes.find(s=>s.id===id);
}

function diff(p0, p1) {
  return {dx: p1.x - p0.x, dy: p1.y - p0.y};
}

function dist(p1, p2) {
  let {dx, dy} = diff(p1.coords, p2.coords);
  return Math.hypot(dy, dx);
}

function addNewShape(type, dependencies) {
  statesetter({shapes: [...mainstate.shapes, new Shape(type, dependencies)]});
}

function copyShape(id) {
  let s = findShapeById(id);
  return Object.assign(new Shape(s.type, s.dependencies, id), s);
}

function replaceShape(id, newShape) {
  let index = mainstate.shapes.findIndex(s=>s.id===id);
  statesetter({shapes: [...mainstate.shapes.slice(0, index), newShape, ...mainstate.shapes.slice(index+1)]})
}

function updateShape(id, updateArray) {
  let newShape = copyShape(id);
  updateArray.forEach(({path, value, toggle})=>{
    let p = newShape;
    let keychain = path.split(".")
    const lastKey = keychain.pop();
    keychain.forEach(i=>p=p[i]);
    p[lastKey] = toggle ? !p[lastKey] : value;
  });
  replaceShape(id, newShape);
}

let selectShape = function(e) {
  let shape = findShapeById(e.currentTarget.dataset.id);
  if(mainstate.activeTask==="addSegment") {
    if(shape.type==="point") {
      if (!mainstate.deps || !mainstate.deps.find(s => s.relation === "from")) {
        statesetter({deps: [...mainstate.deps||[], {relation: "from", id: shape.id}]});
      }
      else if (mainstate.deps && mainstate.deps.find(s => s.relation === "from")) {
        if(mainstate.deps.find(s => s.relation === "from").id === shape.id) return;
        statesetter({deps: [...mainstate.deps, {relation: "to", id: shape.id}]});
      }
    }
    if(shape.hasType("segment")) {
      if (!mainstate.deps || !mainstate.deps.find(s => s.relation === "parallel")) {
        statesetter({deps: [...mainstate.deps||[], {relation: "parallel", id: shape.id}]});
      }
    }
    let deptypes = mainstate.deps.map(d=>d.relation);
    if(deptypes.includes("from") && (deptypes.includes("to") || deptypes.includes("parallel"))) {
      addNewShape("segment", mainstate.deps);
      statesetter({deps: []});
    }
  }
  if(mainstate.activeTask==="addRay") {
    if(shape.type==="point") {
      if (!mainstate.deps || !mainstate.deps.find(s => s.relation === "from")) {
        statesetter({deps: [...mainstate.deps||[], {relation: "from", id: shape.id}]});
      }
      else if (mainstate.deps && mainstate.deps.find(s => s.relation === "from")) {
        if(mainstate.deps.find(s => s.relation === "from").id === shape.id) return;
        statesetter({deps: [...mainstate.deps, {relation: "through", id: shape.id}]});
      }
    }
    if(shape.hasType("direction")) {
      if (!mainstate.deps || !mainstate.deps.find(s => s.relation === "parallel")) {
        statesetter({deps: [...mainstate.deps||[], {relation: "parallel", id: shape.id}]});
      }
    }
    let deptypes = mainstate.deps.map(d=>d.relation);
    if(deptypes.includes("from") && (deptypes.includes("through") || deptypes.includes("parallel"))) {
      addNewShape("ray", mainstate.deps);
      statesetter({deps: []});
    }
  }
  if(mainstate.activeTask==="addLine") {
    if(shape.type==="point") {
      if (!mainstate.deps || !mainstate.deps.find(s => s.relation === "fromThrough")) {
        statesetter({deps: [...mainstate.deps||[], {relation: "fromThrough", id: shape.id}]});
      }
      else if (mainstate.deps && mainstate.deps.find(s => s.relation === "fromThrough")) {
        if(mainstate.deps.find(s => s.relation === "fromThrough").id === shape.id) return;
        statesetter({deps: [...mainstate.deps, {relation: "through", id: shape.id}]});
      }
    }
    if(shape.hasType("direction")) {
      if (!mainstate.deps || !mainstate.deps.find(s => s.relation === "parallel")) {
        statesetter({deps: [...mainstate.deps||[], {relation: "parallel", id: shape.id}]});
      }
    }
    let deptypes = mainstate.deps.map(d=>d.relation);
    if(deptypes.includes("fromThrough") && (deptypes.includes("through") || deptypes.includes("parallel"))) {
      addNewShape("line", mainstate.deps);
      statesetter({deps: []});
    }
  }
  e.stopPropagation();
};

let setHovered = function(e) {
  updateShape(e.currentTarget.dataset.id, [{path:"hovered", value: true}]);
};
let unsetHovered = function(e) {
  updateShape(e.currentTarget.dataset.id, [{path:"hovered", value: false}]);
};

class Optiktask extends React.Component {
	constructor(props) {
    super(props);
    this.state = {
      shapes: []
    };
    statesetter = update => {
      this.setState(update);
      mainstate = {...mainstate, ...update}
    }
  }
	render() {
    return (
    <div style={{position: "relative"}}>
			<Area mainstate={this.state}></Area>
      <Menu mainstate={this.state}></Menu>
      <Protocol mainstate={this.state}></Protocol>
    </div>
    );
	}
}

class Menu extends React.Component {
	constructor(props) {
    super(props);
  }
  setTask = e => statesetter({activeTask: e.currentTarget.dataset.task, deps: []});
  // addPoint = e => statesetter({activeTask: "addPoint", deps: []});
  // addSegment = e => statesetter({activeTask: "addSegment", deps: []});
  // addRay = e => statesetter({activeTask: "addRay", deps: []});
  // addLine = e => statesetter({activeTask: "addLine", deps: []});

  inputChange = e => {
    statesetter({["input_"+e.target.dataset.name]: e.target.value})
  };
  addManualPoint = e => {
    let {shapes, input_newPointX, input_newPointY} = mainstate;
    addNewShape("point", [{relation: "manual", coords: {x: +input_newPointX, y: +input_newPointY}}]);
  };

	render() {
    let {activeTask, shapes, coords, input_newPointX, input_newPointY} = mainstate;
    return (
      <div>
        {["Point_Manual", /*"Point_OnShape",*/ "Point_Intersection",
          "Segment_Points", "Segment_Parallel",
          "Ray_Points", "Ray_Parallel",
          "Line_Points", "Line_Parallel"
        ].map(task=><button title={task} data-task={task} key={task} onClick={this.setTask}><img src={`icons/${task}.gif`}></img></button>)}
      <div>
          {activeTask === "Point_Manual" &&
          <div>
            X: <input data-name="newPointX" onChange={this.inputChange} value={coords ? coords.x : input_newPointX || ""}></input>
            Y: <input data-name="newPointY" onChange={this.inputChange} value={coords ? coords.y : input_newPointY || ""}></input>
            {input_newPointX && input_newPointY && <button onClick={this.addManualPoint}>Add</button>}
          </div>}
          {activeTask !== "addPoint" &&
          <div>{(mainstate.deps||[]).map(d=><span key={d.relation}>{d.relation}: {findShapeById(d.id).name} </span>)}</div>}
        </div>
      </div>);
  }
}

class Area extends React.Component {
	constructor(props) {
    super(props);
    this.selectShape = selectShape.bind(this);
  }
  mouseMove = e => {
    statesetter({coords: getSvgCoords(e)})
  };
  mouseLeave = e => {
    statesetter({coords: false})
  };
  click = e => {
    if(mainstate.activeTask === "addPoint") {
      addNewShape("point", [{relation: "manual", coords: getSvgCoords(e)}]);
    }
  };
	render() {
	  let {mainstate} = this.props;
		return (
    <div style={{position: "relative"}}>
      <svg width="100%" viewBox="0 0 200 100" style={{backgroundColor: "#cde", transform: "scaleY(-1)"}} onMouseMove={this.mouseMove} onMouseLeave={this.mouseLeave} onClick={this.click}>
        {mainstate.shapes.filter(s=>s.type!=="point"&&!s.hidden).map(s=>s.render(this))}
        {mainstate.shapes.filter(s=>s.type==="point"&&!s.hidden).map(s=>s.render(this))}
      </svg>
    </div>
    );
	}
}

class Protocol extends React.Component {
	constructor(props) {
    super(props);
    this.selectShape = selectShape.bind(this);
  }
	render() {
		return (
    <div>
      {mainstate.shapes.filter(s=>s.type!=="point").map(s=><Protocolshape key={s.id} shape={s}></Protocolshape>)}
      {mainstate.shapes.filter(s=>s.type==="point").map(s=><Protocolshape key={s.id} shape={s}></Protocolshape>)}
    </div>
    );
	}
}

class Protocolshape extends React.Component {
  constructor(props) {
    super(props);
    this.selectShape = selectShape.bind(this);
  }
  toggleHide = function(e){
    let id = e.currentTarget.closest("div").dataset.id;
    updateShape(id, [{path: "hidden", toggle: true}]);
    e.stopPropagation();
  };
  deleteShape = e => {
    let id = e.currentTarget.closest("div").dataset.id;
    statesetter({shapes: mainstate.shapes.filter(i=>i.id!==id)});
    e.stopPropagation();
  };
  render() {
    let s = this.props.shape;
    let display =
      s.type === "point" ? <span>({s.coords.x|0},{s.coords.y|0})</span> :
        s.hasType("linear") ? <span>{s.dependencies.map(d=><span key={d.relation}>{d.relation}: {findShapeById(d.id).name}</span>)} {s.dir.toFixed(2)}° {s.type==="segment"&&s.len.toFixed(1)}</span> : "";
    return (<div
      data-name={s.name}
      data-type={s.type}
      data-id={s.id}
      data-hovered={s.hovered}
      onClick={this.selectShape}
      onMouseEnter={setHovered}
      onMouseLeave={unsetHovered}>
      <button onClick={this.toggleHide}><img src="icons/Tool_Show_Hide_Object.gif"></img></button>
      <button onClick={this.deleteShape}>X</button>&nbsp;
      {s.type}&nbsp;
      {s.name}&nbsp;
      {display}
      </div>);
    //
  }
}

ReactDOM.render(
  <Optiktask></Optiktask>,
  document.getElementById('app')
);

function getSvgCoords(e) {
  let m = e.currentTarget.getScreenCTM();
  let p = e.currentTarget.createSVGPoint(); 
  p.x = e.clientX;
  p.y = e.clientY;
  p = p.matrixTransform(m.inverse());
  return {x: p.x, y: p.y}
}

module.hot.accept();