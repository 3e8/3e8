// in the console: webpack --colors --progress --watch

let path = require('path');

let entryObj = {"index-": "./index-raw.js"};

let config = {
  devtool: "source-map",
  mode: "development",
  entry: entryObj,
  output: {
    path: __dirname,
    filename: '[name]bundle.js'
  },
  resolveLoader: {
    modules: ['node_modules', path.join(process.cwd(), "")]
  },
  resolve: {
    modules: [path.resolve(__dirname), "node_modules"]
  },
  module: {
    rules: [
      {test: /\.jsx?$/,   use: [{loader: "babel-loader"}] },
      //{test: /\.jsx?$/, use: [{loader: 'replace-loader'}]},
     ],
  },
  plugins: [
    //new AddHtmlPlugin({title: "test", outpath: BUILD_DIR}),
  ],
};

module.exports = config;