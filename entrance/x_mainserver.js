var http = require('http');
var https = require('https');
var fs = require('fs');
var httpProxy = require('http-proxy');
var os = require('os');
var path = require("path");

var proxy = httpProxy.createProxy({ignorePath: true});

var routes = require("./serversettings");
  // var routes = {
  // localhttp: 81,
  // localhttps: 7443,
  // web: 8001,
  // experiments: 8004,
  // physics: 8010,
  // dev: 8020,
  // db: 8030,
  // log: 8040,
  // aaa: 8050,
  // socket: 8060,
  // sync: require("../sync/syncsettings.json").PORTINTERN,
  // static: 8300 }

require("../metamodules/processSetErrorhandlers");
var {log, logError} = require("../metamodules/logToOpener.js");



proxy.on("error", logError);

function getTarget(req) {
  let host = req.headers.host;
  if(!host) {
    log("request without host? " + req.url + " " + req.headers + " " + req.host);
    return false;
  }
  let route = host.split(".")[0];
  if(route in routes) {
    return "http://localhost:" + routes[route].internalport + req.url;
  }
  else return false;
}

function requestHandler(fromHTTPS, req, res) {
  if(req.url.indexOf("acme-challenge") > -1) {
    console.log(path.join("/home", "pi", "letsencrypt", "challenge", req.url));
    fs.readFile(path.join("/home", "pi",  "letsencrypt","challenge", req.url), function(err, data) {
      if(err) console.log(err);
      res.end(data);
    });
    return;
  }
  if(!getTarget(req)) {
    log(req.url + " not found");
    res.setHeader('Content-Type', 'text/plain');
    res.statusCode = 404;
    res.end('Not found');
    return;
  }
  if(!req.headers.host.includes("localhost") && !fromHTTPS) {
    res.writeHead(301, {'Location': 'https://' + req.headers.host + req.url});
    res.end();
    return;
  }
  proxy.web(req, res, {
    xfwd: true,
    target: getTarget(req)
  });
}

for(let r of ["aaa", "db", "dev", "web"]) {
  log(r + " listening on port " + routes[r].internalport + "...");
  http.createServer(placeHolderHandler).listen(routes[r].internalport);
}

function placeHolderHandler(req, res) {
  res.write(`Placeholder for ${req.headers.host} ${req.url}`);
  res.end();
}

setTimeout(_=>startServer(), 10000); //Waiting for other severs to start

function startServer() {
  
  var mainserver = http.createServer(requestHandler.bind(null, false)).listen(routes.localhttp.internalport);
  mainserver.on('upgrade', function (req, socket, head) {
    proxy.ws(req, socket, head, {
      target: getTarget(req).replace("http", "ws")
    });
  });
  
  log("Mainserver listening on port " + routes.localhttp.internalport + "...");
  
  const {user} = require("../local_nosync/localsettings.json");
  if(user === "nodepi") {
    const sshoptions = {
      key: fs.readFileSync("/home/pi/letsencrypt/certificate/live/web.loooping.ch/privkey.pem"),
      cert: fs.readFileSync("/home/pi/letsencrypt/certificate/live/web.loooping.ch/fullchain.pem")
    };
    var mainserverssh = https.createServer(sshoptions, requestHandler.bind(null, true)).listen(routes.localhttps.internalport);
    mainserverssh.on('upgrade', function (req, socket, head) {
      proxy.ws(req, socket, head, {
        target: getTarget(req).replace("http", "ws")
      });
    });
  }

}



/*
 Im Ordner letsencrypt laufen lassen:
 sudo ./letsencrypt-auto certonly --agree-tos --email adrian.luethi@gmail.com --webroot --webroot-path ~/letsencrypt/challenge --config-dir ~/letsencrypt/certificate --domains web.loooping.ch,physics.loooping.ch,test.loooping.ch,sync.loooping.ch,experiments.loooping.ch,static.loooping.ch,looopy.noip.me,aaa.loooping.ch,dev.loooping.ch,db.loooping.ch,log.loooping.ch,socket.loooping.ch,www.3e8.ch,web.3e8.ch,physics.3e8.ch,test.3e8.ch,sync.3e8.ch,experiments.3e8.ch,static.3e8.ch,aaa.3e8.ch,dev.3e8.ch,db.3e8.ch,log.3e8.ch,socket.3e8.ch,auth.3e8.ch,user.3e8.ch,login.3e8.ch,info.3e8.ch
 
 //weitere Ideen: user, login, auth (derzeit: log) info.loooping.ch,auth.loooping.ch,login.loooping.ch,user.loooping.ch,
 //auch eintragen bei hostinger cnamesocket
 
 IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at
 /home/pi/letsencrypt/certificate/live/web.loooping.ch/fullchain.pem.
 Your cert will expire on 2017-07-28. To obtain a new or tweaked
 version of this certificate in the future, simply run
 letsencrypt-auto again. To non-interactively renew *all* of your
 certificates, run "letsencrypt-auto renew"
 
 
 */