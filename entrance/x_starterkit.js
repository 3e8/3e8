var {fork, spawn} = require("child_process");
var loggers = require("../metamodules/loggersToFile.js");
var log = loggers.createLogger("../local_nosync/log");
var logError = loggers.createErrorLogger("../local_nosync/log");
const pathmodule = require("path");
const readFile = require("util").promisify(require("fs").readFile);

const {user} = require("../local_nosync/localsettings.json");

var kids = {};

async function start() {
  if(user==="nodepi") {
    await addProcess("../sync/syncmaster.js");
    await addProcess("../foto/mediaManagerRemote.js");
    await addProcess("../foto/staticmedia.js");
  }
  else {
    await addProcess("../sync/syncclient.js");
    await addProcess("../sync/node_modules/.bin/electron.cmd", {name: "synchecker", command: "node_modules\\.bin\\electron.cmd", method: spawn, cwd: "../sync", args: ["."]});
    await addProcess("../../3e8/sync2/node_modules/.bin/electron.cmd", {name: "syncclient", command: "node_modules\\.bin\\electron.cmd", method: spawn, cwd: "../../3e8/sync2", args: ["."]});

    //await addProcess("../foto/mediaManagerLoc.js");
    //await addProcess("../foto/staticmedia.js");
  }
  await addProcess("../entrance/globalstatic.js");
  await addProcess("../users/loginserver.js");
  await addProcess("../sensors/sensorserver.js");
  await addProcess("../socket/socketserver.js");
  await addProcess("../experiments/experimentsserver.js");
  await addProcess("../entrance/mainserver.js");
  
  setInterval(async _=>{
    for(let name in kids) {
      let code = await readFile(kids[name].path, "UTF-8");
      if(code !== kids[name].code) {
        setTimeout(async _=>{
          kids[name].process.kill('SIGTERM');
          kids[name].code = await readFile(kids[name].path);
          await addProcess(kids[name].path, kids[name].options);
        }, 3000)
      }
    }
  }, 10000);
}

process.on("uncaughtException", err => {
  console.log(err);
  logError(err);
});

async function addProcess(path, { cwd = pathmodule.dirname(path), command=pathmodule.basename(path), args=[], method = fork, name = pathmodule.basename(path, '.js')} = {}) {
  kids[name] = {
    path,
    options: arguments[1],
    code: await readFile(path, "UTF-8"),
    process: method(command, args, {cwd})
  };
  kids[name].process.on("message", msg=>{
    if(msg.error) {
      logError(msg);
    }
    if(msg.log) {
      log(msg.message);
    }
  });
  kids[name].process.on("exit", exitcode=>{
    if(exitcode===null) {
      log(name + " has restarted after change or finished with code null");
    }
    else log(name + " finished with code: " + exitcode);
  });
}

start().catch(console.log);