const http = require('http'),
      url = require('url'),
      path = require('path'),
      fs = require('fs');

const express = require('express');

require("../metamodules/processSetErrorhandlers");
var {log, logError} = require("../metamodules/logToOpener.js");

var routes = require("./serversettings");
const PORT = routes.static.internalport;

const app = express();

app.use((req, res, next) => {
  //@todo check security
  next();
});

const tasks = {
  inventory: require('../metamodules/inventory').inventory(".."),
};

for(let t in tasks) {
  app.use("(/*)?/"+t+"(/*)?", (req, res, next) => tasks[t](req, res, next));
}

app.use(express.static(path.join(__dirname, '..')));

app.use((err, req, res, next) => {
  if(err) logError(err);
  log('Request to static server not successful: ' + req.url);
  res.status(404).send('Not found');
  res.end();
});


app.listen(PORT, function () {
  log('Static server listening on port ' + PORT);
});
