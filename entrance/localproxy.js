const {log, logError, warn} = require("../my_modules/3e8-log");
const http = require('http');
const https = require('https');
const fs = require('fs');
const httpProxy = require('http-proxy');

const proxy = httpProxy.createProxy();
proxy.on("error", e=>{
  if(e.message.includes("getaddrinfo ENOTFOUND")) warn(e.message);
  else logError(e);
});

const sshOptions = {
  key: fs.readFileSync('https/key-20180503-203515.pem'),
  cert: fs.readFileSync('https/cert-20180503-203515.crt')
};

function startServer() {
  http.createServer(reqToProxy).listen(80);
  https.createServer(sshOptions, reqToProxy).listen(443);
  log("Localproxy on ports 80/443... (1s after nodeproxy?)")
}

function reqToProxy(req, res) {
  proxy.web(req, res, {
    xfwd: true,
    toProxy: true,
    target: {
      host: req.headers.host + (/.*\.localhost$/.test(req.headers.host) ? ".tv" : ""), //localhost.tv offers dns records for subdomains of localhost.tv (except www)
      port: 9123
    }
  });
}

setTimeout(_=>startServer(), 4000); // waiting for nodeproxy to start