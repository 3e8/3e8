const {fork, spawn} = require("child_process");
const {log, logError, warn} = require("../my_modules/3e8-log");
const pathmodule = require("path");
const {readFile} = require("prms-fs");
const delay = (millis) => new Promise((resolve)=>setTimeout(_=>resolve(), millis));

async function startAll(kids) {

  const {user} = require("./local_nosync_settings.json");
  await addProcess(kids, "../logs/logserver.js");
  await addProcess(kids, "../experiments/experimentsserver.js");
  await addProcess(kids, "../socket/socketserver.js");
  await addProcess(kids, "../users/loginserver.js");
  await addProcess(kids, "../dev/devserver.js");
  await addProcess(kids, "../api/apiserver.js");

  if(user==="ds3") {
    await addProcess(kids, "../sync2/syncmaster.js");
  }
  else {
    await addProcess(kids, "../entrance/localproxy.js");
    await addProcess(kids, "../entrance/nodeproxy.js");
    await delay(8000);
    await addProcess(kids, "../sync2/syncElectronLauncher.js", {softkill: true});
  }



  // for(let name in kids) {
  //   // kids[name].process.on("message", msg=>{
  //   //   if(msg.error) {
  //   //     logError(msg);
  //   //   }
  //   //   if(msg.log) {
  //   //     log(msg.message);
  //   //   }
  //   // });
  //   // kids[name].process.on("exit", exitcode=>{
  //   //   log(name + " finished with code: " + exitcode);
  //   // });
  // }

  return kids;

  // setInterval(async _=>{
  //   for(let name in kids) {
  //     let code = await readFile(kids[name].path, "UTF-8");
  //     if(code !== kids[name].code) {
  //       setTimeout(async _=>{
  //         kids[name].process.kill(0);
  //         kids[name].code = await readFile(kids[name].path);
  //         await addProcess(kids[name].path, kids[name].options);
  //       }, 3000)
  //     }
  //   }
  // }, 10000);


}

async function restart(kids, name) {
  await addProcess(kids, kids[name].path, kids[name].options);
  return kids;
}

async function addProcess(kids, path, { cwd = pathmodule.dirname(path), command=pathmodule.basename(path), args=[], method = fork, name = pathmodule.basename(path, '.js'), softkill = false} = {}) {
  kids[name] = {
    path,
    starttime: Date.now(),
    options: arguments[2] || {},
    code: await readFile(path, "UTF-8"),
    process: method(command, args, {cwd}),
    quit: async function() {
      if(softkill) {
        await this.process.send("quit");
      }
      else {
        await this.process.kill("SIGTERM");
      }
    }
  };
  kids[name].process.on("exit", exitcode=>{
    log(name + " finished with code: " + exitcode);
  });
}

module.exports = {startAll, restart};