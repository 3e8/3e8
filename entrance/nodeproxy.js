const {log, logError, warn} = require("../my_modules/3e8-log");
const http = require('http');
const https = require('https');
const fs = require('fs');
const httpProxy = require('http-proxy');
const os = require('os');
const path = require("path");

const proxy = httpProxy.createProxy({ignorePath: true});
proxy.on("error", logError);

const routes = require("./serversettings");
// var routes = {
// localhttp: 81,
// localhttps: 7443,
// web: 8001,
// experiments: 8004,
// physics: 8010,
// dev: 8020,
// db: 8030,
// log: 8040,
// aaa: 8050,
// socket: 8060,
// sync: require("../sync/syncsettings.json").PORTINTERN,
// static: 8300 }

function getTarget(req) {
  let host = req.headers.host;
  if(!host) {
    log("request without host? " + req.url + " " + req.headers + " " + req.host);
    return false;
  }
  let route = host.split(".")[0];
  if(route in routes) {
    return "http://localhost:" + routes[route].internalport + req.url;
  }
  else return false;
}

function requestHandler(fromHTTPS, req, res) {
  if(!getTarget(req)) {
    log(req.url + " not found");
    res.setHeader('Content-Type', 'text/plain');
    res.statusCode = 404;
    res.end('Not found');
    return;
  }
  //console.log(req.headers.host, req.url);
  proxy.web(req, res, {
    xfwd: true,
    target: getTarget(req)
  });
}

// for(let r of ["aaa", "db", "dev", "web"]) {
//   log(r + " listening on port " + routes[r].internalport + "...");
//   http.createServer(placeHolderHandler).listen(routes[r].internalport);
// }

function placeHolderHandler(req, res) {
  res.write(`Placeholder for ${req.headers.host} ${req.url}`);
  res.end();
}

setTimeout(_=>startServer(), 2000); //Waiting for other severs to start

function startServer() {

  const mainserver = http.createServer(requestHandler.bind(null, false)).listen(routes.nodeproxy.internalport);
  mainserver.on('upgrade', function (req, socket, head) {
    proxy.ws(req, socket, head, {
      target: getTarget(req).replace("http", "ws")
    });
  });

  log("Nodeproxy on port " + routes.nodeproxy.internalport + "... (2s after other servers?)");
  //
  // const {user} = require("./local_nosync_settings.json");
  // if(user === "nodepi") {
  //   const sshoptions = {
  //     key: fs.readFileSync("/home/pi/letsencrypt/certificate/live/web.loooping.ch/privkey.pem"),
  //     cert: fs.readFileSync("/home/pi/letsencrypt/certificate/live/web.loooping.ch/fullchain.pem")
  //   };
  //   var mainserverssh = https.createServer(sshoptions, requestHandler.bind(null, true)).listen(routes.localhttps.internalport);
  //   mainserverssh.on('upgrade', function (req, socket, head) {
  //     proxy.ws(req, socket, head, {
  //       target: getTarget(req).replace("http", "ws")
  //     });
  //   });
  // }

}