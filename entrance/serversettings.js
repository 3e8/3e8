const serverSettings = {};

const routes = {
  //localhttp: 80,
  nodeproxy: 9123,
  localhttps: 443,
  //web: 9001,
  experiments: 9004,
  //physics: 8010,
  dev: 9020,
  //db: 8030,
  auth: 9040,
  //aaa: 8050,
  socket: 9060,
  api: 9070,
  sync: require("../sync2/syncsettings.json").PORTINTERN,
  //static: 8300
  logs: 9700,
  control: 9100,
};

for(let r in routes) {
  serverSettings[r] = {
    internalport: routes[r],
    remoteurl: `https://${r}.3e8.ch`,
    //localpi: "http://192.168.1.13:" + routes[r],
    local: `http://${r}.localhost:${routes.nodeproxy}`
  }
}

module.exports = serverSettings;