process.chdir(__dirname);
const {log, logError, warn} = require("../my_modules/3e8-log");

const {fork, spawn, exec} = require("child_process");
const pathmodule = require("path");
const {readFile} = require("prms-fs");

const fs = require("fs");
const http = require("http");
const PORT = require("../entrance/serversettings.js").control.internalport;
const staticPipe = require("../my_modules/3e8-static-pipe");
const delay = (millis) => new Promise((resolve)=>setTimeout(_=>resolve(), millis));

let kids = {};

refreshAll().catch(logError);

http.createServer(async (req, res) => {
  let getState = req.url.match(/getState/);
  if (getState && req.method === "GET") {
    let processes = [];
    for(let name in kids) {
      let {process: {connected, exitCode, pid}, starttime, path} = kids[name];
      //console.log(kids[name].options, kids[name].options.method, kids[name].options.method && kids[name].options.method.name);
      let methodname = kids[name].options.method ? kids[name].options.method.name : "fork";
      processes.push({name, path, starttime, connected, exitCode, pid, methodname});
    }
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(processes));
    // let html = Object.keys(kids).map(name=>`<div class="${kids[name].process.connected ? 'ok' : 'error'}">${name}</div>`).join("");
    // res.end(html);
  }
  let restartMatch = req.url.match(/restart(\/(.*))?/);
  if (restartMatch && req.method === "POST") {
    let name = restartMatch[2] || "none";
    res.end("ok");
    await delay(2000);
    await restartProcess(name);
  }
  let refresh = req.url.match(/refreshAll/);
  if (refresh && req.method === "POST") {
    res.end("ok");
    await delay(2000);
    await refreshAll();
  }
  let deep_install = req.url.match(/npm_deep_install/);
  if (deep_install && req.method === "POST") {
    await npm_deep_install();
    res.end("ok");
  }
  if(["", "/"].includes(req.url)) {
    staticPipe({req, res, filepath: "./restarter_control.html"});
  }
}).listen(PORT);
log(`Restarter Control Server running on port ${PORT}...`);

async function refreshAll() {
  for(let name in kids) {
    await kids[name].quit();
  }
  kids = {};
  delete require.cache[require.resolve("./restarter_processes")];
  let startAll = require("./restarter_processes").startAll;
  kids = await startAll(kids);
}

async function restartProcess(name) {
  await kids[name].quit();
  let restartFn = require("./restarter_processes").restart;
  kids = await restartFn(kids, name);
}

async function npm_deep_install() {
  const {user} = require("./local_nosync_settings.json");
  exec('node ../npm_deep_install' + (user==="ds3"?" --production":""), (error, stdout, stderr) => {
    if (error) {
      logError(`exec error: ${error}`);
      return;
    }
    log(`stdout: ${stdout}`);
    logError(`stderr: ${stderr}`);
  });
}
