var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  port     : 3307,
  user     : 'root',
  password : 'speedoflightdb',
  database : 'test'
});

class Database {
  constructor( config ) {
    this.connection = mysql.createConnection( config );
  }
  query( sql, args ) {
    return new Promise( ( resolve, reject ) => {
      this.connection.query( sql, args, ( err, rows ) => {
        if ( err )
          return reject( err );
        resolve( rows );
      } );
    } );
  }
  close() {
    return new Promise( ( resolve, reject ) => {
      this.connection.end( err => {
        if ( err )
          return reject( err );
        resolve();
      } );
    } );
  }
}

connection.connect();

// connection.query(`CREATE TABLE sus (
//   id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//   firstname VARCHAR(30) NOT NULL,
//   lastname VARCHAR(30) NOT NULL,
//   email VARCHAR(50)
// )`);

// connection.query(`INSERT INTO sus (firstname, lastname, email) VALUES ("ädu", "älusäl", "1@3.ch")`, function (error, results, fields) {
//   if (error) throw error;
//   console.log(results, fields);
// });

let s = Date.now();
connection.query(`SELECT * FROM sus`, function (error, results, fields) {
  if (error) throw error;
  console.log(results.length,Date.now()-s);
  //console.log(results, fields);
});

// connection.query(`DELETE FROM sus`, function (error, results, fields) {
//   if (error) throw error;
//   console.log(results.length,Date.now()-s);
//   //console.log(results, fields);
// });

// connection.query(`SHOW DATABASES`, function (error, results, fields) {
//   if (error) throw error;
//   console.log(results, fields);
// });

let randomstring = n => Math.random().toString(36).replace(/[^A-za-z]+/g, '').substr(0, n);

// let q = new Array(10000).fill(0).map((el,i)=>`("${randomstring(8)}","${randomstring(8)}","${randomstring(8)}")`).join(",")
// console.log(q);
// let s = Date.now();
// connection.query(`INSERT INTO sus (firstname, lastname, email) VALUES ${q}`, function (error, results, fields) {
//   if (error) throw error;
//   console.log(results, fields);
//   console.log(Date.now() - s);
// });

connection.end();