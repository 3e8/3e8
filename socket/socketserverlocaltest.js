var app = require('http').createServer(handler);
var io = require('socket.io')(app);
var fs = require('fs');

require("../metamodules/processSetErrorhandlers");
var {log, logError} = require("../metamodules/logToOpener.js");
var serversettings = require("../entrance/serversettings.js");

function handler (req, res) {
  fs.readFile(__dirname + '/socketDemo.html',
    function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading socketDemo.html');
      }
      res.writeHead(200);
      res.end(data);
    }
  );
}

app.listen(8061);

//io.origins(["localhost:*", "*.loooping.ch:*"]);
io.origins((origin, callback) => {
  if(["localhost", "loooping.ch", "3e8.ch", "looopy.noip.me", "physics.cf"].some(p=>~origin.indexOf(p))) {
    callback(null, true);
  }
  callback("domain not allowed", false);
});

//Namespace
var demo = io.of('/demo').on('connect', function (socket) {
  socket.on("mouse", data => {
    socket.emit("thankyou", Object.keys(demo));
    demo.clients((err, clients)=>{
      socket.emit("thankyou", clients);
    });
    socket.broadcast.volatile.emit("others", data);
    demo.emit("all", data); //can apparently not use broadcast and volatile flags
  });
});

io.on('connection', function (socket) {
  socket.emit('led', { hello: 'world' });
  socket.on('my other event', function (data) {
    console.log(data);
  });
  setInterval(_=>socket.emit("led", {a:1}), 10000);
  setInterval(_=>socket.emit("any", {a:2}), 5000);
});



//var brightness=10;
//
//io.sockets.on('connection', function (socket) { //gets called whenever a client connects
//  socket.emit('led', {value:brightness }); //send the new client the current brightness
//
//  socket.on('led', function (data) { //makes the socket react to 'led'
//    console.log(data);
//    brightness = data.value;  //updates brightness from the data object
//    io.sockets.emit('led', {value: brightness, edit: data.edit}); //sends the updated brightness to all connected clients
//  });
//});

log("Socket server listening on port " + serversettings.socket.internalport);