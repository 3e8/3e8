const fs = require("fs");
const http = require("http");
const {log, logError, warn} = require("../my_modules/3e8-log");
const PORT = require("../entrance/serversettings.js").logs.internalport;
const staticPipe = require("../my_modules/3e8-static-pipe");

http.createServer((req, res) => {
  let getLogs = req.url.match(/getLogs(\/(.*))?/);
  if (getLogs) {
    let channel = getLogs[2] || "logs";
    const filepath = `./local_logs/${channel.replace(/[^-A-z0-9_]/g, "")}.html`;
    staticPipe({req, res, filepath});
  }
  let cleanLogs = req.url.match(/cleanLogs(\/(.*))?/);
  if (cleanLogs && req.method === "DELETE") {
    let channel = cleanLogs[2] || "logs";
    const filepath = `./local_logs/${channel.replace(/[^-A-z0-9_]/g, "")}.html`;
    fs.stat(filepath, (err, stat) => {
      if(err) {
        return res.end("channel not found")
      }
      fs.writeFile(filepath, "", (err) => {
        if(err) throw(err);
        res.end("ok");
      })
    });
  }
  if(["", "/"].includes(req.url)) {
    staticPipe({req, res, filepath: "./viewLogs.html"});
  }
}).listen(PORT);
log(`Logs Server running on port ${PORT}...`);